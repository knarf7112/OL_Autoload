﻿using AsyncMultiSocket;
using Common.Logging;
using System;
using System.ServiceProcess;

namespace ALCenterService
{
    public partial class ALCenterService : ServiceBase
    {
        private static readonly ILog log = LogManager.GetLogger(typeof(ALCenterService));
        ISocketServer socketServer1;
        ISocketServer socketServer2;
        ISocketServer socketServer3;
        ISocketServer socketServer4;
        ISocketServer socketServer5;
        ISocketServer socketServer6;
        public ALCenterService()
        {
            InitializeComponent();
            string autoload = String.IsNullOrEmpty(System.Configuration.ConfigurationSettings.AppSettings["AutoLoadPortAndServiceName"]) ? "6101:AutoLoad" : System.Configuration.ConfigurationSettings.AppSettings["AutoLoadPortAndServiceName"];
            string autoloadPort = autoload.Split(':')[0];
            string autoloadServiceName = autoload.Split(':')[1];
            socketServer1 = new AsyncMultiSocketServer(Convert.ToInt32(autoloadPort), autoloadServiceName);
            string Txlog = String.IsNullOrEmpty(System.Configuration.ConfigurationSettings.AppSettings["TxlogPortAndServiceName"]) ? "6102:Txlog" : System.Configuration.ConfigurationSettings.AppSettings["TxlogPortAndServiceName"];
            string TxlogPort = Txlog.Split(':')[0];
            string TxlogServiceName = Txlog.Split(':')[1];
            socketServer2 = new AsyncMultiSocketServer(Convert.ToInt32(TxlogPort), TxlogServiceName);
            string Query = String.IsNullOrEmpty(System.Configuration.ConfigurationSettings.AppSettings["QueryPortAndServiceName"]) ? "6103:Query" : System.Configuration.ConfigurationSettings.AppSettings["QueryPortAndServiceName"];
            string QueryPort = Query.Split(':')[0];
            string QueryServiceName = Query.Split(':')[1];
            socketServer3 = new AsyncMultiSocketServer(Convert.ToInt32(QueryPort), QueryServiceName);
            string Reversal = String.IsNullOrEmpty(System.Configuration.ConfigurationSettings.AppSettings["ReversalPortAndServiceName"]) ? "6104:Reversal" : System.Configuration.ConfigurationSettings.AppSettings["ReversalPortAndServiceName"];
            string ReversalPort = Reversal.Split(':')[0];
            string ReversalServiceName = Reversal.Split(':')[1];
            socketServer4 = new AsyncMultiSocketServer(Convert.ToInt32(ReversalPort), ReversalServiceName);
            string HiLo = String.IsNullOrEmpty(System.Configuration.ConfigurationSettings.AppSettings["HiLoPortAndServiceName"]) ? "6100:HiLo" : System.Configuration.ConfigurationSettings.AppSettings["HiLoPortAndServiceName"];
            string HiLoPort = HiLo.Split(':')[0];
            string HiLoServiceName = HiLo.Split(':')[1];
            socketServer5 = new AsyncMultiSocketServer(Convert.ToInt32(HiLoPort), HiLoServiceName);
            string LossReportOrAddRejectAuth = String.IsNullOrEmpty(System.Configuration.ConfigurationSettings.AppSettings["LossReportOrAddRejectAuthPortAndServiceName"]) ? "6109:LossReportOrAddRejectAuth" : System.Configuration.ConfigurationSettings.AppSettings["LossReportOrAddRejectAuthPortAndServiceName"];
            string LossReportOrAddRejectAuthPort = LossReportOrAddRejectAuth.Split(':')[0];
            string LossReportOrAddRejectAuthServiceName = LossReportOrAddRejectAuth.Split(':')[1];
            socketServer6 = new AsyncMultiSocketServer(Convert.ToInt32(LossReportOrAddRejectAuthPort), LossReportOrAddRejectAuthServiceName);
        }

        protected override void OnStart(string[] args)
        {
            log.Debug("Start Service...");
            try
            {
                this.socketServer1.Start();
                // none-block, continue....
                this.socketServer2.Start();
                // none-block, continue....
                this.socketServer3.Start();
                // none-block, continue....
                this.socketServer4.Start();
                // none-block, continue....
                this.socketServer5.Start();
                // none-block, continue....
                this.socketServer6.Start();
                // none-block, continue....
            }
            catch (Exception ex)
            {
                log.Error(ex.ToString());
            }
        }

        protected override void OnStop()
        {
            log.Debug("Stop Service...");
            this.socketServer1.Stop();
            // none-block, continue....
            this.socketServer2.Stop();
            // none-block, continue....
            this.socketServer3.Stop();
            // none-block, continue....
            this.socketServer4.Stop();
            // none-block, continue....
            this.socketServer5.Start();
            // none-block, continue....
            this.socketServer6.Start();
            // none-block, continue....
        }
    }
}
