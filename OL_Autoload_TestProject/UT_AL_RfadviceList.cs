﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using ALCommon;
using System.Data.SqlClient;
using System.Data;
using OL_Autoload_Lib;

namespace OL_Autoload_TestProject
{
    [TestClass]
    public class UT_AL_RfadviceList
    {
        AL_DBModule dbModule;
        SqlConnection sqlCon;
        String sSql = String.Empty;

        [TestMethod]
        public void TestMethod1()//主檔無資料，名單新增
        {
            bool expected = true;
            bool actual = false;

            //Test
            AutoloadRqt_FBank rqt = new AutoloadRqt_FBank();            
            rqt.BANK_CODE = "822";
            rqt.FILE_UPDATE_CODE = "1";
            rqt.ICC_NO = "1234567890123456";
            rqt.MESSAGE_TYPE = "0302";
            rqt.PROCESSING_CODE = "990178";
            rqt.RC = "00";
            rqt.RRN = "123456789012";
            rqt.STAN = "789012";
            rqt.TRANS_DATETIME = "20150205121212";
            

            dbModule = new AL_DBModule();
            sqlCon = dbModule.OpenConnection_Merchant("Test");
            AL_FBank fBank = new AL_FBank();

            actual = fBank.Upt_RfadviceList(rqt, dbModule);
            Assert.AreEqual(expected, actual);
            dbModule.CloseConnection();

            //後續處理
            sSql = String.Empty;
            sSql = @"delete OL_AL_RFADVICE_LIST_D
where ICC_NO='1234567890123456';";
            dbModule = new AL_DBModule();
            sqlCon = dbModule.OpenConnection_Merchant("Test");
            dbModule.ExecuteSQL(sSql);
            dbModule.CloseConnection();

        }


        [TestMethod]
        public void TestMethod2()//主檔無資料，名單刪除
        {
            bool expected = true;
            bool actual = false;

            //Test
            AutoloadRqt_FBank rqt = new AutoloadRqt_FBank();
            rqt.BANK_CODE = "822";
            rqt.FILE_UPDATE_CODE = "3";
            rqt.ICC_NO = "1234567890123456";
            rqt.MESSAGE_TYPE = "0302";
            rqt.PROCESSING_CODE = "990178";
            rqt.RC = "00";
            rqt.RRN = "123456789012";
            rqt.STAN = "789012";
            rqt.TRANS_DATETIME = "20150205121212";

            dbModule = new AL_DBModule();
            sqlCon = dbModule.OpenConnection_Merchant("Test");
            AL_FBank fBank = new AL_FBank();

            actual = fBank.Upt_RfadviceList(rqt, dbModule);
            Assert.AreEqual(expected, actual);
            dbModule.CloseConnection();


        }

        [TestMethod]
        public void TestMethod3()//主檔有資料，名單新增
        {
            bool bExpected = true;
            bool bActual = false;
            String sExpected = String.Empty;
            String sActual = String.Empty;

            //前置作業，主檔有資料且狀態為新增
            sSql = @"INSERT INTO OL_AL_RFADVICE_LIST_D
(MERCHANT_NO, BANK_CODE, ICC_NO, STATUS, UPT_DATE, UPT_TIME)
VALUES
('03077208', '822', '1234567890123456', '1', '20150206', '121212')";
            dbModule = new AL_DBModule();
            dbModule.OpenConnection_Merchant("Test");
            dbModule.ExecuteSQL(sSql);
            dbModule.CloseConnection();

            //Test
            AutoloadRqt_FBank rqt = new AutoloadRqt_FBank();
            rqt.BANK_CODE = "822";
            rqt.FILE_UPDATE_CODE = "1";
            rqt.ICC_NO = "1234567890123456";
            rqt.MESSAGE_TYPE = "0302";
            rqt.PROCESSING_CODE = "990178";
            rqt.RC = "00";
            rqt.RRN = "123456789012";
            rqt.STAN = "789012";
            rqt.TRANS_DATETIME = "20150206121212";
            sExpected = "1";

            dbModule = new AL_DBModule();
            sqlCon = dbModule.OpenConnection_Merchant("Test");
            AL_FBank fBank = new AL_FBank();

            bActual = fBank.Upt_RfadviceList(rqt, dbModule);
            Assert.AreEqual(bExpected, bActual);
            sSql = @"SELECT STATUS FROM OL_AL_RFADVICE_LIST_D
WHERE ICC_NO='1234567890123456';";
            dbModule = new AL_DBModule();
            sqlCon = dbModule.OpenConnection_Merchant("Test");
            DataTable dt = dbModule.GetDataTable(sSql);
            Assert.AreEqual(sExpected, dt.Rows[0]["STATUS"].ToString());
            dbModule.CloseConnection();

            //前置作業，主檔有資料且狀態為刪除
            sSql = String.Empty;
            sSql = @"UPDATE OL_AL_RFADVICE_LIST_D
SET STATUS='3'
WHERE ICC_NO='1234567890123456'";
            dbModule = new AL_DBModule();
            dbModule.OpenConnection_Merchant("Test");
            dbModule.ExecuteSQL(sSql);
            dbModule.CloseConnection();

            //Test
            sExpected = "1";
            rqt.FILE_UPDATE_CODE = "1";

            dbModule = new AL_DBModule();
            sqlCon = dbModule.OpenConnection_Merchant("Test");

            bActual = fBank.Upt_RfadviceList(rqt, dbModule);
            Assert.AreEqual(bExpected, bActual);

            sSql = @"SELECT STATUS FROM OL_AL_RFADVICE_LIST_D
WHERE ICC_NO='1234567890123456';";
            dbModule = new AL_DBModule();
            sqlCon = dbModule.OpenConnection_Merchant("Test");
            dt = dbModule.GetDataTable(sSql);
            Assert.AreEqual(sExpected, dt.Rows[0]["STATUS"].ToString());
            dbModule.CloseConnection();

            //後續處理
            sSql = String.Empty;
            sSql = @"DELETE OL_AL_RFADVICE_LIST_D
WHERE ICC_NO='1234567890123456';";
            dbModule = new AL_DBModule();
            sqlCon = dbModule.OpenConnection_Merchant("Test");
            dbModule.ExecuteSQL(sSql);
            dbModule.CloseConnection();
        }



    }
}
