﻿using Common.Logging;
using NUnit.Framework;
using AsyncMultiSocket;
using System.Text;
using System.Threading;

namespace OL_Autoload_TestProject
{
    [TestFixture]
    class UT_AL_SocketServer
    {
        private static readonly ILog log = LogManager.GetLogger(typeof(UT_AL_SocketServer));
        private ISocketServer socketServer_Autoload;
        private ISocketServer SocketServer_HiLo;
        [SetUp]
        public void Init()
        {
            socketServer_Autoload = new AsyncMultiSocketServer(6101);
            SocketServer_HiLo = new AsyncMultiSocketServer(6100, false, "HiLo", Encoding.ASCII);
        }
        [Test]
        public void Test_Strat()
        {
            socketServer_Autoload.Start();
            log.Debug("服務啟動");
            Thread.Sleep(new System.TimeSpan(0, 1, 0));//保持兩分鐘
        }
        [Test]
        public void Test_Stop()
        {
            socketServer_Autoload.Start();
            Thread.Yield();
            socketServer_Autoload.Stop();
            log.Debug("服務停止");
        }
        [TearDown]
        public void TearDown()
        {
            socketServer_Autoload.Stop();
            socketServer_Autoload = null;
        }
    }
}
