﻿using System;
using System.Linq;
//
using Common.Logging;
//
using NUnit.Framework;
using Kms.PosClient;
//

namespace OL_Autoload_TestProject
{
    /// <summary>
    /// UT_AL_SocketServer_AutoLoad 的摘要描述
    /// </summary>
    [TestFixture]
    public class UT_AL_SocketServer_AutoLoad
    {
        private static readonly ILog log = LogManager.GetLogger(typeof(UT_AL_SocketServer_AutoLoad));

        #region Properties
        /// <summary>
        /// 00+6bytes
        /// </summary>
        public string store { get; set; }
        /// <summary>
        /// 1 byte
        /// </summary>
        public string pos { get; set; }
        /// <summary>
        /// 16 bytes
        /// </summary>
        public string cardId { get; set; }
        /// <summary>
        /// 20150112180000
        /// </summary>
        public string ALdate { get; set; }
        /// <summary>
        /// 8 bytes
        /// </summary>
        public string AMT { get; set; }
        /// <summary>
        /// return code
        /// </summary>
        public string RC { get; set; }
        /// <summary>
        ///  加值序號
        /// </summary>
        public string SN { get; set; }
        /// <summary>
        /// 14bytes
        /// </summary>
        public string TID { get; set; }
        /// <summary>
        /// 處理時間
        /// </summary>
        public string ptime { get; set; }
        /// <summary>
        /// 處理Client端的加解密物件
        /// </summary>
        public SessionHandler sessionhandler { get; set; }

        public SocketClient.Domain.SocketClient socketClient { get; set; }
        #endregion

        [SetUp]
        public void Init()
        {
            //SessionHandler的參數設定(固定RanA和RanB)
            SessionHandler sessionhandler = new SessionHandler();
            string ranA = "1234567890ABCDEF1234567890ABCDEF";
            string ranB = "E6EA143CCB47EAB569C60BE26AAE12D5";
            sessionhandler.TId = "8604230F629F2980";//86+POS的Reader ID(7碼Binary)
            sessionhandler.RndA = ranA;
            sessionhandler.FixRndB = true;//固定RandB
            sessionhandler.RndB = ranB;

            //測試時可修改的電文資料(配合DB內有的)
            this.pos = "2";
            this.store = "00110013";
            this.ALdate = "20150112185959";
            this.AMT = "00000500";
            this.cardId = "0417149984000007";
            this.TID = "04230F629F2980";

            // run auth01
            if (Auth01())
            {
                // Run auth02
                if (Auth02()) 
                {
                    //auto01 and auth02 pass
                }
                else
                {
                    log.Error("===> Auth02 failed!");
                }
            }
            else
            {
                log.Error("===> Auth01 failed!");
            }
        }
        /// <summary>
        /// 驗證Auth01流程
        /// </summary>
        /// <returns></returns>
        private bool Auth01()
        {
            string ip = "127.0.0.1";
            int port = 8105;
            socketClient = new SocketClient.Domain.SocketClient(ip, port);

            //----------------------------------------------
            //****Auth01*****第一次驗證**********
            //1.產生SessionHandler,用來處理Auth01與Auth02和使用SessionKEY加解密
            //2.先輸入固定RandA與RandB

            //auth01電文header
            string auth01Str = "01020103110000000186000000018604230F629F2980    ZOO001123456781001100000003200000032            ";
            //auth01電文data(hex)
            string auth01DataHex = "303130303030303030308604230F629F29802020202020202020202020202020";
            string auth01Hex = sessionhandler.HexConverter.Str2Hex(auth01Str) + auth01DataHex;
            byte[] auth01Bytes = sessionhandler.HexConverter.Hex2Bytes(auth01Hex);

            byte[] recieve = null;
            
            if (socketClient.ConnectToServer())
            {    
                recieve = socketClient.SendAndReceive(auth01Bytes);
                socketClient.CloseConnection();
            }
            //傳回來的RandomA'(加密過)
            ArraySegment<byte> randAEnc = new ArraySegment<byte>(recieve, 96 + 2, 16);//去掉header + data無用的header(2bytes) => 取32(binary)的randA'
            string randAEncHex = sessionhandler.HexConverter.Bytes2Hex(randAEnc.ToArray());

            string randAEncryptoHex = "4A92D4FF3C9F568B06D14B6FD87831F6";//K.M.S Data=0x4A,0x92,.....
            try
            {
                //RandomA'解密
                byte[] randAdecrypto = sessionhandler.Auth1Response(sessionhandler.HexConverter.Hex2Bytes(randAEncHex));
                //轉回hex的RandA
                string ranA2 = sessionhandler.HexConverter.Bytes2Hex(randAdecrypto);
                //Assert.AreEqual(ranA2, sessionhandler.RndA);
                if (sessionhandler.RndA.Equals(ranA2))
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception ex)
            {
                log.Error("RandA' Validate Failed: " + ex.Source);
                return false;
            }
        }
        private bool Auth02()
        {
            string ip = "127.0.0.1";
            int port = 8105;
            socketClient = new SocketClient.Domain.SocketClient(ip, port);
            //
            byte[] auth2RequestData = sessionhandler.Auth2Request();//(96bytes)
            //auth02電文內Request = RanA(16binary) + RanB(16binary) + TerminalID(8binary) + Padding(8binary)
            string auth2str = sessionhandler.HexConverter.Bytes2Hex(auth2RequestData);//2C6362CFC1AB965ED3CB4E19186A917CB0416CA8F5E6A0D136E0D1D2F9E4CAAEDB882103E8EA26C1FCCE014CBDD4FEC2
            //auth02電文Header
            string auth2RequestStr = "01020103210000000186000000018604230F629F2980    ZOO001123456781001100000006400000064            ";
            string auth2RequestHex = sessionhandler.HexConverter.Str2Hex(auth2RequestStr);
            //auth02電文data
            string auth2RequestDataHex = "3031" + sessionhandler.HexConverter.Bytes2Hex(auth2RequestData) + "2020202020202020202020202020";//header(2bytes) + request(48bytes) + padding(14bytes)

            byte[] auth2RequestBytes = sessionhandler.HexConverter.Hex2Bytes(auth2RequestHex + auth2RequestDataHex);
            byte[] auth2Result = null;
            if (socketClient.ConnectToServer())
            {
                auth2Result = socketClient.SendAndReceive(auth2RequestBytes);
                socketClient.CloseConnection();
            };

            ArraySegment<byte> requestRandB = new ArraySegment<byte>(auth2Result, 96 + 2, 16);//Data共64byte取RanB部分16bytes
            string randBEncryptoHex = sessionhandler.HexConverter.Bytes2Hex(requestRandB.ToArray());//加密的RanB
            try
            {
                byte[] randBbytes = sessionhandler.Auth2Response(requestRandB.ToArray());//RandB Decrypto => RanB(byte[])有移位過
                string randBHex = sessionhandler.HexConverter.Bytes2Hex(randBbytes);
                string randB =  randBHex.Substring(0, 4) + randBHex.Substring(4,randBHex.Length - 4);//把RanB'[左移的2個字元]轉回原始RanB狀態
                if (sessionhandler.RndB.Equals(randB))
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception ex)
            {
                log.Error("RandA' Validate Failed: " + ex.Source);
                return false;
            }
        }
        /// <summary>
        /// test autoload process
        /// </summary>
        [Test]
        public void UnitTest_State_AutoLoad()
        {
            string ip = "127.0.0.1";
            int port = 8105;
            socketClient = new SocketClient.Domain.SocketClient(ip, port);
            //自動加值電文Header
            string autoLoadRequest = "010302033200000001860000000186" + TID + "    SET001" + store + "100" + pos + "100000014400000160            ";  
            //string autoLoadRequest = "01030203320000000186000000018604230F629F2980    SET001000000011001100000014400000160            ";            
            string ALRequestHex = sessionhandler.HexConverter.Str2Hex(autoLoadRequest);
            byte[] autoloadRequestHeaderBytes = sessionhandler.HexConverter.Hex2Bytes(ALRequestHex);
            //自動加值電文Data
            string autoloadRequestDataStr = "01                FirmwareVerssion" + ALdate + "      0000000"+AMT+"0000                12345678" + cardId + "201512312015123100000001A      ";
            //string autoloadRequestDataStr = "01                FirmwareVerssion20150109185959      0000000000005000000                123456780417149984000007201512312015123100000001A      ";
            byte[] autoloadRequestDataEnc = sessionhandler.Encrypt(sessionhandler.HexConverter.Hex2Bytes(sessionhandler.HexConverter.Str2Hex(autoloadRequestDataStr)));//自動加值電文Data部分加密

            byte[] autoloadRequestAll = new byte[autoloadRequestHeaderBytes.Length + autoloadRequestDataEnc.Length];
            Buffer.BlockCopy(autoloadRequestHeaderBytes, 0, autoloadRequestAll, 0, autoloadRequestHeaderBytes.Length);
            Buffer.BlockCopy(autoloadRequestDataEnc, 0, autoloadRequestAll, autoloadRequestHeaderBytes.Length, autoloadRequestDataEnc.Length);

            byte[] resultBytes = null;
            if (socketClient.ConnectToServer())
            {
                resultBytes = socketClient.SendAndReceive(autoloadRequestAll);
                socketClient.CloseConnection();
            }

            byte[] autoloadResposneDataDecryBefor = new byte[160];
            string autoloadResposnehex = sessionhandler.HexConverter.Bytes2Hex(resultBytes);
            string autoloadresposneStr = sessionhandler.HexConverter.Hex2Str(autoloadResposnehex);
            this.RC = autoloadresposneStr.Substring(10,6);
            Array.Copy(resultBytes, 96, autoloadResposneDataDecryBefor, 0, autoloadResposneDataDecryBefor.Length);
            byte[] autoloadResposneDataAfter = sessionhandler.Decrypt(autoloadResposneDataDecryBefor);
            string autoloadResponseDataHex = sessionhandler.HexConverter.Bytes2Hex(autoloadResposneDataAfter);
            string autoloadResponseDataStr = sessionhandler.HexConverter.Hex2Str(autoloadResponseDataHex);
            this.SN = autoloadResponseDataStr.Substring(2, 8);
        }

    }
}
