﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
//
using Common.Logging;
//
using NUnit.Framework;
//
using HiLo.Business;
using HiLo.Domain.Entities;

namespace OL_Autoload_TestProject
{
    class UT_AL_HiLoGenerator
    {
        private static readonly ILog log = LogManager.GetLogger(typeof(UT_AL_HiLoGenerator));
        private HiLoGenerator hiLoGerenator;
        private Dictionary<string, HiLoDO> hiLoDic;
        private HiLoDO hiLo;
        private List<long> loList = new List<long>();
        private object nLock = new object();
        private object cLock = new object();
        [SetUp]
        public void Init()
        {
            string tableName = "HiLo";
            hiLoGerenator = new HiLoGenerator(tableName);
            hiLoDic = new Dictionary<string, HiLoDO>();
            hiLo = new HiLoDO()
            {
                KeyName = "Qoo",
                NextHi = 0,
                MaxVal = 2000
            };
            this.hiLoGerenator.CreateSequence(hiLo);
        }

        [Test]
        public void Test_NextKey()
        {
            Task task = Task.Factory.StartNew(() => Task_NextKey(900));
            Task_NextKey(1000);
            task.Wait();

            int length = loList.Count;
            for (int i = length; i > 0; i--)
            {
                try
                {
                    long tmp = loList.Single(n => n == (long)i);
                    loList.Remove(tmp);//移除叫來的號碼
                }
                catch (Exception ex)
                {
                    log.Error(loList[i].ToString() + "項目有問題" + ex.Message);
                }
            }


            Assert.AreEqual(0, loList.Count);
        }

        private void Task_NextKey(int count)
        {
            //沒lock會有機率(4/1900)產生小部分的0的號碼
            lock (nLock)
            {
                for (int i = 0; i < count; i++)
                {
                    try
                    {
                        long num = this.hiLoGerenator.NextKey(hiLo.KeyName);
                        loList.Add(num);
                    }
                    catch (Exception ex)
                    {
                        log.Error("NextKey Failed:" + ex.StackTrace);
                    }
                }
            }
        }
        [Test]
        public void Test_CreateSequence()
        {
            HiLoDO hiLoDO = new HiLoDO()
            {
                KeyName = "Test",
                NextHi = 1,
                MaxVal = 1000
            };
            HiLoDO hiLoDO2 = new HiLoDO()
            {
                KeyName = "Num",
                NextHi = 2,
                MaxVal = 2000
            };
            Task task = Task.Factory.StartNew(() => this.Task_CreateSequence(hiLoDO, 3));
            this.Task_CreateSequence(hiLoDO2, 3);
            task.Wait();
            IList<HiLoDO> hiLoList = this.hiLoGerenator.HiLoDAO.FindAll();

            foreach (string keyName in hiLoDic.Keys)
            {
                foreach (HiLoDO actualObj in hiLoList)
                {
                    if (actualObj.KeyName == keyName)
                    {
                        log.Debug("=====================================================");
                        log.Debug("原始HiLo: \n" + hiLoDic[actualObj.KeyName].ToString());

                        log.Debug("資料庫HiLo: \n" + actualObj.ToString());
                        log.Debug("=====================================================");
                        Assert.AreEqual(hiLoDic[actualObj.KeyName].ToString(), actualObj.ToString());
                        break;
                    }
                }
            }

        }

        private void Task_CreateSequence(HiLoDO hiLoDO, int count)
        {
            lock (cLock)
            {
                HiLoDO newHiLo = null;
                for (int i = 0; i < count; i++)
                {
                    string tmp = hiLoDO.KeyName;
                    tmp += i.ToString();
                    try
                    {
                        newHiLo = new HiLoDO()
                        {
                            KeyName = tmp,
                            NextHi = hiLoDO.NextHi,
                            MaxVal = hiLoDO.MaxVal
                        };
                        this.hiLoGerenator.CreateSequence(newHiLo);
                        hiLoDic.Add(tmp, newHiLo);
                    }
                    catch (Exception ex)
                    {
                        log.Error("CreateSequence Failed:" + hiLoDO.KeyName + " => " + ex.StackTrace);
                    }

                }
            }
        }
        [TearDown]
        public void TearDown()
        {
            foreach (string keyName in hiLoDic.Keys)
            {
                if (this.hiLoGerenator.HiLoDAO.Exist(hiLoDic[keyName].KeyName))
                {
                    this.hiLoGerenator.HiLoDAO.Delete(hiLoDic[keyName].KeyName);
                }
            }

            if (this.hiLoGerenator.HiLoDAO.Exist(hiLo.KeyName))
            {
                this.hiLoGerenator.HiLoDAO.Delete(hiLo.KeyName);
            }
        }
    }
}
