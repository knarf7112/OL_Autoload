﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using ALCommon;
using System.Data.SqlClient;
using System.Data;
using OL_Autoload_Lib;

namespace OL_Autoload_TestProject
{
    [TestClass]
    public class UT_AL_Reportloss
    {
        AL_DBModule dbModule;
        SqlConnection sqlCon;
        String sSql = String.Empty;

        [TestMethod]
        public void TestMethod1()//連線掛失，黑名單主檔沒資料
        {
            bool expected = true;
            bool actual = false;

            //前置作業
            sSql = @"DECLARE @chk tinyint
SET @chk = 0
BEGIN Transaction Reportloss
--1. 卡主檔 CM_SVC17_D
insert into CM_SVC17_D
(CARD_ID, CHIP_ID, CSTATUS, MOD_DATE)
values
('1234567890123456', '123456789012345', '2', '20140205')

--2. 記名卡主檔 CC_REGISTER
insert into CC_REGISTER
(CASE_ID, NAME, TW_ID, CARD_NO, CARD_STATUS, SIGN_TYPE, CREATED_TIME)
values
('2015020599', 'KayTest', 'T999999999', '1234567890123456', '1', '1', '20150205121212')

IF @chk <> 0 BEGIN -- 若是新增資料發生錯誤
Rollback Transaction Reportloss -- 復原所有操作所造成的變更
END
ELSE BEGIN
Commit Transaction Reportloss -- 提交所有操作所造成的變更
END";
            dbModule = new AL_DBModule();
            dbModule.OpenConnection_Merchant("Test");
            dbModule.ExecuteSQL(sSql);
            dbModule.CloseConnection();

            //Test
            AutoloadRqt_FBank rqt = new AutoloadRqt_FBank();
            rqt.BANK_CODE = "822";
            rqt.FILE_UPDATE_CODE  = "1";
            rqt.ICC_NO = "1234567890123456";
            rqt.MESSAGE_TYPE = "0302";
            rqt.PROCESSING_CODE = "990176";
            rqt.RC = "00";
            rqt.RRN = "123456789012";
            rqt.STAN = "789012";
            rqt.TRANS_DATETIME = "20150206121212";

            dbModule = new AL_DBModule();
            sqlCon = dbModule.OpenConnection_Merchant("Test");
            AL_FBank fBank = new AL_FBank();
            actual = fBank.Upt_Reportloss(rqt, dbModule);
            Assert.AreEqual(expected, actual);
            dbModule.CloseConnection();

            //後續處理
            sSql = @"DECLARE @chk tinyint
SET @chk = 0
BEGIN Transaction Reportloss

DELETE OL_AL_REPORTLOSS_LOG_D
WHERE ICC_NO = '1234567890123456'

DELETE CM_SVC17_D
WHERE CARD_ID='1234567890123456'

DELETE CM_BLACKLIST_D
WHERE CARD_ID='1234567890123456'

DELETE CM_BLACKLIST_DETAIL_D
WHERE CARD_ID='1234567890123456'

DELETE CC_REGISTER
WHERE CARD_NO = '1234567890123456'

DELETE CC_REGISTER_LOG
WHERE CARD_NO='1234567890123456'

IF @chk <> 0 BEGIN -- 若是新增資料發生錯誤
Rollback Transaction Reportloss -- 復原所有操作所造成的變更
END
ELSE BEGIN
Commit Transaction Reportloss -- 提交所有操作所造成的變更
END";

            dbModule = new AL_DBModule();
            sqlCon = dbModule.OpenConnection_Merchant("Test");
            dbModule.ExecuteSQL(sSql);
            dbModule.CloseConnection();
        }

        [TestMethod]
        public void TestMethod2()//連線掛失，黑名單主檔有資料
        {
            bool expected = true;
            bool actual = false;

            //前置作業
            sSql = @"DECLARE @chk tinyint
SET @chk = 0
BEGIN Transaction Reportloss
--1. 卡主檔 CM_SVC17_D
insert into CM_SVC17_D
(CARD_ID, CHIP_ID, CSTATUS, MOD_DATE)
values
('1234567890123456', '123456789012345', '2', '20140205')

--2. 記名卡主檔 CC_REGISTER
insert into CC_REGISTER
(CASE_ID, NAME, TW_ID, CARD_NO, CARD_STATUS, SIGN_TYPE, CREATED_TIME)
values
('2015020599', 'KayTest', 'T999999999', '1234567890123456', '1', '1', '20150205121212')

--3. 黑名單主檔
INSERT INTO CM_BLACKLIST_D
(CARD_TYPE, CARD_ID, STATUS, MOD_DATE, BANK_CODE)
VALUES
('CB', '1234567890123456', 'U', '20150205121212', '822')

IF @chk <> 0 BEGIN -- 若是新增資料發生錯誤
Rollback Transaction Reportloss -- 復原所有操作所造成的變更
END
ELSE BEGIN
Commit Transaction Reportloss -- 提交所有操作所造成的變更
END";
            dbModule = new AL_DBModule();
            dbModule.OpenConnection_Merchant("Test");
            dbModule.ExecuteSQL(sSql);
            dbModule.CloseConnection();

            //Test
            AutoloadRqt_FBank rqt = new AutoloadRqt_FBank();
            rqt.BANK_CODE = "822";
            rqt.FILE_UPDATE_CODE = "1";
            rqt.ICC_NO = "1234567890123456";
            rqt.MESSAGE_TYPE = "0302";
            rqt.PROCESSING_CODE = "990176";
            rqt.RC = "00";
            rqt.RRN = "123456789012";
            rqt.STAN = "789012";
            rqt.TRANS_DATETIME = "20150206121212";

            dbModule = new AL_DBModule();
            sqlCon = dbModule.OpenConnection_Merchant("Test");
            AL_FBank fBank = new AL_FBank();
            actual = fBank.Upt_Reportloss(rqt, dbModule);
            Assert.AreEqual(expected, actual);
            dbModule.CloseConnection();

            //後續處理
            sSql = @"DECLARE @chk tinyint
SET @chk = 0
BEGIN Transaction Reportloss

DELETE OL_AL_REPORTLOSS_LOG_D
WHERE ICC_NO = '1234567890123456'

DELETE CM_SVC17_D
WHERE CARD_ID='1234567890123456'

DELETE CM_BLACKLIST_D
WHERE CARD_ID='1234567890123456'

DELETE CM_BLACKLIST_DETAIL_D
WHERE CARD_ID='1234567890123456'

DELETE CC_REGISTER
WHERE CARD_NO = '1234567890123456'

DELETE CC_REGISTER_LOG
WHERE CARD_NO='1234567890123456'

IF @chk <> 0 BEGIN -- 若是新增資料發生錯誤
Rollback Transaction Reportloss -- 復原所有操作所造成的變更
END
ELSE BEGIN
Commit Transaction Reportloss -- 提交所有操作所造成的變更
END";

            dbModule = new AL_DBModule();
            sqlCon = dbModule.OpenConnection_Merchant("Test");
            dbModule.ExecuteSQL(sSql);
            dbModule.CloseConnection();
        }
        
        [TestMethod]
        public void TestMethod3()//掛失取消，黑名單主檔沒資料
        {
            bool expected = true;
            bool actual = false;

            //前置作業
            sSql = @"DECLARE @chk tinyint
SET @chk = 0
BEGIN Transaction Reportloss
--1. 卡主檔 CM_SVC17_D
insert into CM_SVC17_D
(CARD_ID, CHIP_ID, CSTATUS, MOD_DATE)
values
('1234567890123456', '123456789012345', '6', '20140205')

--2. 記名卡主檔 CC_REGISTER
insert into CC_REGISTER
(CASE_ID, NAME, TW_ID, CARD_NO, CARD_STATUS, SIGN_TYPE, CREATED_TIME)
values
('2015020599', 'KayTest', 'T999999999', '1234567890123456', '2', '1', '20150205121212')

IF @chk <> 0 BEGIN -- 若是新增資料發生錯誤
Rollback Transaction Reportloss -- 復原所有操作所造成的變更
END
ELSE BEGIN
Commit Transaction Reportloss -- 提交所有操作所造成的變更
END";
            dbModule = new AL_DBModule();
            dbModule.OpenConnection_Merchant("Test");
            dbModule.ExecuteSQL(sSql);
            dbModule.CloseConnection();

            //Test
            AutoloadRqt_FBank rqt = new AutoloadRqt_FBank();
            rqt.BANK_CODE = "822";
            rqt.FILE_UPDATE_CODE = "3";
            rqt.ICC_NO = "1234567890123456";
            rqt.MESSAGE_TYPE = "0302";
            rqt.PROCESSING_CODE = "990176";
            rqt.RC = "00";
            rqt.RRN = "123456789012";
            rqt.STAN = "789012";
            rqt.TRANS_DATETIME = "20150206222222";

            dbModule = new AL_DBModule();
            sqlCon = dbModule.OpenConnection_Merchant("Test");
            AL_FBank fBank = new AL_FBank();
            actual = fBank.Upt_Reportloss(rqt, dbModule);
            Assert.AreEqual(expected, actual);
            dbModule.CloseConnection();

            //後續處理
            sSql = @"DECLARE @chk tinyint
SET @chk = 0
BEGIN Transaction Reportloss

DELETE OL_AL_REPORTLOSS_LOG_D
WHERE ICC_NO = '1234567890123456'

DELETE CM_SVC17_D
WHERE CARD_ID='1234567890123456'

DELETE CM_BLACKLIST_D
WHERE CARD_ID='1234567890123456'

DELETE CM_BLACKLIST_DETAIL_D
WHERE CARD_ID='1234567890123456'

DELETE CC_REGISTER
WHERE CARD_NO = '1234567890123456'

DELETE CC_REGISTER_LOG
WHERE CARD_NO='1234567890123456'

IF @chk <> 0 BEGIN -- 若是新增資料發生錯誤
Rollback Transaction Reportloss -- 復原所有操作所造成的變更
END
ELSE BEGIN
Commit Transaction Reportloss -- 提交所有操作所造成的變更
END";

            dbModule = new AL_DBModule();
            sqlCon = dbModule.OpenConnection_Merchant("Test");
            dbModule.ExecuteSQL(sSql);
            dbModule.CloseConnection();
        }

        [TestMethod]
        public void TestMethod4()//掛失取消，黑名單主檔有資料
        {
            bool expected = true;
            bool actual = false;

            //前置作業
            sSql = @"DECLARE @chk tinyint
SET @chk = 0
BEGIN Transaction Reportloss
--1. 卡主檔 CM_SVC17_D
insert into CM_SVC17_D
(CARD_ID, CHIP_ID, CSTATUS, MOD_DATE)
values
('1234567890123456', '123456789012345', '6', '20140205')

--2. 記名卡主檔 CC_REGISTER
insert into CC_REGISTER
(CASE_ID, NAME, TW_ID, CARD_NO, CARD_STATUS, SIGN_TYPE, CREATED_TIME)
values
('2015020599', 'KayTest', 'T999999999', '1234567890123456', '2', '1', '20150205121212')

--3. 黑名單主檔
INSERT INTO CM_BLACKLIST_D
(CARD_TYPE, CARD_ID, STATUS, MOD_DATE, BANK_CODE)
VALUES
('CB', '1234567890123456', 'I', '20150205121212', '822')

IF @chk <> 0 BEGIN -- 若是新增資料發生錯誤
Rollback Transaction Reportloss -- 復原所有操作所造成的變更
END
ELSE BEGIN
Commit Transaction Reportloss -- 提交所有操作所造成的變更
END";
            dbModule = new AL_DBModule();
            dbModule.OpenConnection_Merchant("Test");
            dbModule.ExecuteSQL(sSql);
            dbModule.CloseConnection();

            //Test
            AutoloadRqt_FBank rqt = new AutoloadRqt_FBank();
            rqt.BANK_CODE = "822";
            rqt.FILE_UPDATE_CODE = "3";
            rqt.ICC_NO = "1234567890123456";
            rqt.MESSAGE_TYPE = "0302";
            rqt.PROCESSING_CODE = "990176";
            rqt.RC = "00";
            rqt.RRN = "123456789012";
            rqt.STAN = "789012";
            rqt.TRANS_DATETIME = "20150206222222";

            dbModule = new AL_DBModule();
            sqlCon = dbModule.OpenConnection_Merchant("Test");
            AL_FBank fBank = new AL_FBank();
            actual = fBank.Upt_Reportloss(rqt, dbModule);
            Assert.AreEqual(expected, actual);
            dbModule.CloseConnection();

            //後續處理
            sSql = @"DECLARE @chk tinyint
SET @chk = 0
BEGIN Transaction Reportloss

DELETE OL_AL_REPORTLOSS_LOG_D
WHERE ICC_NO = '1234567890123456'

DELETE CM_SVC17_D
WHERE CARD_ID='1234567890123456'

DELETE CM_BLACKLIST_D
WHERE CARD_ID='1234567890123456'

DELETE CM_BLACKLIST_DETAIL_D
WHERE CARD_ID='1234567890123456'

DELETE CC_REGISTER
WHERE CARD_NO = '1234567890123456'

DELETE CC_REGISTER_LOG
WHERE CARD_NO='1234567890123456'

IF @chk <> 0 BEGIN -- 若是新增資料發生錯誤
Rollback Transaction Reportloss -- 復原所有操作所造成的變更
END
ELSE BEGIN
Commit Transaction Reportloss -- 提交所有操作所造成的變更
END";

            dbModule = new AL_DBModule();
            sqlCon = dbModule.OpenConnection_Merchant("Test");
            dbModule.ExecuteSQL(sSql);
            dbModule.CloseConnection();
        }

    }
}
