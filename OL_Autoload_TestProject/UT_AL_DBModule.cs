﻿using System;
using System.Xml;
using ALCommon;
using System.Data.SqlClient;
using NUnit.Framework;
using Common.Logging;
using System.Data;

namespace OL_Autoload_TestProject
{
    [TestFixture]
    public class UT_AL_DBModule
    {
        private static readonly ILog log = LogManager.GetLogger(typeof(UT_AL_DBModule));
        AL_DBModule dbModule;
        SqlConnection sqlCon;
        string sSql;
        SqlParameter[] sqlParams;


        //

        [SetUp]
        public void init()
        {
            dbModule = new AL_DBModule();
            sqlCon = dbModule.OpenConnection_Merchant("Test");
            log.Debug("開始連線" + sqlCon.State.ToString());


            sSql = @"
insert into Customers 
(CustomerId,FirstName,LastName) 
Values (@CustomerId,@FirstName,@LastName)
";

            sqlParams = new SqlParameter[3];
            sqlParams[0] = new SqlParameter();
            sqlParams[0].ParameterName = "@CustomerId";
            sqlParams[0].Value = "1";
            sqlParams[0].DbType = DbType.AnsiString;
            sqlParams[1] = new SqlParameter();
            sqlParams[1].ParameterName = "@FirstName";
            sqlParams[1].Value = "A";
            sqlParams[1].DbType = DbType.AnsiString;
            sqlParams[2] = new SqlParameter();
            sqlParams[2].ParameterName = "@LastName";
            sqlParams[2].Value = "B";
            sqlParams[2].DbType = DbType.AnsiString;
            dbModule.SqlExec1(sSql, sqlParams);
        }

        [Test]
        public void TestOpenConnection_Merchant()
        {

            dbModule.CloseConnection();
            log.Debug("先關閉連線:" + sqlCon.State.ToString());

            AL_DBModule dbModuleA = new AL_DBModule();
            sqlCon = dbModuleA.OpenConnection_Merchant("Test");

            String expected = "Open";
            String actual = String.Empty;

            actual = sqlCon.State.ToString();
            log.Debug("開啟連線測試結果:" + actual);
            dbModuleA.CloseConnection();
            dbModule.OpenConnection_Merchant("Test");
            Assert.AreEqual(expected, actual);
        }

        [Test]
        public void TestOpenConnection()
        {

            dbModule.CloseConnection();
            log.Debug("先關閉連線:" + sqlCon.State.ToString());

            AL_DBModule dbModuleA = new AL_DBModule();
            sqlCon = dbModuleA.OpenConnection();

            String expected = "Open";
            String actual = String.Empty;
            actual = sqlCon.State.ToString();
            log.Debug("開啟連線測試結果:" + actual);
            //
            dbModuleA.CloseConnection();
            dbModule.OpenConnection_Merchant("Test");
            Assert.AreEqual(expected, actual);
        }

        [Test]
        public void TestOpenConnection_Dll()
        {

            dbModule.CloseConnection();
            log.Debug("先關閉連線:" + sqlCon.State.ToString());

            AL_DBModule dbModuleA = new AL_DBModule();
            sqlCon = dbModuleA.OpenConnection_Dll();

            String expected = "Open";
            String actual = String.Empty;
            actual = sqlCon.State.ToString();
            log.Debug("開啟連線測試結果:" + actual);
            //
            dbModuleA.CloseConnection();
            dbModule.OpenConnection_Merchant("Test");
            Assert.AreEqual(expected, actual);
        }


        [Test]
        public void TestSqlExec1()
        {
            string expected = "1";

            sSql = @"
update Customers 
set FirstName=@FirstName 
WHERE CustomerId=@CustomerId 
and LastName=@LastName
"
                ;
            SqlParameter[] sqlParamsExec1 = new SqlParameter[3];


            sqlParamsExec1[0] = new SqlParameter();
            sqlParamsExec1[0].ParameterName = "@CustomerId";
            sqlParamsExec1[0].Value = "1";
            sqlParamsExec1[0].DbType = DbType.AnsiString;
            sqlParamsExec1[1] = new SqlParameter();
            sqlParamsExec1[1].ParameterName = "@FirstName";
            sqlParamsExec1[1].Value = "TEST";
            sqlParamsExec1[1].DbType = DbType.AnsiString;
            sqlParamsExec1[2] = new SqlParameter();
            sqlParamsExec1[2].ParameterName = "@LastName";
            sqlParamsExec1[2].Value = "B";
            sqlParamsExec1[2].DbType = DbType.AnsiString;

           // log.Debug("cmd=" + sSql + "  sp1=" + sqlParamsExec1[0].Value + "  sq2=" + sqlParamsExec1[1].Value);
            string actual = dbModule.SqlExec1(sSql, sqlParamsExec1);
            log.Debug("SqlExec1測試結果:" + actual);
            Assert.AreEqual(expected, actual);



        }


        [Test]
        public void TestSqlExec()
        {
            string expected = "1";

            sSql = @"
update Customers 
set FirstName=@FirstName 
WHERE CustomerId=@CustomerId 
and LastName=@LastName
"
                ;
            SqlParameter[] sqlParamsExec1 = new SqlParameter[3];


            sqlParamsExec1[0] = new SqlParameter();
            sqlParamsExec1[0].ParameterName = "@CustomerId";
            sqlParamsExec1[0].Value = "1";
            sqlParamsExec1[0].DbType = DbType.AnsiString;
            sqlParamsExec1[1] = new SqlParameter();
            sqlParamsExec1[1].ParameterName = "@FirstName";
            sqlParamsExec1[1].Value = "TEST";
            sqlParamsExec1[1].DbType = DbType.AnsiString;
            sqlParamsExec1[2] = new SqlParameter();
            sqlParamsExec1[2].ParameterName = "@LastName";
            sqlParamsExec1[2].Value = "B";
            sqlParamsExec1[2].DbType = DbType.AnsiString;
          //  log.Debug("cmd=" + sSql + "  sp1=" + sqlParamsExec1[0].Value + "  sq2=" + sqlParamsExec1[1].Value);
            string actual = dbModule.SqlExec(sSql, sqlParamsExec1);

            log.Debug("SqlExec測試結果:" + actual);
            Assert.AreEqual(expected, actual);



        }

        [Test]
        public void TestSqlExec1HasTimeout()
        {
            string expected = "1";
            int timeout = 3;
            sSql = @"
update Customers 
set FirstName=@FirstName 
WHERE CustomerId=@CustomerId 
and LastName=@LastName
"
                ;
            SqlParameter[] sqlParamsExec1 = new SqlParameter[3];


            sqlParamsExec1[0] = new SqlParameter();
            sqlParamsExec1[0].ParameterName = "@CustomerId";
            sqlParamsExec1[0].Value = "1";
            sqlParamsExec1[0].DbType = DbType.AnsiString;
            sqlParamsExec1[1] = new SqlParameter();
            sqlParamsExec1[1].ParameterName = "@FirstName";
            sqlParamsExec1[1].Value = "TEST";
            sqlParamsExec1[1].DbType = DbType.AnsiString;
            sqlParamsExec1[2] = new SqlParameter();
            sqlParamsExec1[2].ParameterName = "@LastName";
            sqlParamsExec1[2].Value = "B";
            sqlParamsExec1[2].DbType = DbType.AnsiString;

         //   log.Debug("cmd=" + sSql + "  sp1=" + sqlParamsExec1[0].Value + "  sq2=" + sqlParamsExec1[1].Value);
            string actual = dbModule.SqlExec1(sSql, sqlParamsExec1,timeout);
            log.Debug("SqlExec1HasTimeout測試結果:" + actual);
            Assert.AreEqual(expected, actual);



        }

        [Test]
        public void TestSqlExecHasTimeout()
        {
            string expected = "1";
            int timeout = 3;
            sSql = @"
update Customers 
set FirstName=@FirstName 
WHERE CustomerId=@CustomerId 
and LastName=@LastName
"
                ;
            SqlParameter[] sqlParamsExec1 = new SqlParameter[3];


            sqlParamsExec1[0] = new SqlParameter();
            sqlParamsExec1[0].ParameterName = "@CustomerId";
            sqlParamsExec1[0].Value = "1";
            sqlParamsExec1[0].DbType = DbType.AnsiString;
            sqlParamsExec1[1] = new SqlParameter();
            sqlParamsExec1[1].ParameterName = "@FirstName";
            sqlParamsExec1[1].Value = "TEST";
            sqlParamsExec1[1].DbType = DbType.AnsiString;
            sqlParamsExec1[2] = new SqlParameter();
            sqlParamsExec1[2].ParameterName = "@LastName";
            sqlParamsExec1[2].Value = "B";
            sqlParamsExec1[2].DbType = DbType.AnsiString;
            //log.Debug("cmd=" + sSql + "  sp1=" + sqlParamsExec1[0].Value + "  sq2=" + sqlParamsExec1[1].Value);
            string actual = dbModule.SqlExec(sSql, sqlParamsExec1,timeout);

            log.Debug("SqlExecHasTimeout測試結果:" + actual);
            Assert.AreEqual(expected, actual);



        }

        [Test]
        public void TestExecuteSQL()
        { 
bool expected = true;
int count = 0;
sSql = @"
select * from Customers 
WHERE CustomerId='1' 
"
    ;

    using (SqlDataReader dr = dbModule.ExecuteSQL(sSql))
    {
        while (dr.Read())
        {
            count++;
        }
    }
    log.Debug("CustomerId=1的資料數: "+count);
         bool actual = (count > 0) ? true : false;
         Assert.AreEqual(expected, actual);
        }

        [Test]
        public void TestExecuteSQLHasParams()
        {
            bool expected = true;
            int count = 0;
          
            sSql = @"
select * from Customers 
WHERE CustomerId=@CustomerId 
"
                ;
            SqlParameter[] TestParams = new SqlParameter[1];
            TestParams[0] = new SqlParameter();
            TestParams[0].ParameterName = "@CustomerId";
            TestParams[0].Value = "1";
            TestParams[0].DbType = DbType.AnsiString;
            using (SqlDataReader dr = dbModule.ExecuteSQL(sSql, TestParams))
            {
                while (dr.Read())
                {
                    count++;
                }
            }

            bool actual = (count > 0) ? true : false;
            log.Debug("ExecuteSQLHasParams" + actual);
            Assert.AreEqual(expected, actual);
        }

        [Test]
        public void TestExecuteSQLHasTimeout()
        {
            bool expected = true;
            int count = 0;
            int timeout = 3;
            sSql = @"
select * from Customers 
WHERE CustomerId='1' 
"
                ;
            
                using (SqlDataReader dr = dbModule.ExecuteSQL(sSql, timeout))
                {
                    while (dr.Read())
                    {
                        count++;
                    }
                }
               
            bool actual = (count > 0) ? true : false;
            log.Debug("ExecuteSQLHasTimeout:" + actual);
            Assert.AreEqual(expected, actual);
        }

        [Test]
        public void TestExecuteNonQuery()
        {
           bool expected = true;

            sSql = @"
update Customers 
set FirstName='Test'  
WHERE CustomerId='1'  

"
;
     
           bool actual = dbModule.ExecuteNonQuery(sSql);
           log.Debug("ExecuteNonQuery測試結果:" + actual);
            Assert.AreEqual(expected, actual);



        }

        [Test]
        public void TestExecuteNonQueryHasTimeout()
        {
            string expected = "1";
            int timeOut = 3;
            sSql = @"
update Customers 
set FirstName='Test'  
WHERE CustomerId='1'  

"
;

            string actual = dbModule.ExecuteNonQuery(sSql,timeOut);
            log.Debug("TestExecuteNonQueryHasTimeout測試結果:" + actual);
            Assert.AreEqual(expected, actual);
        }

        [Test]
        public void TestGetDataSet()
        {
            string expected = "A";//FirstName

            sSql = @"
select * from Customers 
WHERE CustomerId='1'

"
;
           System.Data.DataSet ds = new DataSet();


            ds=dbModule.GetDataSet(sSql);
           

            string actual=  ds.Tables[0].Rows[0].ItemArray[1].ToString();

            log.Debug(" TestGetDataSet測試結果:" + actual);
            Assert.AreEqual(expected, actual);



        }

        [Test]
        public void TestGetDataSetHasTableName()
        {
            string expected = "A";//FirstName

            sSql = @"
select * from Customers 
WHERE CustomerId='1'   

"
;
            System.Data.DataSet ds = new DataSet();
            ds = dbModule.GetDataSet(sSql, "Customers");


            string actual = ds.Tables[0].Rows[0].ItemArray[1].ToString();

            log.Debug("GetDataSetHasTableName:" + actual);
            Assert.AreEqual(expected, actual);



        }

        [Test]
        public void TestGetDataSetHasTimeout()
        {
            string expected = "A";//FirstName
            int timeout = 3;
            sSql = @"
select * from Customers 
WHERE CustomerId='1'   

"
;
            System.Data.DataSet ds = new DataSet();
            ds = dbModule.GetDataSet(sSql,timeout);


            string actual = ds.Tables[0].Rows[0].ItemArray[1].ToString();

            log.Debug("TestGetDataSetHasTimeout 測試結果:" + actual);
            Assert.AreEqual(expected, actual);



        }

        [Test]
        public void TestParaGetDataSet()
        {
            string expected = "A";//FirstName

            sSql = @"
select * from Customers 
WHERE CustomerId= @CustomerId   

"
;
            SqlParameter[] TestParams = new SqlParameter[1];
            TestParams[0] = new SqlParameter();
            TestParams[0].ParameterName = "@CustomerId";
            TestParams[0].Value = "1";
            TestParams[0].DbType = DbType.AnsiString;

            System.Data.DataSet ds = new DataSet();
            ds = dbModule.ParaGetDataSet(sSql,TestParams);


            string actual = ds.Tables[0].Rows[0].ItemArray[1].ToString();

            log.Debug("TestParaGetDataSet測試結果:" + actual);
            Assert.AreEqual(expected, actual);

        }

        [Test]
        public void TestParaGetDataSet1()
        {
            string expected = "A";//FirstName

            sSql = @"
select * from Customers 
WHERE CustomerId= @CustomerId   

"
;
            SqlParameter[] TestParams = new SqlParameter[1];
            TestParams[0] = new SqlParameter();
            TestParams[0].ParameterName = "@CustomerId";
            TestParams[0].Value = "1";
            TestParams[0].DbType = DbType.AnsiString;

            System.Data.DataSet ds = new DataSet();
            ds = dbModule.ParaGetDataSet1(sSql, TestParams);


            string actual = ds.Tables[0].Rows[0].ItemArray[1].ToString();

            log.Debug("TestParaGetDataSet1測試結果:" + actual);
            Assert.AreEqual(expected, actual);

        }

        [Test]
        public void TestGetDataTable()
        {
            string expected = "A";//FirstName

            sSql = @"
select * from Customers 
WHERE CustomerId='1'   

"
;
           DataTable dt = new DataTable();
            dt = dbModule.GetDataTable(sSql);


            string actual = (dt == null || dt.Rows.Count == 0) ? "" : dt.Rows[0].ItemArray[1].ToString();

            log.Debug("ParaGetDataTable測試結果:" + actual);
            Assert.AreEqual(expected, actual);



        }

        [Test]
        public void TestParaGetDataTable1()
        {
            string expected = "A";//FirstName

            sSql = @"
select * from Customers 
WHERE CustomerId= @CustomerId   

"
;
            SqlParameter[] TestParams = new SqlParameter[1];
            TestParams[0] = new SqlParameter();
            TestParams[0].ParameterName = "@CustomerId";
            TestParams[0].Value = "1";
            TestParams[0].DbType = DbType.AnsiString;

            System.Data.DataTable dt =new DataTable();
            dt = dbModule.ParaGetDataTable1(sSql, TestParams);


            string actual = (dt == null || dt.Rows.Count == 0) ? "" : dt.Rows[0].ItemArray[1].ToString();

            log.Debug("ParaGetDataTable1測試結果:" + actual);
            Assert.AreEqual(expected, actual);

        }

        [Test]
        public void TestParaGetDataTable()
        {
            string expected = "A";//FirstName

            sSql = @"
select * from Customers 
WHERE CustomerId= @CustomerId   

"
;
            SqlParameter[] TestParams = new SqlParameter[1];
            TestParams[0] = new SqlParameter();
            TestParams[0].ParameterName = "@CustomerId";
            TestParams[0].Value = "1";
            TestParams[0].DbType = DbType.AnsiString;

            System.Data.DataTable dt = new DataTable();
            dt = dbModule.ParaGetDataTable(sSql, TestParams);


            string actual = (dt == null || dt.Rows.Count == 0) ? "" : dt.Rows[0].ItemArray[1].ToString();

            log.Debug("ParaGetDataTable測試結果:" + actual);
            Assert.AreEqual(expected, actual);

        }

        [Test]
        public void TestSqlExecuteScalar()
        { 
         string expected = "1";//FirstName

         sSql = @"
select count(1) from Customers 
WHERE CustomerId='1'
        ";

       
        string actual  = dbModule.SqlExecuteScalar(sSql).ToString();

        log.Debug("SqlExecuteScalar測試結果:" + actual);
         Assert.AreEqual(expected, actual);


        }
        [Test]
        public void TestSqlExecuteScalarSqlExecuteScalarHasParams()
        {
            string expected = "1";//FirstName

            sSql = @"
select count(1) from Customers 
WHERE CustomerId=@CustomerId
        ";
            SqlParameter[] sp = new SqlParameter[1];
            sp[0] = new SqlParameter();
            sp[0].ParameterName = "@CustomerId";
            sp[0].Value = "1";
            sp[0].DbType = DbType.AnsiString;
            string actual = dbModule.SqlExecuteScalarHasParams(sSql, sp).ToString();

            log.Debug("SqlExecuteScalar測試結果:" + actual);
            Assert.AreEqual(expected, actual);


        }



        [TearDown]
        public void finish()
        {

            sSql = @"
delete Customers 
where CustomerId='1'
"
                ;

            dbModule.ExecuteNonQuery(sSql);

            dbModule.CloseConnection();
            log.Debug("關閉連線" + sqlCon.State.ToString());
        }
















    }
}
