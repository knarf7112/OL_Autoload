﻿using System;
//
using Common.Logging;
using NUnit.Framework;
using HiLo.Data;
using HiLo.Domain.Entities;
using System.Collections.Generic;

namespace OL_Autoload_TestProject
{
    [TestFixture]
    public class UT_AL_HiLoDAO
    {
        private static readonly ILog log = LogManager.GetLogger(typeof(UT_AL_HiLoDAO));
        private HiLoDAO hiLoDAO;
        private HiLoDO expected;
        private Dictionary<string, HiLoDO> hiLoDic;
        private IList<HiLoDO> actualList;
        [SetUp]
        public void Init()
        {
            string tableName = "HiLo";
            this.hiLoDAO = new HiLoDAO(tableName);
            hiLoDic = new Dictionary<string, HiLoDO>();
            this.expected = new HiLoDO()
            {
                KeyName = "Test",
                NextHi = 1,
                MaxVal = 3000
            };
            this.hiLoDic.Add(this.expected.KeyName, this.expected);
        }

        [Test]
        public void Test_Insert()
        {
            try
            {
                this.hiLoDAO.Insert(this.hiLoDic[this.expected.KeyName]);
                HiLoDO actual = this.hiLoDAO.FindByPk(this.hiLoDic[this.expected.KeyName].KeyName);
                Assert.AreEqual(expected.ToString(), actual.ToString());
            }
            catch (Exception ex)
            {
                log.Error("Insert Failed:" + ex.Source);
            }
        }
        [Test]
        public void Test_Update()
        {
            this.Test_Insert();
            this.expected.NextHi = 2;
            HiLoDO actual = null;
            try
            {
                this.hiLoDAO.Update(expected);
                actual = this.hiLoDAO.FindByPk(expected.KeyName);
            }
            catch (Exception ex)
            {
                log.Error("Update Failed:" + ex.StackTrace);
            }
            Assert.AreEqual(this.expected.ToString(), actual.ToString());
        }
        [Test]
        public void Test_Delete()
        {
            try
            {
                this.Test_Insert();
                this.hiLoDAO.Delete(this.expected.KeyName);
            }
            catch (Exception ex)
            {
                log.Error("Delete Failed:" + ex.StackTrace);
            }
            Assert.IsFalse(this.hiLoDAO.Exist(this.expected.KeyName));
        }
        [Test]
        public void Test_FindByPk()
        {
            HiLoDO actual = null;
            try
            {
                this.Test_Insert();
                actual = this.hiLoDAO.FindByPk(this.expected.KeyName);
                //actual.MaxVal = 2;
            }
            catch (Exception ex)
            {
                log.Error("FindByPK Failed:" + ex.StackTrace);
            }
            Assert.AreEqual(this.expected.ToString(), actual.ToString());

        }
        [Test]
        public void Test_FindAll()
        {
            try
            {
                this.Test_Insert();
                HiLoDO expected1 = new HiLoDO()
                {
                    KeyName = "Test2",
                    NextHi = 2,
                    MaxVal = 6000
                };
                hiLoDic.Add(expected1.KeyName, expected1);
                this.hiLoDAO.Insert(expected1);
                expected1 = new HiLoDO()
                {
                    KeyName = "Test3",
                    NextHi = 3,
                    MaxVal = 7000
                };
                hiLoDic.Add(expected1.KeyName, expected1);
                this.hiLoDAO.Insert(expected1);
                actualList = this.hiLoDAO.FindAll();
            }
            catch (Exception ex)
            {
                log.Error("FindALL Failed:" + ex.StackTrace);
            }
            foreach (string keyName in hiLoDic.Keys)
            {
                foreach (HiLoDO obj in actualList)
                {
                    if (obj.KeyName == keyName)
                    {
                        log.Debug(">>Expected " + keyName + ": \n" + hiLoDic[keyName].ToString());
                        log.Debug(">>Actual " + obj.KeyName + ": \n" + obj.ToString());
                        Assert.AreEqual(hiLoDic[keyName].ToString(), obj.ToString());
                        break;
                    }
                }
            }
        }
        [Test]
        public void Test_Exist()
        {
            bool actual = false;
            try
            {
                this.Test_Insert();
                actual = this.hiLoDAO.Exist(this.expected.KeyName);
            }
            catch (Exception ex)
            {
                log.Error("Exist Failed:" + ex.StackTrace);
            }
            Assert.IsTrue(actual);
        }
        [TearDown]
        public void TearDown()
        {
            foreach (string actual in this.hiLoDic.Keys)
            {
                if (this.hiLoDAO.Exist(actual))
                {
                    this.hiLoDAO.Delete(actual);
                }
            }
        }
    }
}
