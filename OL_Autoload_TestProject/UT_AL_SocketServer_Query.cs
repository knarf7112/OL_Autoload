﻿using System;
using System.Linq;
//using Microsoft.VisualStudio.TestTools.UnitTesting;
//
using Common.Logging;
using NUnit.Framework;
//
using Kms.PosClient;
namespace OL_Autoload_TestProject
{
    [TestFixture]
    public class UT_AL_SocketServer_Query
    {
        private static readonly ILog log = LogManager.GetLogger(typeof(UT_AL_SocketServer_Query));
        
        #region Properties
        /// <summary>
        /// 00+6bytes
        /// </summary>
        public string store { get; set; }
        /// <summary>
        /// 1 byte
        /// </summary>
        public string pos { get; set; }
        /// <summary>
        /// 16 bytes
        /// </summary>
        public string cardId { get; set; }
        /// <summary>
        /// 20150112180000
        /// </summary>
        public string ALdate { get; set; }
        /// <summary>
        /// 8 bytes
        /// </summary>
        public string AMT { get; set; }
        /// <summary>
        /// return code
        /// </summary>
        public string RC { get; set; }
        /// <summary>
        ///  加值序號
        /// </summary>
        public string SN { get; set; }
        /// <summary>
        /// 14bytes
        /// </summary>
        public string TID { get; set; }
        /// <summary>
        /// 處理時間
        /// </summary>
        public string ptime { get; set; }
        /// <summary>
        /// 處理Client端的加解密物件
        /// </summary>
        public SessionHandler sessionhandler { get; set; }

        public SocketClient.Domain.SocketClient socketClient { get; set; }
        #endregion

        [SetUp]
        public void Init()
        {
            //SessionHandler的參數設定(固定RanA和RanB)
            SessionHandler sessionhandler = new SessionHandler();
            string ranA = "1234567890ABCDEF1234567890ABCDEF";
            string ranB = "E6EA143CCB47EAB569C60BE26AAE12D5";
            sessionhandler.TId = "8604230F629F2980";//86+POS的Reader ID(7碼Binary)
            sessionhandler.RndA = ranA;
            sessionhandler.FixRndB = true;//固定RandB
            sessionhandler.RndB = ranB;

            //測試時可修改的電文資料(配合DB內有的)
            this.pos = "2";
            this.store = "00852067";
            this.ALdate = "20150112185959";
            this.AMT = "00000500";
            this.cardId = "0417149984000007";
            this.TID = "04230F629F2980";

            // run auth01
            if (Auth01())
            {
                // Run auth02
                if (Auth02())
                {
                    //auto01 and auth02 pass
                }
                else
                {
                    log.Error("===> Auth02 failed!");
                }
            }
            else
            {
                log.Error("===> Auth01 failed!");
            }
        }
        /// <summary>
        /// 驗證Auth01流程
        /// </summary>
        /// <returns></returns>
        private bool Auth01()
        {
            string ip = "127.0.0.1";
            int port = 8105;
            socketClient = new SocketClient.Domain.SocketClient(ip, port);

            //----------------------------------------------
            //****Auth01*****第一次驗證**********
            //1.產生SessionHandler,用來處理Auth01與Auth02和使用SessionKEY加解密
            //2.先輸入固定RandA與RandB

            //auth01電文header
            string auth01Str = "01020103110000000186000000018604230F629F2980    ZOO001123456781001100000003200000032            ";
            //auth01電文data(hex)
            string auth01DataHex = "303130303030303030308604230F629F29802020202020202020202020202020";
            string auth01Hex = sessionhandler.HexConverter.Str2Hex(auth01Str) + auth01DataHex;
            byte[] auth01Bytes = sessionhandler.HexConverter.Hex2Bytes(auth01Hex);

            byte[] recieve = null;

            if (socketClient.ConnectToServer())
            {
                recieve = socketClient.SendAndReceive(auth01Bytes);
                socketClient.CloseConnection();
            }
            //傳回來的RandomA'(加密過)
            ArraySegment<byte> randAEnc = new ArraySegment<byte>(recieve, 96 + 2, 16);//去掉header + data無用的header(2bytes) => 取32(binary)的randA'
            string randAEncHex = sessionhandler.HexConverter.Bytes2Hex(randAEnc.ToArray());

            string randAEncryptoHex = "4A92D4FF3C9F568B06D14B6FD87831F6";//K.M.S Data=0x4A,0x92,.....
            try
            {
                //RandomA'解密
                byte[] randAdecrypto = sessionhandler.Auth1Response(sessionhandler.HexConverter.Hex2Bytes(randAEncHex));
                //轉回hex的RandA
                string ranA2 = sessionhandler.HexConverter.Bytes2Hex(randAdecrypto);
                //Assert.AreEqual(ranA2, sessionhandler.RndA);
                if (sessionhandler.RndA.Equals(ranA2))
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception ex)
            {
                log.Error("RandA' Validate Failed: " + ex.Source);
                return false;
            }
        }
        private bool Auth02()
        {
            string ip = "127.0.0.1";
            int port = 8105;
            socketClient = new SocketClient.Domain.SocketClient(ip, port);
            //
            byte[] auth2RequestData = sessionhandler.Auth2Request();//(96bytes)
            //auth02電文內Request = RanA(16binary) + RanB(16binary) + TerminalID(8binary) + Padding(8binary)
            string auth2str = sessionhandler.HexConverter.Bytes2Hex(auth2RequestData);//2C6362CFC1AB965ED3CB4E19186A917CB0416CA8F5E6A0D136E0D1D2F9E4CAAEDB882103E8EA26C1FCCE014CBDD4FEC2
            //auth02電文Header
            string auth2RequestStr = "01020103210000000186000000018604230F629F2980    ZOO001123456781001100000006400000064            ";
            string auth2RequestHex = sessionhandler.HexConverter.Str2Hex(auth2RequestStr);
            //auth02電文data
            string auth2RequestDataHex = "3031" + sessionhandler.HexConverter.Bytes2Hex(auth2RequestData) + "2020202020202020202020202020";//header(2bytes) + request(48bytes) + padding(14bytes)

            byte[] auth2RequestBytes = sessionhandler.HexConverter.Hex2Bytes(auth2RequestHex + auth2RequestDataHex);
            byte[] auth2Result = null;
            if (socketClient.ConnectToServer())
            {
                auth2Result = socketClient.SendAndReceive(auth2RequestBytes);
                socketClient.CloseConnection();
            };

            ArraySegment<byte> requestRandB = new ArraySegment<byte>(auth2Result, 96 + 2, 16);//Data共64byte取RanB部分16bytes
            string randBEncryptoHex = sessionhandler.HexConverter.Bytes2Hex(requestRandB.ToArray());//加密的RanB
            try
            {
                byte[] randBbytes = sessionhandler.Auth2Response(requestRandB.ToArray());//RandB Decrypto => RanB(byte[])有移位過
                string randBHex = sessionhandler.HexConverter.Bytes2Hex(randBbytes);
                string randB = randBHex.Substring(0, 4) + randBHex.Substring(4, randBHex.Length - 4);//把RanB'[左移的2個字元]轉回原始RanB狀態
                if (sessionhandler.RndB.Equals(randB))
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception ex)
            {
                log.Error("RandA' Validate Failed: " + ex.Source);
                return false;
            }
        }

        /// <summary>
        /// Service 開了才能測試
        /// </summary>
        [Test]
        public void Test_Query()
        {
            //****Query*****發送查詢電文**********
            //SocketClient.Domain.SocketClient
            //string queryRequestHeaderStr = "01030105310000000186000000018604230F629F2980    ZOO001123456781001100000008000000080            ";
            string queryRequestHeaderStr = "010301053100000001860000000186" + this.TID + "    SET001" + this.store + "100" + this.pos + "100000008000000080            ";
            string queryRequestHeaderHex = sessionhandler.HexConverter.Str2Hex(queryRequestHeaderStr);
            byte[] queryRequestHeaderBytes = sessionhandler.HexConverter.Hex2Bytes(queryRequestHeaderHex);
            //string queryRequestDataEnc = "SAM           ID" + "FirmwareVerssion" + "12345678" + "0417149984000007" + "20151231" + "20151231";//"0417149984000007"Credit Card
            string queryRequestDataEnc = "SAM           ID" + "FirmwareVerssion" + "12345678" + this.cardId + "20151231" + "20151231";//"0417149984000007"Credit Card
            string queryRequestDataEncHex = sessionhandler.HexConverter.Str2Hex(queryRequestDataEnc);
            //byte[] queryRequestDataEncBytes = hexworker.Hex2Bytes(queryRequestDataEncHex);
            string queryRequestDataHex = "3031" + queryRequestDataEncHex + "202020202020";
            byte[] queryRequestDataEncBytesBefor = sessionhandler.HexConverter.Hex2Bytes(queryRequestDataHex);//Request資料加密前
            byte[] queryRequestDataEncBytesAfter = sessionhandler.Encrypt(queryRequestDataEncBytesBefor);//Request資料加密後

            byte[] queryAll = new byte[queryRequestHeaderBytes.Length + queryRequestDataEncBytesAfter.Length];
            Buffer.BlockCopy(queryRequestHeaderBytes, 0, queryAll, 0, queryRequestHeaderBytes.Length);
            Buffer.BlockCopy(queryRequestDataEncBytesAfter, 0, queryAll, queryRequestHeaderBytes.Length, queryRequestDataEncBytesAfter.Length);
            this.socketClient = new SocketClient.Domain.SocketClient("10.27.68.155", 8104);//127.0.0.1", 8104);
            string resultHex = "";
            string resultStr = "";
            string resultHexAns = "";
            string resultAns = "";
            byte[] receiveBytes = null;
            if (this.socketClient.ConnectToServer())
            {

                receiveBytes = this.socketClient.SendAndReceive(queryAll);//送出加密資料後的電文並取得傳回電文結果
                this.socketClient.CloseConnection();
            }
            if (receiveBytes != null)
            {
                resultHex = sessionhandler.HexConverter.Bytes2Hex(receiveBytes);
                byte[] formCenter = new byte[80];
                this.RC = sessionhandler.HexConverter.Hex2Str(resultHex).Substring(10, 6);
                Array.Copy(receiveBytes, 96, formCenter, 0, formCenter.Length);
                byte[] ans = sessionhandler.Decrypt(formCenter);
                resultStr = sessionhandler.HexConverter.Hex2Str(resultHex);

                resultHexAns = sessionhandler.HexConverter.Bytes2Hex(ans);
                resultAns = sessionhandler.HexConverter.Hex2Str(resultHexAns);
                this.SN = resultAns.Substring(2, 8);
            }
        }
    }
}
