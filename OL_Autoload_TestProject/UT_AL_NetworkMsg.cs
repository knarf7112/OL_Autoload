﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using ALCommon;
using System.Data.SqlClient;
using OL_Autoload_Lib;
using System.Data;

namespace OL_Autoload_TestProject
{
    [TestClass]
    public class UT_AL_NetworkMsg
    {
        AL_DBModule dbModule;
        SqlConnection sqlCon;
        String sSql = String.Empty;
        SqlParameter[] sqlParams;

        [TestMethod]
        public void Test_NetworkStatu_Check()
        {
            bool expected = true;
            bool actual = false;
            //前置作業
            sSql = @"insert into OL_AL_NETWORK_MSG_D
(LOG_DATE, BANK_CODE, STAN, INFO_CODE, LOG_TIME)
VALUES
('20150205', '822', '999999', '071', '20150205121212');";
            dbModule = new AL_DBModule();
            sqlCon = dbModule.OpenConnection_Merchant("Test");
            dbModule.ExecuteSQL(sSql);
            dbModule.CloseConnection();

            //Test
            AutoloadRqt_NetMsg rqt = new AutoloadRqt_NetMsg();
            rqt.BANK_CODE="822";

            dbModule = new AL_DBModule();
            sqlCon = dbModule.OpenConnection_Merchant("Test");
            Network_Msg netMsg = new Network_Msg();
            actual = netMsg.NetworkStatu_Check(rqt.BANK_CODE, dbModule);
            Assert.AreEqual(expected, actual);
            dbModule.CloseConnection();

            //後續處理
            sSql = String.Empty;
            sSql = @"DELETE OL_AL_NETWORK_MSG_D
WHERE BANK_CODE='822' AND LOG_TIME='20150205121212';";
            dbModule = new AL_DBModule();
            sqlCon = dbModule.OpenConnection_Merchant("Test");
            dbModule.ExecuteSQL(sSql);
            dbModule.CloseConnection();
        }


        [TestMethod]
        public void Test_UptNetMsg()
        {
 
        }


    }
}
