﻿using System;
using System.Text;
using System.Collections.Generic;
using NUnit.Framework;
using Kms.PosClient;
using Common.Logging;
namespace OL_Autoload_TestProject
{
    /// <summary>
    /// UT_AL_SocketServer_AutoLoadTxlog 的摘要描述
    /// </summary>
    [TestFixture]
    public class UT_AL_SocketServer_AutoLoadTxlog
    {
        private static readonly ILog log;
        #region Properties

        /// <summary>
        /// 00+6bytes
        /// </summary>
        public string store { get; set; }
        /// <summary>
        /// 1 byte
        /// </summary>
        public string pos { get; set; }
        /// <summary>
        /// 16 bytes
        /// </summary>
        public string cardId { get; set; }
    

        /// <summary>
        /// 8 bytes
        /// </summary>
        public string AMT { get; set; }
        /// <summary>
        /// return code
        /// </summary>
        public string RC { get; set; }
        /// <summary>
        ///  加值序號
        /// </summary>
        public string SN { get; set; }
        /// <summary>
        /// 14bytes
        /// </summary>
        public string TID { get; set; }
        /// <summary>
        /// comm_Type033
        /// </summary>
        public string COMM_TYPE { get; set; }
        /// <summary>
        /// 承認編號
        /// </summary>
        public string Ans { get; set; }
        /// <summary>
        /// pos交易序號
        /// </summary>
        public string PosNo { get; set; }



        #endregion


        /// <summary>
        /// 處理Client端的加解密物件
        /// </summary>
        public SessionHandler sessionhandler { get; set; }

        public SocketClient.Domain.SocketClient socketClient { get; set; }

        [SetUp]
        public void init()
        {
            SessionHandler sessionhandler = new SessionHandler();
            string ranA = "1234567890ABCDEF1234567890ABCDEF";
            string ranB = "E6EA143CCB47EAB569C60BE26AAE12D5";
            sessionhandler.TId = "8604230F629F2980";//86+POS的Reader ID(7碼Binary)
            sessionhandler.RndA = ranA;
            sessionhandler.FixRndB = true;//固定RandB
            sessionhandler.RndB = ranB;


            this.pos = "1";
            this.store = "00110013";           
            this.AMT = "00000500";
            this.cardId = "0417149982000008";
            this.TID = "04230F629F2980";
            Auth01();// run auth01
            Auth02();// run auth02

        }

        private void Auth01()
        {
            string ip = "127.0.0.1";
            int port = 8105;
            socketClient = new SocketClient.Domain.SocketClient(ip, port);
            byte[] recieve = null;
            if (socketClient.ConnectToServer())
            {
                //----------------------------------------------
                //****Auth01*****第一次驗證**********
                //1.產生SessionHandler,用來處理Auth01與Auth02和使用SessionKEY加解密
                //2.先輸入固定RandA與RandB

                //auth01電文header
                string auth01Str = "01020103110000000186000000018604230F629F2980    ZOO001123456781001100000003200000032            ";
                //auth01電文data(hex)
                string auth01DataHex = "303130303030303030308604230F629F29802020202020202020202020202020";
                string auth01Hex = sessionhandler.HexConverter.Str2Hex(auth01Str) + auth01DataHex;
                byte[] auth01Bytes = sessionhandler.HexConverter.Hex2Bytes(auth01Hex);
                recieve = socketClient.SendAndReceive(auth01Bytes);
                socketClient.CloseConnection();
            }
            //傳回來的RandomA'(加密過)
            ArraySegment<byte> randAEnc = new ArraySegment<byte>(recieve, 96 + 2, 16);//去掉header + data無用的header(2bytes) => 取32(binary)的randA'
            string randAEncHex = sessionhandler.HexConverter.Bytes2Hex(randAEnc.Array);

           // string randAEncryptoHex = "4A92D4FF3C9F568B06D14B6FD87831F6";//K.M.S Data=0x4A,0x92,.....
            try
            {
                //RandomA'解密
                byte[] randAdecrypto = sessionhandler.Auth1Response(sessionhandler.HexConverter.Hex2Bytes(randAEncHex));
                //轉回hex的RandA
                string ranA2 = sessionhandler.HexConverter.Bytes2Hex(randAdecrypto);
            }
            catch (Exception ex)
            {

            }
        }

        private void Auth02()
        {
            //****Auth02*****第二次驗證**********
            //sessionhandler.RndB = ranB;
        
            byte[] auth2RequestData = sessionhandler.Auth2Request();
            //
            string auth2str = sessionhandler.HexConverter.Bytes2Hex(auth2RequestData);//2C6362CFC1AB965ED3CB4E19186A917CB0416CA8F5E6A0D136E0D1D2F9E4CAAEDB882103E8EA26C1FCCE014CBDD4FEC2

            string auth2RequestStr = "01020103210000000186000000018604230F629F2980    ZOO001123456781001100000006400000064            ";
            string auth2RequestHex = sessionhandler.HexConverter.Str2Hex(auth2RequestStr);
            string auth2RequestDataHex = "3031" + sessionhandler.HexConverter.Bytes2Hex(auth2RequestData) + "2020202020202020202020202020";//header(2bytes) + request(48bytes) + padding(14bytes)

            byte[] auth2RequestBytes = sessionhandler.HexConverter.Hex2Bytes(auth2RequestHex + auth2RequestDataHex);
            SocketClient.Domain.SocketClient s2 = new SocketClient.Domain.SocketClient("127.0.0.1", 8105);
            if (s2.ConnectToServer())
            {

                byte[] auth2Result = s2.SendAndReceive(auth2RequestBytes);
                s2.CloseConnection();
                byte[] formCenter = new byte[sessionhandler.HexConverter.Hex2Bytes(auth2RequestDataHex).Length];
                var sss = sessionhandler.HexConverter.Bytes2Hex(auth2Result);
                Array.Copy(auth2Result, 96, formCenter, 0, formCenter.Length);
                ArraySegment<byte> requestRandB = new ArraySegment<byte>(formCenter, 2, 16);//Data共64byte取中間Request部分48byte
                byte[] randBbytes = sessionhandler.Auth2Response(requestRandB.Array);
                string randB = sessionhandler.HexConverter.Bytes2Hex(randBbytes);
            }




        }

        [Test]
        public void ALTxlog()
        {

            //****Txlog*****發送自動加值Txlog電文**********
            string txlogRequestHeader = "010401" + this.COMM_TYPE + "00000001860000000186" + this.TID + "    SET001" + this.store + "100" + this.pos + "100000035200000368            ";
            //string txlogRequestHeader = "01040103330000000186000000018604230F629F2980    SET001000000011001100000035200000368            ";
            //Txlog資料(288bytes)
            string txlogRequestData_txlog = "75" + "20150115135959" + "00000000" + this.cardId + this.AMT + "04230F629F2980001555000469570004424000065488000027170006469204230F629F29800000000000000000000015230000152320151231000000000000F093000ASET00100999407100318604230F00500001000000000000000000000000000000000000000000000000000000000000000000000A2";
            //string txlogRequestData_txlog = "75201501010101010000000004171499840000070000050004230F629F2980001555000469570004424000065488000027170006469204230F629F29800000000000000000000015230000152320151231000000000000F093000ASET00100999407100318604230F00500001000000000000000000000000000000000000000000000000000000000000000000000A2";
            string txlogRequestData_All = "01" + this.Ans + this.PosNo + txlogRequestData_txlog + "                                                ";//[header + Ans + PosNo](2+8+6) + txlog(288bytes) + 空白(48bytes)
            string txlogRequestHeaderHex = sessionhandler.HexConverter.Str2Hex(txlogRequestHeader);//Txlog的header部分(96bytes)
            string txlogRequestData_AllHex = sessionhandler.HexConverter.Str2Hex(txlogRequestData_All);//要加密的Request全部Data 長度:352 bytes
            byte[] txlogRequestData_AllBytes = sessionhandler.HexConverter.Hex2Bytes(txlogRequestData_AllHex);//轉byte[]
            byte[] txlogRequestData_txlogHeaderBytes = sessionhandler.HexConverter.Hex2Bytes(txlogRequestHeaderHex);//header的 hex=>byte[]

            //
            byte[] txlogRequestData_AllBytesEnc = sessionhandler.Encrypt(txlogRequestData_AllBytes);//加密後Txlog資料部分(352)
            byte[] SendData = new byte[txlogRequestData_txlogHeaderBytes.Length + txlogRequestData_AllBytesEnc.Length];
            Buffer.BlockCopy(txlogRequestData_txlogHeaderBytes, 0, SendData, 0, txlogRequestData_txlogHeaderBytes.Length);
            Buffer.BlockCopy(txlogRequestData_AllBytesEnc, 0, SendData, txlogRequestData_txlogHeaderBytes.Length, txlogRequestData_AllBytesEnc.Length);
            byte[] recieve=null;
            SocketClient.Domain.SocketClient s3 = new SocketClient.Domain.SocketClient("127.0.0.1", 8105);          
            if (s3.ConnectToServer())
            {
                recieve = s3.SendAndReceive(SendData);
                s3.CloseConnection();
            }
            string receiveTxlogHex = sessionhandler.HexConverter.Bytes2Hex(recieve);
            this.RC = sessionhandler.HexConverter.Hex2Str(receiveTxlogHex).Substring(10, 6);
            byte[] decBefor = new byte[144];

            Array.Copy(recieve, 96, decBefor, 0, decBefor.Length);
            byte[] decAfter = sessionhandler.Decrypt(decBefor);
            string TxlogDataHex = sessionhandler.HexConverter.Bytes2Hex(decAfter);
            string TxlogDataStr = sessionhandler.HexConverter.Hex2Str(TxlogDataHex);
            string receiveTxlogStr = sessionhandler.HexConverter.Hex2Str(receiveTxlogHex);
                 
        }


    }

    
}
