﻿using ALCommon;
using Common.Logging;
using NUnit.Framework;
using OL_Autoload_Lib;
using OL_Autoload_Lib.Controller.TOL;
using System;
using System.Text;


namespace OL_Autoload_TestProject
{
    [TestFixture]
    public class UT_AL_TOLModual
    {
        
        private static readonly ILog log = LogManager.GetLogger(typeof(UT_AL_TOLModual));
        AL_DBModule obDB;
        CTxlog ct;
        Txlog_Parsing tp;
        ALTxlog_Domain ALTxlogDomain;
Txlog_Module chT;

        [SetUp]
        public void init()
        {
            tp = new Txlog_Parsing();
            obDB = new AL_DBModule();
            chT = new Txlog_Module();
            obDB.OpenConnection_Merchant("Test");
            ALTxlogDomain = new ALTxlog_Domain()
            {
                M_KIND = "21",
                MERCHANT_NO = "22555003",
                REG_ID = "01",
                POS_SEQNO = "888888",
                READER_ID = "888888",
                STORE_NO = "000251",
                TRANS_TYPE = "74",
                TXLOG = "74201412301908470000000073111400240070060000020004241F6A9F2980000005000000420000001500009100000000270000880604241F6A9F298000000000000000000000000100000200000000006520C0FBSET00100113584100118604241F6A9F2980    00000294000000000000000000000000000000000000000000000000000000000000000000000A2",
                TXLOG_RC = null,
                MERC_FLG = "SET"
                
            };

            ct = new CTxlog()
           {
               M_KIND = "21",
               MERCHANT_NO = "88888888",
               SETTLE_DATE = "20130325",
               ACT_DATE = "20130325",
               STORE_NO = "000251",
               REG_ID = "01",
               SEQ_NO = "888888",
               TRANS_TYPE = "74",
               TRANS_DATE_TXLOG = "20130325165747",
               RETURN_CODE = "00000000",
               ICC_NO = "7115120019000001",
               TRANS_AMT = "00000400",
               TSAM_OSN = "99999999999999",
               TSAM_TSN = "999999",
               READER_ID = "888888",
               ICC_LSN = "99999999",
               ICC_LA = "99999999",
               ICC_DSN = "99999999",
               ICC_DA = "99999999",
               LSAM_ID = "99999999999999",
               MAC = "",
               TAR_FILE_NAME = "OL_ACICICCG091201",
               DATETIME = "",
               IDOLLAR_AMT = "",
               ICC_TSN = "",
               ICC_BL = "",
               LSAM_DA = "",
               LSAM_DSN = "",
               LSAM_CA = "",
               LSAM_CSN = "",
               LSAM_DV = "",
               PSAM_ID = "",
               PSAM_CA = "",
               PSAM_CSN = "",
               PSAM_NRS = "",
               PSAM_SBN = "",
               PSAM_SV = "",
               RID = "",
               ZIP_FILE_NAME = "",
               CM_TAG = "",
               CM_VAL = "",
               EDC_RFU = "",
               FILE_NAME = "",
               VALID_DATE = "",
               TXLOG = "",
               TXLOG_RC = "000000",
               HM_VAL = "",
               HSM_CODE = "",
               HSM_YN = "",
               INIT_AUTH_CODE = "",
               MAX_APDU_SPEND_TIME = "",
               MSRW_Terminal_SN = "",
               NECM_ID = "",
               PLSAM_TEST = "",
               POS_SEQNO = "",
               SAM_SN_CHECK = "",
               SEND_TAG = "",
               SN = "",
               TM_CAL = "",
               TM_RCODE = "",
               TM_STATUS = "",
               TSAM_TEST = "",
               TX_SPEND_TIME = "",
               UT_VAL = "",
               UT3_VAL = ""
           };

        }



        [Test]
        public void TestGet_Txlog_Len()
        {
            int expect = 288;
            int actual = chT.AL_GetTxlogLen(ALTxlogDomain.MERCHANT_NO, ALTxlogDomain.M_KIND);         
            Assert.AreEqual(expect, actual);

        }

        [Test]
        public void TestChk_Txlog_Length()
        {
            bool expect = true;
            bool actual = chT.AL_ChkTxlogLen(ALTxlogDomain);
            Assert.AreEqual(expect, actual);
        }

        [Test]
        public void TestChk_Duplicate_Txlog()
        {
            chT.AL_Import(obDB, ct);                    
            bool expect = false;
            bool actual = chT.AL_ChkDuplicate(obDB,ct);
            
            log.Debug("Txlog是否重複: "+actual);
            Assert.AreEqual(expect, actual);
        
        }


        [Test]
        public void TestImport_Txlog()
        {
         
            bool expect = true;
            chT.AL_Import(obDB, ct);
            bool actual = !chT.AL_ChkDuplicate(obDB, ct);
         
            log.Debug("新增資料是否存在: " + actual);
            Assert.AreEqual(expect, actual);
           
        }

        [Test]
        public void TestParsingTxlog()
        {
               string exception="";
            
            CTxlog ctxlog=new CTxlog();
            if (!tp.AL_Parsing(ALTxlogDomain, ctxlog, out exception))
            {
                log.Debug("parsing Txlox fail"+exception);
            }
            bool expect = true;

            bool actual = ctxlog.TRANS_TYPE == "74" && ctxlog.RETURN_CODE == "00000000" && ctxlog.ICC_NO == "7311140024007006";
            Assert.AreEqual(expect, actual);

        }

      

        [Test]
        public void TestdoTxlog()
        {
        
            ALTxlogDomain.MERCHANT_NO = "";
            ALTxlogDomain.MERC_FLG = "SET";
            bool expect = true;
            bool actual = chT.do_TOL(ALTxlogDomain).TXLOG_RC=="000000";
           
            Assert.AreEqual(expect, actual);
        }

        [Test]
        public void TestDo_TOL_Flow()
        {
            CTxlog ctxlog = new CTxlog();
            string excep = "";
            chT.Do_TOL_Flow(obDB, ALTxlogDomain, ref ctxlog, out excep);
            
            string expect = "000000";//資料重複
            string actual = ctxlog.TXLOG_RC;
            log.Debug("return code= " + ctxlog.TXLOG_RC + " ,out excep= "+excep);
            Assert.AreEqual(expect, actual);
        }






        [TearDown]
        public void finish()
        {
            #region 刪除測試資料
            string deletesStr = "delete IM_OL_TXLOG_T where TXLOG_ID='0' and TRANS_TYPE = '74'";
            log.Debug("測試資料刪除結果: " + obDB.ExecuteNonQuery(deletesStr));
            #endregion

            obDB.CloseConnection();
            chT = null;
            obDB = null;
            ct = null;
            ALTxlogDomain = null;
        }






    }
}
