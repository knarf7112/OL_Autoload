﻿using ALCommon;
using Common.Logging;
using NUnit.Framework;
using OL_Autoload_Lib.Controller.ALOL.Ad_control;
using OL_Autoload_Lib.Controller.Common;
using System;


namespace OL_Autoload_TestProject
{
    [TestFixture]
    public class UT_AL_AdviseController
    {
        string ACTDate;
        AL_DBModule obDB;
        Ad_Controller adc;
        private static readonly ILog log = LogManager.GetLogger(typeof(UT_AL_AdviseController));
        string MerchNO;
        string BandCo;
        [SetUp]
        public void init()
        {
           MerchNO = "88888888";
          BandCo = "000";
            adc=new Ad_Controller();
            obDB = new AL_DBModule();
            obDB.OpenConnection_Merchant("Test");
            ACTDate = DateTime.Now.Date.ToString("yyyyMMdd");
        }



        [Test]
        public void TestgetBankCo()
        {
            MerchNO = "22555003";
            Get_BandCo getBankco = new Get_BandCo();
            string expect = "000";
            string actual = getBankco.AL_Get_BankCode(obDB, MerchNO);
            Assert.AreEqual(expect, actual);
        }

        [Test]
        public void TestgetBalLimit()
        {
     
   
            string AdCNT = "";
            string AdAMT = "";
            adc.AL_Get_AdviceLimit(obDB, MerchNO, BandCo, out AdCNT, out  AdAMT);
            bool expect = true;
            bool actual = (int.Parse(AdAMT) == 500 && int.Parse(AdCNT) == 3) ? true : false;
            Assert.AreEqual(expect, actual);
        }

        [Test]
        public void TestAdCount()
        {
            //卡號暫時先插入  後面再改
            string expect = "1";
            string actual = "";
            if (adc.AL_AddAdviceLog(obDB, MerchNO, "22555003", BandCo, "110035", "01", "0417149984000007", 500, "readerid","123456"))
            {

                actual = adc.AL_GetAdviceCount(obDB, MerchNO, BandCo, ACTDate, "0417149984000007");
                deleteDetailAD();
            }
            Assert.AreEqual(expect, actual);

        }
        [Test]
        public void TestadDetailInsert()
        {
            //卡號暫時先插入  後面再改
            bool expect = true;
            bool actual = false;
            if (adc.AL_AddAdviceLog(obDB, MerchNO,"BankMerch", BandCo, "110035", "01", "0417149984000007", 500,"readerID", "123456"))
            {
            
                actual = int.Parse(adc.AL_GetAdviceCount(obDB, MerchNO, BandCo, ACTDate,"0417149984000007"))>0;
                deleteDetailAD();
            }
            
            Assert.AreEqual(expect, actual);

        }

        [Test]
        public void TestCheckAdviceflow()
        {
            //卡號暫時先插入  後面再改
            bool expect = true;
            bool actual = adc.AL_CheckAdvice(obDB, MerchNO, BandCo, 500, "0417149984000007") == "0"
                && adc.AL_CheckAdvice(obDB, MerchNO, BandCo, 600, "0417149984000007") == "1";
            Assert.AreEqual(expect, actual);
        }

        private void deleteDetailAD()
        {
        string    deleStr = @"  delete OL_AL_ADVICE_DETAIL_D where MERCHANT_NO='88888888' and CARD_ID ='0417149984000007'";
       log.Debug("刪除測試資料結果"+ obDB.ExecuteNonQuery(deleStr));
        }
        [TearDown]
        public void finisf()
        {
            obDB.CloseConnection();
        
        }



    }
}
