﻿using ALCommon;
using OL_Autoload_Lib;
using NUnit.Framework;
using System;
using System.Data;
using Common.Logging;
using System.Data.SqlClient;
using OL_Autoload_Lib.Controller.Common.Record;
using OL_Autoload_Lib.Sql.Common;


namespace OL_Autoload_TestProject
{
    [TestFixture]
    public class UT_AL_LogModule
    {
        private static readonly ILog log = LogManager.GetLogger(typeof(UT_AL_LogModule));
        AL_LogModule logModual;
        AL_DBModule obDB;
        LogModual logM;


        [SetUp]
        public void init()
        {
            logM = new LogModual();
            logModual = new AL_LogModule();
            obDB = new AL_DBModule();
            obDB.OpenConnection_Merchant("Test");
        }



        [Test]
        public void Testlog2file()
        {
            log.Debug("=== log2file Test Start ===");
            string ErrorMsg="errorTest";
            bool expected=true; 
            bool actual=logModual.log2file(ErrorMsg);
            Assert.AreEqual(expected,actual);           
        }

        [Test]
        public void TestSaveTransLog()
        {
            log.Debug("=== SaveTransLog Test Start ===");

            AutoloadRqt _trans = new AutoloadRqt()
            {
                M_KIND = "21",
                READER_ID = "8604230F629F2980",
                REG_ID = "03",
                MERCHANT_NO = "22555003",
                RETURN_CODE = "000000",
                SN = "33333333",
                STORE_NO = "999407",
                TRANS_AMT = 1,
                TRANS_TYPE = CTransType.AUTOLOAD,

            };

            String _leftBal = "";
            DateTime dtrqt = DateTime.Now;
            DateTime dtrsp = DateTime.Now.AddMilliseconds(99);
            logM.SaveTransLog(_trans, obDB, _leftBal, dtrqt, dtrsp);


            string selectCmd = @"
select *  
from OL_ISET_TRANS_LOG_D 
where RESPONSE_TIME='99'
and SN='33333333'

" ;

            bool expected = true;
            bool actual = ((DataTable)obDB.GetDataTable(selectCmd)).Rows.Count > 0;
            log.Debug("insert log Result: " + actual);

            #region   刪除測試資料
            if (actual)
            {
                selectCmd = @"
delete OL_ISET_TRANS_LOG_D  
where RESPONSE_TIME='99'
and SN='33333333'
"
                    ;
                log.Debug("delete test data Result: " + obDB.ExecuteNonQuery(selectCmd));
            }
            #endregion

            Assert.AreEqual(expected, actual);



        }

        [Test]
        public void TestSaveTolLog()
        {
            SqlMod_SAVE_Txlog_Result sTxlog = new SqlMod_SAVE_Txlog_Result();
            log.Debug("=== SaveTolLog Test Start ===");
            CTxlog cTxlog = new CTxlog()
            {
                TXLOG_RC = "000000",
                STORE_NO = "999407",
                REG_ID = "99",
                READER_ID = "8604230F629F2980",
                M_KIND = "21",
                MERCHANT_NO = "22555003",
                SEQ_NO = "888888",
                TRANS_TYPE = "76",
                TRANS_AMT = "1",
                TAR_FILE_NAME = "OL_ACICICCG111401",
                TXLOG = "53201311142113030000000071171202150000450000000104230F629F2980001565000469670004425000065498000027170006469204230F629F2980000000000000000000001533000015330000100000000000000002FEB26FSET00100999407100318604230F629F2980    00000806000000000000000000000000000000000000000000000000000000000A2"

            };
            String sResponseTime = "99";
            String iSql = "";
            SqlParameter[] sqlParams = null;
            sTxlog.SaveTolLogResult(cTxlog, sResponseTime, out iSql, ref  sqlParams);
            bool actual = obDB.SqlExec1(iSql, sqlParams) == "1";
            bool expected = true;

            #region 確認並刪除測試資料
            if (actual)
            {
                iSql = @"
select *
from OL_ISET_TOL_LOG_D
where  REG_ID='99' 
  and SEQ_NO ='888888'
";
                actual = ((DataTable)obDB.GetDataTable(iSql)).Rows.Count > 0;
                log.Debug("insert data exist= " + actual);
            }
            if (actual)
            {
                iSql = @"
delete OL_ISET_TOL_LOG_D 
where  REG_ID='99' 
  and SEQ_NO ='888888'
";
                log.Debug("delete test data Result: " + obDB.ExecuteNonQuery(iSql));
            }
            #endregion

            Assert.AreEqual(expected, actual);

        }

         [Test]
        public void TestLogTxlogResult()
        {
            CTxlog ctxlog = new CTxlog()
            {
                TXLOG_RC = "000000",
                STORE_NO = "999407",
                REG_ID = "99",
                READER_ID = "8604230F629F2980",
                M_KIND = "21",
                MERCHANT_NO = "22555003",
                SEQ_NO = "888888",
                TRANS_TYPE = "76",
                TRANS_AMT = "1",
                TAR_FILE_NAME = "OL_ACICICCG111401",
                TXLOG = "53201311142113030000000071171202150000450000000104230F629F2980001565000469670004425000065498000027170006469204230F629F2980000000000000000000001533000015330000100000000000000002FEB26FSET00100999407100318604230F629F2980    00000806000000000000000000000000000000000000000000000000000000000A2"

            };
            bool expect = true;
            bool actual =logM.SaveTolLog(obDB, ctxlog,DateTime.Now,DateTime.Now.AddMilliseconds(99));

            String iSql = ""; 
             if (actual)
            {
                iSql = @"
select *
from OL_ISET_TOL_LOG_D
where  REG_ID='99' 
  and SEQ_NO ='888888'
";
                actual = ((DataTable)obDB.GetDataTable(iSql)).Rows.Count > 0;
                log.Debug("insert data exist= " + actual);
            }
            if (actual)
            {
                iSql = @"
delete OL_ISET_TOL_LOG_D 
where  REG_ID='99' 
  and SEQ_NO ='888888'
";
                log.Debug("delete test data Result: " + obDB.ExecuteNonQuery(iSql));
            }
             
             
             
             
             Assert.AreEqual(expect, actual);
        }







        [Test]
        public void TestSaveErrorLog()
        {
            log.Debug("=== SaveErrorLog Test Start ===");
            string errorMessage=DateTime.Now.ToString()+" : My Test ErrorMessage";
           

            logM.SaveErrorLog(obDB,errorMessage,"TEST");
            log.Debug("save error msg= "+errorMessage );
           
            string selectCmd = @"
select *  
from OL_ISET_ERR_LOG_D  
where MSG='"+errorMessage+@"'

"
                               ;

            bool expected = true;
            bool actual = ((DataTable)obDB.GetDataTable(selectCmd)).Rows.Count > 0;
            log.Debug("insert log Exist: " + actual);

            //刪除測試資料
            if (actual)
            {
                selectCmd = @"
delete OL_ISET_ERR_LOG_D  
where MSG='" + errorMessage + @"'
"
                    ;
                log.Debug("delete test data Result: " + obDB.ExecuteNonQuery(selectCmd));
            }
            Assert.AreEqual(expected, actual);


        }
        
        [TearDown]
        public void finish()
        {
            obDB.CloseConnection();
            log.Debug("Test End");
        }
    }
}
