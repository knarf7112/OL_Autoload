﻿using System;
using CardValidator;
using NUnit.Framework;
using Common.Logging;

namespace TestCardValidator
{
    [TestFixture]
    public class UT_AL_CardValidator
    {
        private static readonly ILog log = LogManager.GetLogger(typeof(UT_AL_CardValidator));
        string cardIdG1;
        string cardIdG2;
        bool resultG1;
        bool resultG2;
        bool result3;
        VertifyTool2 verifyTool2;


        [SetUp]
        public void init()
        {
            verifyTool2 = new VertifyTool2();
            //產生卡號
            cardIdG1 = verifyTool2.CreateId("710106017300809");//餵給他除了chk以外其他15碼
            cardIdG2 = verifyTool2.CreateId("731114003" + "020339");//餵給他除了chk以外其他15碼       
        }
        [Test]
        public void TestCreateiCashId()
        {
           log.Debug(cardIdG1);
            Assert.AreEqual("7101060173008097",cardIdG1);
        }

        [Test]
        public void TestCreateiCash2Id()
        {
            log.Debug(cardIdG2);
            Assert.AreEqual("7311140035020339", cardIdG2);
        }

        [Test]
        public void TestiCashId()
        {
            resultG1 = verifyTool2.CardChkVertify("7101060173008097");//直接餵1代卡卡號
            log.Debug("iCash: 7101060173008097 檢驗結果: " + resultG1);
            Assert.AreEqual(true,resultG1 );
        }
        [Test]
        public void TestiCash2Id()
        {
            resultG2 = verifyTool2.CardChkVertify("7311140035020339");//直接餵2代卡卡號
            log.Debug("iCash2: 7311140035020339 檢驗結果: " + resultG2);
            Assert.AreEqual( true,resultG2);
        }
        [Test]
        public void TestCreaditCardVerify()
        {
            result3 = verifyTool2.CreditCardChkVertify("0417149984000007");//餵聯名卡卡號
            log.Debug("CreditCard: 0417149984000007 檢驗結果: " + result3);
            Assert.AreEqual(true, result3);
        }
        [TearDown]
        public void finis()
        {
            cardIdG1 = null;
            cardIdG2 = null;
            verifyTool2 = null;
        }
    }
}
