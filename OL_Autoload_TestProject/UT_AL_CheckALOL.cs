﻿using NUnit.Framework;
using System;
using OL_Autoload_Lib;
using ALCommon;
using Common.Logging;
using OL_Autoload_Lib.Controller.ALOL.Bkl_Check;
using OL_Autoload_Lib.Controller.ALOL.Rw_Check;
using OL_Autoload_Lib.Controller.ALOL.Store_Check;
using OL_Autoload_Lib.Controller.ALOL.CardSt_Check;
using OL_Autoload_Lib.Controller.Common;
using OL_Autoload_Lib.Controller.ALOL.AdL_Check;
namespace OL_Autoload_TestProject
{
    [TestFixture]
    public class UT_AL_CheckALOL
    {
        private static readonly ILog log = LogManager.GetLogger(typeof(UT_AL_CheckALOL));
        AL_DBModule obDB;

        [SetUp]
        public void init()
        {
            obDB = new AL_DBModule();
            obDB.OpenConnection();
            log.Debug("===== connection open =====");         
        }

        [Test]
        public void TestCheckBkl()
        {
            BkL_Check bk = new BkL_Check();
            bool act = bk.AL_CheckBKL(obDB, "7006040040000250");

            log.Debug("Test Bkl_Check CheckBkl");
            log.Debug("Result : " + act);
            Assert.AreEqual(true, act);
        }
        [Test]
        public void TestCheckCardSt()
        {
            //如果無此卡主檔????
            CardStatus_Check CS = new CardStatus_Check();
            string expect = "0";
            string act = CS.AL_CheckCardStatus(obDB, "0417149984000007");

            log.Debug("Test ChkCardSt");
            log.Debug("Result : " + act);
            Assert.AreEqual(expect, act);
        }

        [Test]
        public void TestCheckREADER()
        {
            RW_Check rd = new RW_Check();
            AutoloadRqt _trans = new AutoloadRqt() {
                M_KIND = "21",
                READER_ID = "8604042B6A9F2980",
                REG_ID = "01",
                MERCHANT_NO = "22555003",             
                STORE_NO = "110035",
            };
         
            bool act = rd.AL_CheckReader(obDB, _trans);
            log.Debug("Test Chk_Reader_Pos_Cmp");
            log.Debug("Result : " + act);
            Assert.AreEqual(true, act);
        }
        [Test]
        public void TestCheckStore()
        {
            STORE_Check st = new STORE_Check();
            AutoloadRqt _trans = new AutoloadRqt()
            {
                M_KIND = "21",               
                MERCHANT_NO = "22555003",
                STORE_NO = "144463",
            };

           bool act= st.AL_CheckStore(_trans, obDB);
           log.Debug("Test CkeckOLType");
           log.Debug("Result : " + act);
            Assert.AreEqual(true, act);
        }

        [Test]
        public void TestGetMERCHANT_NO()
        {
          Get_MerchantNO GM = new Get_MerchantNO();
          string MercNO = "";
          GM.GetMerchantNo("21","SET", obDB, out MercNO);
          string expect = "22555003";
          Assert.AreEqual(expect, MercNO);
        }

        [Test]
        public void TestAdl_CheckADL()
        {
            AdL_Check cADL = new AdL_Check();
            bool actual=  cADL.AL_CheckADL(obDB, "0417149984000007");
            bool expect = false;//不能進行代行授權
            Assert.AreEqual(expect, actual);
        
        }

    [TearDown]
        public void finish()
        {
            log.Debug("===== connection close =====");
            obDB.CloseConnection();
        }


    }
}
