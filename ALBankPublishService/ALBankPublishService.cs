﻿using System.ServiceProcess;
//
using AsyncMultiSocket;
using Common.Logging;
using System;
namespace ALBankPublishService
{
    public partial class ALBankPublishService : ServiceBase
    {
        private static readonly ILog log = LogManager.GetLogger(typeof(ALBankPublishService));
        ISocketServer socketServer1;
        public ALBankPublishService()
        {
            InitializeComponent();
            string BankListener = String.IsNullOrEmpty(System.Configuration.ConfigurationSettings.AppSettings["BankListenerPortAndServiceName"]) ? "6105:BankListener" : System.Configuration.ConfigurationSettings.AppSettings["BankListenerPortAndServiceName"];
            string BankListenerPort = BankListener.Split(':')[0];
            string BankListenerServiceName = BankListener.Split(':')[1];
            socketServer1 = new AsyncMultiSocketServer(Convert.ToInt32(BankListenerPort), BankListenerServiceName);
        }

        protected override void OnStart(string[] args)
        {
            log.Debug("Start Bank Publish Service...");
            try
            {
                this.socketServer1.Start();
            }
            catch (Exception ex)
            {
                log.Error(ex.ToString());
            }
        }

        protected override void OnStop()
        {
            log.Debug("Stop Bank Publish Service...");
            this.socketServer1.Stop();
            // none-block, continue....
        }
    }
}
