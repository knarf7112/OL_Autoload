﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace OL_Autoload_Lib.Model.ReturnCode
{
    /// <summary>
    /// 通用(記得轉成2 bytes)
    /// </summary>
    public enum BReturn_Code_General
    {
        /// <summary>
        /// 正常
        /// </summary>
        OK = 0,
        /// <summary>
        /// 傳輸錯誤
        /// </summary>
        TransError = 1,
        /// <summary>
        /// 電文格式錯誤
        /// </summary>
        DataFormatterError = 2
    }

    /// <summary>
    /// 卡片狀態查詢(記得轉成2 bytes)
    /// </summary>
    public enum BReturn_Code_StatusQuery
    {
        /// <summary>
        /// 正常
        /// </summary>
        OK = 0,
        /// <summary>
        /// 其他錯誤
        /// </summary>
        OtherError = 1,
        /// <summary>
        /// 聯名卡未開卡
        /// </summary>
        NotActivate = 3,
        /// <summary>
        /// 聯名卡掛失
        /// </summary>
        LossReport = 4,
        /// <summary>
        /// 聯名卡停卡
        /// </summary>
        Terminate = 5,
        /// <summary>
        /// 無法授權
        /// </summary>
        NotAuthorize = 6,
        /// <summary>
        /// 聯名卡黑名單
        /// </summary>
        BlackList = 7
    }

    /// <summary>
    /// iCash功能開啟回饋(記得轉成2 bytes)
    /// </summary>
    public enum BReturn_Code_iCashFeedback
    {
        /// <summary>
        /// 正常
        /// </summary>
        OK = 0,
        /// <summary>
        /// 其他錯誤
        /// </summary>
        OtherError = 1,
    }

    /// <summary>
    /// 自動加值授權(記得轉成2 bytes)
    /// </summary>
    public enum BReturn_Code_AutoLoadAuthorize 
    {
        /// <summary>
        /// 正常
        /// </summary>
        OK = 0,
        /// <summary>
        /// 其他錯誤
        /// </summary>
        OtherError = 1,
        /// <summary>
        /// 聯名卡未開卡
        /// </summary>
        NotActivate = 3,
        /// <summary>
        /// 聯名卡掛失
        /// </summary>
        LossReport = 4,
        /// <summary>
        /// 聯名卡停卡
        /// </summary>
        Terminate = 5,
        /// <summary>
        /// 無法授權
        /// </summary>
        NotAuthorize = 6,
        /// <summary>
        /// 聯名卡黑名單
        /// </summary>
        BlackList = 7,
        /// <summary>
        /// 未提供自動加值服務(拒絕授權名單)
        /// </summary>
        NoneAutoLoadService = 8
    }

    /// <summary>
    /// 自動加值授權沖正(記得轉成2 bytes)
    /// </summary>
    public enum BReturn_Code_AutoLoadAuthorizeReveral
    {
        /// <summary>
        /// 正常
        /// </summary>
        OK = 0,
        /// <summary>
        /// 其他錯誤
        /// </summary>
        OtherError = 1,
    }

    /// <summary>
    /// 代行授權(記得轉成2 bytes)
    /// </summary>
    public enum BReturn_Code_AutoLoadAuthorizeAgent
    {
        /// <summary>
        /// 正常
        /// </summary>
        OK = 0,
        /// <summary>
        /// 其他錯誤
        /// </summary>
        OtherError = 1,
        /// <summary>
        /// 聯名卡未開卡
        /// </summary>
        NotActivate = 3,
        /// <summary>
        /// 聯名卡掛失
        /// </summary>
        LossReport = 4,
        /// <summary>
        /// 聯名卡停卡
        /// </summary>
        Terminate = 5,
        /// <summary>
        /// 無法授權
        /// </summary>
        NotAuthorize = 6,
        /// <summary>
        /// 聯名卡黑名單
        /// </summary>
        BlackList = 7,
        /// <summary>
        /// 未提供自動加值服務(拒絕授權名單)
        /// </summary>
        NoneAutoLoadService = 8
    }

    /// <summary>
    /// 連線掛失卡片或取消掛失卡片(記得轉成2 bytes)
    /// </summary>
    public enum BReturn_Code_LossReport
    {
        /// <summary>
        /// 正常(00)
        /// </summary>
        OK = 0,
        /// <summary>
        /// 其他錯誤(01)
        /// </summary>
        OtherError = 1,
        /// <summary>
        /// 聯名卡停卡(05)
        /// </summary>
        Terminate = 5,
        /// <summary>
        /// 無此卡號(09)
        /// </summary>
        NoneCardID = 9,
        /// <summary>
        /// iCash鎖卡(10)
        /// </summary>
        BlockCard = 10,
        /// <summary>
        /// iCash退卡(11)
        /// </summary>
        ReturnCard = 11,
        /// <summary>
        /// iCash非有效卡(12)
        /// </summary>
        InvaildCard = 12,
        /// <summary>
        /// iCash有效期限已過(13)
        /// </summary>
        OverExpirationDate = 13,
        /// <summary>
        /// iCash已做餘額返還(14)
        /// </summary>
        AlreadyBalanceReturn = 14,
        /// <summary>
        /// 重複掛失(15)
        /// </summary>
        Repeat = 15,
        /// <summary>
        /// 取消掛失無法找到上次掛失紀錄(16)
        /// </summary>
        NotFind = 16,
        /// <summary>
        /// 取消掛失超過時限(17)
        /// </summary>
        Overdue = 17,
        /// <summary>
        /// 取消掛失iCash不在黑名單內(18)
        /// </summary>
        NonBlackList = 18
    }

    /// <summary>
    /// 拒絕代行授權(新增或刪除名單)(記得轉成2 bytes)
    /// </summary>
    public enum BReturn_Code_RejectAuthorizeAgent
    {
        /// <summary>
        /// 正常(00)
        /// </summary>
        OK = 0,
        /// <summary>
        /// 其他錯誤
        /// </summary>
        OtherError = 1,
        /// <summary>
        /// iCash中心無此卡號
        /// </summary>
        NoneCardID = 9,
    }
}
