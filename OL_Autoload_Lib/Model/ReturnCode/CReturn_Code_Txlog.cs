﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace OL_Autoload_Lib
{
    /// <summary>
    /// Txlog Return Code
    /// </summary>
    public enum CReturn_Code_Txlog
    {
        /// <summary>
        /// 770001後台通訊錯誤
        /// </summary>
        Ap_Error = 770001,

        /// <summary>
        /// 770002重複資料
        /// </summary>
        Txlog_Duplicate = 770002,

        /// <summary>
        /// 770003Txlog資料錯誤
        /// </summary>
        Txlog_Error = 770003
    }
}
