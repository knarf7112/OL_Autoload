﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace OL_Autoload_Lib
{
    /// <summary>
    /// Autoload Return Code
    /// </summary>
    public enum CReturn_Code_AL
    {
        /// <summary>
        /// 990001後台通訊錯誤
        /// </summary>
        Ap_Error = 990001,

        /// <summary>
        /// 990002加值失敗
        /// </summary>
        Load_Fail = 990002,

        /// <summary>
        /// 990003此卡為黑名單
        /// </summary>
        Black_List = 990003,

        /// <summary>
        /// 990004無效特店
        /// </summary>
        Invalid_Store = 990004,

        /// <summary>
        /// 990005無該端末於特店設置記錄
        /// </summary>
        Invalid_Terminal = 990005,

        /// <summary>
        /// 990006配額次數超過限制
        /// </summary>
        Request_Bal_Denied = 990006,

        /// <summary>
        /// 990007Ctag失敗
        /// </summary>
        Ctag_Fail = 990007,

        /// <summary>
        /// 990008端末配對錯誤
        /// </summary>
        Terminal_Cmp_Fail = 990008,

        /// <summary>
        /// 990009前台通訊錯誤
        /// </summary>
        RwApi_Error = 990009,

        /// <summary>
        /// 990010特店不開放加值
        /// </summary>
        Load_Nonopen = 990010,
        /// <summary>
        /// 990011拒絕代行授權
        /// </summary>
        Refuse_Advice_List = 990011,
        /// <summary>
        /// 990012非有效卡/非聯名卡
        /// </summary>
        Invalid_Card = 990012,
        /// <summary>
        /// 000000成功的交易
        /// </summary>
        Success = 0,
        /// <summary>
        /// 000001 代行授權成功
        /// </summary>
        Advice_Success = 1
    }
}
