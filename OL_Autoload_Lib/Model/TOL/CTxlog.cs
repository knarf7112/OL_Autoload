﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace OL_Autoload_Lib
{
    public class CTxlog:ITxlog
    {
        String sM_KIND; //特店種別
        String sMERCHANT_NO;  ///特約機構代號
        String sSTORE_NO; //店號
        String sREG_ID; //POS機號
        String sREADER_ID; //端末製造序號
        String sTRANS_TYPE; //交易別
        String sPOS_SEQNO; //POS交易序號
        String sTXLOG; //TXLOG
        String sSN; //承認序號
        String sTXLOG_RC; //Return Code

        /******************************************************************
         * Txlog 其他欄位
         * ****************************************************************/
        /// <summary>
        /// 序號
        /// </summary>
        public String TXLOG_ID;

        //public String  M_KIND;	//特店種類 : 已定義
        //public String MERCHANT_NO; //特約機構代碼 : 已定義
        
        /// <summary>
        /// ICASH立帳日
        /// </summary>
        public String SETTLE_DATE;

        /// <summary>
        /// 會計日期
        /// </summary>
        public String ACT_DATE;

        //public String STORE_NO; //	特店店號 : 已定義
        //public String REG_ID; //	POS機號 : 已定義
        
        /// <summary>
        /// 交易序號
        /// </summary>
        public String SEQ_NO;

        /// <summary>
        /// 交易日期時間
        /// </summary>
        public String DATETIME;

        //public String TRANS_TYPE; //交易類別 : 已定義
        
        /// <summary>
        /// 交易日期時間2
        /// </summary>
        public String TRANS_DATE_TXLOG;

        /// <summary>
        /// 回應代碼
        /// </summary>
        public String RETURN_CODE;

        //public String READER_ID; //	讀卡機序號 : 已定義
        
        /// <summary>
        /// 	預付卡卡號
        /// </summary>
        public String ICC_NO;

        /// <summary>
        /// 交易金額點數
        /// </summary>
        public String TRANS_AMT;

        /// <summary>
        /// iDollar餘額
        /// </summary>
        public String IDOLLAR_AMT;

        /// <summary>
        /// 端末物件序號
        /// </summary>
        public String TSAM_OSN;

        /// <summary>
        /// 端末交易序號
        /// </summary>
        public String TSAM_TSN;

        /// <summary>
        /// ICC交易序號
        /// </summary>
        public String ICC_TSN;

        /// <summary>
        /// 卡片餘額
        /// </summary>
        public String ICC_BL;

        /// <summary>
        /// ICC加值序號
        /// </summary>
        public String ICC_LSN;

        /// <summary>
        /// ICC加值總額
        /// </summary>
        public String ICC_LA;

        /// <summary>
        /// ICC扣款序號
        /// </summary>
        public String ICC_DSN;

        /// <summary>
        /// ICC扣款總額
        /// </summary>
        public String ICC_DA;

        /// <summary>
        /// LSAM ID
        /// </summary>
        public String LSAM_ID;

        /// <summary>
        /// LSAM加值序號
        /// </summary>
        public String LSAM_CSN;

        /// <summary>
        /// LSAM加值總額
        /// </summary>
        public String LSAM_CA;

        /// <summary>
        /// LSAM扣款序號
        /// </summary>
        public String LSAM_DSN;

        /// <summary>
        /// LSAM扣款總額
        /// </summary>
        public String LSAM_DA;

        /// <summary>
        /// 	上回加值時端末撥款金額
        /// </summary>
        public String LSAM_DV;

        /// <summary>
        /// PSAM ID
        /// </summary>
        public String PSAM_ID;

        /// <summary>
        /// PSAM加值序號
        /// </summary>
        public String PSAM_CSN;

        /// <summary>
        /// 	PSAM加值總額
        /// </summary>
        public String PSAM_CA;

        /// <summary>
        /// PSAM請款批次號碼
        /// </summary>
        public String PSAM_SBN;

        /// <summary>
        /// PSAM請款資料筆數
        /// </summary>
        public String PSAM_NRS;

        /// <summary>
        /// PSAM請款金額
        /// </summary>
        public String PSAM_SV;

        /// <summary>
        /// 積點欄位RID
        /// </summary>
        public String RID;

        /// <summary>
        /// 有效期限
        /// </summary>
        public String VALID_DATE;

        /// <summary>
        /// 初始及授權代碼
        /// </summary>
        public String INIT_AUTH_CODE;

        /// <summary>
        /// PLSAM測試結果
        /// </summary>
        public String PLSAM_TEST;

        /// <summary>
        /// TSAM測試結果
        /// </summary>
        public String TSAM_TEST;

        /// <summary>
        /// SAM序號檢查結果
        /// </summary>
        public String SAM_SN_CHECK;

        /// <summary>
        /// 訊息確認碼( Message Authentication Code)
        /// </summary>
        public String MAC;

        /// <summary>
        /// TM帳務處理 ID
        /// </summary>
        public String TM_CAL;

        /// <summary>
        /// 	TM作業處理 ID
        /// </summary>
        public String TM_STATUS;

        /// <summary>
        /// TM作業處理回應結果
        /// </summary>
        public String TM_RCODE;

        /// <summary>
        /// CM記號
        /// </summary>
        public String CM_TAG;

        /// <summary>
        /// 傳送記號
        /// </summary>
        public String SEND_TAG;

        /// <summary>
        /// HSM驗證註記
        /// </summary>
        public String HSM_YN;

        /// <summary>
        /// 檔案名稱
        /// </summary>
        public String FILE_NAME;

        /// <summary>
        /// 壓縮檔名
        /// </summary>
        public String ZIP_FILE_NAME;

        /// <summary>
        /// TAR檔案名稱
        /// </summary>
        public String TAR_FILE_NAME;

        /// <summary>
        /// 一般商品金額
        /// </summary>
        public String CM_VAL;

        /// <summary>
        /// 健康捐金額
        /// </summary>
        public String HM_VAL;

        /// <summary>
        /// 代售商品金額
        /// </summary>
        public String UT3_VAL;

        /// <summary>
        /// 代收金額
        /// </summary>
        public String UT_VAL;

        /// <summary>
        /// 交易所需花的時間
        /// </summary>
        public String MAX_APDU_SPEND_TIME;

        /// <summary>
        /// 計算產txlog所需的時間
        /// </summary>
        public String TX_SPEND_TIME;

        /// <summary>
        /// 端末物件序號
        /// </summary>
        public String MSRW_Terminal_SN;

        /// <summary>
        /// NEC manufacturer ID
        /// </summary>
        public String NECM_ID;

        /// <summary>
        /// 保留欄位
        /// </summary>
        public String EDC_RFU;

        /// <summary>
        /// 	HSM回應碼
        /// </summary>
        public String HSM_CODE;

        /// <summary>
        /// 授權序號
        /// </summary>
        public String STAN;

        /// <summary>
        /// 檢索編號
        /// </summary>
        public String RRN;

        ///// <summary>
        ///// 交易前餘額
        ///// </summary>
        //public String BF_BAL;
        ///// <summary>
        ///// 交易後餘額
        ///// </summary>
        //public String AF_BAL;

        /// <summary>
        /// 特店種別
        /// </summary>
        public String M_KIND
        {
            get
            { return sM_KIND; }
            set
            { sM_KIND = value; }
        }

        /// <summary>
        /// 特約機構代號
        /// </summary>
        public String MERCHANT_NO
        {
            get
            { return sMERCHANT_NO; }
            set
            { sMERCHANT_NO = value; }
        }

        /// <summary>
        /// 店號
        /// </summary>
        public String STORE_NO
        {
            get
            { return sSTORE_NO; }
            set
            { sSTORE_NO = value; }
        }

        /// <summary>
        /// POS機號
        /// </summary>
        public String REG_ID
        {
            get
            { return sREG_ID; }
            set
            { sREG_ID = value; }
        }

        /// <summary>
        /// 端末製造序號
        /// </summary>
        public String READER_ID
        {
            get
            { return sREADER_ID; }
            set
            { sREADER_ID = value; }
        }

        /// <summary>
        /// 交易別
        /// </summary>
        public String TRANS_TYPE
        {
            get
            { return sTRANS_TYPE; }
            set
            { sTRANS_TYPE = value; }
        }

        /// <summary>
        /// POS交易序號
        /// </summary>
        public String POS_SEQNO
        {
            get
            { return sPOS_SEQNO; }
            set
            { sPOS_SEQNO = value; }
        }

        /// <summary>
        /// TXLOG
        /// </summary>
        public String TXLOG
        {
            get
            { return sTXLOG; }
            set
            { sTXLOG = value; }
        }

        /// <summary>
        /// 承認序號
        /// </summary>
        public String SN
        {
            get
            { return sSN; }
            set
            { sSN = value; }
        }

        /// <summary>
        /// Return Code
        /// </summary>
        public String TXLOG_RC
        {
            get
            { return sTXLOG_RC; }
            set
            { sTXLOG_RC = value; }
        }
    }
}
