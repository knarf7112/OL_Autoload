﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace OL_Autoload_Lib
{
    public interface ITxlog
    {
        //特店種別
        String M_KIND { get; set; }
        ///特約機構代號
        String MERCHANT_NO { get; set; }
        //店號
        String STORE_NO { get; set; }
        //POS機號
        String REG_ID { get; set; }
        //端末製造序號
        String READER_ID { get; set; }
        //交易別
        String TRANS_TYPE { get; set; }
        //POS交易序號
        String POS_SEQNO { get; set; }
        //TXLOG
        String TXLOG { get; set; }
        //承認序號
        String SN { get; set; }
        //Return Code
        String TXLOG_RC { get; set; }
        ////交易前餘額
        //String BF_BAL { get;set; }
        ////交易後餘額
        //String AF_BAL { get; set; }
    }
}
