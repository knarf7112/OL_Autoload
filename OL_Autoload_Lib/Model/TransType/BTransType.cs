﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace OL_Autoload_Lib
{
    /// <summary>
    /// 發卡行電文交易代號
    /// </summary>
    public enum BTransType
    {
        /// <summary>
        /// 卡片狀態查詢(預留)
        /// </summary>
        CHECK_CC = 990172,
        /// <summary>
        /// icash功能開啟回饋(預留)
        /// </summary>        
        AL_ENABLE = 990173,
        /// <summary>
        /// 自動加值授權
        /// </summary>
        AUTOLOAD = 990174,
        /// <summary>
        /// 代行授權
        /// </summary>
        ADVICE = 990175,
        /// <summary>
        /// 連線掛失/取消掛失
        /// </summary>
        CANCELCARD = 990176,
        /// <summary>
        /// 拒絕代行授權名單維護
        /// </summary>
        REFUSE_ADVICE = 990178	
    }
}
