﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace OL_Autoload_Lib
{
    /// <summary>
    /// 系統規範訊息
    /// </summary>
    class CSysMsg
    {
        private String infoMsg;

        /// <summary>
        /// 資料庫連線未開啟
        /// </summary>
        public static readonly CSysMsg DbConnFail = new CSysMsg("資料庫連線未開啟!");
        /// <summary>
        /// 資料不可為空值
        /// </summary>
        public static readonly CSysMsg DataIsNull = new CSysMsg("資料不可為空值!");

        /// <summary>
        /// Initial
        /// </summary>
        /// <param name="_intoMsg">訊息</param>
        public CSysMsg(String _intoMsg)
        {
            infoMsg = _intoMsg;
        }

        /// <summary>
        /// override ToString
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            return infoMsg;
        }

    }
}
