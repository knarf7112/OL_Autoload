﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace OL_Autoload_Lib
{
    public class AutoloadRqt_NetMsg:INETMSG
    {
        private String sMESSAGE_TYPE; //Message Type ID
        private String sTRANS_DATETIME;   //交易時間
        private String sSTAN = "000000"; //Syatem traceaduit number
        private String sRC = "00";
        private String sINFO_CODE;
        private String sBANK_CODE;//銀行簡碼

        public String MESSAGE_TYPE
        {
            get { return sMESSAGE_TYPE; }
            set { sMESSAGE_TYPE = value; }
        }

        public String TRANS_DATETIME
        {
            get { return sTRANS_DATETIME; }
            set { sTRANS_DATETIME = value; }
        }

        public String STAN
        {
            get { return sSTAN; }
            set { sSTAN = value; }
        }

        public String RC
        {
            get { return sRC; }
            set { sRC = value; }
        }

        /// <summary>
        /// Sign on:071  /// Sign off:072  /// Echo test:301
        /// </summary>
        public String INFO_CODE
        {
            get { return sINFO_CODE; }
            set { sINFO_CODE = value; }
        }

        public String BANK_CODE
        { 
            get { return sBANK_CODE; }
            set { sBANK_CODE = value; }
        }
    }
}
