﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace OL_Autoload_Lib
{
    public class AutoloadRqt_FBank:IFBANK
    {
        private String sMESSAGE_TYPE; //Message Type ID
        //private String sBIT_MAP;
        private String sPROCESSING_CODE;//Trans Type
        private String sICC_NO;
        private String sTRANS_DATETIME;   //交易時間
        private String sSTAN = "000000"; //Syatem traceaduit number
        private String sVALID_DATE; //卡片有效期
        private String sRRN;
        private String sRC = "00";
        private String sBANK_CODE;//銀行簡碼
 
        public String MESSAGE_TYPE {
            get { return sMESSAGE_TYPE; }
            set { sMESSAGE_TYPE = value; }
        }
        
        public String ICC_NO {
            get { return sICC_NO; }
            set { sICC_NO = value; }
        }
        /// <summary>
        /// Trans type 
        /// 990178: 拒絕代行授權名單維護
        /// 990176: 連線掛失/取消
        /// </summary>
        public String PROCESSING_CODE {
            get { return sPROCESSING_CODE; }
            set { sPROCESSING_CODE = value; }
        }

        /// <summary>
        /// 交易時間MMDDHHmmss
        /// </summary>
        public String TRANS_DATETIME
        {
            get { return sTRANS_DATETIME; }
            set { sTRANS_DATETIME = value; }
        }

        public String STAN {
            get { return sSTAN; }
            set { sSTAN = value; }
        }

        /// <summary>
        /// 卡片有效期 Date Expiration
        /// </summary>
        public String VALID_DATE {
            get { return sVALID_DATE; }
            set { sVALID_DATE = value; }
        }

        public String RRN
        {
            get { return sRRN; }
            set { sRRN = value; }
        }

        public String RC {
            get { return sRC; }
            set { sRC = value; }
        }

        public String BANK_CODE
        {
            get { return sBANK_CODE; }
            set { sBANK_CODE = value; }
        }

        /// <summary>
        /// Fields 61 data
        /// </summary>
        public ICC_INFO ICC_info { get; set; }       

        public String FILE_UPDATE_CODE { get; set; }

    }

    /// <summary>
    /// 卡片資訊
    /// </summary>
    //public class ICC_INFO
    //{
    //    public String BIT_MAP { get; set; }
    //    public String TX_DATETIME { get; set; }
    //}
}
