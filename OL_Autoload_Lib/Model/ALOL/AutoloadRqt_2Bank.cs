﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace OL_Autoload_Lib
{
    public class AutoloadRqt_2Bank:IFBANK
    {
        private String sMESSAGE_TYPE; //Message Type ID
        //private String sBIT_MAP;
        private String sPROCESSING_CODE; //Trans Type
        private String sICC_NO;
        private String sTRANS_DATETIME;   //交易時間
        private String sSTAN = "000000"; //Syatem traceaduit number
        private String sRRN;
        private String sRC = "00";
        private String sBANK_CODE;//銀行簡碼
        //------------------------------------------------
        private String sAMOUNT;
        private String sSTORE_NO;
        private String sPOS_NO;
        private String sMERCHANT_NO;

        /// <summary>
        /// Message Type ID
        /// </summary>
        public String MESSAGE_TYPE
        {
            get { return sMESSAGE_TYPE; }
            set { sMESSAGE_TYPE = value; }
        }

        /// <summary>
        /// Primary Account Number 卡號("16"代表長度+16 byte 卡號)
        /// </summary>
        public String ICC_NO
        {
            get { return sICC_NO; }
            set { sICC_NO = value; }
        }
        /// <summary>
        /// Trans type 
        /// 990174: 自動加值授權
        /// 990175: 代行授權
        /// </summary>
        public String PROCESSING_CODE
        {
            get { return sPROCESSING_CODE; }
            set { sPROCESSING_CODE = value; }
        }

        /// <summary>
        /// 交易時間MMDDHHmmss
        /// </summary>
        public String TRANS_DATETIME
        {
            get { return sTRANS_DATETIME; }
            set { sTRANS_DATETIME = value; }
        }
        /// <summary>
        /// 6 byte 
        /// </summary>
        public String STAN
        {
            get { return sSTAN; }
            set { sSTAN = value; }
        }
        /// <summary>
        /// 西元年最後1碼+Julian Date3碼+24小時制2碼+ STAN6碼
        /// </summary>
        public String RRN
        {
            get { return sRRN; }
            set { sRRN = value; }
        }

        /// <summary>
        /// Reponse Code (2byte)
        /// </summary>
        public String RC
        {
            get { return sRC; }
            set { sRC = value; }
        }
        /// <summary>
        /// "0"+銀行代號 4byte
        /// </summary>
        public String BANK_CODE
        {
            get { return sBANK_CODE; }
            set { sBANK_CODE = value; }
        }

        //------------------------------------------------------------------------------------

        /// <summary>
        /// 交易金額 12byte
        /// </summary>
        public String AMOUNT {
            get { return sAMOUNT; }
            set { sAMOUNT = value; }
        }

        /// <summary>
        /// 收單行代號 8碼，"ib"+ibonID,"st"+storeno
        /// Acquiring Institution Identification Code
        /// </summary>
        public String STORE_NO {
            get { return sSTORE_NO; }
            set { sSTORE_NO = value; }
        }

        /// <summary>
        /// POS機編號 (8 byte)
        /// Card acceptor terminal identification
        /// </summary>
        public String POS_NO
        {
            get { return sPOS_NO; }
            set { sPOS_NO = value; }
        }

        /// <summary>
        /// 特約機構代號 15byte (統編)
        /// Card acceptor identification code
        /// </summary>
        public String MERCHANT_NO
        {
            get { return sMERCHANT_NO; }
            set { sMERCHANT_NO = value; }
        }
        /// <summary>
        /// Fields 61 data
        /// </summary>
        public ICC_INFO ICC_info { get; set; }
        /// <summary>
        /// Fields 90 date
        /// </summary>
        public ORI_DATA ORI_dtat { get; set; }
        
    }

    /// <summary>
    /// 卡片資訊(存放原始POS傳送的資料)
    /// </summary>
    public class ICC_INFO
    {
      
        public String BIT_MAP { get; set; }
        /// <summary>
        /// 6 byte(原始POS傳送的資料[門神已將8碼->6碼])
        /// </summary>
        public String STORE_NO { get; set; }
        /// <summary>
        /// 3 byte
        /// </summary>
        public String REG_ID { get; set; }
        /// <summary>
        /// YYYYMMDDhhmmss
        /// </summary>
        public String TX_DATETIME { get; set; }

        /// <summary>
        /// 16 byte
        /// </summary>
        public String ICC_NO { get; set; }
        /// <summary>
        /// 8 byte
        /// </summary>
        public String AMT { get; set; }
        /// <summary>
        /// read id 20 byte
        /// </summary>
        public String NECM_ID { get; set; }
        /// <summary>
        /// 卡機回傳狀態 8 Byte
        /// </summary>
        public String RETURN_CODE { get; set; }

    }

    /// <summary>
    /// 沖正交易原始自動加值資料
    /// Original Data Elements
    /// </summary>
    public class ORI_DATA
    {
        public String MESSAGE_TYPE { get; set; }
        public String TRANSACTION_DATE { get; set; }
        public String STAN { get; set; }
        public String STORE_NO { get; set; }
        public String RRN { get; set; }
    }
}
