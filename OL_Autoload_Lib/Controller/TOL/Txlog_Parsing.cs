﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Xml;

namespace OL_Autoload_Lib.Controller.TOL
{
   public class Txlog_Parsing
    {

        /// <summary>
        /// Online Txlog Parsing 
        ///  </summary>
        /// <param name="cTxlog">CTxlog物件</param>
        /// <param name="txlogDomain">ALTxlog_Domain物件</param>
        /// <param name="exception">exception</param>
        /// <return> true(parsing成功)/false(parsing失敗)</return>
       public bool AL_Parsing(ALCommon.ALTxlog_Domain txlogDomain, CTxlog cTxlog, out string exception)
       {
           bool bResult = true;
           string MERCHANT_NO = txlogDomain.MERCHANT_NO;
           string M_KIND = txlogDomain.M_KIND;
           string TRANS_TYPE = txlogDomain.TRANS_TYPE;
           TxlogRec.Txlog_ParseLength txlogStart = new TxlogRec.Txlog_ParseLength();
           TxlogRec.Txlog_ParseLength txlogLength = new TxlogRec.Txlog_ParseLength();

           //Step1. 讀取Txlog_Parse.xml
           try
           {
               Assembly asm = Assembly.GetExecutingAssembly();//讀取內嵌資源


               string xmlName = asm.GetName().Name;
               Stream stream = asm.GetManifestResourceStream(xmlName + ".Controller.TOL.Txlog_Parse.xml");

               XmlDocument xmlDoc = null;
               xmlDoc = new XmlDocument();
               xmlDoc.Load(stream);
               string sNode1, sNode2, sNode3, sNode4;
               bool bFlag = false;
               int i = 1;
               int xmlStart = 0;
               int xmldata;
               XmlNode xNode;

               xNode = xmlDoc.SelectSingleNode("INFOs");
               foreach (XmlNode node1 in xNode.ChildNodes)
               {
                   sNode1 = node1.Attributes["MERCHANT_NO"].Value;
                   sNode2 = node1.Attributes["M_KIND"].Value;
                   sNode3 = node1.Attributes["TRANS_TYPE"].Value;
                   if (sNode1 == MERCHANT_NO && sNode2 == M_KIND && sNode3 == TRANS_TYPE)
                   {
                       bFlag = true;

                       foreach (XmlNode node2 in node1.ChildNodes)
                       {
                           sNode4 = node2.Name;
                           xmldata = Convert.ToInt32(node2.InnerText);//取得或設定節點和其所有子節點的串連值


                           //Step2. Get TxlogLen
                           GetParsTxlogLen(ref txlogStart, ref txlogLength, sNode4, xmlStart, xmldata);

                           xmlStart = xmlStart + Convert.ToUInt16(xmldata);
                           i = i + 1;
                       }
                       break;
                   }
               }

               if (bFlag != true)
               {
                   exception = "非有效交易類別(沒有對應的Txlog欄位參數檔)";
                   bResult = false;
                   return bResult;
                   //throw new Exception("非有效交易類別(沒有對應的Txlog欄位參數檔)");
               }

           }
           catch (Exception ex)
           {          
               throw new Exception("讀取Txlog_Parse.xml錯誤：" + ex);
           }

           //Step3. Parse Txlog
           try
           {
               bResult = DoParse(txlogDomain, ref cTxlog, ref txlogStart, ref txlogLength, out exception);
           }
           catch (Exception ex)
           {
               throw new Exception("Parse Txlog錯誤：" + ex);
           }

           return bResult;
       
       }


       
        /// <summary>
        /// GetTxlogLen
        /// </summary>
        /// <param name="txlogStart">txlog各欄位起始位置</param>
        /// <param name="txlogLength">txlog各欄位長度</param>
        /// <param name="sNode4">xml各欄位node</param>
        /// <param name="xmlStart">計算txlog各欄位node</param>
        /// <param name="xmldata">xml各欄位內含值</param>
        private static void GetParsTxlogLen(ref TxlogRec.Txlog_ParseLength txlogStart, ref TxlogRec.Txlog_ParseLength txlogLength, string sNode4, int xmlStart, int xmldata)
        {
            switch (sNode4)
            {
                case "TRANS_TYPE":
                    txlogStart.TRANS_TYPE = xmlStart;
                    txlogLength.TRANS_TYPE = xmldata;
                    break;
                case "TRANS_DATE_TXLOG":
                    txlogStart.TRANS_DATE_TXLOG = xmlStart;
                    txlogLength.TRANS_DATE_TXLOG = xmldata;
                    break;
                case "RETURN_CODE":
                    txlogStart.RETURN_CODE = xmlStart;
                    txlogLength.RETURN_CODE = xmldata;
                    break;
                case "ICC_NO":
                    txlogStart.ICC_NO = xmlStart;
                    txlogLength.ICC_NO = xmldata;
                    break;
                case "TRANS_AMT":
                    txlogStart.TRANS_AMT = xmlStart;
                    txlogLength.TRANS_AMT = xmldata;
                    break;
                case "IDOLLAR_AMT":
                    txlogStart.IDOLLAR_AMT = xmlStart;
                    txlogLength.IDOLLAR_AMT = xmldata;
                    break;
                case "TSAM_OSN":
                    txlogStart.TSAM_OSN = xmlStart;
                    txlogLength.TSAM_OSN = xmldata;
                    break;
                case "TSAM_TSN":
                    txlogStart.TSAM_TSN = xmlStart;
                    txlogLength.TSAM_TSN = xmldata;
                    break;
                case "ICC_TSN":
                    txlogStart.ICC_TSN = xmlStart;
                    txlogLength.ICC_TSN = xmldata;
                    break;
                case "ICC_LSN":
                    txlogStart.ICC_LSN = xmlStart;
                    txlogLength.ICC_LSN = xmldata;
                    break;
                case "ICC_LA":
                    txlogStart.ICC_LA = xmlStart;
                    txlogLength.ICC_LA = xmldata;
                    break;
                case "ICC_DSN":
                    txlogStart.ICC_DSN = xmlStart;
                    txlogLength.ICC_DSN = xmldata;
                    break;
                case "ICC_DA":
                    txlogStart.ICC_DA = xmlStart;
                    txlogLength.ICC_DA = xmldata;
                    break;
                case "LSAM_ID":
                    txlogStart.LSAM_ID = xmlStart;
                    txlogLength.LSAM_ID = xmldata;
                    break;
                case "LSAM_CSN":
                    txlogStart.LSAM_CSN = xmlStart;
                    txlogLength.LSAM_CSN = xmldata;
                    break;
                case "LSAM_CA":
                    txlogStart.LSAM_CA = xmlStart;
                    txlogLength.LSAM_CA = xmldata;
                    break;
                case "LSAM_DSN":
                    txlogStart.LSAM_DSN = xmlStart;
                    txlogLength.LSAM_DSN = xmldata;
                    break;
                case "LSAM_DA":
                    txlogStart.LSAM_DA = xmlStart;
                    txlogLength.LSAM_DA = xmldata;
                    break;
                case "LSAM_DV":
                    txlogStart.LSAM_DV = xmlStart;
                    txlogLength.LSAM_DV = xmldata;
                    break;
                case "PSAM_ID":
                    txlogStart.PSAM_ID = xmlStart;
                    txlogLength.PSAM_ID = xmldata;
                    break;
                case "PSAM_CSN":
                    txlogStart.PSAM_CSN = xmlStart;
                    txlogLength.PSAM_CSN = xmldata;
                    break;
                case "PSAM_CA":
                    txlogStart.PSAM_CA = xmlStart;
                    txlogLength.PSAM_CA = xmldata;
                    break;
                case "PSAM_SBN":
                    txlogStart.PSAM_SBN = xmlStart;
                    txlogLength.PSAM_SBN = xmldata;
                    break;
                case "PSAM_NRS":
                    txlogStart.PSAM_NRS = xmlStart;
                    txlogLength.PSAM_NRS = xmldata;
                    break;
                case "PSAM_SV":
                    txlogStart.PSAM_SV = xmlStart;
                    txlogLength.PSAM_SV = xmldata;
                    break;
                case "RID":
                    txlogStart.RID = xmlStart;
                    txlogLength.RID = xmldata;
                    break;
                case "VALID_DATE":
                    txlogStart.VALID_DATE = xmlStart;
                    txlogLength.VALID_DATE = xmldata;
                    break;
                case "INIT_AUTH_CODE":
                    txlogStart.INIT_AUTH_CODE = xmlStart;
                    txlogLength.INIT_AUTH_CODE = xmldata;
                    break;
                case "PLSAM_TEST":
                    txlogStart.PLSAM_TEST = xmlStart;
                    txlogLength.PLSAM_TEST = xmldata;
                    break;
                case "TSAM_TEST":
                    txlogStart.TSAM_TEST = xmlStart;
                    txlogLength.TSAM_TEST = xmldata;
                    break;
                case "SAM_SN_CHECK":
                    txlogStart.SAM_SN_CHECK = xmlStart;
                    txlogLength.SAM_SN_CHECK = xmldata;
                    break;
                case "CM_VAL":
                    txlogStart.CM_VAL = xmlStart;
                    txlogLength.CM_VAL = xmldata;
                    break;
                case "HM_VAL":
                    txlogStart.HM_VAL = xmlStart;
                    txlogLength.HM_VAL = xmldata;
                    break;
                case "UT3_VAL":
                    txlogStart.UT3_VAL = xmlStart;
                    txlogLength.UT3_VAL = xmldata;
                    break;
                case "UT_VAL":
                    txlogStart.UT_VAL = xmlStart;
                    txlogLength.UT_VAL = xmldata;
                    break;
                case "MAC":
                    txlogStart.MAC = xmlStart;
                    txlogLength.MAC = xmldata;
                    break;
                case "Padding":
                    txlogStart.Padding = xmlStart;
                    txlogLength.Padding = xmldata;
                    break;
                case "MAX_APDU_SPEND_TIME":
                    txlogStart.MAX_APDU_SPEND_TIME = xmlStart;
                    txlogLength.MAX_APDU_SPEND_TIME = xmldata;
                    break;
                case "TX_SPEND_TIME":
                    txlogStart.TX_SPEND_TIME = xmlStart;
                    txlogLength.TX_SPEND_TIME = xmldata;
                    break;
                case "MSRW_Terminal_SN":
                    txlogStart.MSRW_Terminal_SN = xmlStart;
                    txlogLength.MSRW_Terminal_SN = xmldata;
                    break;
                case "NECM_ID":
                    txlogStart.NECM_ID = xmlStart;
                    txlogLength.NECM_ID = xmldata;
                    break;
                case "ICC_BL":
                    txlogStart.ICC_BL = xmlStart;
                    txlogLength.ICC_BL = xmldata;
                    break;
                case "TM_CAL":
                    txlogStart.TM_CAL = xmlStart;
                    txlogLength.TM_CAL = xmldata;
                    break;
                case "TM_STATUS":
                    txlogStart.TM_STATUS = xmlStart;
                    txlogLength.TM_STATUS = xmldata;
                    break;
                case "TM_RCODE":
                    txlogStart.TM_RCODE = xmlStart;
                    txlogLength.TM_RCODE = xmldata;
                    break;
                case "CM_TAG":
                    txlogStart.CM_TAG = xmlStart;
                    txlogLength.CM_TAG = xmldata;
                    break;
                case "SEND_TAG":
                    txlogStart.SEND_TAG = xmlStart;
                    txlogLength.SEND_TAG = xmldata;
                    break;
                case "HSM_YN":
                    txlogStart.HSM_YN = xmlStart;
                    txlogLength.HSM_YN = xmldata;
                    break;
                case "FILE_NAME":
                    txlogStart.FILE_NAME = xmlStart;
                    txlogLength.FILE_NAME = xmldata;
                    break;
                case "ZIP_FILE_NAME":
                    txlogStart.ZIP_FILE_NAME = xmlStart;
                    txlogLength.ZIP_FILE_NAME = xmldata;
                    break;
                case "TAR_FILE_NAME":
                    txlogStart.TAR_FILE_NAME = xmlStart;
                    txlogLength.TAR_FILE_NAME = xmldata;
                    break;
                case "SYSTEM_ID":
                    txlogStart.SYSTEM_ID = xmlStart;
                    txlogLength.SYSTEM_ID = xmldata;
                    break;
                case "EDC_RFU":
                    txlogStart.EDC_RFU = xmlStart;
                    txlogLength.EDC_RFU = xmldata;
                    break;
                case "HSM_CODE":
                    txlogStart.HSM_CODE = xmlStart;
                    txlogLength.HSM_CODE = xmldata;
                    break;
                case "BF_BAL":
                    txlogStart.BF_BAL = xmlStart;
                    txlogLength.BF_BAL = xmldata;
                    break;
                //case "AF_BAL":
                //    txlogStart.AF_BAL = xmlStart;
                //    txlogLength.AF_BAL = xmldata;
                //    break;
                case "READER_ID"://目前電文Header會帶
                    //txlogStart.READER_ID = xmlStart;
                    //txlogLength.READER_ID = xmldata;
                    break;
                default:
                    throw new Exception("sNode4設定錯誤");
                    //break;

            }
        }

        /// <summary>
        /// DoParse
        /// </summary>
        /// <param name="txlogDomain">WS帶入txlog各項資訊</param>
        /// <param name="cTxlog">Parse後填值struct</param>
        /// <param name="txlogStart">txlog各欄位起始位置</param>
        /// <param name="txlogLength">txlog各欄位長度</param>
        private static bool DoParse(ALCommon.ALTxlog_Domain txlogDomain, ref CTxlog cTxlog, 
                                    ref TxlogRec.Txlog_ParseLength txlogStart,
                                    ref TxlogRec.Txlog_ParseLength txlogLength, out string exception)
        {
            bool bResult = true;
            exception = "";          

            //txlogDomain帶入部份
            cTxlog.TXLOG_ID = "";
            cTxlog.M_KIND = txlogDomain.M_KIND;
            cTxlog.MERCHANT_NO = txlogDomain.MERCHANT_NO;
            cTxlog.SETTLE_DATE = txlogDomain.TXLOG.Substring(txlogStart.TRANS_DATE_TXLOG, txlogLength.TRANS_DATE_TXLOG).Substring(0, 8);
            cTxlog.ACT_DATE = txlogDomain.TXLOG.Substring(txlogStart.TRANS_DATE_TXLOG, txlogLength.TRANS_DATE_TXLOG).Substring(0, 8);
            cTxlog.STORE_NO = txlogDomain.STORE_NO;
            cTxlog.REG_ID = txlogDomain.REG_ID;
            cTxlog.READER_ID = txlogDomain.READER_ID;
            cTxlog.SEQ_NO = txlogDomain.POS_SEQNO;//ask JB
            cTxlog.SN = txlogDomain.SN;
            cTxlog.DATETIME = txlogDomain.TXLOG.Substring(txlogStart.TRANS_DATE_TXLOG, txlogLength.TRANS_DATE_TXLOG);
            cTxlog.POS_SEQNO = txlogDomain.POS_SEQNO;
            cTxlog.TXLOG = txlogDomain.TXLOG;
            cTxlog.TXLOG_RC = txlogDomain.TXLOG_RC;

            //txlog parse部份
            cTxlog.TRANS_TYPE = txlogDomain.TXLOG.Substring(txlogStart.TRANS_TYPE, txlogLength.TRANS_TYPE);
            cTxlog.TRANS_DATE_TXLOG = txlogDomain.TXLOG.Substring(txlogStart.TRANS_DATE_TXLOG, txlogLength.TRANS_DATE_TXLOG);
            cTxlog.RETURN_CODE = txlogDomain.TXLOG.Substring(txlogStart.RETURN_CODE, txlogLength.RETURN_CODE);
            cTxlog.ICC_NO = txlogDomain.TXLOG.Substring(txlogStart.ICC_NO, txlogLength.ICC_NO);
            cTxlog.TRANS_AMT = txlogDomain.TXLOG.Substring(txlogStart.TRANS_AMT, txlogLength.TRANS_AMT);
            cTxlog.IDOLLAR_AMT = txlogDomain.TXLOG.Substring(txlogStart.IDOLLAR_AMT, txlogLength.IDOLLAR_AMT);
            cTxlog.TSAM_OSN = txlogDomain.TXLOG.Substring(txlogStart.TSAM_OSN, txlogLength.TSAM_OSN);
            cTxlog.TSAM_TSN = txlogDomain.TXLOG.Substring(txlogStart.TSAM_TSN, txlogLength.TSAM_TSN);
            cTxlog.ICC_TSN = txlogDomain.TXLOG.Substring(txlogStart.ICC_TSN, txlogLength.ICC_TSN);
            cTxlog.ICC_LSN = txlogDomain.TXLOG.Substring(txlogStart.ICC_LSN, txlogLength.ICC_LSN);
            cTxlog.ICC_LA = txlogDomain.TXLOG.Substring(txlogStart.ICC_LA, txlogLength.ICC_LA);
            cTxlog.ICC_DSN = txlogDomain.TXLOG.Substring(txlogStart.ICC_DSN, txlogLength.ICC_DSN);
            cTxlog.ICC_DA = txlogDomain.TXLOG.Substring(txlogStart.ICC_DA, txlogLength.ICC_DA);
            cTxlog.LSAM_ID = txlogDomain.TXLOG.Substring(txlogStart.LSAM_ID, txlogLength.LSAM_ID);
            cTxlog.LSAM_CSN = txlogDomain.TXLOG.Substring(txlogStart.LSAM_CSN, txlogLength.LSAM_CSN);
            cTxlog.LSAM_CA = txlogDomain.TXLOG.Substring(txlogStart.LSAM_CA, txlogLength.LSAM_CA);
            cTxlog.LSAM_DSN = txlogDomain.TXLOG.Substring(txlogStart.LSAM_DSN, txlogLength.LSAM_DSN);
            cTxlog.LSAM_DA = txlogDomain.TXLOG.Substring(txlogStart.LSAM_DA, txlogLength.LSAM_DA);
            cTxlog.LSAM_DV = txlogDomain.TXLOG.Substring(txlogStart.LSAM_DV, txlogLength.LSAM_DV);
            cTxlog.PSAM_ID = txlogDomain.TXLOG.Substring(txlogStart.PSAM_ID, txlogLength.PSAM_ID);
            cTxlog.PSAM_CSN = txlogDomain.TXLOG.Substring(txlogStart.PSAM_CSN, txlogLength.PSAM_CSN);
            cTxlog.PSAM_CA = txlogDomain.TXLOG.Substring(txlogStart.PSAM_CA, txlogLength.PSAM_CA);
            cTxlog.PSAM_SBN = txlogDomain.TXLOG.Substring(txlogStart.PSAM_SBN, txlogLength.PSAM_SBN);
            cTxlog.PSAM_NRS = txlogDomain.TXLOG.Substring(txlogStart.PSAM_NRS, txlogLength.PSAM_NRS);
            cTxlog.PSAM_SV = txlogDomain.TXLOG.Substring(txlogStart.PSAM_SV, txlogLength.PSAM_SV);
            cTxlog.RID = txlogDomain.TXLOG.Substring(txlogStart.RID, txlogLength.RID);
            cTxlog.VALID_DATE = txlogDomain.TXLOG.Substring(txlogStart.VALID_DATE, txlogLength.VALID_DATE);
            cTxlog.INIT_AUTH_CODE = txlogDomain.TXLOG.Substring(txlogStart.INIT_AUTH_CODE, txlogLength.INIT_AUTH_CODE);
            cTxlog.PLSAM_TEST = txlogDomain.TXLOG.Substring(txlogStart.PLSAM_TEST, txlogLength.PLSAM_TEST);
            cTxlog.TSAM_TEST = txlogDomain.TXLOG.Substring(txlogStart.TSAM_TEST, txlogLength.TSAM_TEST);
            cTxlog.SAM_SN_CHECK = txlogDomain.TXLOG.Substring(txlogStart.SAM_SN_CHECK, txlogLength.SAM_SN_CHECK);
            cTxlog.CM_VAL = txlogDomain.TXLOG.Substring(txlogStart.CM_VAL, txlogLength.CM_VAL);
            cTxlog.HM_VAL = txlogDomain.TXLOG.Substring(txlogStart.HM_VAL, txlogLength.HM_VAL);
            cTxlog.UT3_VAL = txlogDomain.TXLOG.Substring(txlogStart.UT3_VAL, txlogLength.UT3_VAL);
            cTxlog.UT_VAL = txlogDomain.TXLOG.Substring(txlogStart.UT_VAL, txlogLength.UT_VAL);
            cTxlog.MAC = txlogDomain.TXLOG.Substring(txlogStart.MAC, txlogLength.MAC);
            cTxlog.ICC_BL = txlogDomain.TXLOG.Substring(txlogStart.ICC_BL, txlogLength.ICC_BL);
            cTxlog.TM_CAL = txlogDomain.TXLOG.Substring(txlogStart.TM_CAL, txlogLength.TM_CAL);
            cTxlog.TM_STATUS = txlogDomain.TXLOG.Substring(txlogStart.TM_STATUS, txlogLength.TM_STATUS);
            cTxlog.TM_RCODE = txlogDomain.TXLOG.Substring(txlogStart.TM_RCODE, txlogLength.TM_RCODE);
            cTxlog.CM_TAG = txlogDomain.TXLOG.Substring(txlogStart.CM_TAG, txlogLength.CM_TAG);
            cTxlog.SEND_TAG = txlogDomain.TXLOG.Substring(txlogStart.SEND_TAG, txlogLength.SEND_TAG);
            cTxlog.HSM_YN = txlogDomain.TXLOG.Substring(txlogStart.HSM_YN, txlogLength.HSM_YN);
            cTxlog.FILE_NAME = txlogDomain.TXLOG.Substring(txlogStart.FILE_NAME, txlogLength.FILE_NAME);
            cTxlog.ZIP_FILE_NAME = txlogDomain.TXLOG.Substring(txlogStart.ZIP_FILE_NAME, txlogLength.ZIP_FILE_NAME);
            cTxlog.TAR_FILE_NAME = txlogDomain.TXLOG.Substring(txlogStart.TAR_FILE_NAME, txlogLength.TAR_FILE_NAME);
            cTxlog.MAX_APDU_SPEND_TIME = txlogDomain.TXLOG.Substring(txlogStart.MAX_APDU_SPEND_TIME, txlogLength.MAX_APDU_SPEND_TIME);
            cTxlog.TX_SPEND_TIME = txlogDomain.TXLOG.Substring(txlogStart.TX_SPEND_TIME, txlogLength.TX_SPEND_TIME);
            cTxlog.MSRW_Terminal_SN = txlogDomain.TXLOG.Substring(txlogStart.MSRW_Terminal_SN, txlogLength.MSRW_Terminal_SN);
            cTxlog.NECM_ID = txlogDomain.TXLOG.Substring(txlogStart.NECM_ID, txlogLength.NECM_ID);
            cTxlog.EDC_RFU = txlogDomain.TXLOG.Substring(txlogStart.EDC_RFU, txlogLength.EDC_RFU);
            cTxlog.HSM_CODE = txlogDomain.TXLOG.Substring(txlogStart.HSM_CODE, txlogLength.HSM_CODE);

            //另行處理部分         
            cTxlog.TM_CAL = "0";//TM帳務處理 ID
            cTxlog.TM_STATUS = "0";//TM作業處理 ID
            cTxlog.TM_RCODE = "0000";//TM作業處理回應結果
            cTxlog.SEND_TAG = "N";//傳送記號

            cTxlog.HSM_YN = "0";
            cTxlog.EDC_RFU = "00000000";
            cTxlog.FILE_NAME = "OL_ACICICCG" + cTxlog.TRANS_DATE_TXLOG.Substring(4, 4) + "01";//檔案名稱
            cTxlog.ZIP_FILE_NAME = "OL_ACICICCG" + cTxlog.TRANS_DATE_TXLOG.Substring(4, 4) + "01";
            cTxlog.TAR_FILE_NAME = "OL_ACICICCG" + cTxlog.TRANS_DATE_TXLOG.Substring(4, 4) + "01";

            OL_Autoload_Lib.Controller.TOL.Txlog_Module txlogModule = new Txlog_Module();
            //判斷M_KIND
            if (txlogDomain.M_KIND != "21")
            {
                exception = "M_KIND代碼錯誤";
                bResult= false;

            }
             
            return bResult;
        }


    }


    /// <summary>
    /// Txlog Record 存放
    /// </summary>
    public class TxlogRec
    {
        /// <summary>
        /// Txlog Parse 各欄位長度
        /// </summary>
        public struct Txlog_ParseLength
        {
            public int TRANS_TYPE;
            public int TRANS_DATE_TXLOG;
            public int RETURN_CODE;
            public int READER_ID;
            public int ICC_NO;
            public int TRANS_AMT;
            public int IDOLLAR_AMT;
            public int TSAM_OSN;
            public int TSAM_TSN;
            public int ICC_TSN;
            public int ICC_LSN;
            public int ICC_LA;
            public int ICC_DSN;
            public int ICC_DA;
            public int LSAM_ID;
            public int LSAM_CSN;
            public int LSAM_CA;
            public int LSAM_DSN;
            public int LSAM_DA;
            public int LSAM_DV;
            public int PSAM_ID;
            public int PSAM_CSN;
            public int PSAM_CA;
            public int PSAM_SBN;
            public int PSAM_NRS;
            public int PSAM_SV;
            public int RID;
            public int VALID_DATE;
            public int INIT_AUTH_CODE;
            public int PLSAM_TEST;
            public int TSAM_TEST;
            public int SAM_SN_CHECK;
            public int CM_VAL;
            public int HM_VAL;
            public int UT3_VAL;
            public int UT_VAL;
            public int MAC;
            public int ICC_BL;
            public int TM_CAL;
            public int TM_STATUS;
            public int TM_RCODE;
            public int CM_TAG;
            public int SEND_TAG;
            public int HSM_YN;
            public int FILE_NAME;
            public int ZIP_FILE_NAME;
            public int TAR_FILE_NAME;
            public int MAX_APDU_SPEND_TIME;
            public int TX_SPEND_TIME;
            public int MSRW_Terminal_SN;
            public int NECM_ID;
            public int SYSTEM_ID;
            public int EDC_RFU;
            public int HSM_CODE;
            public int Padding;
            public int BF_BAL;
            public int AF_BAL;
        }
    }


    


    
}
