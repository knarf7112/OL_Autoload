﻿using ALCommon;
using OL_Autoload_Lib.Controller.Common;
using OL_Autoload_Lib.Controller.Common.Record;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Transactions;

namespace OL_Autoload_Lib.Controller.TOL
{
    public partial class Txlog_Module
    {
        /// <summary>
        /// 執行Txlog ONLINE流程並參考轉換後的Txlog資料物件
        /// </summary>
        /// <param name="obTD">ALTxlog_Domain(request物件)</param>
        /// <param name="cTxlog">CTxlog(Parse後的txlog物件)</param>
        /// <return> ALTxlog_Domain(response物件)</return>
        public ALTxlog_Domain do_TOL(ALTxlog_Domain obTD, ref CTxlog cTxlog)
        {
            string excep = "";
            AL_DBModule obDB = null;
            LogModual logM = null;
            DateTime dtrqt = DateTime.Now;
            DateTime dtrsp = DateTime.Now;

            try
            {
                if (cTxlog == null)
                    cTxlog = new CTxlog();
                //初始值設定(for錯誤時的log紀錄)
                cTxlog.SN = "00000000";
                cTxlog.M_KIND = "";
                cTxlog.MERCHANT_NO = "";
                cTxlog.STORE_NO = "";
                cTxlog.REG_ID = "";
                cTxlog.READER_ID = "";
                cTxlog.SEQ_NO = "";
                cTxlog.TAR_FILE_NAME = "";
                cTxlog.TXLOG = "";
                obTD.TRANS_TYPE = "";

                obDB = new AL_DBModule();
                logM = new LogModual();

                //開啟連線
                obDB.OpenConnection();

                //執行online Txlog flow
                Do_TOL_Flow(obDB, obTD, ref cTxlog, out excep);

                //加上RETURN CODE和序號
                obTD.TXLOG_RC = cTxlog.TXLOG_RC;
                obTD.SN = cTxlog.SN;


                return obTD;
            }
            catch (Exception ex)
            {
                CReturn_Code_Txlog rc = CReturn_Code_Txlog.Ap_Error;
                obTD.TXLOG_RC = ((int)rc).ToString();
                obTD.SN = cTxlog.SN;
                excep = obTD.SN + ex;
                return obTD;
            }
            finally
            {
                try
                {
                    //記錄發生之錯誤
                    if (excep != "")
                    {
                        logM.SaveErrorLog(obDB, excep, "TAOL");
                    }

                    //Txlog執行結果匯入
                    dtrsp = DateTime.Now;//處理完成之時間
                    cTxlog.TAR_FILE_NAME = "OL_ACICICCG" + DateTime.Now.ToString("MMdd") + "01";
                    logM.SaveTolLog(obDB, cTxlog, dtrqt, dtrsp);//記錄處理txlog之結果
                }
                catch
                {
                    throw new Exception("記錄Txlog to DB 失敗");
                }
                finally
                {
                    obDB.CloseConnection();
                    logM = null;
                    obDB = null;
                }
            }
        }
        /// <summary>
        /// Txlog ONLINE流程
        /// </summary>
        /// <param name="obTD">ALTxlog_Domain(request物件)</param>
        /// <return> ALTxlog_Domain(response物件)</return>
        public ALTxlog_Domain do_TOL(ALTxlog_Domain obTD)
        {
            string excep = "";
            AL_DBModule obDB = null;
            CTxlog cTxlog=null;
            LogModual logM = null;
            DateTime dtrqt = DateTime.Now;
            DateTime dtrsp = DateTime.Now;

            try
            {
                cTxlog = new CTxlog();
                //初始值設定(for錯誤時的log紀錄)
                cTxlog.SN = "00000000";
                cTxlog.M_KIND = "";
                cTxlog.MERCHANT_NO = "";
                cTxlog.STORE_NO = "";
                cTxlog.REG_ID = "";
                cTxlog.READER_ID = "";
                cTxlog.SEQ_NO = "";
                cTxlog.TAR_FILE_NAME = "";
                cTxlog.TXLOG = "";
                obTD.TRANS_TYPE = "";

                obDB = new AL_DBModule();
                logM = new LogModual();
               
                //開啟連線
                obDB.OpenConnection();

                //執行online Txlog flow
                Do_TOL_Flow(obDB, obTD, ref cTxlog, out excep);

                //加上RETURN CODE和序號
                obTD.TXLOG_RC = cTxlog.TXLOG_RC;
                obTD.SN = cTxlog.SN;

                     
                return obTD;
            }
            catch (Exception ex)
            {
                CReturn_Code_Txlog rc = CReturn_Code_Txlog.Ap_Error;
                obTD.TXLOG_RC = ((int)rc).ToString();
                obTD.SN = cTxlog.SN;
                excep = obTD.SN + ex;
                return obTD;
            }
            finally
            {
                try
                {
                    //記錄發生之錯誤
                    if (excep != "")
                    {
                        logM.SaveErrorLog(obDB, excep,"TAOL");
                    }

                    //Txlog執行結果匯入
                    dtrsp = DateTime.Now;//處理完成之時間
                    cTxlog.TAR_FILE_NAME = "OL_ACICICCG" + DateTime.Now.ToString("MMdd") + "01";
                    logM.SaveTolLog(obDB, cTxlog, dtrqt, dtrsp);//記錄處理txlog之結果
                }
                catch
                {
                    throw new Exception("記錄Txlog to DB 失敗");
                }
                finally
                {
                    obDB.CloseConnection();
                    logM = null;
                    obDB = null;
                }           
            }

        }




        /// <summary>
        /// Txlog insert
        /// </summary>
        /// <param name="obDB">連線模組</param>
        /// <param name="obTD">ALTxlog_Domain(request物件)</param>
        /// <param name="cTxlog">Txlog 物件</param>
        /// <param name="excep">錯誤訊息</param>

        public void Do_TOL_Flow(AL_DBModule obDB, ALTxlog_Domain obTD, ref CTxlog cTxlog, out string excep)
        {
            excep = "";
            string par_exp;

            if (obTD == null)
            {
                throw new Exception("未初始化TXLOG Online socket物件");
            }

            if (cTxlog == null)
            {
                throw new Exception("未初始化TXLOG Online web service物件");
            }
            //建立TOL檢查物件
            Txlog_Module obTxChk=null;
            Get_MerchantNO getMerchNo = null;
            Txlog_Parsing obTxParse=null;

            try
            {
                //Step1:帶值TRANS_TYPER,M_KIND,MERCHANT_NO,TXLOG及POS HEADER的值(STORE_NO,REG_ID,SEQ_NO,READER_ID)

                if (String.IsNullOrEmpty(obTD.M_KIND))
                {
                    obTD.M_KIND = "21";
                }             
                if (!String.IsNullOrEmpty(obTD.TXLOG))
                {
                    obTD.TRANS_TYPE = obTD.TXLOG.ToString().Substring(0, 2);
                }
                obTxChk = new Txlog_Module();//建立連線紀錄模組物件               
                getMerchNo = new Get_MerchantNO();//建立獲得MerchantNO模組

                   //獲得MERCHANT_NO
                String sMERCHANT_NO = String.Empty;
                getMerchNo.GetMerchantNo(obTD.M_KIND, obTD.MERC_FLG, obDB, out sMERCHANT_NO);
                obTD.MERCHANT_NO = sMERCHANT_NO;

                   //mapper
                cTxlog.M_KIND = obTD.M_KIND;
                cTxlog.MERCHANT_NO = obTD.MERCHANT_NO;
                cTxlog.TRANS_TYPE = obTD.TRANS_TYPE;
                cTxlog.STORE_NO = obTD.STORE_NO;
                cTxlog.REG_ID = obTD.REG_ID;
                cTxlog.READER_ID = obTD.READER_ID;
                cTxlog.SEQ_NO = obTD.POS_SEQNO;
                cTxlog.TXLOG = obTD.TXLOG;

                ///Step2:資料列長度是否符合?
                if (obTxChk.AL_ChkTxlogLen(obTD))
                {
                    ///Step3 :Online Txlog模組:Parse
                    obTxParse = new Txlog_Parsing();
                    if (obTxParse.AL_Parsing(obTD, cTxlog, out par_exp))
                    {
                         //設定 TransactionScope的 Option
                        TransactionOptions transOptions = new TransactionOptions();
                        transOptions.IsolationLevel = System.Transactions.IsolationLevel.RepeatableRead; //鎖定是加諸於查詢中使用的所有資料，以防止其他使用者更新資料。
                        
                        //_transOptions.IsolationLevel = System.Transactions.IsolationLevel.ReadCommitted; //當正在讀取資料來避免 Dirty 讀取時，會使用共用鎖定，但是在交易結束之前，資料可以變更，這將會產生非重複讀取或虛設資料。 
                        transOptions.Timeout = new TimeSpan(0, 1, 0); //timeout : 1 min
                        using (TransactionScope scope = new System.Transactions.TransactionScope(TransactionScopeOption.Required, transOptions))
                        {
                            //Step4 : Online Txlog模組做DB相關的處理(INSERT前檢查 + INSERT)的流程
                            string SN = "00000000";
                            //設定STAN & RRN
                            if (cTxlog.TRANS_TYPE == "74")//自動加值
                            {
                                cTxlog.STAN = obTxChk.AL_GetStan(obDB, cTxlog, out cTxlog.RRN);
                            }

                            if (cTxlog.TRANS_TYPE == "75")//匯入Txlog前先取得代行授權的STAN & RRN
                            {
                                cTxlog.STAN = obTxChk.AL_GetStan(obDB, cTxlog, out cTxlog.RRN);
                                cTxlog.RRN = String.Format("{0}{1}{2}{3}",
                                    cTxlog.DATETIME.Substring(3, 1),
                                    DateTime.ParseExact(cTxlog.DATETIME, "yyyyMMddHHmmss", System.Globalization.CultureInfo.InvariantCulture).DayOfYear.ToString("D3"),
                                    cTxlog.DATETIME.Substring(8, 2),
                                    cTxlog.STAN
                                    );//12碼(西元年最後1碼+Julian Date3碼+24小時制2碼+STAN6碼)                            
                            }

                            //是否無重複資料  
                            if (obTxChk.AL_ChkDuplicate(obDB, cTxlog))
                            {
                                //匯入Txlog資料
                                if (obTxChk.AL_Import(obDB, cTxlog))
                                {
                                    cTxlog.TXLOG_RC = "000000";

                                    //取得SN number
                                    HiLowClient.HiLowClient _hilo = new HiLowClient.HiLowClient();
                                    SN = _hilo.GetHiLowValue("OL_Txlog").ToString();
                                    _hilo = null;
                                    cTxlog.SN = SN.PadLeft(8, '0');
                                }
                                else//匯入Txlog失敗
                                {
                                    throw new Exception("Txlog匯入資料庫異常");
                                }
                            }
                            else//資料重複
                            {
                                excep = "Txlog資料重複: " + obTD.TXLOG;
                                CReturn_Code_Txlog rc = CReturn_Code_Txlog.Txlog_Duplicate;
                                cTxlog.TXLOG_RC = ((int)rc).ToString();
                            }
                            scope.Complete();
                        }
                    }
                    else
                    {
                        excep = par_exp + " : " + obTD.TXLOG;
                        CReturn_Code_Txlog rc = CReturn_Code_Txlog.Txlog_Error;
                        cTxlog.TXLOG_RC = ((int)rc).ToString();
                    }
                }
                else //長度不符
                {
                    excep = "長度不正確:" + obTD.TXLOG;
                    CReturn_Code_Txlog rc = CReturn_Code_Txlog.Txlog_Error;
                    cTxlog.TXLOG_RC = ((int)rc).ToString();
                }
            }
            catch (Exception ex)
            {
                excep = ex + obTD.TXLOG;
                CReturn_Code_Txlog rc = CReturn_Code_Txlog.Ap_Error;
                cTxlog.TXLOG_RC = ((int)rc).ToString();

            }
            finally
            {
                obTxParse = null;
                obTxChk = null;
                getMerchNo = null;
            }
        }
    }
}
