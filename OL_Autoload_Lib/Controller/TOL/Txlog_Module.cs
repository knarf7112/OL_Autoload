﻿using ALCommon;
using OL_Autoload_Lib.Sql.TOL;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Xml;
using Newtonsoft.Json;

namespace OL_Autoload_Lib.Controller.TOL
{
    public partial class Txlog_Module
    {
        /// <summary>
        /// 提供Contactless Txlog長度
        /// </summary>
        /// <param name="MERCHANT_NO">string 特約機構代號</param>
        /// <param name="M_KIND"> M_KIND</param>
        /// <returns>int Txlog長度</returns>
        public int AL_GetTxlogLen(string MERCHANT_NO, string M_KIND)
        {
            //參數長度判定
            if (MERCHANT_NO.Length != 8)
            {
                throw new Exception("MERCHANT_NO 長度不符");
            }
            if (M_KIND.Length != 2)
            {
                Console.WriteLine("有執行");
                throw new Exception("M_KIND 長度不符");
            }

            try
            {
                //讀取內嵌資源設定
                Assembly asm = Assembly.GetExecutingAssembly();
                string assemblyName = asm.GetName().Name;
                Stream stream = asm.GetManifestResourceStream(assemblyName + ".Controller.TOL.TxlogLen.xml");

                //讀取內嵌資源.xml
                XmlDocument xmlDoc = null;
                xmlDoc = new XmlDocument();
                xmlDoc.Load(stream);
                string sNode1, sNode2;
                string sResult = "";
                XmlNode xNode;
                xNode = xmlDoc.SelectSingleNode("INFOs");

                foreach (XmlNode node1 in xNode.ChildNodes)
                {
                    sNode1 = node1.Attributes["id"].Value;//MERCHANT_NO
                    if (sNode1 == MERCHANT_NO)
                    {
                        foreach (XmlNode node2 in node1.ChildNodes)
                        {
                            sNode2 = node2.Attributes["id"].Value;//M_KIND
                            if (sNode2 == M_KIND)
                            {
                                sResult = node2.Attributes["value"].Value;
                            }
                        }
                        break;
                    }
                }
                if (sResult == "")
                {
                    throw new Exception("XML查無資訊 MERCHANT_NO=" + MERCHANT_NO);
                }
                return Convert.ToInt32(sResult);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// 判斷Txlog資料長度是否符合
        /// </summary>
        /// <param name="_txlogSkt">Socket Txlog物件</param>
        /// <returns>true(Txlog長度正確)/false(Txlog長度不正確)</returns>
        public bool AL_ChkTxlogLen(ALCommon.ALTxlog_Domain _txlogSkt)
        {
            Txlog_Module txlogChk = new Txlog_Module();

            try
            {
                //取得該Txlog對應的正常長度 : int
                int _txlogLen = txlogChk.AL_GetTxlogLen(
                    _txlogSkt.MERCHANT_NO,
                    _txlogSkt.M_KIND);

                //檢查長度是否正常
                return (_txlogSkt.TXLOG.Length == _txlogLen) ? true : false;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                txlogChk = null;
            }
        }

        /// <summary>
        /// 匯入前檢查是否無重複資料
        /// </summary>
        /// <param name="obCT">txlog物件</param>
        /// <param name="obDB">db連線物件</param>
        /// <returns>true: 未重複 ; false:重複</returns>
        public bool AL_ChkDuplicate( AL_DBModule obDB,CTxlog obCT)
        {
            try
            {
                //判斷資料庫連線有無開啟            
                if (obDB.ALCon.State != ConnectionState.Open)
                {
                    throw new Exception(CSysMsg.DbConnFail.ToString());
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

            String strSql = ""; //Sql
            SqlParameter[] SqlParamSet = null;
            SqlMod_Chk_Add_Txlog ChkDT = new SqlMod_Chk_Add_Txlog();
            try
            {
                ChkDT.Exist(obCT, out strSql, ref SqlParamSet);
                string strCou = obDB.SqlExecuteScalarHasParams(strSql, SqlParamSet);
                return (strCou == "" || strCou == "0") ? true : false;    
                              
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                Array.Resize(ref SqlParamSet, 0);
                ChkDT=null;
          
            }
        }

        /// <summary>
        /// 匯入OnLine Txlog資料表
        /// </summary>
        /// <param name="obDB">資料庫物件</param>
        /// <param name="cTxlog">CTxlog物件</param>
        /// <returns>true(成功) / false(失敗)</returns>
        public bool AL_Import(AL_DBModule obDB, CTxlog cTxlog)
        {
            //檢查連線
            try
            {
                //判斷資料庫連線有無開啟            
                if (obDB.ALCon.State != ConnectionState.Open)
                {
                    throw new Exception(CSysMsg.DbConnFail.ToString());
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }


            String iSql = ""; //Insert 紀錄Sql
            SqlParameter[] _sqlParams = null;
            SqlMod_Chk_Add_Txlog _sqlMod = null;
            HiLowClient.HiLowClient _hilo = null;
            try
            {
                //設定Txlog TXLOG_ID
                String txlogId = "";
                _hilo = new HiLowClient.HiLowClient();
                txlogId = _hilo.GetHiLowValue("OL_Contactless").ToString();
               
                cTxlog.TXLOG_ID = txlogId.PadLeft(8, '0');

                //設定相關Sql指令
                _sqlMod = new SqlMod_Chk_Add_Txlog();
                _sqlMod.Insert(cTxlog, out iSql, ref  _sqlParams);

                //執行Sql
                return (obDB.SqlExec1(iSql, _sqlParams) == "1") ? true : false;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                _hilo = null;
                Array.Resize(ref _sqlParams, 0);
                _sqlMod=null;
            }
        }

        /// <summary>
        /// 取得STAN
        /// </summary>
        /// <param name="obDB">AL_DBModule</param>
        /// <param name="_cTxlog">CTxlog</param>
        /// <returns>STAN</returns>
        public String AL_GetStan(AL_DBModule obDB, CTxlog _cTxlog, out String rrn)
        {
            String sql = String.Empty;
            SqlParameter[] sqlParams = null;
            SqlMod sqlMod = new SqlMod();
            OL_Autoload_Lib.Controller.Common.Get_BandCo getBankCo = new Common.Get_BandCo();
            String _bankCo = getBankCo.AL_Get_BankCode2Bank(obDB, _cTxlog.ICC_NO);
            try
            {
                if (obDB.ALCon.State.ToString() == "Open")
                {
                    if (_cTxlog.TRANS_TYPE == "74") 
                    {
                        sqlMod.GetSTAN74(_cTxlog.READER_ID, out sql, ref sqlParams);
                        DataTable dt = obDB.ParaGetDataTable1(sql, sqlParams);
                        Array.Resize(ref sqlParams, 0);
                        if (dt.Rows.Count == 0)
                        {
                            rrn = String.Empty;
                            return String.Empty;
                        }
                        else
                        {
                            string jsonMsg = dt.Rows[0]["JSON_MSG"].ToString();
                            AutoloadRqt_2Bank al2Bank = JsonConvert.DeserializeObject<AutoloadRqt_2Bank>(jsonMsg);
                            rrn = al2Bank.RRN;
                            return al2Bank.STAN;
                        }
                    }
                    else if (_cTxlog.TRANS_TYPE == "75")
                    {
                        sqlMod.GetSTAN75(_cTxlog,  _bankCo, out sql, ref sqlParams);
                        DataTable dt = obDB.ParaGetDataTable1(sql, sqlParams);
                        Array.Resize(ref sqlParams, 0);
                        if (dt.Rows.Count == 0)
                        {
                            rrn = String.Empty;
                            return String.Empty;
                        }
                        else
                        {
                            string _stan = dt.Rows[0]["STAN"].ToString();
                            rrn = String.Empty;
                            return _stan;
                        }
                    }
                    else
                    {
                        rrn = String.Empty;
                        return String.Empty;
                    }
                }
                else
                {
                    throw new Exception(CSysMsg.DbConnFail.ToString());
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
