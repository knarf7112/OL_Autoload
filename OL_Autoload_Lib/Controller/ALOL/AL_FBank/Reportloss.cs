﻿using ALCommon;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;

namespace OL_Autoload_Lib
{
    public partial class AL_FBank
    {
        /// <summary>
        /// 更新連線掛失/取消相關Table
        /// 1.卡主檔更新 2.黑名單主檔新增/更新 3.黑名單明細檔新增 4.記名卡主檔更新 5.記名卡明細檔新增
        /// </summary>
        /// <param name="rqt">連線掛失/取消物件</param>
        /// <param name="obDB">DBModule</param>
        /// <returns>true/false</returns>
        public bool Upt_Reportloss(AutoloadRqt_FBank rqt, AL_DBModule obDB)
        {
            try
            {
                sqlMod.Upt_Reportloss(rqt, out sql, ref sqlParams);
                if (obDB.ALCon.State.ToString() == "Open")
                {
                    obDB.SqlExec1(sql, sqlParams);
                    Array.Resize(ref sqlParams, 0);
                    return true;
                }
                else
                {
                    throw new Exception(CSysMsg.DbConnFail.ToString());
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// 新增連線掛失/掛失取消Log
        /// </summary>
        /// <param name="rqt">連線掛失/取消物件</param>
        /// <param name="oriCstatus">原卡主檔狀態</param>
        /// <param name="obDB">DBModule物件</param>
        /// <returns>true/false</returns>
        public bool Add_Reportloss_Log(AutoloadRqt_FBank rqt, String oriCstatus, AL_DBModule obDB)
        {
            try
            {
                sqlMod.Add_Reportloss_log(oriCstatus, rqt, out sql, ref sqlParams);
                if (obDB.ALCon.State.ToString() == "Open")
                {
                    obDB.SqlExec1(sql, sqlParams);
                    Array.Resize(ref sqlParams, 0);
                    return true;
                }
                else
                {
                    throw new Exception(CSysMsg.DbConnFail.ToString());
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// 查詢是否重複掛失
        /// </summary>
        /// <param name="iccNo">卡號</param>
        /// <param name="obDB">DBModule</param>
        /// <returns>true: 重複掛失/ false:沒有重複掛失</returns>
        public bool Reportloss_Check(String iccNo, AL_DBModule obDB)
        {
            String action = String.Empty;
            try
            {
                sqlMod.ChkReportloss(iccNo, out sql, ref sqlParams);
                if (obDB.ALCon.State.ToString() == "Open")
                {
                    DataTable dt = obDB.ParaGetDataTable1(sql, sqlParams);
                    Array.Resize(ref sqlParams, 0);                    
                    if (dt.Rows.Count == 0)
                        return false;
                    action = dt.Rows[0]["ACTION"].ToString();
                    if (action == "1")
                        return true;
                    else if (action == "3")
                        return false;
                    else
                        return false;                    
                }
                else
                {
                    throw new Exception(CSysMsg.DbConnFail.ToString());
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        ///<summary>
        /// 檢核取消掛失時限
        /// </summary>
        /// <param name="iccNo">卡號</param>
        /// <param name="obDB">DBModule</param>
        /// <returns>0:無資料/ 1:時限內/ 2:超過時限</returns>
        public int CancelReportloss_Check(String iccNo, AL_DBModule obDB)
        {
            String lossTime1 = String.Empty;
            String lossTime2 = String.Empty;
            try
            {
                sqlMod.ChkReportloss(iccNo, out sql, ref sqlParams);
                if (obDB.ALCon.State.ToString() == "Open")
                {
                    DataTable dt = obDB.ParaGetDataTable1(sql, sqlParams);
                    Array.Resize(ref sqlParams, 0);
                    if (dt.Rows.Count == 0)
                        return 0;

                    lossTime1 = dt.Rows[0]["REPORTLOSS_TIME"].ToString();
                    lossTime2 = String.Format("{0}/{1}/{2} {3}:{4}:{5}",
                    DateTime.Now.ToString("yyyy"), lossTime1.Substring(0, 2), lossTime1.Substring(2, 2),
                    lossTime1.Substring(4, 2), lossTime1.Substring(6, 2), lossTime1.Substring(8, 2));
                    DateTime dtLoss = DateTime.Parse(lossTime2); //掛失時間
                    DateTime yesterday = DateTime.Parse(DateTime.Now.AddDays(-1).ToString("yyyy/MM/dd 22:00:00")); //比對時間1
                    DateTime today = DateTime.Parse(DateTime.Now.ToString("yyyy/MM/dd 22:00:00")); //比對時間2
                    if (yesterday <= dtLoss && dtLoss < today)
                        return 1;
                    else
                        return 2;
                }
                else
                {
                    throw new Exception(CSysMsg.DbConnFail.ToString());
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

    }
}
