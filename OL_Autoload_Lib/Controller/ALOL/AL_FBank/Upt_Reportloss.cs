﻿using ALCommon;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace OL_Autoload_Lib
{
    public partial class AL_FBank
    {
        public bool Upt_Reportloss(AutoloadRqt_FBank rqt, AL_DBModule obDB)
        {
            try
            {
                sqlMod.Upt_Reportloss(rqt, out sql, ref sqlParams);
                if (obDB.ALCon.State.ToString() == "Open")
                {
                    obDB.SqlExec1(sql, sqlParams);
                    Array.Resize(ref sqlParams, 0);
                    return true;
                }
                else
                {
                    throw new Exception(CSysMsg.DbConnFail.ToString());
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
