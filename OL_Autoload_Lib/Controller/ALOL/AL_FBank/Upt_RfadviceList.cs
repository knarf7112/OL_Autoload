﻿using ALCommon;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;

namespace OL_Autoload_Lib
{
    public partial class AL_FBank
    {            
        //INIT
        String sql = String.Empty;
        SqlParameter[] sqlParams;
        SqlMod sqlMod = new SqlMod();

        /// <summary>
        /// 更新拒絕代行授權名單主檔
        /// </summary>
        /// <param name="rqt">拒絕代行授權名單維護物件</param>
        /// <param name="obDB">DBModule物件</param>
        /// <returns>true/false</returns>
        public bool Upt_RfadviceList(AutoloadRqt_FBank rqt, AL_DBModule obDB)
        {
            try
            {
                sqlMod.Upt_RfadviceList(rqt, out sql, ref sqlParams);
                if (obDB.ALCon.State.ToString() == "Open")
                {
                    obDB.SqlExec1(sql, sqlParams);
                    Array.Resize(ref sqlParams, 0);
                    return true;
                }
                else
                {
                    throw new Exception(CSysMsg.DbConnFail.ToString());
                }
            }
            catch (Exception ex)
            { 
                throw ex; 
            }

        }

    }
}
