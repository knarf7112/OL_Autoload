﻿using ALCommon;
using OL_Autoload_Lib.Sql.AL;
using OL_Autoload_Lib.Sql.Domain;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;

namespace OL_Autoload_Lib.Controller.ALOL.Ad_control
{
    public partial class Ad_Controller
    {
        /// <summary>
        /// 新增行使代行授權之資料
        /// </summary>        
        /// <param name="obDB">資料庫物件</param>
        /// <param name="MerchNO">特約機構代號</param>
        /// <param name="BandCo">銀行代碼</param>
        /// <param name="StoreNO">店號</param>
        /// <param name="RegID">POS代號</param>
        /// <param name="CardID">卡號</param>
        /// <param name="Amount">金額</param>
        ///  <returns> true(新增成功)/false(新增失敗)</returns>

        public bool AL_AddAdviceLog(AL_DBModule obDB, string MerchNO, string bankMerchantNo, string BankCo, string StoreNO, string RegID, string CardID, int Amount, string ReaderID, string STAN)
        {
            try
            {
                //判斷資料庫連線有無開啟            
                if (obDB.ALCon.State != ConnectionState.Open)
                {
                    throw new Exception(CSysMsg.DbConnFail.ToString());
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

            String sSql = ""; //Sql
            SqlParameter[] sqlParams = null;
            SqlMod_Advice_Detail insertAD = new SqlMod_Advice_Detail();
            OL_AL_ADVICE_DETAIL_D AdDomain = null;
            try
            {
                DateTime now = DateTime.Now;
                AdDomain = new OL_AL_ADVICE_DETAIL_D() 
                { 
                    ACT_DATE = now.Date.ToString("yyyyMMdd"),
                    BANK_CODE = BankCo,
                    CARD_ID = CardID,
                    MERCHANT_NO = MerchNO,
                    REG_ID = RegID,
                    STORE_NO = StoreNO,
                    LOG_TIME = now.ToString("yyyyMMddHHmmss"),
                    AMOUNT = (decimal)Amount,
                    STAN = STAN,//STAN(6 bytes)
                    READER_ID = ReaderID
                };

                //2015-03-13  Oldder by Frank
                //AdDomain.ACT_DATE = DateTime.Now.Date.ToString("yyyyMMdd");
                //AdDomain.BANK_CODE = BankCo;
                //AdDomain.CARD_ID = CardID;
                //AdDomain.MERCHANT_NO = MerchNO;
                //AdDomain.REG_ID = RegID;
                //AdDomain.STORE_NO = StoreNO;
                //AdDomain.LOG_TIME = DateTime.Now.ToString("yyyyMMddHHmmss");
                //AdDomain.AMOUNT = (decimal)Amount;

                //加入代行授權之資料
                insertAD.insert_AdDetail(AdDomain, bankMerchantNo, out sSql, ref sqlParams);

                return (obDB.SqlExec1(sSql, sqlParams) == "1") ? true : false;

            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                AdDomain = null;
                insertAD=null;
                Array.Resize(ref sqlParams, 0);
            }
        }
    }
}
