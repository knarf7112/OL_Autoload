﻿using ALCommon;
using OL_Autoload_Lib.Sql.AL;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;

namespace OL_Autoload_Lib.Controller.ALOL.Ad_control
{
    public partial class Ad_Controller
    {
        /// <summary>
        /// 取得代行授權之額度和次數
        /// </summary>        
        /// <param name="obDB">資料庫物件</param>
        /// <param name="MerchNO">特約機構代號</param>
        /// <param name="AdCNT">代行授權次數上限</param>
        /// <param name="AdAMT">代行授權額度上限</param>
        /// <param name="BandCo">銀行代號</param>

        public void AL_Get_AdviceLimit(AL_DBModule obDB, string MerchNo,string BandCo, out string AdCNT, out string AdAMT)
        {
            try
            {
                //判斷資料庫連線有無開啟            
                if (obDB.ALCon.State != ConnectionState.Open)
                {
                    throw new Exception(CSysMsg.DbConnFail.ToString());
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

            String sSql = ""; //Sql
            SqlParameter[] sqlParams = null;
            SqlMod_Chk_ADBal checkAd = new SqlMod_Chk_ADBal();
            try
            {
                checkAd.checkBal(MerchNo, BandCo, out sSql, ref sqlParams);
                DataTable dt=obDB.ParaGetDataTable1(sSql,sqlParams);
                if (dt == null || dt.Rows.Count==0)
                {
                    AdCNT = "";
                    AdAMT = "";               
                }
                else
                {
                    AdCNT = dt.Rows[0]["CNT"].ToString();
                    AdAMT = dt.Rows[0]["AMT"].ToString();                
                }
                
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                checkAd=null;
                Array.Resize(ref sqlParams, 0);
            }       
        }
    }
}
