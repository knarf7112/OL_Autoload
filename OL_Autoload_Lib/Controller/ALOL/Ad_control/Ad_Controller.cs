﻿using ALCommon;
using Common.Logging;
using OL_Autoload_Lib.Controller.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace OL_Autoload_Lib.Controller.ALOL.Ad_control
{
    public partial class Ad_Controller
    {
        private static readonly ILog log = LogManager.GetLogger(typeof(Ad_Controller));
        /// <summary>
        /// 確認代行授權之額度和次數是否低於上限
        /// </summary>        
        /// <param name="obDB">資料庫物件</param>
        /// <param name="MerchNO">特約機構代號</param>
        /// <param name="TAMT">加值金額</param>
        /// <param name="bankCode">銀行代碼</param>
        /// <returns> 0:額度和次數都小於上限 </returns>
        /// <returns> 1:額度超過上限 </returns>
        /// <returns> 2:次數超過上限 </returns>
        /// <returns> 3:找不到資料 </returns>
        public string AL_CheckAdvice(AL_DBModule obDB, string MerchNO, string bankCode, int TAMT, string IccNo)
      {
          //Get_BandCo getBankco;

          int AdCNTLimit=0;//代行授權次數上限
          int AdAMTLimit=0;//代行授權額度上限
          string ACTDate;//日期
          
          string AdCNTLimitStr="";//代行授權次數上限
          string AdAMTLimitStr="";//代行授權額度上限
          try
          {
    

              //step1. 獲得 Count_Limit & AMT_Limit
              //string AdCNTLimitStr="";//代行授權次數上限
              //string AdAMTLimitStr="";//代行授權額度上限
              AL_Get_AdviceLimit(obDB, MerchNO, bankCode, out AdCNTLimitStr, out AdAMTLimitStr);
              if (AdCNTLimitStr == "" || AdAMTLimitStr == "")
              {
                  return "3";
              }
              else 
              {
                  int.TryParse(AdCNTLimitStr, out AdCNTLimit);
                  int.TryParse(AdAMTLimitStr,out AdAMTLimit);
              }

              //step2. 比對加值額度
              if (TAMT > AdAMTLimit)
              {
                  return "1";
              }
              
              //step3. 獲得 今日已授權次數
              ACTDate = DateTime.Now.Date.ToString("yyyyMMdd");
              int Adcount;//今日代行授權次數
              int.TryParse(AL_GetAdviceCount(obDB, MerchNO, bankCode, ACTDate, IccNo),out Adcount);
           

              //step4. 比對授權次數
              if (Adcount >= AdCNTLimit)
              {
                  return "2";
              }

              return "0";

          }
          catch (Exception ex)
          {
              throw ex;
          }
          finally
          {
              log.Debug("[AL_CheckAdvice]BankMerchNO:" + MerchNO + 
                                        " BankCode:" + bankCode + 
                                        " TAMT:" + TAMT +
                                        "代行授權次數上限:" + AdCNTLimitStr +
                                        "代行授權額度上限" + AdAMTLimitStr);
          }

      }





    }
}
