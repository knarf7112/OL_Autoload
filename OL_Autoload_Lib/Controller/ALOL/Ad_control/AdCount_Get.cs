﻿using ALCommon;
using OL_Autoload_Lib.Sql.AL;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;

namespace OL_Autoload_Lib.Controller.ALOL.Ad_control
{
    public partial class Ad_Controller
    {
        /// <summary>
        /// 取得已經代行授權之次數
        /// </summary>        
        /// <param name="obDB">資料庫物件</param>
        /// <param name="MerchNO">特約機構代號</param>
        /// <param name="BandCo">銀行代碼</param>
        /// <param name="ACTDate">代行授權日(當日)</param>
        ///  <returns> COUNT :當日代行授權次數</returns>

        public string AL_GetAdviceCount(AL_DBModule obDB, string MerchNo, string BandCo, string ACTDate, string IccNo)
        {

            try
            {
                //判斷資料庫連線有無開啟            
                if (obDB.ALCon.State != ConnectionState.Open)
                {
                    throw new Exception(CSysMsg.DbConnFail.ToString());
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

            String sSql = ""; //Sql
            SqlParameter[] sqlParams = null;
            SqlMod_Advice_Detail getAdCount = new SqlMod_Advice_Detail();

            try
            {
                getAdCount.AdCount(MerchNo, BandCo,ACTDate, IccNo, out sSql, ref sqlParams);
                return obDB.SqlExecuteScalarHasParams(sSql,sqlParams);               
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                getAdCount=null;
                Array.Resize(ref sqlParams, 0);
            }
        }
    }
}
