﻿using ALCommon;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace OL_Autoload_Lib
{
    public partial class AL_2Bank
    {
        /// <summary>
        /// 紀錄原始電文(2Bank)
        /// </summary>
        /// <param name="tId">Reader ID</param>
        /// <param name="json">自動加值原始電文(All)</param>
        /// <param name="obDB">DBModule物件</param>
        /// <returns>true/false</returns>
        public bool Backup_Json(String tId, String json, AL_DBModule obDB)
        {
            try
            {
                sqlMod.Backup_Json(tId, json, out sql, ref sqlParams);
                if (obDB.ALCon.State.ToString() == "Open")
                {
                    obDB.SqlExec1(sql, sqlParams);
                    Array.Resize(ref sqlParams, 0);
                    return true;
                }
                else
                {
                    throw new Exception(CSysMsg.DbConnFail.ToString());
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

    }
}
