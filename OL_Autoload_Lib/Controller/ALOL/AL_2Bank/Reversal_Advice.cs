﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Data;
using ALCommon;

namespace OL_Autoload_Lib
{

    public partial class AL_2Bank
    {
        //INIT
        String sql = String.Empty;
        SqlParameter[] sqlParams;
        SqlMod sqlMod = new SqlMod();

        /// <summary>
        /// 取得沖正原始資料(單筆)
        /// </summary>
        /// <param name="tId">Reader ID</param>
        /// <param name="obDB">DBModule物件</param>
        /// <returns>沖正原始資料JSON字串</returns>
        public String Get_Reversal(String tId, AL_DBModule obDB)
        {
            try
            {
                sqlMod.Get_Reversal(tId, out sql, ref sqlParams);
                if (obDB.ALCon.State.ToString() == "Open")
                {
                    DataTable dt = obDB.ParaGetDataTable1(sql, sqlParams);
                    Array.Resize(ref sqlParams, 0);
                    if (dt.Rows.Count == 0)
                        return String.Empty;
                    else
                        return dt.Rows[0]["JSON_MSG"].ToString();
                }
                else
                {
                    throw new Exception(CSysMsg.DbConnFail.ToString());
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// 取得沖正原始資料(All)
        /// </summary>
        /// <param name="obDB">DBModule</param>
        /// <returns>DataTable物件</returns>
        public DataTable GetAll_Reversal(AL_DBModule obDB)
        {
            try
            {
                sqlMod.GetAll_Reversal(out sql);
                if (obDB.ALCon.State.ToString() == "Open")
                {
                    DataTable dt = obDB.GetDataTable(sql);
                    Array.Resize(ref sqlParams, 0);
                    return dt;
                }
                else
                {
                    throw new Exception(CSysMsg.DbConnFail.ToString());
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// 更新沖正原始資料(單筆)
        /// </summary>
        /// <param name="tId">Reader ID</param>
        /// <param name="json">JSON Message</param>
        /// <param name="obDB">DBModule物件</param>
        /// <returns>true/false</returns>
        public bool Upt_Reversal(String tId, String json, AL_DBModule obDB)
        {
            try
            {
                sqlMod.Upt_Reversal(tId, json, out sql, ref sqlParams);
                if (obDB.ALCon.State.ToString() == "Open")
                {
                    obDB.SqlExec1(sql, sqlParams);
                    Array.Resize(ref sqlParams, 0);
                    return true;
                }
                else
                {
                    throw new Exception(CSysMsg.DbConnFail.ToString());
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// 新增沖正原始資料(單筆)
        /// </summary>
        /// <param name="tId">Reader ID</param>
        /// <param name="json">JSON Message</param>
        /// <param name="obDB">DBModule物件</param>
        /// <returns>true/false</returns>
        public bool Insert_Reversal(String tId, String json, AL_DBModule obDB)
        {
            try
            {
                sqlMod.Add_Reversal(tId, json, out sql, ref sqlParams);
                if (obDB.ALCon.State.ToString() == "Open")
                {
                    obDB.SqlExec1(sql, sqlParams);
                    Array.Resize(ref sqlParams, 0);
                    return true;
                }
                else
                {
                    throw new Exception(CSysMsg.DbConnFail.ToString());
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// 新增或更新沖正原始資料(單筆)
        /// 可自行判斷是否有該TID資料，若有則更新，若無則新增
        /// </summary>
        /// <param name="tId">Reader ID</param>
        /// <param name="json">JSON Message</param>
        /// <param name="obDB">DBModule物件</param>
        /// <returns>true/false</returns>
        public bool InsertOrUpt_Reversal(String tId, String json, AL_DBModule obDB)
        {
            try
            {
                sqlMod.AddOrUpt_Reversal(tId, json, out sql, ref sqlParams);
                if (obDB.ALCon.State.ToString() == "Open")
                {
                    obDB.SqlExec1(sql, sqlParams);
                    Array.Resize(ref sqlParams, 0);
                    return true;
                }
                else
                {
                    throw new Exception(CSysMsg.DbConnFail.ToString());
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

    }
}
