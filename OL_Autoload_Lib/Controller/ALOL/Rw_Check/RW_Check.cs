﻿using ALCommon;
using OL_Autoload_Lib.Sql.AL;
using OL_Autoload_Lib.Sql.Domain;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;

namespace OL_Autoload_Lib.Controller.ALOL.Rw_Check
{

    /// <summary>
    /// 端末檢核
    /// </summary>
   public class RW_Check
    {
        /// <summary>
        /// 判斷是否為有效端末
        /// </summary>
        /// <param name="_trans">AutoloadRqt參數物件</param>
        /// <param name="obDB">DB連線物件</param>
        /// <returns>true(有效端末)/false(無效端末)</returns>

       public bool AL_CheckReader(AL_DBModule obDB, AutoloadRqt _trans)
       {
           try
           {
               //Step1. 判斷連線是否開啟
               if (obDB.ALCon.State != ConnectionState.Open)  //DB模組的連線是否接通?
               {
                   throw new Exception(CSysMsg.DbConnFail.ToString());
               }
           }
           catch (Exception ex)
           {
               throw ex;
           }
               //Step2. 嘗試取得端末主檔是否有對應的店號,機號,POS硬碟序號
               String sSql = ""; //Sql
               SqlParameter[] _sqlParams = null;
           
               SqlMod_Chk_READER _sqlModCheckReader = new SqlMod_Chk_READER();


               try
               {
                   //設定SQL
                   _sqlModCheckReader.Exist(_trans, out sSql, ref _sqlParams);

                   //檢查端末
                   string result = obDB.SqlExecuteScalarHasParams(sSql, _sqlParams);
                   int CNT = 0;
                   if (result != "")
                   {
                       int.TryParse(result, out CNT);
                   }

                   return (CNT > 0) ? true : false;

               }
               catch (Exception ex)
               {
                   throw ex;
               }
           finally
           {
               _sqlModCheckReader=null;
               Array.Resize(ref _sqlParams, 0);
           }
       }

    }
}
