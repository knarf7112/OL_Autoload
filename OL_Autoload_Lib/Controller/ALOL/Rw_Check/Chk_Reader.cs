﻿using ALCommon;
using OL_Autoload_Lib.Sql.AL;
using OL_Autoload_Lib.Sql.Domain;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;

namespace OL_Autoload_Lib.Controller.ALOL.Rw_Check
{

    /// <summary>
    /// 端末檢核
    /// </summary>
   public class Chk_Reader
    {


       public bool Chk_Reader_Pos_Cmp<T>(AL_DBModule obDB, T _trans, String sPOS_VOLINFO) where T : ICash
       {
           try
           {
               //Step1. 判斷連線是否開啟
               if (obDB.ALCon.State != ConnectionState.Open)  //DB模組的連線是否接通?
               {
                   throw new Exception(CSysMsg.DbConnFail.ToString());
               }

               //Step2. 嘗試取得端末主檔是否有對應的店號,機號,POS硬碟序號
              String sSql = ""; //Sql
               SqlParameter[] _sqlParams = null;
               using (SqlMod_Chk_READER _sqlModCheckReader = new SqlMod_Chk_READER())
               {
                   //mapping to TM_ISET_READER_D                           
               TM_ISET_READER_D TMReader = new TM_ISET_READER_D();
               TMReader.M_KIND = _trans.M_KIND;
               TMReader.MERCHANT_NO = _trans.MERCHANT_NO;
               TMReader.STORE_NO = _trans.STORE_NO;
               TMReader.REG_ID = _trans.REG_ID;
               TMReader.READER_ID = _trans.READER_ID;
               TMReader.POS_VOLINFO = sPOS_VOLINFO;

                   //設定SQL
                   _sqlModCheckReader.Exist(TMReader, out sSql, ref _sqlParams);

               }

               //執行SQL
               Double _cntRD = 0; //端末為有效端末的筆數
               DataTable dt = obDB.ParaGetDataTable1(sSql, _sqlParams);
    
               Double.TryParse(dt.Rows[0]["CNT"].ToString(), out _cntRD);

               Console.WriteLine("符合條件之筆數為: " + _cntRD);
               return (_cntRD == 0) ? false : true;
              
           }
           catch (Exception ex)
           {
               throw ex;
           }
       }






    }
}
