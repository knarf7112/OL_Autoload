﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ALCommon;
using System.Data;
using OL_Autoload_Lib.Sql.AL;
using System.Data.SqlClient;

namespace OL_Autoload_Lib.Controller.ALOL.Bkl_Check
{
    public class Chk_BKL
    {
        public string sResult = "000000";
        protected string sM_Kind = "";

        /// <summary>
        /// 檢查該卡號是否為黑名單
        /// </summary>
        /// <returns>true (黑名單) / false (非黑名單)</returns>
        /// <param name="obDB">資料庫物件</param>
        /// <param name="sCardID">卡號</param>

        public bool CheckBkl(ALCommon.AL_DBModule obDB, String sCardID)
        {
            try
            {
                //判斷資料庫連線有無開啟
              //  log.Debug("連線開始,狀態為: " + obDB.LOLCon.State.ToString());
                if (obDB.ALCon.State != ConnectionState.Open)
                {
                    throw new Exception(CSysMsg.DbConnFail.ToString());
                }
            }
            catch (Exception ex)
            {
               Console.WriteLine(ex);
                throw ex;
            }
            //檢查黑名單 
            double _cntBkl = 0; //卡號為黑名單的筆數
            SqlMod_Chk_Bkl BkL = new SqlMod_Chk_Bkl();//產生sql指令
            SqlParameter[] SqlParamSet = null;
            String sSql; //查詢SQL
            try
            {
                BkL.Exist(sCardID, out sSql, ref SqlParamSet);

                DataTable dt_Bkl = obDB.ParaGetDataTable1(sSql, SqlParamSet);
                if (dt_Bkl != null && dt_Bkl.Rows.Count > 0)
                {
                    DataRow dr_Bkl = dt_Bkl.Rows[0];
                    Double.TryParse(dr_Bkl["CNT"].ToString(), out _cntBkl);
                  Console.WriteLine("符合卡號之黑名單資料筆數為: " + _cntBkl);
                }

                return (_cntBkl > 0) ? true : false;
            }
            catch (Exception ex)
            {
              //  log.Debug(ex);
                throw ex;
            }
            finally
            {
                BkL = null;
                Array.Resize(ref SqlParamSet, 0);
            }
        }


    }
}
