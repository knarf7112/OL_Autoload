﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ALCommon;
using System.Data;
using OL_Autoload_Lib.Sql.AL;
using System.Data.SqlClient;

namespace OL_Autoload_Lib.Controller.ALOL.Bkl_Check
{
    public class BkL_Check
    {
    
        /// <summary>
        /// 檢查該卡號是否為黑名單
        /// </summary>
        /// <returns>true (黑名單) / false (非黑名單)</returns>
        /// <param name="obDB">資料庫物件</param>
        /// <param name="sCardID">卡號</param>
        /// <returns>  true (是黑名單) / false (不是黑名單)</returns>

        public bool AL_CheckBKL(ALCommon.AL_DBModule obDB, String sCardID)
        {
            try
            {
                //判斷資料庫連線有無開啟            
                if (obDB.ALCon.State != ConnectionState.Open)
                {
                    throw new Exception(CSysMsg.DbConnFail.ToString());
                }
            }
            catch (Exception ex)
            {
               Console.WriteLine(ex);
                throw ex;
            }
                     
            SqlMod_Chk_Bkl BkL = new SqlMod_Chk_Bkl();//產生sql指令
            SqlParameter[] SqlParamSet = null;
            String sSql; //查詢SQL
       
            try
            {
                //檢查是否在黑名單
                BkL.Exist(sCardID, out sSql, ref SqlParamSet);

                string result = obDB.SqlExecuteScalarHasParams(sSql, SqlParamSet);
                return (result == "" || result == "0") ? false : true;
                    
            }
            catch (Exception ex)
            {
             
                throw ex;
            }
            finally
            {
                BkL=null;
                Array.Resize(ref SqlParamSet, 0);
            }
        }


    }
}
