﻿using ALCommon;
using OL_Autoload_Lib.Sql.AL;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;

namespace OL_Autoload_Lib.Controller.ALOL.Store_Check
{
  public class STORE_Check
    {
        /// <summary>
        /// 判斷門市是否開放自動加值/是否為有效店
        /// </summary>
        /// <param name="obLWCL">AutoloadRqt參數物件</param>
        /// <param name="obDB">DB連線物件</param>
        /// <returns>0:正常/1:無效店/2:不開放自動加值</returns>

      public String AL_CheckStore(AutoloadRqt obLWCL, AL_DBModule obDB)
      {
          try
          {
              //判斷資料庫連線有無開啟            
              if (obDB.ALCon.State != ConnectionState.Open)
              {
                  throw new Exception(CSysMsg.DbConnFail.ToString());
              }
          }
          catch (Exception ex)
          {
              throw ex;
          }

          SqlMod_Chk_Store CStore = new SqlMod_Chk_Store();
          SqlParameter[] SqlParamSet = null;
          String sSql; //查詢SQL
          try
          {
              string OLType = string.Empty;
              String StoreWork = String.Empty;
             //檢查是否開放自動加值
              CStore.Chk_LoadOptype(obLWCL, out sSql, ref SqlParamSet);
              //string OLType = obDB.SqlExecuteScalarHasParams(sSql, SqlParamSet);//oringe
              //-------------------------------------------------------------
              //將POCO內的Bank_STORE_NO 和 Bank_REG_LEN設定從IM_STOREM_VIEW取得(bala要用)
              //using (SqlDataReader dr = obDB.ExecuteSQL(sSql, SqlParamSet))
              //{
              //    if (dr.HasRows && dr.Read())
              //    {
              //        OLType = dr.GetString(0);
              //        obLWCL.Bank_STORE_NO = dr.GetString(1);
              //        obLWCL.BANK_REG_LEN = dr.GetString(2);
              //    }
              //}
              DataTable dt = obDB.ParaGetDataTable1(sSql, SqlParamSet);
              if (dt.Rows.Count == 0)
              {
                  return "1";
              }
              OLType = dt.Rows[0]["OL_TYPE"].ToString();
              StoreWork = dt.Rows[0]["STORE_WORKS"].ToString();
              //-------------------------------------------------------------
              //return (OLType == "3") ? true : false;             
              if (StoreWork == "N")
                  return "1";
              else
              {
                  switch (OLType)
                  {
                      case "0"://不開放加值
                          return "2";
                      case "1"://僅開放一般加值
                          return "2";
                      case "2"://僅開放自動加值
                          return "0";
                      case "3"://開放一般加值＆自動加值
                          return "0";
                      default:
                          return "2";
                  }
              }
          }
          catch (Exception ex)
          {
              throw ex;
          }
          finally
          {
              CStore=null;
              Array.Resize(ref SqlParamSet, 0);
          }
      }


      /// <summary>
      /// 取得銀行電文StoreNo與RegId長度
      /// </summary>
      /// <param name="obLWCL">AutoloadRqt</param>
      /// <param name="obDB">DB連線物件</param>
      public void AL_GetBankStoreNo(ref AutoloadRqt obLWCL, AL_DBModule obDB)
      {
          try
          {
              //判斷資料庫連線有無開啟            
              if (obDB.ALCon.State != ConnectionState.Open)
              {
                  throw new Exception(CSysMsg.DbConnFail.ToString());
              }
          }
          catch (Exception ex)
          {
              throw ex;
          }

          SqlMod_Chk_Store CStore = new SqlMod_Chk_Store();
          SqlParameter[] SqlParamSet = null;
          String sSql; //查詢SQL
          try
          {
              CStore.Get_BankStoreNo(obLWCL, out sSql, ref SqlParamSet);
              DataTable dt = obDB.ParaGetDataTable1(sSql, SqlParamSet);
              obLWCL.Bank_STORE_NO = dt.Rows[0]["BANK_STORE_NO"].ToString();
              obLWCL.BANK_REG_LEN = dt.Rows[0]["BANK_REG_LEN"].ToString();
          }
          catch (Exception ex)
          {
              throw ex;
          }
          finally
          {
              CStore = null;
              Array.Resize(ref SqlParamSet, 0);
          }
      }


    }
}
