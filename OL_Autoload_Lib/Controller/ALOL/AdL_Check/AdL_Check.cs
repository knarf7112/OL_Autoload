﻿using ALCommon;
using OL_Autoload_Lib.Sql.AL;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;

namespace OL_Autoload_Lib.Controller.ALOL.AdL_Check
{
  public  class AdL_Check
    {
        /// <summary>
        /// 檢查拒絕代行授權名單
        /// </summary>    
        /// <param name="obDB">資料庫物件</param>
        /// <param name="sCardID">卡號</param>
        /// <returns>  true (可進行代行授權) / false (不可代行授權)</returns>
      public bool AL_CheckADL(AL_DBModule obDB, String sCardID)
      {
          try
          {
              //判斷資料庫連線有無開啟            
              if (obDB.ALCon.State != ConnectionState.Open)
              {
                  throw new Exception(CSysMsg.DbConnFail.ToString());
              }
          }
          catch (Exception ex)
          {
              throw ex;
          }
          //檢查黑名單             
          SqlMod_Chk_ADL ADL = null;//產生sql指令
          SqlParameter[] SqlParamSet = null;
          String sSql=""; //查詢SQL

          try
          {
              ADL = new SqlMod_Chk_ADL();
              //檢查是否在拒絕代行授權名單內
              ADL.Exist(sCardID, out sSql, ref SqlParamSet);
              string result = obDB.SqlExecuteScalarHasParams(sSql, SqlParamSet);
              return (result == "" || result == "0") ? true : false;
          }
          catch (Exception ex)
          {
              throw ex;
          }
          finally
          {
              ADL=null;
              Array.Resize(ref SqlParamSet, 0);
          }
      }





    }
}
