﻿using ALCommon;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;

namespace OL_Autoload_Lib
{
    public partial class Network_Msg
    {
        /// <summary>
        /// 更新網路訊息相關Table
        /// </summary>
        /// <param name="netMsgRqt">網路訊息管理電文物件</param>
        /// <param name="merchantNo">特約機構代號</param>
        /// <param name="bankCode">銀行簡碼</param>
        /// <param name="obDB">DBModule物件</param>
        public void Upt_NetMsg(OL_Autoload_Lib.AutoloadRqt_NetMsg netMsgRqt, AL_DBModule obDB)
        {
            try
            {
                sqlMod.Upt_NetMsg(netMsgRqt, out sql, ref sqlParams);
                if (obDB.ALCon.State.ToString() == "Open")
                {
                    obDB.SqlExec1(sql, sqlParams);
                    Array.Resize(ref sqlParams, 0);
                }
                else
                {
                    throw new Exception(CSysMsg.DbConnFail.ToString());
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

    }
}
