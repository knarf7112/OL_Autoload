﻿using ALCommon;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Data;

namespace OL_Autoload_Lib
{
    public partial class Network_Msg
    {            
        //INIT
        String sql = String.Empty;
        SqlParameter[] sqlParams;
        SqlMod sqlMod = new SqlMod();

        /// <summary>
        /// 查詢網路訊息狀態
        /// </summary>
        /// <param name="bankCode">特約機構代號</param>
        /// <param name="obDB">DBModule物件</param>
        /// <returns>true/false</returns>
        public bool NetworkStatu_Check(String bankCode, AL_DBModule obDB)
        {
            try 
            {
                sqlMod.Get_InfoCode(bankCode, out sql, ref sqlParams);
                if (obDB.ALCon.State.ToString() == "Open")
                {
                    DataTable dt = obDB.ParaGetDataTable1(sql, sqlParams);
                    Array.Resize(ref sqlParams, 0);
                    if (dt.Rows[0].ToString() == "072")
                    {
                        return false;
                    }
                    else
                    {
                        return true;
                    }
                }
                else
                {
                    throw new Exception(CSysMsg.DbConnFail.ToString());
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        /// <summary>
        /// 取得網路訊息管理主檔所有資料
        /// </summary>
        /// <param name="obDB">DBModule</param>
        /// <returns>DataTable物件</returns>
        public DataTable Get_NetMsgAll(AL_DBModule obDB)
        {
            try
            {
                sqlMod.Get_NetMsgAll(out sql);
                if (obDB.ALCon.State.ToString() == "Open")
                {
                    DataTable dt = obDB.GetDataTable(sql);
                    Array.Resize(ref sqlParams, 0);                    
                    return dt;
                }
                else
                {
                    throw new Exception(CSysMsg.DbConnFail.ToString());
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


    }
}
