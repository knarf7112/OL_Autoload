﻿using OL_Autoload_Lib.Sql.AL;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;

namespace OL_Autoload_Lib.Controller.ALOL.CardSt_Check
{
   public class CardStatus_Check
    {
        /// <summary>
        /// 檢查該卡號是否為有效卡
        /// </summary>
        /// <returns> 1 :null(查無卡主檔)</returns>
        /// <returns> 2 :CStatus==5(聯名卡停卡),6(卡片掛失),9(已餘額返還)</returns>
        /// <returns> 3 :CStatus==3(鎖卡),4(註銷),7(含iDollar鎖卡),8(餘額轉置鎖卡)</returns>
        /// <returns> 0 :CStatus==other</returns>
        ///
        /// <param name="obDB">資料庫物件</param>
        /// <param name="sCardID">卡號</param>

       public string AL_CheckCardStatus(ALCommon.AL_DBModule obDB, String sCardID)
       {
           try
           {
               //判斷資料庫連線有無開啟            
               if (obDB.ALCon.State != ConnectionState.Open)
               {
                   throw new Exception(CSysMsg.DbConnFail.ToString());
               }
           }
           catch (Exception ex)
           {
              // Console.WriteLine(ex);
               throw ex;
           }
           //檢查黑名單             
           SqlMod_Chk_CardSt CS = new SqlMod_Chk_CardSt();//產生sql指令
           SqlParameter[] SqlParamSet = null;
           String sSql; //查詢SQL
      
           try
           {
               CS.chk_CStatus(sCardID, out sSql, ref SqlParamSet);
               string _CardSt = obDB.SqlExecuteScalarHasParams(sSql, SqlParamSet);
               switch (_CardSt)
             {
                 case "" : return "1";

                 case "5":
                 case "6":
                 case "9": return "2";

                 case "3":
                 case "4":
                 case "7":
                 case "8": return "3";

                 default: return "0";
             }
                            
               //if (_CardSt == "")
               //{
               //    return "1";
               //}
               //else if (_CardSt == "5" || _CardSt == "6" || _CardSt == "9")
               //{

               //    return "2";
               //}
               //else if (_CardSt == "3" || _CardSt == "4" || _CardSt == "7" || _CardSt == "8")
               //{
               //    return "3";
               //}
               //else return "0";

           }
           catch (Exception ex)
           {
               //  log.Debug(ex);
               throw ex;
           }
           finally
           {
               CS=null;
               Array.Resize(ref SqlParamSet, 0);
           }
       }

        /// <summary>
       /// 檢查該卡號在卡主檔狀態
        /// </summary>
       /// <param name="obDB">AL_DBModule</param>
       /// <param name="sCardID">卡號</param>
        /// <param name="_oriCstatus">原卡主檔狀態</param>
        /// <returns>
       /// Return 該卡狀態對應Response Code
       /// 05: 5(聯名卡停卡)
       /// 09: null(查無卡主檔)
       /// 00: CStatus==0(新卡)，1(開卡)，2(流通)
       /// 10: CStatus==3(鎖卡)，7(含iDollar鎖卡)，8(餘額轉置鎖卡)
       /// 12: CStatus==4(換卡停用(註銷))
       /// 14: CStatus==9(已餘額返還)
       /// 15: CStatus==6(卡片掛失)
        /// </returns>
       public String AL_ChkCardSts4RL(ALCommon.AL_DBModule obDB, String sCardID, out String _oriCstatus)
       {
           try
           {
               //判斷資料庫連線有無開啟            
               if (obDB.ALCon.State != ConnectionState.Open)
               {
                   throw new Exception(CSysMsg.DbConnFail.ToString());
               }
           }
           catch (Exception ex)
           {
               // Console.WriteLine(ex);
               throw ex;
           }
           //檢查黑名單             
           SqlMod_Chk_CardSt CS = new SqlMod_Chk_CardSt();//產生sql指令
           SqlParameter[] SqlParamSet = null;
           String sSql; //查詢SQL

           try
           {
               CS.chk_CStatus(sCardID, out sSql, ref SqlParamSet);
               string _CardSt = obDB.SqlExecuteScalarHasParams(sSql, SqlParamSet);
               _oriCstatus = _CardSt;
               switch (_CardSt)
               {
                   case "": return "09";
                   case "0": return "00";
                   case "1": return "00";
                   case "2": return "00";

                   case "3": return "10";
                   case "7": return "10";
                   case "8": return "10";

                   case "4": return "12";
                   case "5": return "05";
                   case "6": return "15";
                   case "9": return "14";
                   default: return "";
               }

           }
           catch (Exception ex)
           {
               //  log.Debug(ex);
               throw ex;
           }
           finally
           {
               CS = null;
               Array.Resize(ref SqlParamSet, 0);
           }
       }

    }
}
