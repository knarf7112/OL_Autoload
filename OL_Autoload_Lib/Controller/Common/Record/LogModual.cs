﻿#define DEBUG
//#undef DEBUG


using ALCommon;
using OL_Autoload_Lib.Sql.Domain;
using OL_Autoload_Lib.Sql.Common;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Diagnostics;
using System.Linq;
using System.Text;

namespace OL_Autoload_Lib.Controller.Common.Record
{
    public partial class LogModual
    {
        /// <summary>
        /// 儲存所有之online交易
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="_trans">ICash</param>
        /// <param name="obDB">資料庫物件</param>
        /// <param name="_leftBal">剩餘額度</param>
        /// <param name="dtrqt">交易起始時間</param>
        /// <param name="dtrsp">交易結束時間</param>
        /// <returns>true(紀錄成功)/false(紀錄失敗)</returns>
        public bool SaveTransLog<T>
            (T _trans, AL_DBModule obDB, String _leftBal, DateTime dtrqt, DateTime dtrsp   ) where T : ICash
        {
            //交易流程時間
            String gapTime = "";
            TimeSpan ts = new TimeSpan(dtrsp.Ticks - dtrqt.Ticks);
            double _gapMSec = ts.TotalMilliseconds >= 10000000000 ?
                9999999999 : System.Math.Round(ts.TotalMilliseconds, 0, MidpointRounding.AwayFromZero);
            gapTime = _gapMSec.ToString();

            String LOG_DATE = System.DateTime.Now.ToString("yyyyMMdd");
            String LOG_TIME = System.DateTime.Now.ToString("yyyyMMddHHmmss");

            String strSql = "";

            if (String.IsNullOrEmpty(_leftBal) == true)
            {
                strSql = @"
INSERT INTO OL_ISET_TRANS_LOG_D
(
LOG_DATE, RETURN_CODE, SN, RESPONSE_TIME	,
M_KIND, MERCHANT_NO, STORE_NO, REG_ID,READER_ID,		
TRANS_TYPE, TRANS_AMT, LOG_TIME
)
VALUES
(
@LOG_DATE, @RETURN_CODE, @SN, @RESPONSE_TIME,
@M_KIND, @MERCHANT_NO, @STORE_NO, @REG_ID, @READER_ID,
@TRANS_TYPE, @TRANS_AMT, @LOG_TIME
)";
            }
            else
            {
                strSql = @"
INSERT INTO OL_ISET_TRANS_LOG_D
(
LOG_DATE, RETURN_CODE, SN, RESPONSE_TIME	,
M_KIND, MERCHANT_NO, STORE_NO, REG_ID,READER_ID,		
TRANS_TYPE, TRANS_AMT, LEFT_BAL,	LOG_TIME
)
VALUES
(
@LOG_DATE, @RETURN_CODE, @SN, @RESPONSE_TIME,
@M_KIND, @MERCHANT_NO, @STORE_NO, @REG_ID, @READER_ID,
@TRANS_TYPE, @TRANS_AMT, @LEFT_BAL, @LOG_TIME
)";
            }

            SqlParameter[] SqlParamSet = new SqlParameter[13];

            try
            {
                for (int i = 0; i < SqlParamSet.Length; i++)
                {
                    SqlParamSet[i] = new SqlParameter();
                    SqlParamSet[i].DbType = DbType.AnsiString;
                }

                SqlParamSet[0].ParameterName = "@LOG_DATE";
                SqlParamSet[0].Value = LOG_DATE;
                SqlParamSet[1].ParameterName = "@RETURN_CODE";
                SqlParamSet[1].Value = String.IsNullOrEmpty(_trans.RETURN_CODE) ? "" : _trans.RETURN_CODE;
                SqlParamSet[2].ParameterName = "@SN";
                SqlParamSet[2].Value = String.IsNullOrEmpty(_trans.SN) ? "" : _trans.SN;
                SqlParamSet[3].ParameterName = "@RESPONSE_TIME";
                SqlParamSet[3].Value = String.IsNullOrEmpty(gapTime) ? "" : gapTime;
                SqlParamSet[4].ParameterName = "@M_KIND";
                SqlParamSet[4].Value = String.IsNullOrEmpty(_trans.M_KIND) ? "" : _trans.M_KIND;
                SqlParamSet[5].ParameterName = "@MERCHANT_NO";
                SqlParamSet[5].Value = String.IsNullOrEmpty(_trans.MERCHANT_NO) ? "" : _trans.MERCHANT_NO;
                SqlParamSet[6].ParameterName = "@STORE_NO";
                SqlParamSet[6].Value = String.IsNullOrEmpty(_trans.STORE_NO) ? "" : _trans.STORE_NO;
                SqlParamSet[7].ParameterName = "@REG_ID";
                SqlParamSet[7].Value = String.IsNullOrEmpty(_trans.REG_ID) ? "" : _trans.REG_ID;
                SqlParamSet[8].ParameterName = "@READER_ID";
                SqlParamSet[8].Value = String.IsNullOrEmpty(_trans.READER_ID) ? "" : _trans.READER_ID.Trim();
                SqlParamSet[9].ParameterName = "@TRANS_TYPE";
                SqlParamSet[9].Value = ((int)_trans.TRANS_TYPE).ToString(); //交易別轉換成String
                SqlParamSet[10].ParameterName = "@TRANS_AMT";
                SqlParamSet[10].Value = Convert.ToDouble(_trans.TRANS_AMT);
                SqlParamSet[11].ParameterName = "@LEFT_BAL";
                //處理剩餘額度
                if (String.IsNullOrEmpty(_leftBal) == true)
                {
                    SqlParamSet[11].Value = 0;
                }
                else
                {
                    SqlParamSet[11].Value = Convert.ToDouble(_leftBal);
                }
                SqlParamSet[12].ParameterName = "@LOG_TIME";
                SqlParamSet[12].Value = LOG_TIME;

                

                //執行Sql
                String sRslt = obDB.SqlExec1(strSql, SqlParamSet);

                return (sRslt == "1" ? true : false);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                Array.Resize(ref SqlParamSet, 0);
            }
        }

        /// <summary>
        /// 紀錄愛金卡對發卡行端交易電文
        /// </summary>
        /// <param name="_trans">IFBANK</param>
        /// <param name="obDB">資料庫物件</param>
        /// <param name="dtrqt">交易起始時間</param>
        /// <param name="dtrsp">交易結束時間</param>
        /// <returns>true(紀錄成功)/false(紀錄失敗)</returns>
        public bool SaveTransLog2Bank(IFBANK _trans, AL_DBModule obDB, DateTime dtrqt, DateTime dtrsp) //where T : IFBANK
        {
            try 
            {
                //交易流程時間
                String gapTime = String.Empty;
                TimeSpan ts = new TimeSpan(dtrsp.Ticks - dtrqt.Ticks);
                double _gapMSec = ts.TotalMilliseconds >= 10000000000 ?
                    9999999999 : System.Math.Round(ts.TotalMilliseconds, 0, MidpointRounding.AwayFromZero);
                gapTime = _gapMSec.ToString();
                //Log 時間
                string _logTime = DateTime.Now.ToString("yyyyMMddHHmmss");
                
                SqlParameter[] SqlParamSet = new SqlParameter[16];
                for (int i = 0; i < SqlParamSet.Length; i++)
                {
                    SqlParamSet[i] = new SqlParameter();
                    SqlParamSet[i].DbType = DbType.AnsiString;
                }
#region SQL參數 
                if (_trans.PROCESSING_CODE == "990174" ||    //自動加值(沖正)
                    _trans.PROCESSING_CODE == "990175")     //代行授權
                {
                    AutoloadRqt_2Bank _transLog = (AutoloadRqt_2Bank)_trans;

                    SqlParamSet[0].ParameterName = "@LOG_DATE";
                    SqlParamSet[0].Value = _logTime.Substring(0, 8);
                    SqlParamSet[1].ParameterName = "@MSG_TYPE";
                    SqlParamSet[1].Value = String.IsNullOrEmpty(_transLog.MESSAGE_TYPE) ? "" : _transLog.MESSAGE_TYPE;
                    SqlParamSet[2].ParameterName = "@RETURN_CODE";
                    SqlParamSet[2].Value = String.IsNullOrEmpty(_transLog.RC) ?"":_transLog.RC;
                    SqlParamSet[3].ParameterName = "@STAN";
                    SqlParamSet[3].Value = String.IsNullOrEmpty(_transLog.STAN) ?"":_transLog.STAN;
                    SqlParamSet[4].ParameterName = "@RESPONSE_TIME";
                    SqlParamSet[4].Value = String.IsNullOrEmpty(gapTime) ? "" : gapTime;
                    SqlParamSet[5].ParameterName = "@MERCHANT_NO";
                    SqlParamSet[5].Value = String.IsNullOrEmpty(_transLog.MERCHANT_NO) ?"": _transLog.MERCHANT_NO;
                    SqlParamSet[6].ParameterName = "@BANK_CODE";
                    SqlParamSet[6].Value = String.IsNullOrEmpty(_transLog.BANK_CODE) ? "" : _transLog.BANK_CODE;
                    SqlParamSet[7].ParameterName = "@STORE_NO";
                    SqlParamSet[7].Value = String.IsNullOrEmpty(_transLog.STORE_NO) ? "" : _transLog.STORE_NO;
                    SqlParamSet[8].ParameterName = "@REG_ID";
                    SqlParamSet[8].Value = String.IsNullOrEmpty(_transLog.ICC_info.REG_ID) ? "" : _transLog.ICC_info.REG_ID;
                    SqlParamSet[9].ParameterName = "@TRANS_TYPE";
                    SqlParamSet[9].Value = String.IsNullOrEmpty(_transLog.PROCESSING_CODE) ? "" : _transLog.PROCESSING_CODE;
                    SqlParamSet[10].ParameterName = "@ICC_NO";
                    SqlParamSet[10].Value = String.IsNullOrEmpty(_transLog.ICC_NO) ? "" : _transLog.ICC_NO;
                    SqlParamSet[11].ParameterName = "@AMT";
                    SqlParamSet[11].Value = String.IsNullOrEmpty(_transLog.AMOUNT) ? "" : _transLog.AMOUNT;
                    SqlParamSet[12].ParameterName = "@TRANS_TIME";
                    SqlParamSet[12].Value = String.IsNullOrEmpty(_transLog.ICC_info.TX_DATETIME) ? "" : _transLog.ICC_info.TX_DATETIME;
                    SqlParamSet[13].ParameterName = "@RRN";
                    SqlParamSet[13].Value = String.IsNullOrEmpty(_transLog.RRN) ? "" : _transLog.RRN;
                    SqlParamSet[14].ParameterName = "@READER_ID";
                    SqlParamSet[14].Value = String.IsNullOrEmpty(_transLog.ICC_info.NECM_ID) ? "" : _transLog.ICC_info.NECM_ID;
                    SqlParamSet[15].ParameterName = "@LOG_TIME";
                    SqlParamSet[15].Value = _logTime;   
                }
                else if (_trans.PROCESSING_CODE == "990176" ||  //連線掛失(取消)
                   _trans.PROCESSING_CODE == "990178")              //拒絕代行授權名單維護
                {
                    //銀行特約機構代號
                    Get_MerchantNO getMerNo = new Get_MerchantNO();
                    string bankMerchantNo = getMerNo.GetMerchantNo_Bank(_trans.BANK_CODE, obDB);

                    AutoloadRqt_FBank _transLog = (AutoloadRqt_FBank)_trans;

                    SqlParamSet[0].ParameterName = "@LOG_DATE";
                    SqlParamSet[0].Value = _logTime.Substring(0, 8);
                    SqlParamSet[1].ParameterName = "@MSG_TYPE";
                    SqlParamSet[1].Value = String.IsNullOrEmpty(_transLog.MESSAGE_TYPE) ? "" : _transLog.MESSAGE_TYPE;
                    SqlParamSet[2].ParameterName = "@RETURN_CODE";
                    SqlParamSet[2].Value = String.IsNullOrEmpty(_transLog.RC) ? "" : _transLog.RC;
                    SqlParamSet[3].ParameterName = "@STAN";
                    SqlParamSet[3].Value = String.IsNullOrEmpty(_transLog.STAN) ? "" : _transLog.STAN;
                    SqlParamSet[4].ParameterName = "@RESPONSE_TIME";
                    SqlParamSet[4].Value = String.IsNullOrEmpty(gapTime) ? "" : gapTime;
                    SqlParamSet[5].ParameterName = "@MERCHANT_NO";
                    SqlParamSet[5].Value = String.IsNullOrEmpty(bankMerchantNo) ? "" : bankMerchantNo;
                    SqlParamSet[6].ParameterName = "@BANK_CODE";
                    SqlParamSet[6].Value = String.IsNullOrEmpty(_transLog.BANK_CODE) ? "" : _transLog.BANK_CODE;
                    SqlParamSet[7].ParameterName = "@STORE_NO";
                    SqlParamSet[7].Value = String.Empty;
                    SqlParamSet[8].ParameterName = "@REG_ID";
                    SqlParamSet[8].Value = String.Empty;
                    SqlParamSet[9].ParameterName = "@TRANS_TYPE";
                    SqlParamSet[9].Value = String.IsNullOrEmpty(_transLog.PROCESSING_CODE) ?"": _transLog.PROCESSING_CODE;
                    SqlParamSet[10].ParameterName = "@ICC_NO";
                    SqlParamSet[10].Value = String.IsNullOrEmpty(_transLog.ICC_NO) ? "" : _transLog.ICC_NO;
                    SqlParamSet[11].ParameterName = "@AMT";
                    SqlParamSet[11].Value = String.Empty;
                    SqlParamSet[12].ParameterName = "@TRANS_TIME";
                    SqlParamSet[12].Value = String.IsNullOrEmpty(_transLog.TRANS_DATETIME) ? "" : _transLog.TRANS_DATETIME;
                    SqlParamSet[13].ParameterName = "@RRN";
                    SqlParamSet[13].Value = String.IsNullOrEmpty(_transLog.RRN) ? "" : _transLog.RRN;
                    SqlParamSet[14].ParameterName = "@READER_ID";
                    SqlParamSet[14].Value = String.Empty;
                    SqlParamSet[15].ParameterName = "@LOG_TIME";
                    SqlParamSet[15].Value = _logTime;
                }
                else
                {
                    throw new Exception(String.Format("非合法交易類別，{0}", _trans.PROCESSING_CODE));
                }
#endregion

                String sql = String.Empty;
                sql = @"INSERT INTO OL_AL_TRANS_LOG_D
(LOG_DATE, MSG_TYPE, RETURN_CODE, STAN, RESPONSE_TIME, 
MERCHANT_NO, BANK_CODE, STORE_NO, REG_ID, TRANS_TYPE,
ICC_NO, AMT, TRANS_TIME, RRN, READER_ID, LOG_TIME)
VALUES
(@LOG_DATE, @MSG_TYPE, @RETURN_CODE, @STAN, @RESPONSE_TIME, 
@MERCHANT_NO, @BANK_CODE, @STORE_NO, @REG_ID, @TRANS_TYPE,
@ICC_NO, @AMT, @TRANS_TIME, @RRN, @READER_ID, @LOG_TIME)";

                //執行Sql
                String sRslt = obDB.SqlExec1(sql, SqlParamSet);
                Array.Resize(ref SqlParamSet, 0);

                return (sRslt == "1" ? true : false);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

       
        /// <summary>
        /// 寫最外層Error Log到資料庫
        /// </summary>
        /// <param name="errorMessage">exception</param>
        /// <param name="myDBModule">資料庫物件</param>
        /// <param name="serviceName">服務名稱</param>
        public void SaveErrorLog( AL_DBModule myDBModule,string errorMessage,string serviceName)
        {


            using (ALErrorLogDAO _logger = new ALErrorLogDAO(myDBModule.ALCon, serviceName, TraceEventType.Error))
            {
                _logger.WriteLog(String.Format("{0}", errorMessage));
            }
            //myDBModule.CloseConnection(); //關閉連線

        }

        class ALErrorLogDAO : IDisposable
        {
            /// <summary>
            /// Timeout for sql query
            /// </summary>
            private readonly int SQL_CMD_TIMEOUT = 10; //10 sec for Timeout

            /// <summary>
            /// Dispose Flag
            /// </summary>
            private static bool isDisposed = false;

            /// <summary>
            /// SqlConnection
            /// </summary>
            private SqlConnection sqlConnection = new SqlConnection();

            
            OL_ISET_ERR_LOG_D  ELoggerDomain; 



            /// <summary>
            /// 建構子
            /// </summary>
            public ALErrorLogDAO(SqlConnection _obdb, string title, TraceEventType eventType)
            {
                sqlConnection = _obdb;
                ELoggerDomain = new OL_ISET_ERR_LOG_D();
                //Default
                ELoggerDomain.PRIORITY = 0;
                ELoggerDomain.SEVERITY = eventType;
                ELoggerDomain.TITLE = title;

                //檢查連線
                if (sqlConnection.State != ConnectionState.Open)
                {
                    throw new Exception("資料庫連線沒有開啟! 無法寫入紀錄!");
                }
            }

           

            /// <summary>
            /// 寫Log到資料庫
            /// </summary>
            public void WriteLog(string errorMessage)
            {
                ELoggerDomain.LOG_DATE = DateTime.Now.ToString("yyyyMMdd");
                ELoggerDomain.LOG_TIME = DateTime.Now;
                ELoggerDomain.MSG = errorMessage;

                //檢核欄位
                if (
                    (String.IsNullOrEmpty(ELoggerDomain.TITLE) == true ||
                    ELoggerDomain.TITLE.Length > 50) ||
                    (ELoggerDomain.MSG.Length > 500) ||
                     (ELoggerDomain.SEVERITY.ToString().Length > 50)
                    )
                {
                    throw new Exception("寫入LOG的欄位長度不正確!");
                }

                SqlMod_SAVE_ERR_LOG saveError =null;
                SqlParameter[] SqlParam=null;
                string iSql = "";             
                SqlCommand cmdGeneral = null;
                try
                {
                    saveError = new SqlMod_SAVE_ERR_LOG();
                    saveError.insert(ref ELoggerDomain, out iSql, ref SqlParam);
                    cmdGeneral = new SqlCommand(iSql, sqlConnection);
                    cmdGeneral.CommandTimeout = SQL_CMD_TIMEOUT;
                    for (int i = 0; i < SqlParam.Length; i++)
                    {
                        cmdGeneral.Parameters.AddWithValue(SqlParam[i].ParameterName, SqlParam[i].Value);
                    }

                    double Result = (double)cmdGeneral.ExecuteNonQuery();

                    return;
                }
                catch (Exception ex)
                {
                    throw ex;
                }
                finally
                {
                    saveError = null;
                    cmdGeneral=null;
                    Array.Resize(ref SqlParam, 0);

                }
            }







            /// <summary>
            /// 讀取資料庫中的LOG
            /// </summary>
            /// <returns></returns>
            public Dictionary<String, String> ReadLog()
            {
                throw new NotImplementedException();
            }

            /// <summary>
            /// Dispose
            /// </summary>
            public virtual void Dispose()
            {
                if (isDisposed == false)
                {
                    //Do dispose
                    isDisposed = true;
                }
            }




        } 


    }
}
