﻿#define Debug
//#undef Debug

using ALCommon;
using OL_Autoload_Lib.Sql.Common;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;

namespace OL_Autoload_Lib.Controller.Common.Record
{
    public partial class LogModual
    {

        /// <summary>
        /// 紀錄每次online txlog匯入後的結果資料 
        /// </summary>
        /// <param name="obDB">資料庫處理物件</param>
        /// <param name="cTxlog">online txlog物件</param>
        /// <param name="dtRequest">要求的DateTime</param>
        /// <param name="dtResponse">回應的DateTime</param>
        /// <returns>true(成功)/false(失敗)</returns>
        public bool SaveTolLog(AL_DBModule obDB, CTxlog cTxlog, DateTime dtRequest, DateTime dtResponse)
        {
            //判斷連線
            try
            {
                //判斷資料庫連線有無開啟            
                if (obDB.ALCon.State != ConnectionState.Open)
                {
                    throw new Exception(CSysMsg.DbConnFail.ToString());
                }
            }
            catch (Exception ex)
            {
                //  Console.WriteLine(ex);
                throw ex;
            }

            SqlMod_SAVE_Txlog_Result obTxLog = null;
            SqlParameter[] SqlParams = null;

            try
            {

                obTxLog = new SqlMod_SAVE_Txlog_Result();
                //兩日期相隔間隔
                TimeSpan ts = new TimeSpan(dtResponse.Ticks - dtRequest.Ticks);
                double _gapMSec = ts.TotalMilliseconds >= 10000000000 ?
                    9999999999 : System.Math.Round(ts.TotalMilliseconds, 0, MidpointRounding.AwayFromZero);

                //紀錄到明細
               string isql = "";

                obTxLog.SaveTolLogResult(cTxlog, _gapMSec.ToString(), out isql, ref SqlParams);
                string result = obDB.SqlExec1(isql, SqlParams);
                return (result == "1") ? true : false;
            }
            catch (Exception ex)
            {
                //記錄到DB
                LogModual logerror;
                try
                {
                    logerror = new LogModual();
                    logerror.SaveErrorLog(obDB, ex.Message,"TAOL");
                }
                catch
                {
                    //記錄到DB失敗,改存到檔案裡
                    AL_LogModule Log2file;

                    try
                    {

                        Log2file = new AL_LogModule();
                        Log2file.log2file(ex.Message);
                    }
                    catch
                    {
                        throw ex;
                    }
                    finally { Log2file = null; }
                }
                finally
                {
                    logerror = null;
                }

                return false;

            }
            finally
            {
                obTxLog=null;
                Array.Resize(ref SqlParams, 0);
            }
        }




    }
}
