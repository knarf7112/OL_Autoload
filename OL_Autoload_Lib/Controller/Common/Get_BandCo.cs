﻿using ALCommon;
using OL_Autoload_Lib.Sql.Common;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;

namespace OL_Autoload_Lib.Controller.Common
{
   public  class Get_BandCo
    {

        /// <summary>
        /// 取得銀行代號
        /// </summary>
        /// <param name="MerchNO">特約機構代號</param>
        /// <param name="obDB">資料庫物件</param>
        /// <returns>BankCode</returns>
       public string AL_Get_BankCode(AL_DBModule obDB, String MerchNO)
       {
           try
           {
               //判斷資料庫連線有無開啟            
               if (obDB.ALCon.State != ConnectionState.Open)
               {
                   throw new Exception(CSysMsg.DbConnFail.ToString());
               }
           }
           catch (Exception ex)
           {
               throw ex;
           }


           String sSql = ""; //Sql
           SqlParameter[] sqlParams = null;    
           SqlMod_Get_BankCode getBankCo = new SqlMod_Get_BankCode();
           try
           {
               //取得BankCode
               getBankCo.GetBankCode(MerchNO, out sSql, ref sqlParams);
             string bc=obDB.SqlExecuteScalarHasParams(sSql, sqlParams);
             return bc;
           }
           catch (Exception ex)
           {
               throw ex;
           }
           finally
           {
               getBankCo=null;
               Array.Resize(ref sqlParams, 0);
           }


       }

       /// <summary>
       /// 取得Bank Code(愛金卡2發卡行)
       /// </summary>
       /// <param name="obDB">資料庫物件</param>
       /// <param name="iccNo">卡號</param>
       /// <returns>銀行代碼</returns>
       public String AL_Get_BankCode2Bank(AL_DBModule obDB, String iccNo)
       {
           try
           {
               String sql = ""; //Sql
               SqlParameter[] sqlParams = null; 
               SqlMod_Get_BankCode sqlMod = new SqlMod_Get_BankCode();
               sqlMod.GetBankCode2Bank(iccNo, out sql, ref sqlParams);

               if (obDB.ALCon.State.ToString() == "Open")
               {
                   string bc = obDB.SqlExecuteScalarHasParams(sql, sqlParams);
                   Array.Resize(ref sqlParams, 0);
                   return bc;
               }
               else
               {
                   throw new Exception(CSysMsg.DbConnFail.ToString());
               }
           }
           catch (Exception ex)
           {
               throw ex;
           }
       }


    }
}
