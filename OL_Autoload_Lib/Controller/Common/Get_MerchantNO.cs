﻿using ALCommon;
using OL_Autoload_Lib.Sql.AL;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;

namespace OL_Autoload_Lib.Controller.Common
{
   public class Get_MerchantNO
    {

        /// <summary>
        /// 取得特約機構代號
        /// </summary>
        /// <param name="mKind">M_KIND</param>
        /// <param name="merchTimd">MERCH_TIMD</param>
        /// <param name="obDB">資料庫物件</param>
        /// <param name="merchantNo">特約機構代號</param>
       public void GetMerchantNo(String mKind, String merchTimd, AL_DBModule obDB, out String merchantNo)
       {

           try
           {
               //判斷資料庫連線有無開啟            
               if (obDB.ALCon.State != ConnectionState.Open)
               {
                   throw new Exception(CSysMsg.DbConnFail.ToString());
               }
           }
           catch (Exception ex)
           {
               throw ex;
           }




           String sSql = ""; //Sql
           SqlParameter[] sqlParams = null;
     

           SqlMod_Chk_Store sqlMod = new SqlMod_Chk_Store();
try
           {
               //取得GetMerchantNo
               sqlMod.GetMerchantNo(mKind, merchTimd, out sSql, ref sqlParams);                  
               merchantNo = obDB.SqlExecuteScalarHasParams(sSql, sqlParams);    
           }
           catch (Exception  ex)
           {
               throw ex;
           }
           finally
           {
               sqlMod=null;
            Array.Resize(ref sqlParams, 0);
           }
       }

       /// <summary>
       /// 取得特約機構代號(Bank Code)
       /// </summary>
       /// <param name="bankCode">銀行簡碼</param>
       /// <param name="obDB">資料庫物件</param>
       /// <returns>特約機構代號</returns>
       public String GetMerchantNo_Bank(String bankCode, AL_DBModule obDB)
       {
           try
           {
               String sql = String.Empty;
               SqlParameter[] sqlParams = null;
               SqlMod_Chk_Store sqlMod = new SqlMod_Chk_Store();
               sqlMod.GetMerchantNo_Bank(bankCode, out sql, ref sqlParams);

               if (obDB.ALCon.State.ToString() == "Open")
               {
                   String _merchantNo = obDB.SqlExecuteScalarHasParams(sql, sqlParams);
                   Array.Resize(ref sqlParams, 0);
                   return _merchantNo;
               }
               else
               {
                   throw new Exception(CSysMsg.DbConnFail.ToString());
               }
           }
           catch (Exception ex)
           {
               throw ex;
           }
       }

    }
}
