﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace OL_Autoload_Lib
{
    /// <summary>
    /// ICash介面 網路管理訊息
    /// </summary>
    public interface INETMSG
    {
        /// <summary>
        /// Message Type ID
        /// </summary>
        String MESSAGE_TYPE { get; set; }
        /// <summary>
        /// Transmission Date and Time
        /// MMDDhhmmss
        /// </summary>
        String TRANS_DATETIME { get; set; }
        /// <summary>
        /// Systems Trace Audit Number
        /// </summary>
        String STAN { get; set; }
        /// <summary>
        /// Response Code
        /// </summary>
        String RC { get; set; }
        /// <summary>
        /// Network management information code
        /// </summary>
        String INFO_CODE { get; set; }
        /// <summary>
        /// 銀行簡碼
        /// </summary>
        String BANK_CODE { get; set; }
    }

    /// <summary>
    /// ICash介面 From Bank Message
    /// </summary>
    public interface IFBANK
    {
        /// <summary>
        /// Message Type ID
        /// </summary>
        String MESSAGE_TYPE { get; set; }
        /// <summary>
        /// 卡號 Primary Account Number
        /// </summary>
        String ICC_NO { get; set; }
        /// <summary>
        /// Trans type 
        /// </summary>
        String PROCESSING_CODE { get; set; }
        /// <summary>
        /// Transmission Date and Time
        /// MMDDhhmmss
        /// </summary>
        String TRANS_DATETIME { get; set; }
        /// <summary>
        /// Systems Trace Audit Number
        /// </summary>
        String STAN { get; set; } 
        /// <summary>
        /// Retrieval Reference Number
        /// 檢索參考編號
        /// </summary>
        String RRN { get; set; }
        /// <summary>
        /// Response Code
        /// </summary>
        String RC { get;set; }
        /// <summary>
        /// 銀行簡碼
        /// </summary>
        String BANK_CODE { get; set; }
    }

}
