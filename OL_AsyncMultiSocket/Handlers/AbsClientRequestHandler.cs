﻿using System;
using System.ComponentModel;
using System.Net.Sockets;
//
using Common.Logging;
//
using AsyncMultiSocket.State;
using AsyncMultiSocket.Handlers.State;
using System.Collections.Generic;
using AsyncMultiSocket.Handlers.State.Entities;
//using MqTest.Consumer;
using Newtonsoft.Json;
using OL_Autoload_Lib;
using System.Reflection;
using System.Data;
using System.Threading;
using ActiveMqLab.Consumer;


namespace AsyncMultiSocket.Handlers
{
    public abstract class AbsClientRequestHandler : IClientRequestHandler
    {

        #region Static object
        private static readonly ILog log = LogManager.GetLogger(typeof(AbsClientRequestHandler));
        public static bool isInitialDicBanks = false;
        /// <summary>
        /// 註冊MQ的Subscriber靜態集合,在AsyncMultiSocketServer做初始化.
        /// </summary>
        public static IDictionary<string, TopicSubscriber> DicTopicSubscriber = new Dictionary<string, TopicSubscriber>();
        /// <summary>
        /// 銀行連線狀態紀錄靜態集合(對應到DB的NETWORK_M表格)
        /// </summary>
        public static IDictionary<string, NETWORK_MSG> DicBanks = new Dictionary<string, NETWORK_MSG>();
        /// <summary>
        /// 連線銀行端的IP和Port設定與逾時設定,[Key]BankCode+MessageType , [Value]IP+Port:SendTimeout:ReceiveTimeout
        /// </summary>
        public static IDictionary<string, string> toBankConfigList = new Dictionary<string, string>();

        private static object toBankConfigLock = new object();
        private static object cLock = new object();

        private static int totalClient = 0;

        private static object totalLock = new object();
        #endregion

        #region Property
        /// <summary>
        ///   socket client 識別號碼   
        /// </summary>
        public int ClientNo { get; set; }

        /// <summary>
        ///   socket client reuqest
        /// </summary>
        public virtual Socket ClientSocket { get; set; }

        /// <summary>
        ///   socket server
        /// </summary>
        public virtual ISocketServer SocketServer { get; set; }

        public virtual IState ServiceState { get; set; }

        public virtual bool KeepService { get; set; }

        public virtual byte[] MsgBytes { get; set; }

        /// <summary>
        ///   background worker 
        /// </summary>
        protected BackgroundWorker backgroundWorker { get; set; }

        protected int readTimeout = 30000; // Set to 30000 ms
        protected int writeTimeout = 30000; // Set to 30000 ms
        #endregion

        #region public Static Method
        /// <summary>
        /// 初始化靜態集合DicBanks,從DB取得銀行最後更新狀態
        /// </summary>
        public static void InitDicBanks()
        {
            try
            {
                #region 0.檢查DicBanks初始狀態
                //if (AbsClientRequestHandler.isInitialDicBanks) return;
                //init dictionary
                if (AbsClientRequestHandler.DicBanks != null && AbsClientRequestHandler.DicBanks.Count > 0)
                {
                    AbsClientRequestHandler.DicBanks.Clear();
                }
                if (AbsClientRequestHandler.DicBanks == null)
                {
                    AbsClientRequestHandler.DicBanks = new Dictionary<string, NETWORK_MSG>();
                }
                #endregion

                #region 1.取得資料物件列表
                //建立DB模組
                ALCommon.AL_DBModule obDB = new ALCommon.AL_DBModule();
                //建立Network_Msg模組
                Network_Msg network_Msg_Module = new Network_Msg();
                //開啟連線
                obDB.OpenConnection();
                //從DB取得銀行連線狀態的table
                DataTable dt = network_Msg_Module.Get_NetMsgAll(obDB);
                //關閉連線
                obDB.CloseConnection();
                //建立對應用的字典集合key=物件屬性名稱/value=Row的Column名稱
                //Dictionary<string,string> dic = new Dictionary<string,string>();

                //dic.Add("","BANK_CODE");
                //dic.Add("","STATUS");
                //dic.Add("","INFO_CODE");
                ////dic.Add("","REMARK");
                //dic.Add("","MOD_TIME");

                //將DataTable Mapping成List<NETWORK_MSG>資料物件列表
                IList<NETWORK_MSG> bankStatus = Extensions.ToList<NETWORK_MSG>(dt);
                #endregion

                #region 2.初始化DicBanks內容
                foreach (NETWORK_MSG item in bankStatus)
                {
                    string key = item.BANK_CODE.PadLeft(4, '0');//key = 銀行代碼(3碼->4碼),Value = NETWORK_MSG Object
                    AbsClientRequestHandler.DicBanks.Add(key, item);
                    log.Debug(">> AbsClientRequestHandler.DicBanks[" + key + "] => \n" + item.ToString());
                }
                //表示已初始化完畢
                AbsClientRequestHandler.isInitialDicBanks = true;
                #endregion
            }
            catch (Exception ex)
            {
                log.Error(">> [AbsClientRequestHandler][InitDicBanks] Error:" + ex.ToString());
                AbsClientRequestHandler.isInitialDicBanks = false;
            }
        }

        /// <summary>
        /// 註冊Sign On/Off
        /// </summary>
        public static void InitRegisterSignON_OFF()
        {
            try
            {
                //Init
                if (AbsClientRequestHandler.DicTopicSubscriber == null)
                {
                    AbsClientRequestHandler.DicTopicSubscriber = new Dictionary<string, TopicSubscriber>();
                }

                #region 1.檢查銀行狀態列表
                if (!AbsClientRequestHandler.isInitialDicBanks)
                {
                    AbsClientRequestHandler.InitDicBanks();
                }
                #endregion

                #region
                /* 目前直接在集合內檢查
                foreach (string item in AbsClientRequestHandler.DicTopicSubscriber.Keys)
                {
                    
                    MessageReceivedDelegate<string> dele = GetObjectDele<TopicSubscriber, MessageReceivedDelegate<string>>(DicTopicSubscriber[item], "OnMessageReceived");
                    if (dele.GetInvocationList().Length == 0)
                    {
                        //表示沒註冊過
                        
                    }
                    else if (dele.GetInvocationList().Length > 1)
                    {
                        //表示註冊超過一次
                    }
                }
                */
                #endregion

                #region 2.檢查並註冊ActiveMQ上的某個服務通道並記錄到集合內
                //取得MQ的URL
                string brokerURL = System.Configuration.ConfigurationManager.AppSettings["BrokerURL"];
                foreach (string item in AbsClientRequestHandler.DicBanks.Keys)
                {
                    string destination = item + "0800";// 0822(銀行代碼) + 0800(SignOn/Off)
                    if (!AbsClientRequestHandler.DicTopicSubscriber.ContainsKey(destination))
                    {
                        #region 舊的
                        /*
                        //設定SignOn/Off事件要做的事
                        MessageReceivedDelegate<string> signEvent =
                            ReceiveString =>
                            {
                                try
                                {
                                    //接收發佈者傳來的Json物件
                                    AutoloadRqt_NetMsg receiveObj = JsonConvert.DeserializeObject<AutoloadRqt_NetMsg>(ReceiveString);
                                    lock (cLock)
                                    {
                                        string bankCo = receiveObj.BANK_CODE.PadLeft(4, '0');
                                        //表示銀行可連線
                                        if (receiveObj.INFO_CODE == "071")
                                        {
                                            //將銀行物件內的狀態改變,key=銀行代號
                                            AbsClientRequestHandler.DicBanks[bankCo].STATUS = "0";
                                        }
                                        //表示銀行不可連線
                                        else if (receiveObj.INFO_CODE == "072")
                                        {
                                            //將銀行物件內的狀態改變
                                            AbsClientRequestHandler.DicBanks[bankCo].STATUS = "1";
                                        }
                                        AbsClientRequestHandler.DicBanks[bankCo].INFO_CODE = receiveObj.INFO_CODE;
                                        AbsClientRequestHandler.DicBanks[bankCo].MOD_TIME = receiveObj.TRANS_DATETIME;
                                    }
                                }
                                catch (Exception ex)
                                {
                                    log.Error(">> [Event]Subscriber Failed:" + ex.ToString());
                                }
                            };
                        */
                        #endregion
                        //subscriber設定URL,destination和委派的靜態方法
                        TopicSubscriber subscriber = RegisterTopicSubscriber(brokerURL, destination, SignOn_Off_Event);

                        //加入靜態訂閱者集合內
                        DicTopicSubscriber.Add(destination, subscriber);
                    }
                    else
                    {
                        //此destination存在於DicTopicSubscriber集合內
                        //檢查此物件的註冊數量
                        TopicSubscriber ts = AbsClientRequestHandler.DicTopicSubscriber[destination];//取得物件
                        int eventCount = AbsClientRequestHandler.CheckMessageReceivedDelegateCount(ts);//檢查註冊數量
                        //沒有註冊過事件
                        if (eventCount == 0)
                        {
                            ts.OnMessageReceived += SignOn_Off_Event;
                        }
                    }
                }

                #endregion

            }
            catch (Exception ex)
            {
                log.Error(">> [AbsClientRequestHandler][InitRegisterSignON_OFF] Error:" + ex.ToString());
            }
        }

        /// <summary>
        /// 設定SignOn/Off事件要做的事
        /// </summary>
        /// <returns>invoke method</returns>
        private static void SignOn_Off_Event(string receiveString)
        {
            //設定SignOn/Off事件要做的事
            try
            {
                lock (cLock)
                {
                    //接收發佈者傳來的Json物件
                    AutoloadRqt_NetMsg receiveObj = JsonConvert.DeserializeObject<AutoloadRqt_NetMsg>(receiveString);

                    string bankCo = receiveObj.BANK_CODE.PadLeft(4, '0');
                    NETWORK_MSG bankSignMsg = AbsClientRequestHandler.DicBanks[bankCo];
                    //表示銀行可連線
                    if (receiveObj.INFO_CODE == "071")
                    {
                        //將銀行物件內的狀態改變,key=銀行代號
                        bankSignMsg.STATUS = "0";
                    }
                    //表示銀行不可連線
                    else if (receiveObj.INFO_CODE == "072")
                    {
                        //將銀行物件內的狀態改變
                        bankSignMsg.STATUS = "1";
                    }
                    bankSignMsg.INFO_CODE = receiveObj.INFO_CODE;
                    bankSignMsg.MOD_TIME = receiveObj.TRANS_DATETIME;
                    log.Debug(">> [" + bankSignMsg.BANK_CODE + "] Bank Status:" + bankSignMsg.STATUS + " Changed at:" + bankSignMsg.MOD_TIME);
                }
            }
            catch (Exception ex)
            {
                log.Error(">> [Event]Subscriber Failed:" + ex.ToString());
            }
        }

        /// <summary>
        /// 檢查註冊TopicSubscriber的OnMessageReceived事件被註冊的數量
        /// </summary>
        /// <param name="topicSubscriber">實作的TopicSubscriber物件</param>
        /// <returns>被註冊的數量</returns>
        public static int CheckMessageReceivedDelegateCount(TopicSubscriber topicSubscriber)
        {
            try
            {
                MessageReceivedDelegate<string> dele = GetObjectDele<TopicSubscriber, MessageReceivedDelegate<string>>(topicSubscriber, "OnMessageReceived");
                return dele.GetInvocationList().Length;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Register and Start TopicSubscriber
        /// </summary>
        /// <param name="brokerURL">ActiveMQ URL</param>
        /// <param name="destination">ActiveMQ Column Name</param>
        /// <param name="dele">delegate object</param>
        /// <returns>TopicSubscriber(Registered and Started )</returns>
        public static TopicSubscriber RegisterTopicSubscriber(string brokerURL, string destination, MessageReceivedDelegate<string> dele)
        {
            TopicSubscriber subscriber = new TopicSubscriber(brokerURL, destination);
            subscriber.Start();
            subscriber.OnMessageReceived += dele;
            return subscriber;
        }

        /// <summary>
        /// 用來檢查物件的註冊事件數量
        /// 利用傳回物件的委派使用GetInvocationList().Length取得註冊事件的數量
        /// </summary>
        /// <typeparam name="T">物件類別</typeparam>
        /// <typeparam name="DeleType">委派型別</typeparam>
        /// <param name="obj">instance的物件</param>
        /// <param name="eventName">事件的名稱</param>
        /// <returns>物件實作的委派</returns>
        public static DeleType GetObjectDele<T, DeleType>(T obj, string eventName)
        {
            try
            {
                //取得輸入物件型態
                Type objType = obj.GetType();
                //取得此物件型別的指定field資訊(域資訊),[目前是取某event]
                FieldInfo fieldInfo = objType.GetField(eventName, System.Reflection.BindingFlags.GetField
                    | System.Reflection.BindingFlags.NonPublic
                    | System.Reflection.BindingFlags.Instance);
                //從field資訊內找出丟入的instance物件,[丟回委派]
                DeleType objdele = (DeleType)fieldInfo.GetValue(obj);
                return objdele;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// 從靜態集合取得所有銀行目前紀錄的狀態
        /// </summary>
        public static void GetBankStatusToLog()
        {
            log.Debug("銀行目前狀態列表:");
            foreach (string item in AbsClientRequestHandler.DicBanks.Keys)
            {
                log.Debug(AbsClientRequestHandler.DicBanks[item].ToString());
            }
        }

        /// <summary>
        /// 取得設定檔內到銀行端門神的IP和Port和逾時設定
        /// </summary>
        /// <param name="key">BankCode+MessageType(4 + 4 bytes)</param>
        /// <returns>127.0.0.1(ip):6105(port):3000(送出逾時):3000(接收逾時)</returns>
        public static string GetToBankConfig(string key)
        {
            
            if (toBankConfigList.ContainsKey(key))
            {
                return toBankConfigList[key];
            }
            else
            {
                lock (toBankConfigLock)
                {
                    try
                    {
                        log.Debug("取設定檔的Key:" + key);
                        string config = System.Configuration.ConfigurationManager.AppSettings[key];
                        if (!toBankConfigList.ContainsKey(key))
                            toBankConfigList.Add(key, config);
                    }
                    catch (Exception ex)
                    {
                        log.Error("[AbsClientRequestHandler][GetToBankConfig] Error:" + ex.ToString());
                        return "0.0.0.0:0:100:100";//ip:port:sendTimeout:receiveTimeout
                    }
                }
                return toBankConfigList[key];
            }
        }
        #endregion

        #region Public Method 繼承IClientRequestHandler介面的實作方法
        public void DoCommunicate()
        {
            //觸發backgroundWorker的DoWork事件去委派執行backgroundWorker_DoWork方法
            if (!this.backgroundWorker.IsBusy)
            {
                this.backgroundWorker.RunWorkerAsync();
            }
            else
            {
                Thread.Yield();
                this.DoCommunicate();
            }
        }

        public void CancelAsync()
        {
            //backgroundworker is running
            if (this.backgroundWorker.IsBusy)
            {
                this.backgroundWorker.CancelAsync();
                if (this.ClientSocket != null)
                {
                    try
                    {
                        this.ClientSocket.Shutdown(SocketShutdown.Both);
                        this.ClientSocket.Close();
                        this.ClientSocket = null;
                    }
                    catch (SocketException ex)
                    {
                        log.Error(">> [AbsClientRequestHandler][CancelAsync]Client " + this.ClientNo + " Socket Close Failed: " + ex.Message);
                    }
                }
            }
        }
        #endregion

        #region Event 用BackgroundWorker來分送任務
        public virtual void backgroundWorker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            try
            {
                if (e.Error == null)
                {
                    if (e.Cancelled)
                    {
                        //Console.WriteLine(">> [" + e.Result.ToString() + "] Client " + this.ClientNo + " Task Cancelled!");
                        log.Error(">> [" + e.Result.ToString() + "] Client " + this.ClientNo + " Task Cancelled!");
                    }
                    else
                    {
                        //Console.WriteLine(">> [" + e.Result.ToString() + "] Client " + this.ClientNo + " Task completed!");
                        //log.Info(">> [" + e.Result.ToString() + "] Client " + this.ClientNo + " Task completed!");
                    }
                }
                else
                {
                    log.Error(">> [backgroundWorker_RunWorkerCompleted][" + e.Result.ToString() + "] Client " + this.ClientNo + " Task failed:" + e.Error.ToString());
                }
                lock (totalLock)
                {
                    totalClient--;
                }
                log.Debug(">> [backgroundWorker_RunWorkerCompleted]Total Client:" + totalClient);
            }
            catch (Exception ex)
            {
                log.Error("[AbsClientRequestHandler][backgroundWorker_RunWorkerCompleted] Error:" + ex.ToString());
            }
        }

        public virtual void backgroundWorker_DoWork(object sender, DoWorkEventArgs e)
        {
            lock (totalLock)
            {
                totalClient++;
            }
            e.Result = this.ServiceState.GetType().Name;
            DateTime startTime = DateTime.Now;
            while (this.KeepService)
            {
                this.ServiceState.Handle(this);
            }
            DateTime endTime = DateTime.Now;
            log.Debug(">> [" + e.Result.ToString() + "] Task Spend:" + (endTime - startTime).TotalMilliseconds.ToString() + "ms");
        }
    }
        #endregion
}

