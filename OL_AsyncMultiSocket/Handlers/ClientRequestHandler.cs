﻿using AsyncMultiSocket.Handlers.State;
using AsyncMultiSocket.Handlers.State.Entities;
using AsyncMultiSocket.State;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Net.Sockets;

namespace AsyncMultiSocket.Handlers
{
    public class ClientRequestHandler : AbsClientRequestHandler
    {
        #region Constructor
        public ClientRequestHandler(int clientNo,Socket socket4Client,ISocketServer socketServer,IState state)
        {
            //init backgroundworker
            this.backgroundWorker = new BackgroundWorker()
            {
                WorkerSupportsCancellation = true
            };
            this.backgroundWorker.DoWork += new DoWorkEventHandler(backgroundWorker_DoWork);
            this.backgroundWorker.RunWorkerCompleted += new RunWorkerCompletedEventHandler(backgroundWorker_RunWorkerCompleted);
            //init variable
            this.ClientNo = clientNo;
            this.ClientSocket = socket4Client;
            this.SocketServer = socketServer;
            //this.MainSocket.SendTimeout = this.writeTimeout;
            //this.MainSocket.ReceiveTimeout = this.readTimeout;
            this.KeepService = true;
            this.ServiceState = state;
            //init logger
            //if (log == null)
            //{
            //    log = LogManager.GetLogger(typeof(ClientRequestHandler));
            //}
        }

        public ClientRequestHandler(int clientNo, Socket socket4Client, ISocketServer socketServer):this(clientNo,socket4Client,socketServer,new State_AutoLoad())
        {

        }
        public ClientRequestHandler()
            : this(-1, null, null)
        {

        }
        #endregion
    }
}
