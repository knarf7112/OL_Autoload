﻿using System.Collections.Generic;
//
using MqTest.Consumer;
using System;
using Common.Logging;
using System.Net.NetworkInformation;
using System.Text;
using Newtonsoft.Json;

namespace AsyncMultiSocket.Handlers.State
{
    /// <summary>
    /// 訂閱者管理員,接收發佈者的訊息後,執行動作
    /// </summary>
    public class State_BankStatusSubScriber: IState
    {
        private static readonly ILog log = LogManager.GetLogger(typeof(State_BankStatusSubScriber));

        private static IDictionary<string, TopicSubscriber> DicTopicSubscriber;

        protected string BrokerURL { get; set; }
        //protected string Destination { get; set; }

        private object lockObj = new object();

        /// <summary>
        /// 初始化靜態物件
        /// </summary>
        public State_BankStatusSubScriber()
        {
            if (DicTopicSubscriber == null)
            {
                DicTopicSubscriber = new Dictionary<string, TopicSubscriber>();
            }
        }

        public void Handle(AbsClientRequestHandler absClientRequestHandler)
        {
            try
            {
                byte[] receiveBytes = new byte[1024];
                int receiveLength = absClientRequestHandler.ClientSocket.Receive(receiveBytes,receiveBytes.Length,System.Net.Sockets.SocketFlags.None);
                if (receiveLength == 0)
                {
                    return;
                }
                if (receiveLength > 0)
                {
                    log.Debug(">> State Bank Start ...");
                    //取得要註冊MQ的哪種服務目的
                    string destination = Encoding.UTF8.GetString(receiveBytes, 0, receiveLength);
                    //log.Debug(">> Bank Status :" + bankAlive.ToString());
                    this.BrokerURL = "tcp://10.27.68.155:61616";//目前使用Server的MQ
                    //this.Destination = destination;
                    //是否有註冊服務
                    bool isReg = false;
                    foreach (var item in DicTopicSubscriber.Keys)
                    {
                        if (item == destination)
                        {
                            isReg = true;
                            break;
                        }
                    }
                    //若沒註冊過,則註冊到靜態dictionary
                    if (!isReg)
                    {
                        TopicSubscriber subscriber = new TopicSubscriber(this.BrokerURL, destination) { };
                        subscriber.Start();
                        subscriber.OnMessageReceived += (
                            ReceiveString =>
                            {
                                object receiveObj = JsonConvert.DeserializeObject<object>(ReceiveString);
                                switch (receiveObj.ToString())
                                {
                                    case "071":
                                    case "072":
                                        BankChangeStatus(receiveObj);//執行銀行狀態改變
                                        break;
                                    case "301":
                                        //執行Echo交訊
                                        break;

                                }
                            });
                        //加入靜態訂閱者集合內
                        DicTopicSubscriber.Add(destination, subscriber);
                    }
                }
            }
            catch (Exception ex)
            {
                log.Error(">> [" + this.GetType().Name + "] TopicSubscriber Error :" + ex.ToString());
            }
            finally
            {
                absClientRequestHandler.ServiceState = new State_Exit();
            }
        }
        private void BankChangeStatus(object BankObj)
        {
            object tmp = (string)BankObj;
            //
            if (tmp == "071")
            {
                  lock (lockObj)
                  {
                       //將靜態dictionary內存放的某銀行物件內的狀態改變,key=銀行代號
                       //DicBanks[bandCo].CanConnect  = true;
                  }
            }
            else
            {
                  lock (lockObj)
                  {
                       //將靜態dictionary內存放的某銀行物件內的狀態改變
                       //DicBanks[bandCo].CanConnect  = false;
                  }
            }
            //log.Debug(">> Bank Status :" + bankAlive.ToString());
                
        }
    }

}
