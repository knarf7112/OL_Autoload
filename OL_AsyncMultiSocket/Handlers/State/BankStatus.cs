﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AsyncMultiSocket.Handlers.State
{
    public class BankStatus : EventArgs
    {
        public delegate void ChangeStatus(bool isConnect);

        public static bool IsConnect;
 
        public event ChangeStatus OnChangeStatus;

    }
}
