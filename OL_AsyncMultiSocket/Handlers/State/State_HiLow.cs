﻿using System;

//
using Common.Logging;
//
using HiLo.Utilities;
using HiLo.Domain.Entities;
using HiLo.Business;
using AsyncMultiSocket.Handlers.State;
using AsyncMultiSocket.Handlers;
using OL_Autoload_Lib.Controller.Common.Record;
using System.Text;

namespace AsyncMultiSocket.State
{
    /// <summary>
    /// 取號機
    /// </summary>
    public class State_HiLow : IState
    {
        private static readonly ILog log = LogManager.GetLogger(typeof(State_HiLow));
        
        private ISerializer<HiLoDO> hiLoParser;
        //private static IApplicationContext ctx = ContextRegistry.GetContext();
        public  HiLoGenerator hiLowGenerator { get; set; }
        public string tableName;
        public State_HiLow(string tableName)
        {
            this.tableName = tableName;
            //ctx = ContextRegistry.GetContext();
            //hiLowGenerator = ctx["sequenceGenerator"] as HiLoGenerator;
        }
        public void Handle(AbsClientRequestHandler absClientRequestHandler)
        {
            byte[] receiveBytes = new byte[1024];
            try
            {
                int receiveLength = absClientRequestHandler.ClientSocket.Receive(receiveBytes, System.Net.Sockets.SocketFlags.None);
                if (receiveLength == 0)
                {
                    absClientRequestHandler.ServiceState = new State_Exit();
                    return;
                }
                else
                {
                    log.Debug(">> State : HiLow");

                    this.hiLoParser = new XmlWorker<HiLoDO>();
                    this.hiLowGenerator = new HiLoGenerator(this.tableName);

                    Array.Resize(ref receiveBytes, receiveLength);
                    HiLoDO hilo = this.hiLoParser.Deserialize(receiveBytes);
                    //log.Debug(">> HiLo String:" + hilo.ToString());
                    if (!hiLowGenerator.HiLoDAO.Exist(hilo.KeyName))
                    {
                        //if key name not exist and create hilow sequence
                        hiLowGenerator.CreateSequence(hilo);
                    }

                    //拿號碼牌塞入物件
                    hilo.NextHi = hiLowGenerator.NextKey(hilo.KeyName);
                    

                    //log.Debug(">> 要傳回的物件:" + hilo.ToString());
                    //Serialize to byte[]
                    byte[] sendBytes = this.hiLoParser.Serialize2Bytes(hilo);
                    //send to client
                    int sendLength = absClientRequestHandler.ClientSocket.Send(sendBytes);
                    if (sendLength > 0)
                    {
                        log.Info(">> " + hilo.KeyName + "取號成功 => " + hilo.NextHi);
                    }
                    else
                    {
                        log.Error(">> [HiLow][Handle]Transfer Failed!");
                    }
                }
            }
            catch (Exception ex)
            {
                log.Error(">> [HiLow][Handle] Error:\n" + ex.ToString());
            }
            finally
            {
                absClientRequestHandler.ServiceState = new State_Exit();
            }
        }
    }
}
