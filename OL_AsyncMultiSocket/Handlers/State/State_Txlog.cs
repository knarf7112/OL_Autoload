﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
//
using Common.Logging;
using OL_Autoload_Lib;
using ALCommon;
using OL_AsyncMultiSocket.Utilities;
using OL_Autoload_Lib.Controller.TOL;
using OL_Autoload_Lib.Controller.Common;
using OL_Autoload_Lib.Controller.Common.Record;
using System.Net.Sockets;
//using MqTest.Producer;
using Newtonsoft.Json;
using System.Net;
using ActiveMqLab.Producer;
namespace AsyncMultiSocket.Handlers.State
{
    /// <summary>
    /// (0333) 自動加值成功交易後的Txlog流程
    /// </summary>
    class State_Txlog : IState
    {
        private static readonly ILog log = LogManager.GetLogger(typeof(State_Txlog));

        #region Property
        /// <summary>
        /// 從Socket接收的物件
        /// </summary>
        private ALTxlog_Domain SocketRqt { set; get; }
        /// <summary>
        /// 從Socket送出的物件
        /// </summary>
        private ALTxlog_Domain SocketRsp { set; get; }
        #endregion

        public void Handle(AbsClientRequestHandler absClientRequestHandler)
        {
            AL_DBModule obDB = null;
            try
            {
                byte[] receiveBytes = new byte[4096];
                int receiveLength = absClientRequestHandler.ClientSocket.Receive(receiveBytes);
                if (receiveLength == 0)
                {
                    absClientRequestHandler.ServiceState = new State_Exit();
                    return;
                }
                //外部發命令來查詢靜態物件儲存的狀態 => "BankStatus" + Enter
                else if (receiveLength == 11 && Encoding.UTF8.GetString(receiveBytes, 0, receiveLength).Contains("BankStatus"))
                {
                    //輸出目前靜態集合的銀行狀態物件到CommonLogging
                    AbsClientRequestHandler.GetBankStatusToLog();
                    absClientRequestHandler.ServiceState = new State_Exit();
                }
                else
                {
                    log.Debug(">> State : Txlog");

                    Array.Resize(ref receiveBytes, receiveLength);
                    string data = Encoding.UTF8.GetString(receiveBytes);
                    log.Debug("Receive Data => " + data);
                    //建立轉換模組並將收到的Json陣列轉回物件
                    ISerializer<ALTxlog_Domain> pocoParser = new JsonWorker<ALTxlog_Domain>();
                    //還原Socket傳來的物件
                    this.SocketRqt = pocoParser.Deserialize(receiveBytes);
                    //建立Socket要傳出的物件(空)
                    this.SocketRsp = new ALTxlog_Domain();
                    
                    //建立 Txlog ONLINE流程 module
                    Txlog_Module txlog_Module = new Txlog_Module();
                    //建立Txlog資料物件
                    CTxlog cTxlog = new CTxlog();
                    //建立取號機
                    HiLowClient.HiLowClient hilo = new HiLowClient.HiLowClient();

                    log.Debug("(Txlog)流程一:開始執行Txlog流程");
                    //執行Txlog online並回傳socketRsp                    
                    //if (cTxlog.TRANS_TYPE == "75")//匯入Txlog前先取得代行授權的STAN & RRN
                    //{
                    //    cTxlog.STAN = hilo.GetHiLowValue("STAN").ToString("D6");//STAN=(6碼);
                    //    cTxlog.RRN = String.Format("{0}{1}{2}{3}",
                    //        cTxlog.DATETIME.Substring(3, 1),
                    //        DateTime.ParseExact(cTxlog.DATETIME,"yyyyMMddHHmmss",System.Globalization.CultureInfo.InvariantCulture).DayOfYear.ToString("D3"),
                    //        cTxlog.DATETIME.Substring(8, 2),
                    //        cTxlog.STAN
                    //        );//12碼(西元年最後1碼+Julian Date3碼+24小時制2碼+STAN6碼)                            
                    //}
                    SocketRsp = txlog_Module.do_TOL(this.SocketRqt, ref cTxlog);
                    //(代行自動加值)Txlog寫入成功 And 交易成功(ReturnCode=00000000) And 為代行授權(transType=75) => 給銀行的資料物件發佈到ActiveMQ
                    if (SocketRsp.TXLOG_RC == "000000" && cTxlog.RETURN_CODE == "00000000" && cTxlog.TRANS_TYPE == "75")
                    {
                        log.Debug("(Txlog)流程二:開始發佈給銀行的物件(代行授權)到ActiveMQ上");
                        //銀行端Message Type(代行授權)
                        string messageType = "0120";
                        //銀行端Processing Code
                        string processingCode = "990175";
                        //建立DB模組
                        if (obDB == null)
                            obDB = new AL_DBModule();
                        //建立取銀行代號模組
                        Get_BandCo get_BandCo = new Get_BandCo();
                        //開啟DB連線
                        if (obDB.ALCon == null || obDB.ALCon.State == System.Data.ConnectionState.Closed)
                            obDB.OpenConnection();
                        //取得銀行代碼
                        string bankCo = get_BandCo.AL_Get_BankCode2Bank(obDB, cTxlog.ICC_NO).PadLeft(4, '0');//銀行代碼補齊4碼
                        //轉換成要傳給銀行端的Json物件: 0120(表示代行授權的Message Type), 990175(與發卡機構連線之Trans type)
                        AutoloadRqt_2Bank sendToBankObj = Convert2BankObj(cTxlog, bankCo, hilo, messageType, processingCode);
                        string destination = bankCo + messageType;//0000(銀行代碼) + 0120(MessageType)
                        //要傳給銀行端的Json物件丟到ActiveMQ
                        this.SendBankJsonToActiveMQ(sendToBankObj, obDB, destination);
                    }
                    //傳回SocketRsp
                    SendSocketRespsone(absClientRequestHandler, SocketRsp);
                }
            }
            #region 錯誤處理
            catch (Exception ex)
            {
                log.Error(">> [State_Txlog][Handle] Error:" + ex.ToString());
                try
                {
                    if (obDB == null)
                        obDB = new AL_DBModule();
                    obDB.OpenConnection();
                    LogModual LogM = new LogModual();

                    
                    LogM.SaveErrorLog(obDB,this.GetType().Name + ":" + ex.Message,"TAOL");//SocketRqt.TXLOG.Substring(24,16) => ICC_NO

                }
                catch
                {
                    log.Error(">> [State_Txlog][Handle][SaveErrorLog]Save Error Log Failed:" + ex.ToString());
            }
                finally
                {
                    obDB.CloseConnection();
                    obDB = null;
                    if (SocketRsp != null && (SocketRsp.TXLOG_RC == null || SocketRsp.TXLOG_RC.Equals("000000")))
                    {
                        SocketRsp.TXLOG_RC = ((int)CReturn_Code_Txlog.Ap_Error).ToString();
                    }
                    SendSocketRespsone(absClientRequestHandler, SocketRsp);
                }
            }
            #endregion
            finally
            {
                try
                {
                    if (obDB != null && obDB.ALCon != null)
                    {
                        obDB.CloseConnection();
                        obDB = null;
                    }
                }
                catch (Exception ex)
                {
                    log.Error(">>[State_Txlog][Handle][finally] Close DB Connection Error:" + ex.ToString());
                }
            }
            
        }

        #region Private method
        /// <summary>
        /// 要傳給銀行的Json資料物件發佈到ActiveMQ
        /// </summary>
        /// <param name="sendToBankObj">要發佈的銀行資料物件</param>
        /// <param name="obDB">DB連線模組</param>
        private void SendBankJsonToActiveMQ(AutoloadRqt_2Bank sendToBankObj, AL_DBModule obDB,string MQdestination)
        {
            try
            {
                log.Debug("開始發佈銀行端資料物件(代行授權): destination=>" + MQdestination);
                //建立toBank模組
                AL_2Bank al_2bank = new AL_2Bank();
                //將AutoloadRqt_2Bank Object轉成json String
                string sendToBankJsonStr = JsonConvert.SerializeObject(sendToBankObj);
                log.Debug(">> To Bank Json String [length:" + sendToBankJsonStr.Length + "] :\n" + sendToBankJsonStr);
                //DB紀錄傳給銀行的物件(更新)
                bool isBankup = al_2bank.InsertOrUpt_Reversal(this.SocketRqt.READER_ID, sendToBankJsonStr, obDB);
                
                //將給銀行的Json物件發佈到ActiveMQ存放
                bool isSendMsg = SendMessage<string>(sendToBankJsonStr, MQdestination);
                if (!(isBankup && isSendMsg))
                {
                    //發佈失敗或DB紀錄發佈訊息失敗,將給銀行的資料物件(JSON)存入ErrorLog內
                    string jsonString = JsonConvert.SerializeObject(sendToBankObj);
                    throw new Exception("Publish ActiveMQ Failed: JsonString=" + jsonString);
                }
            }
            catch (Exception ex)
            {
                throw new Exception("[SendBankJsonToActiveMQ] Error:\n" + ex.ToString());
            }
        }

        /// <summary>
        /// AutoloadRqt資料物件 => AutoloadRqt_2Bank資料物件
        /// </summary>
        /// <param name="AutoloadRqt">內部的資料物件</param>
        /// <param name="bankCo">銀行代碼</param>
        /// <param name="hilo">取號機物件</param>
        /// <param name="message_type">服務類別(4碼)</param>
        /// <param name="processing_code">對發卡機構的Trans Type</param>
        /// <returns>給銀行的AutoloadRqt_2Bank資料物件</returns>
        private AutoloadRqt_2Bank Convert2BankObj(CTxlog cTxlog, string bankCo, HiLowClient.HiLowClient hilo , string message_type, string processing_code)
        {
            if (cTxlog == null)
                throw new Exception("[Convert2BankObj]CTxlog is null");
            try
            {
                log.Debug("開始轉換給銀行的資料物件");
                //string nowStr = cTxlog.DATETIME;//從txlog取得交易日期時間
                //DateTime tmp = DateTime.ParseExact(cTxlog.DATETIME,"yyyyMMddHHmmss",System.Globalization.CultureInfo.InvariantCulture);
                //string stan = hilo.GetHiLowValue("STAN").ToString("D6");//STAN=(6碼)
                //建立傳向銀行的Poco物件並給予值，STAN&RRN移到匯入Txlog前取得
                AutoloadRqt_2Bank RqtToBank = new AutoloadRqt_2Bank()
                {
                    AMOUNT = cTxlog.TRANS_AMT.PadLeft(12,'0'),//原來Txlog.交易金額(8碼)=>補滿12碼
                    BANK_CODE = bankCo.PadLeft(4, '0'),//4碼
                    ICC_NO = cTxlog.ICC_NO,//16碼
                    MERCHANT_NO = cTxlog.MERCHANT_NO.PadLeft(15, '0'),//15碼
                    MESSAGE_TYPE = message_type,//訊息種類(4碼)
                    POS_NO = this.SocketRqt.REG_ID.PadLeft(8, '0'),//8碼
                    PROCESSING_CODE = processing_code,//與發卡機構連線之Trans type(6碼)
                    STAN = cTxlog.STAN,//stan,//stan,//6碼
                    STORE_NO = cTxlog.STORE_NO.PadLeft(8, '0'),//("st" + store no (6碼補0->8碼))
                    RRN = cTxlog.RRN,//(nowStr.Substring(3, 1) + tmp.DayOfYear.ToString("D3") + nowStr.Substring(8, 2) + cTxlog.STAN),//12碼
                    TRANS_DATETIME = cTxlog.DATETIME.Substring(4, 10)//nowStr.Substring(4, 10),//10碼
                };
                RqtToBank.ICC_info = new ICC_INFO()
                {
                    AMT = cTxlog.TRANS_AMT.PadLeft(8,'0'),//原來Txlog.交易金額(8碼)
                    ICC_NO = cTxlog.ICC_NO,//16碼
                    NECM_ID = cTxlog.READER_ID.PadLeft(20, '0'),//20碼
                    REG_ID = this.SocketRqt.REG_ID.PadLeft(3, '0'),//3碼
                    RETURN_CODE = "00000000",//8碼
                    STORE_NO = cTxlog.STORE_NO.PadLeft(8, '0'),//8碼
                    TX_DATETIME = cTxlog.DATETIME.PadLeft(14, '0')//14碼
                };
                return RqtToBank;
            }
            catch (Exception ex)
            {
                throw new Exception("[Convert2BankObj] Convert Bank Object failed: " + ex.ToString());
            }
        }

        /// <summary>
        /// 將要給銀行的物件發佈到Active存放等待發佈
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="jsonString"></param>
        /// <param name="destination"></param>
        /// <returns></returns>
        private bool SendMessage<T>(T jsonString, string destination)
        {
            try
            {
                log.Debug("開始發佈訊息到ActiveMQ:\n" + jsonString.ToString());
                //設定檔取得ActiveMQ的URL字串
                string brokerURL = System.Configuration.ConfigurationManager.AppSettings["BrokerURL"];
                //建立發佈者物件
                TopicPublisher producer = new TopicPublisher(brokerURL, destination);
                //啟動發佈程序
                producer.Start();
                //送出發佈訊息
                producer.SendMessage<T>(jsonString);
                return true;
            }
            catch (Exception ex)
            {
                log.Error("[State_Txlog][SendMessage<T>]Send Messsage to ActiveMQ Failed: " + ex.ToString());
                return false;
            }
        }

        /// <summary>
        /// 傳回 socketResponse
        /// </summary>
        /// <param name="socketRsp">要給Socket傳回的ALTxlog_Domain物件</param>
        /// <param name="absClientRequestHandler">此次任務的Client物件</param>
        private void SendSocketRespsone(AbsClientRequestHandler absClientRequestHandler, ALTxlog_Domain socketRsp)
        {
            try
            {
                log.Debug("(Txlog)開始送回Respsone資料物件");
                //建立Json轉換器
                ISerializer<ALTxlog_Domain> parser = new JsonWorker<ALTxlog_Domain>();
                byte[] responseBytes = parser.Serialize2Bytes(socketRsp);
                string sendJSONstr = parser.Serialize(socketRsp);
                log.Debug(">> (Respsone)ClientSocket Send to [" + ((IPEndPoint)absClientRequestHandler.ClientSocket.RemoteEndPoint).Address.ToString() + "] =>\n" + sendJSONstr);
                if (absClientRequestHandler.ClientSocket.Connected || absClientRequestHandler.ClientSocket.Poll(1000000, SelectMode.SelectWrite))
                {
                    //送出
                    int sendLength = absClientRequestHandler.ClientSocket.Send(responseBytes);
                    if (sendLength != responseBytes.Length)
                    {
                        log.Error(">> [State_Txlog][SendSocketRespsone] Client " + absClientRequestHandler.ClientNo + " => Send Data length:" + sendLength + @" <=\=> Data length:" + responseBytes.Length);
                    }
                }
                else
                {
                    log.Error(">> [State_Txlog][SendSocketRespsone] Client[" + absClientRequestHandler.ClientNo + "]: Not Connected!");
                }
            }
            catch (Exception ex)
            {
                log.Error(">> [State_Txlog][SendSocketRespsone] Failed: " + ex.ToString());
            }
            finally
            {
                log.Debug(">> State : Txlog End");
                absClientRequestHandler.ServiceState = new State_Exit();
            }
        }
        #endregion
    }
}
