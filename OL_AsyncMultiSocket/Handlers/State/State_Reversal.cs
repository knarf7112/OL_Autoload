﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
//
using Common.Logging;
using ALCommon;
using OL_AsyncMultiSocket.Utilities;
using OL_Autoload_Lib.Controller.TOL;
using OL_Autoload_Lib.Controller.Common.Record;
using OL_Autoload_Lib;
using OL_Autoload_Lib.Controller.Common;
//using MqTest.Producer;
using Newtonsoft.Json;
using System.Net.Sockets;
using System.Threading;
using System.Net;
using AsyncMultiSocket.Handlers.State.Entities;
using ActiveMqLab.Producer;
namespace AsyncMultiSocket.Handlers.State
{
    /// <summary>
    /// (0334) 自動加值失敗後的Txlog與銀行沖正流程
    /// </summary>
    class State_Reversal : IState
    {
        private static readonly ILog log = LogManager.GetLogger(typeof(State_Txlog));

        #region Field
        //開始時間
        private DateTime dtrqt = DateTime.Now;
        //結束時間
        private DateTime dtrsp;
        #endregion

        #region Property
        /// <summary>
        /// 從Socket接收的物件
        /// </summary>
        private ALTxlog_Domain SocketRqt { set; get; }
        /// <summary>
        /// 從Socket送出的物件
        /// </summary>
        private ALTxlog_Domain SocketRsp { set; get; }
        #endregion

        public State_Reversal()
        {
            //this.OnChangeBankAlive += State_Reversal_OnChangeBankAlive;
        }

        public void Handle(AbsClientRequestHandler absClientRequestHandler)
        {
            //建立DB連線模組
            AL_DBModule obDB = null;
            try
            {
                byte[] receiveBytes = new byte[4096];
                int receiveLength = absClientRequestHandler.ClientSocket.Receive(receiveBytes);
                if (receiveLength == 0)
                {
                    absClientRequestHandler.ServiceState = new State_Exit();
                    return;
                }
                //外部發命令來查詢靜態物件儲存的狀態 => "BankStatus" + Enter
                else if (receiveLength == 11 && Encoding.UTF8.GetString(receiveBytes, 0, receiveLength).Contains("BankStatus"))
                {
                    //輸出目前靜態集合的銀行狀態物件到CommonLogging
                    AbsClientRequestHandler.GetBankStatusToLog();
                    absClientRequestHandler.ServiceState = new State_Exit();
                }
                else
                {
                    log.Debug(">> State : Reversal Txlog");

                    Array.Resize(ref receiveBytes, receiveLength);
                    //建立轉換模組並將收到的Json陣列轉回物件
                    ISerializer<ALTxlog_Domain> pocoParser = new JsonWorker<ALTxlog_Domain>();
                    //還原Socket傳來的物件
                    this.SocketRqt = pocoParser.Deserialize(receiveBytes);
                    //建立Socket要傳出的物件(空)
                    this.SocketRsp = new ALTxlog_Domain();

                    //create Txlog ONLINE流程 module
                    Txlog_Module txlog_Module = new Txlog_Module();
                    //建立Txlog資料物件
                    CTxlog cTxlog = new CTxlog();
                    log.Debug("(Reversal)流程一:開始執行Txlog流程");
                    //執行Txlog online並回傳socketRsp
                    this.SocketRsp = txlog_Module.do_TOL(this.SocketRqt,ref cTxlog);

                    //(沖正自動加值)Txlog寫入成功 And 失敗交易transType=74[表示自動加值成功取得銀行授權] => 給銀行的沖正資料物件發佈到ActiveMQ
                    if (SocketRsp.TXLOG_RC == "000000" && cTxlog.TRANS_TYPE == "74")
                    {
                        log.Debug("(Reversal)流程二:開始發佈給銀行的沖正授權物件到ActiveMQ上");
                        //銀行端Message Type
                        string messageType = "0420";
                        //銀行代碼
                        string bankCo = string.Empty;
                        //
                        string jsonString = String.Empty;
                        //給銀行端的資料物件
                        AutoloadRqt_2Bank toBankObj = null;
                        //建立DB模組
                        obDB = (obDB == null) ? new AL_DBModule() : obDB;
                        //建立取號機
                        HiLowClient.HiLowClient hilo = new HiLowClient.HiLowClient();
                        //建立取銀行代號模組
                        //Get_BandCo get_BandCo = new Get_BandCo();

                        //開啟DB連線
                        obDB.OpenConnection();

                        //取得此端末最近傳給銀行的資料物件
                        toBankObj = this.GetToBankObj(this.SocketRqt.READER_ID, obDB);
                        //轉換成要傳給銀行端的沖正資料物件: 0420(表示沖正自動加值授權的Message Type)
                        AutoloadRqt_2Bank sendToBankObj = Convert2BankObj(toBankObj, hilo, messageType, cTxlog.RETURN_CODE);
                        //取得銀行代碼
                        //bankCo = get_BandCo.AL_Get_BankCode2Bank(obDB, cTxlog.ICC_NO).PadLeft(4, '0');//銀行代碼補齊4
                        bankCo = sendToBankObj.BANK_CODE;
                        if (AbsClientRequestHandler.DicBanks[bankCo].STATUS == "0")
                        {
                            AutoloadRqt_2Bank recieveBankObj = this.ConnectToBack(sendToBankObj, obDB, out jsonString);
                            if (recieveBankObj == null || recieveBankObj.RC != "00")
                            {
                                sendToBankObj.MESSAGE_TYPE = "0421";
                                string destination = bankCo + sendToBankObj.MESSAGE_TYPE;
                                log.Debug("連線異常:改發訊到ActiveMQ:\n  destination:" + destination + "  jsonString:" + jsonString);
                                this.SendBankJsonToActiveMQ(sendToBankObj, obDB, destination);
                            }
                        }
                        else
                        {
                            string destination2 = bankCo + sendToBankObj.MESSAGE_TYPE;
                            //要傳給銀行端的Json物件丟到ActiveMQ
                            this.SendBankJsonToActiveMQ(sendToBankObj, obDB, destination2);
                        }
                    }
                    //傳回SocketRsp
                    this.SendSocketRespsone(absClientRequestHandler, SocketRsp);
                }
            }
            #region 錯誤處理
            catch (Exception ex)
            {

                log.Error(">> " + absClientRequestHandler.ServiceState.ToString() + " Error:" + ex.Message);
                try
                {
                    obDB = new AL_DBModule();
                    obDB.OpenConnection();
                    LogModual LogM = new LogModual();
                    LogM.SaveErrorLog(obDB,this.GetType().Name + ex.Message,"RTAOL");//SocketRqt.TXLOG.Substring(24,16) => ICC_NO

                }
                catch
                {
                    log.Error(">> Save Error Failed:" + ex.Message);
                }
                finally
                {
                    if (SocketRsp != null && SocketRsp.TXLOG_RC.Equals("000000"))
                    {
                        SocketRsp.TXLOG_RC = ((int)CReturn_Code_AL.Ap_Error).ToString();
                    }
                    SendSocketRespsone(absClientRequestHandler, SocketRsp);
                }

            }
            #endregion
            finally
            {
                try
                {
                    if (obDB != null && obDB.ALCon != null)
                    {
                        obDB.CloseConnection();
                        obDB = null;
                    }
                }
                catch (Exception ex)
                {
                    log.Error(">>[State_Reversal][Handle][finally] Close DB Connection Error:" + ex.ToString());
                }
            }
        }
        
        #region Private Method
        /// <summary>
        /// 傳送Json物件到門神
        /// 透過門神傳送電文到銀行並取得Response並記錄銀行交易log
        /// </summary>
        /// <param name="RqtToBank">給銀行端的資料物件</param>
        /// <param name="obDB">DB模組</param>
        /// <returns>銀行Response的物件</returns>
        private AutoloadRqt_2Bank ConnectToBack(AutoloadRqt_2Bank RqtToBank, AL_DBModule obDB, out string jsonString)
        {
            log.Debug("開始連結銀行並交訊沖正");
            //連到沖正服務的門神
            byte[] FromBankBytes = null;
            AutoloadRqt_2Bank rspFromBank = null;
            SocketError socketErr = SocketError.SocketError;
            string RALBankService = AbsClientRequestHandler.GetToBankConfig(RqtToBank.BANK_CODE + RqtToBank.MESSAGE_TYPE);//System.Configuration.ConfigurationManager.AppSettings["RALBankService"];
            string[] BankConnectInfo = RALBankService.Split(':');
            log.Debug("BankAgent Setting:" + RALBankService);
            string ip = BankConnectInfo[0];//"127.0.0.1";
            int port = Convert.ToInt32(BankConnectInfo[1]);
            int sendTimeout = Convert.ToInt32(BankConnectInfo[2]);
            int receiveTimeout = Convert.ToInt32(BankConnectInfo[3]);
            SocketClient.Domain.SocketClient client = null;
            try
            {
                jsonString = JsonConvert.SerializeObject(RqtToBank);
                log.Debug(">> To Bank Json String:" + jsonString);
                byte[] toBankBytes = Encoding.UTF8.GetBytes(jsonString);

                client = new SocketClient.Domain.SocketClient(ip, port, sendTimeout, receiveTimeout);
                try
                {
                    if (client.ConnectToServer())
                    {
                        FromBankBytes = client.SendAndReceive(toBankBytes, out socketErr);
                    }
                    if (FromBankBytes == null || socketErr != SocketError.Success)
                    {
                        //throw new Exception("Trans data to Bank Proxy Service Error!");
                        log.Debug("FromBankBytes == null?" + (FromBankBytes == null).ToString() + "  Scoket Status:" + socketErr.ToString());
                        throw new Exception("BankAgent Connection Error");
                    }
                    string FBankJson = Encoding.UTF8.GetString(FromBankBytes);
                    log.Debug(">> (RALOL)From Bank Json String:" + FBankJson);
                    rspFromBank = JsonConvert.DeserializeObject<AutoloadRqt_2Bank>(FBankJson);
                    return rspFromBank;
                }
                catch (Exception ex)
                {
                    try
                    {
                        //對銀行端的傳輸發生timeout或連現異常時改丟到AciveMQ
                        log.Error("[State_Reversal][ConnectToBack] URL=>" + ip + ":" + port + " Error:\n" + ex.ToString());
                        //傳送銀行timeout或連線異常Repeat = 0421
                        //RqtToBank.MESSAGE_TYPE = "0421";
                        //發佈到ActiveMQ
                        //log.Debug("連線異常:改發訊到ActiveMQ MSG:\n" + jsonString);
                        //this.SendBankJsonToActiveMQ(RqtToBank, obDB, RqtToBank.MESSAGE_TYPE);
                    }
                    catch(Exception ex2)
                    {
                        log.Error("[SendBankJsonToActiveMQ] Error:\n" + ex2.ToString());
                    }
                    return null;
                }
                
            }
            catch (Exception ex)
            {
                throw new Exception("[ConnectToBack] send data to Bank failed: " + ex.ToString());
            }
            finally
            {
                //紀錄銀行端沖正交訊log
                try
                {
                    log.Debug("[紀錄]給銀行的JSON:" + JsonConvert.SerializeObject(RqtToBank));
                    log.Debug("[紀錄]銀行回來的的JSON:" + JsonConvert.SerializeObject(rspFromBank));
                    //如果連線門神(銀行)有問題則不寫DB log紀錄
                   
                        //記錄此次交易結果
                        LogModual logM = new LogModual();
                        dtrsp = DateTime.Now;
                        //給銀行需要12碼,給DB紀錄只能8碼,故切金額頭4碼
                        RqtToBank.AMOUNT = (RqtToBank.AMOUNT.Length > 8) ? RqtToBank.AMOUNT.Substring(RqtToBank.AMOUNT.Length - 8, 8) : RqtToBank.AMOUNT;
                        //記錄此次交易
                        
                        logM.SaveTransLog2Bank(RqtToBank, obDB, dtrqt, dtrsp);
                    if (socketErr == SocketError.Success)
                    {
                        if ((rspFromBank != null) && (!string.IsNullOrEmpty(rspFromBank.BANK_CODE)))
                        {
                            //因為銀行回傳無Field 61,所以從給銀行的POCO取
                            rspFromBank.ICC_info = RqtToBank.ICC_info;
                            //截掉銀行的代碼(DB只能3碼)
                            //rspFromBank.BANK_CODE = rspFromBank.BANK_CODE.Substring((rspFromBank.BANK_CODE.Length - 3), 3);
                            //銀行回應的有12碼,給DB紀錄只能8碼,故金額切掉多餘的碼(剩8碼)
                            rspFromBank.AMOUNT = (rspFromBank.AMOUNT.Length > 8) ? rspFromBank.AMOUNT.Substring(rspFromBank.AMOUNT.Length - 8, 8) : rspFromBank.AMOUNT;
                            dtrsp = DateTime.Now;
                            logM.SaveTransLog2Bank(rspFromBank, obDB, dtrqt, dtrsp);
                        }
                    }

                    if (client != null)
                    {
                        client.CloseConnection();
                        client = null;
                    }
                }
                catch (Exception ex)
                {
                    log.Error(">> [State_Reversal][ConnectToBack][SaveTransLog2Bank]Save RALOLAutoload Trans2Bank Log Failed:\n" + ex.ToString());
                }
            }
        }

        /// <summary>
        /// 取得端末儲存的最近一筆給銀行端的資料物件
        /// </summary>
        /// <param name="reader_id">端末Reader_ID</param>
        /// <param name="obDB">DB連線模組</param>
        /// <returns>給銀行端的資料物件</returns>
        private AutoloadRqt_2Bank GetToBankObj(string reader_id,AL_DBModule obDB)
        {
            AL_2Bank al_2bank = null;
            try
            {
                log.Debug("開始取得需沖正的原始授權資料");
                //建立2Bank模組
                al_2bank = new AL_2Bank();
                //取得此端末儲存的銀行端資料物件字串
                string toBankjsonString = al_2bank.Get_Reversal(reader_id, obDB);
                //Json String => 銀行端的資料物件(已取得銀行授權)
                AutoloadRqt_2Bank toBankObj = JsonConvert.DeserializeObject<AutoloadRqt_2Bank>(toBankjsonString);
                return toBankObj;
            }
            catch (Exception ex)
            {
                throw new Exception("[GetToBankObj]Get toBank Object Error:\n" + ex.ToString());
            }
        }

        /// <summary>
        /// 要傳給銀行的Json資料物件發佈到ActiveMQ
        /// </summary>
        /// <param name="sendToBankObj">要發佈的銀行資料物件</param>
        /// <param name="obDB">DB連線模組</param>
        private void SendBankJsonToActiveMQ(AutoloadRqt_2Bank sendToBankObj, AL_DBModule obDB,string MQdestination)
        {
            try
            {
                log.Debug("開始發佈銀行端資料物件(沖正自動加值):  destination=>" + MQdestination );
                //建立toBank模組
                AL_2Bank al_2bank = new AL_2Bank();
                //將AutoloadRqt_2Bank Object轉成json String
                string sendToBankJsonStr = JsonConvert.SerializeObject(sendToBankObj);
                log.Debug(">> To Bank Json String [length:" + sendToBankJsonStr.Length + "] :\n" + sendToBankJsonStr);
                //DB紀錄傳給銀行的物件(更新)
                bool isBankup = al_2bank.InsertOrUpt_Reversal(this.SocketRqt.READER_ID, sendToBankJsonStr, obDB);
                //將給銀行的Json物件發佈到ActiveMQ存放
                bool isSendMsg = SendMessage<string>(sendToBankJsonStr, MQdestination);
                if (!(isBankup && isSendMsg))
                {
                    //發佈失敗或DB紀錄發佈訊息失敗,將給銀行的資料物件(JSON)存入ErrorLog內
                    string jsonString = JsonConvert.SerializeObject(sendToBankObj);
                    throw new Exception("Publish ActiveMQ Failed: JsonString=" + jsonString);
                }
            }
            catch (Exception ex)
            {
                throw new Exception("[SendBankJsonToActiveMQ] Error:\n"+ ex.ToString());
            }
        }

        /// <summary>
        /// AutoloadRqt資料物件 => AutoloadRqt_2Bank資料物件
        /// </summary>
        /// <param name="AutoloadRqt">內部的資料物件</param>
        /// <param name="bankCo">銀行代碼</param>
        /// <param name="hilo">取號機物件</param>
        /// <param name="message_type">服務類別(4碼)</param>
        /// <param name="processing_code">對發卡機構的Trans Type</param>
        /// <returns>給銀行的AutoloadRqt_2Bank資料物件</returns>
        private AutoloadRqt_2Bank Convert2BankObj(AutoloadRqt_2Bank toBankObj, HiLowClient.HiLowClient hilo, string message_type,string txlogRC)
        {
            try
            {
                log.Debug("(沖正)開始轉換給銀行的資料物件");
                string stan = hilo.GetHiLowValue("STAN").ToString("D6");//STAN=(6 bytes)
                //建立傳向銀行的Poco物件並給予值
                AutoloadRqt_2Bank tempObj = ObjectCopier.JSONCopy<AutoloadRqt_2Bank>(toBankObj);//複製物件
                tempObj.MESSAGE_TYPE = message_type;//4碼
                tempObj.STAN = stan;//6碼
                tempObj.RRN = toBankObj.RRN.Substring(0, 6) + stan;//12碼
                tempObj.ICC_info.RETURN_CODE = txlogRC;//加入txlog內的卡機回傳狀態(8 bytes)
                
                //原始交易物件Field 90
                tempObj.ORI_dtat = new ORI_DATA
                {
                    MESSAGE_TYPE = toBankObj.MESSAGE_TYPE,//4碼
                    TRANSACTION_DATE = toBankObj.TRANS_DATETIME,//10碼
                    STAN = toBankObj.STAN,//原始交易STAN(6碼)
                    STORE_NO = toBankObj.STORE_NO.Replace("st", "").PadLeft(8, '0'),//8碼 ->Field 90 規定=>st00123456->00123456
                    RRN = toBankObj.RRN//12碼
                };
                return tempObj;
            }
            catch (Exception ex)
            {
                throw new Exception(">> [State_Reversal][Convert2BankObj] Convert Bank Object failed: " + ex.ToString());
            }
        }

        /// <summary>
        /// 將要給銀行的物件發佈到Active存放等待發佈
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="jsonString"></param>
        /// <param name="destination"></param>
        /// <returns></returns>
        private bool SendMessage<T>(T jsonString, string destination)
        {
            try
            {
                log.Debug("開始發佈沖正訊息到ActiveMQ:\n" + jsonString.ToString());
                //設定檔取得ActiveMQ的URL字串
                string brokerURL = System.Configuration.ConfigurationManager.AppSettings["BrokerURL"];
                //建立發佈者物件
                TopicPublisher producer = new TopicPublisher(brokerURL, destination);
                //啟動發佈程序
                producer.Start();
                //送出發佈訊息
                producer.SendMessage<T>(jsonString);
                return true;
            }
            catch (Exception ex)
            {
                log.Error(">> [State_Reversal][SendMessage<T>]Send Messsage to ActiveMQ Failed: " + ex.ToString());
                return false;
            }
        }

        /// <summary>
        /// 傳回 socketResponse
        /// </summary>
        /// <param name="socketRsp">要給Socket傳回的ALTxlog_Domain物件</param>
        /// <param name="absClientRequestHandler">此次任務的Client物件</param>
        private void SendSocketRespsone(AbsClientRequestHandler absClientRequestHandler, ALTxlog_Domain socketRsp)
        {
            try
            {
                log.Debug("(沖正自動加值)開始送回Respsone資料物件");
                //建立Json轉換器
                ISerializer<ALTxlog_Domain> parser = new JsonWorker<ALTxlog_Domain>();
                byte[] responseBytes = parser.Serialize2Bytes(socketRsp);
                string sendJSONstr = parser.Serialize(socketRsp);
                log.Debug(">> (Respsone)ClientSocket Send to [" + ((IPEndPoint)absClientRequestHandler.ClientSocket.RemoteEndPoint).Address.ToString() + "] =>\n" + sendJSONstr);
                if (absClientRequestHandler.ClientSocket.Connected || absClientRequestHandler.ClientSocket.Poll(1000000, SelectMode.SelectWrite))
                {
                    //送出
                    int sendLength = absClientRequestHandler.ClientSocket.Send(responseBytes);
                    if (sendLength != responseBytes.Length)
                    {
                        log.Error(">> [State_Reversal][SendSocketRespsone] Client " + absClientRequestHandler.ClientNo + " => Send Data length:" + sendLength + @" <=\=> Data length:" + responseBytes.Length);
                    }
                }
                else
                {
                    log.Error(">> [State_Reversal][SendSocketRespsone] Client[" + absClientRequestHandler.ClientNo + "]: Not Connected!");
                }
            }
            catch (Exception ex)
            {
                log.Error(">> [State_Reversal][SendSocketRespsone] Failed:\n" + ex.ToString());
            }
            finally
            {
                log.Debug(">> State : Reversal End");
                absClientRequestHandler.ServiceState = new State_Exit();
            }
        }
        #endregion
    }
}
