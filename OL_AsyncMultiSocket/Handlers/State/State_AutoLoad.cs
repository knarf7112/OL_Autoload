﻿using ActiveMqLab.Producer;
using ALCommon;
using AsyncMultiSocket.Handlers.State.Entities;
using CardValidator;
using Common.Logging;
//using MqTest.Producer;
using Newtonsoft.Json;
using OL_AsyncMultiSocket.Utilities;
using OL_Autoload_Lib;
using OL_Autoload_Lib.Controller.ALOL.Ad_control;
using OL_Autoload_Lib.Controller.ALOL.AdL_Check;
using OL_Autoload_Lib.Controller.ALOL.Bkl_Check;
using OL_Autoload_Lib.Controller.ALOL.CardSt_Check;
using OL_Autoload_Lib.Controller.ALOL.Rw_Check;
using OL_Autoload_Lib.Controller.ALOL.Store_Check;
using OL_Autoload_Lib.Controller.Common;
using OL_Autoload_Lib.Controller.Common.Record;
using System;
using System.Globalization;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Transactions;
namespace AsyncMultiSocket.Handlers.State
{
    /// <summary>
    /// 自動加值流程
    /// </summary>
    public class State_AutoLoad : IState
    {
        private static readonly ILog log = LogManager.GetLogger(typeof(State_AutoLoad));

        #region Property
        /// <summary>
        /// 從Socket接收的物件
        /// </summary>
        private AL2POS_Domain SocketRqt { get; set; }
        /// <summary>
        /// 從Socket送出的物件
        /// </summary>
        private AL2POS_Domain SocketRsp { get; set; }

        private AL_DBModule obDB { get; set; }
        #endregion

        #region Field
        /// <summary>
        /// 內部處理用的物件
        /// </summary>
        private AutoloadRqt AutoloadRqt;
        DateTime dtrqt = DateTime.Now;
        DateTime dtrsp = DateTime.Now;
        #endregion

        #region Constructor
        public State_AutoLoad(AL2POS_Domain socketRqt)
        {
            this.SocketRqt = socketRqt;
            //準備空的物件容器
            this.AutoloadRqt = new AutoloadRqt();
            this.SocketRsp = new AL2POS_Domain();
        }
        public State_AutoLoad()
        {
        }
        #endregion

        /// <summary>
        /// 要處理的自動加值流程
        /// </summary>
        /// <param name="absClientRequestHandler">Client Handler</param>
        public void Handle(AbsClientRequestHandler absClientRequestHandler)
        {
            try
            {
                byte[] receiveBytes = new byte[4096];
                int receiveLength = absClientRequestHandler.ClientSocket.Receive(receiveBytes, System.Net.Sockets.SocketFlags.None);
                if (receiveLength == 0)
                {
                    absClientRequestHandler.ServiceState = new State_Exit();
                    return;
                }
                //外部發命令來查詢靜態物件儲存的狀態 =>( "BankStatus" + Enter )=> length:11
                else if (receiveLength == 11 && Encoding.UTF8.GetString(receiveBytes, 0, receiveLength).Contains("BankStatus"))
                {
                    //輸出目前靜態集合的銀行狀態物件到CommonLogging
                    AbsClientRequestHandler.GetBankStatusToLog();
                    absClientRequestHandler.ServiceState = new State_Exit();
                }
                else
                {
                    log.Debug(">> State : AutoLoad");

                    Array.Resize(ref receiveBytes, receiveLength);
                    //建立DB模組
                    this.obDB = new ALCommon.AL_DBModule();
                    //建立內部緩存物件
                    this.AutoloadRqt = new AutoloadRqt();
                    //建立Socket送出物件
                    this.SocketRsp = new AL2POS_Domain();
                    //建立轉換模組並將收到的Json陣列轉回物件
                    ISerializer<AL2POS_Domain> pocoParser = new JsonWorker<AL2POS_Domain>();
                    //還原Socket傳來的物件
                    this.SocketRqt = pocoParser.Deserialize(receiveBytes);
                    //建立卡號檢驗模組
                    VertifyTool2 vertifyObj = new VertifyTool2();
                    //建立卡片有效性檢查模組
                    CardStatus_Check checkCardStatus = new CardStatus_Check();
                    //建立黑名單檢驗模組
                    BkL_Check checkBlackList = new BkL_Check();
                    //建立端末檢查模組
                    RW_Check checkReader = new RW_Check();
                    //建立門市是否開放自動加值檢查模組
                    STORE_Check checkStore = new STORE_Check();
                    //建立取號模組
                    HiLowClient.HiLowClient hilo = new HiLowClient.HiLowClient();

                    //開啟DB連線
                    this.obDB.OpenConnection();
                    //Socket物件轉成自動加值物件
                    this.SocketRqtToAutoloadRqt(this.SocketRqt, ref AutoloadRqt, this.obDB);
                    log.Debug("(自動加值)流程一:開始驗證卡號是否為正常卡");
                    #region 1.驗證卡號是否為正常卡(boolean)
                    bool isNormalCard = vertifyObj.CardChkVertify(AutoloadRqt.ICC_NO);
                    bool isCreditCard = vertifyObj.CreditCardChkVertify(AutoloadRqt.ICC_NO);
                    if (!(isNormalCard && isCreditCard))
                    {
                        SendSocketRespsone(absClientRequestHandler, SocketRsp, AutoloadRqt, CReturn_Code_AL.Invalid_Card);
                        return;
                    }
                    #endregion
                    log.Debug("(自動加值)流程二:開始檢核卡片是否為有效卡");
                    #region 2.檢核卡片是否為有效卡(boolean)
                    CardStatus isValidCard = ParseEnum<CardStatus>(checkCardStatus.AL_CheckCardStatus(obDB, AutoloadRqt.ICC_NO));
                    switch (isValidCard)
                    {
                        case CardStatus.Other:
                            break;
                        case CardStatus.Status3478:
                            SendSocketRespsone(absClientRequestHandler, SocketRsp, AutoloadRqt, CReturn_Code_AL.Invalid_Card);
                            return;
                        case CardStatus.Status569:
                            SendSocketRespsone(absClientRequestHandler, SocketRsp, AutoloadRqt, CReturn_Code_AL.Black_List);
                            return;
                        default:
                            //Null
                            SendSocketRespsone(absClientRequestHandler, SocketRsp, AutoloadRqt, CReturn_Code_AL.Ap_Error);
                            return;
                    }
                    #endregion
                    log.Debug("(自動加值)流程三:開始黑名單檢核");
                    #region 3.黑名單檢核(boolean)

                    bool isBlack = checkBlackList.AL_CheckBKL(obDB, AutoloadRqt.ICC_NO);
                    if (isBlack)
                    {
                        SendSocketRespsone(absClientRequestHandler, SocketRsp, AutoloadRqt, CReturn_Code_AL.Black_List);
                        return;
                    }
                    #endregion
                    log.Debug("(自動加值)流程四:開始門市與端末檢核");
                    #region 4.門市與端末檢核(boolean)
                    bool isValidTerminal = checkReader.AL_CheckReader(obDB, AutoloadRqt);
                    string isValidStore = checkStore.AL_CheckStore(AutoloadRqt, obDB);
                    
                    if (!isValidTerminal)
                    {
                        SendSocketRespsone(absClientRequestHandler, SocketRsp, AutoloadRqt, CReturn_Code_AL.Invalid_Terminal);
                        return;
                    }
                    //無效店
                    if (isValidStore == "1")
                    {
                        SendSocketRespsone(absClientRequestHandler, SocketRsp, AutoloadRqt, CReturn_Code_AL.Invalid_Store);
                        return;
                    }
                    //不給加值店
                    if (isValidStore == "2")
                    {
                        SendSocketRespsone(absClientRequestHandler, SocketRsp, AutoloadRqt, CReturn_Code_AL.Load_Nonopen);
                        return;
                    }
                    checkStore.AL_GetBankStoreNo(ref AutoloadRqt, obDB);//將對應銀行端的BANK_STORE_NO和BANK_REG_LEN加到AutoloadRqt內
                    #endregion
                    log.Debug("(自動加值)流程五:開始轉向銀行端請求授權或使用代行授權模組");
                    #region 5.轉向銀行端請求授權或使用代行授權模組

                    //手動修改銀行連線狀態  [不用的話要改false]
                    bool ManualStatus = System.Configuration.ConfigurationSettings.AppSettings["BankAlive"] == "true" ? true : false;
                    //建立取銀行代號模組
                    Get_BandCo get_BandCo = new Get_BandCo();
                    //取得銀行代碼
                    string bankCo = get_BandCo.AL_Get_BankCode2Bank(obDB, AutoloadRqt.ICC_NO).PadLeft(4, '0');//銀行代碼補齊4碼
                    //銀行連線狀態
                    log.Debug(">> Bank Code:" + AbsClientRequestHandler.DicBanks[bankCo].BANK_CODE + " Connection Status:" + AbsClientRequestHandler.DicBanks[bankCo].STATUS);
                    //檢查銀行連線狀態
                    if (AbsClientRequestHandler.DicBanks[bankCo].STATUS == "0" || ManualStatus)
                    {
                        log.Debug("流程五之一:銀行可連線狀態-請求授權");
                        #region 5-1.銀行可連線狀態-請求授權
                        //初始化給銀行的json String
                        string toBankJsonString = String.Empty;
                        //AutoloadRqt(object) => AutoloadRqt_2Bank(object): 0100(表示自動加值授權Message Type), 990174(與發卡機構連線之Trans type)
                        AutoloadRqt_2Bank sendToBankObj = Convert2BankObj(AutoloadRqt, bankCo, hilo, "0100", "990174");
                        //iCash <---(Json)---> iCashAgent <---(ascii)---> Bank 
                        AutoloadRqt_2Bank receiveFromBankObj = ConnectToBack(sendToBankObj, obDB, out toBankJsonString);
                        //若連銀行有問題,改走代行授權流程
                        if (receiveFromBankObj == null)
                        {
                            this.AgentAuthorize_Flow(absClientRequestHandler, AutoloadRqt, bankCo, hilo);
                            return;
                        }

                        //手動設定銀行拒絕授權
                        bool BankPass = System.Configuration.ConfigurationSettings.AppSettings["ManualBankPass"] == "true" ? true : false;
                        //Bank Return Code => 00(正常),01(其他錯誤),03(聯名卡未開卡),04(聯名卡掛失)
                        if (receiveFromBankObj.RC == "00" || BankPass)
                        {
                            //建立toBank模組
                            AL_2Bank al_2bank = new AL_2Bank();
                            //DB紀錄傳給銀行的物件(更新)
                            al_2bank.InsertOrUpt_Reversal(this.SocketRqt.READER_ID, toBankJsonString, obDB);
                            //取得序號 and 傳回正常交易Socket物件
                            long SN = hilo.GetHiLowValue("Autoload");
                            AutoloadRqt.SN = SN.ToString("D8");
                            try
                            {
                                //自動加值成功
                                SendSocketRespsone(absClientRequestHandler, SocketRsp, AutoloadRqt, CReturn_Code_AL.Success);
                            }
                            catch (Exception ex)
                            {
                                #region 已取得銀行授權但送到POS端有問題,所以無言的自己組沖正物件作沖正
                                try
                                {
                                    log.Debug("(Reversal)流程二:開始發佈給銀行的沖正授權物件到ActiveMQ上");
                                    //銀行端Message Type
                                    string messageType = "0420";
                                    //
                                    string jsonString = String.Empty;
                                    //給銀行端的資料物件
                                    AutoloadRqt_2Bank toBankObj = null;
                                    //建立DB模組
                                    obDB = (obDB == null) ? new AL_DBModule() : obDB;
                                    //取得此端末最近傳給銀行的資料物件
                                    toBankObj = this.GetToBankObj(this.SocketRqt.READER_ID, obDB);
                                    //轉換成要傳給銀行端的沖正資料物件: 0420(表示沖正自動加值授權的Message Type)
                                    sendToBankObj = Convert2BankObj(toBankObj, hilo, messageType, "59980001");
                                    

                                    if (AbsClientRequestHandler.DicBanks[bankCo].STATUS == "0")
                                    {
                                        AutoloadRqt_2Bank fromBankObj = this.ConnectToBack(sendToBankObj, obDB, out jsonString);
                                        if (fromBankObj.RC != "00")
                                        {
                                            sendToBankObj.MESSAGE_TYPE = "0421";
                                            string destination = sendToBankObj.BANK_CODE.PadLeft(4, '0') + sendToBankObj.MESSAGE_TYPE;//銀行代碼 + MessageType當作發佈的destination name
                                            //要傳給銀行端的Json物件丟到ActiveMQ(因傳送失敗故MessageType變更為0421)
                                            this.SendBankJsonToActiveMQ(sendToBankObj, obDB, destination);
                                        }
                                    }
                                    else
                                    {
                                        //要傳給銀行端的Json物件丟到ActiveMQ
                                        string destination2 = sendToBankObj.BANK_CODE.PadLeft(4, '0') + sendToBankObj.MESSAGE_TYPE;//銀行代碼 + MessageType當作發佈的destination name
                                        this.SendBankJsonToActiveMQ(sendToBankObj, obDB, destination2);
                                    }
                                }
                                catch (Exception innerEx)
                                {
                                    throw innerEx;
                                }
                                #endregion
                                //跟銀行要求授權的資料物件=>sendToBankObj;
                                throw ex;
                            }
                        }
                        else if (receiveFromBankObj.RC == "04" || receiveFromBankObj.RC == "05" || receiveFromBankObj.RC == "07")
                        {
                            SendSocketRespsone(absClientRequestHandler, SocketRsp, AutoloadRqt, CReturn_Code_AL.Black_List);
                        }
                        else
                        {
                            SendSocketRespsone(absClientRequestHandler, SocketRsp, AutoloadRqt, CReturn_Code_AL.Load_Fail);
                        }
                        #endregion
                    }
                    else
                    {
                        log.Debug("流程五之二:銀行不可連線狀態-代行授權");
                        #region 5-2.銀行不可連線狀態-代行授權
                        this.AgentAuthorize_Flow(absClientRequestHandler, AutoloadRqt, bankCo, hilo);
                        #endregion
                    }
                    #endregion
                    log.Debug("(自動加值)結束自動加值流程");
                }
            }
            #region 錯誤處理
            catch (Exception ex)
            {
                log.Error(">> [State_AutoLoad][Handle] Error:" + ex.ToString());
                //寫入DB的Error Log表格
                try
                {
                    obDB.OpenConnection();
                    LogModual LogM = new LogModual();
                    LogM.SaveErrorLog(obDB, this.GetType().Name + ex.Message, "ALOL");
                }
                catch
                {
                    log.Error(">> [State_AutoLoad][Handle]Save Error Failed:" + ex.ToString());
                }
                finally
                {
                    //obDB.CloseConnection();
                    //傳回物件資訊
                    try
                    {
                        SendSocketRespsone(absClientRequestHandler, SocketRsp, AutoloadRqt, CReturn_Code_AL.Ap_Error);
                    }
                    catch (Exception ex2)
                    {
                        log.Error(">> [State_AutoLoad][Handle][SendSocketRespsone]ClientSocket Connection Error:" + ex2.ToString());
                    }
                }
            }
            #endregion
            finally
            {
                try
                {
                    if (obDB != null && obDB.ALCon != null)
                    {
                        obDB.CloseConnection();
                        obDB = null;
                    }
                }
                catch (Exception ex)
                {
                    log.Error(">>[State_AutoLoad][Handle][finally] Close DB Connection Error:" + ex.ToString());
                }
            }
        }
     
        #region Private Method
        /// <summary>
        /// 代行授權流程
        /// </summary>
        private void AgentAuthorize_Flow(AbsClientRequestHandler absClientRequestHandler, AutoloadRqt autoloadRqt, string bankCo, HiLowClient.HiLowClient hilo)
        {
            try
            {
                //拒絕代行授權名單檢核(boolean)
                AdL_Check checkBankAuth = new AdL_Check();
                bool isBankAuth = checkBankAuth.AL_CheckADL(obDB, autoloadRqt.ICC_NO);
                //卡片可否代行授權
                if (!isBankAuth)
                {
                    //是銀行拒絕授權的卡片
                    autoloadRqt.TRANS_TYPE = CTransType.REFUSE_ADVICE;
                    SendSocketRespsone(absClientRequestHandler, SocketRsp, autoloadRqt, CReturn_Code_AL.Refuse_Advice_List);
                    return;
                }
                //建立代行授權檢核模組
                Ad_Controller ad_Controller = new Ad_Controller();
                using (TransactionScope ts = new TransactionScope(TransactionScopeOption.Required, new TransactionOptions() { IsolationLevel = IsolationLevel.RepeatableRead, Timeout = new TimeSpan(0, 0, 10) }))
                {
                    //建立取得銀行統編模組
                    Get_MerchantNO getBankMerchantNo = new Get_MerchantNO();
                    //取得銀行統一編號
                    string bankMerchantNo = getBankMerchantNo.GetMerchantNo_Bank(bankCo.Substring(1, 3), obDB);
                    //檢核額度與次數
                    string authorizeCode = ad_Controller.AL_CheckAdvice(obDB, bankMerchantNo, bankCo.Substring(1, 3), autoloadRqt.TRANS_AMT, autoloadRqt.ICC_NO);//這邊的BankCo取3碼而已(資料庫只有3碼)
                    //額度超過上限 || 次數超過上限 || 找不到資料
                    if (authorizeCode == "1" || authorizeCode == "2" || authorizeCode == "3")
                    {
                        SendSocketRespsone(absClientRequestHandler, SocketRsp, autoloadRqt, CReturn_Code_AL.Request_Bal_Denied);
                    }
                    //額度和次數都小於上限
                    else
                    {
                        //Trans Type = 75(代行授權)
                        autoloadRqt.TRANS_TYPE = CTransType.ADVICE;
                        string stan = hilo.GetHiLowValue("STAN").ToString("D6");//代行授權的成功交易新增一個STAN
                        bool isAddAuthorizeLog = ad_Controller.AL_AddAdviceLog(obDB, autoloadRqt.MERCHANT_NO, bankMerchantNo, bankCo.Substring(1, 3), autoloadRqt.STORE_NO, autoloadRqt.REG_ID, autoloadRqt.ICC_NO, autoloadRqt.TRANS_AMT, autoloadRqt.READER_ID, stan);//銀行代碼只有3碼(DB只接受3碼)
                        //結束Transaction
                        ts.Complete();
                        //新增行使代行授權之資料成功
                        if (isAddAuthorizeLog)
                        {
                            //(代行授權)自動加值交易成功
                            string hiloNum = hilo.GetHiLowValue("Autoload").ToString("D8");
                            autoloadRqt.SN = hiloNum;
                            SendSocketRespsone(absClientRequestHandler, SocketRsp, autoloadRqt, CReturn_Code_AL.Advice_Success);
                        }
                        else
                        {
                            //SendSocketRespsone(absClientRequestHandler, SocketRsp, AutoloadRqt, CReturn_Code_AL.Load_Fail);
                            throw new Exception("Insert advice_authorize Log Failed!");
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// 傳送Json物件到門神
        /// 透過門神傳送電文到銀行並取得Response並記錄與銀行端的交易log
        /// </summary>
        /// <param name="RqtToBank">給銀行端的資料物件</param>
        /// <param name="obDB">DB模組</param>
        /// <returns>銀行Response的物件</returns>
        private AutoloadRqt_2Bank ConnectToBack(AutoloadRqt_2Bank RqtToBank, AL_DBModule obDB, out string jsonString)
        {
            AutoloadRqt_2Bank rspFromBank = null;
            SocketError socketErr = SocketError.SocketError;
            //連到對銀行Service的門神IP和port,給門神依照銀行代碼判斷該發送的位址
            
            string BankService = AbsClientRequestHandler.GetToBankConfig(RqtToBank.BANK_CODE + RqtToBank.MESSAGE_TYPE);//System.Configuration.ConfigurationManager.AppSettings["BankService"];
            log.Debug("BankAgent Setting:" + BankService);
            string[] BankConnectInfo = BankService.Split(':');
            string ip = BankConnectInfo[0];//"127.0.0.1";
            int port = Convert.ToInt32(BankConnectInfo[1]);
            int sendTimeout = Convert.ToInt32(BankConnectInfo[2]);
            int receiveTimeout = Convert.ToInt32(BankConnectInfo[3]);
            SocketClient.Domain.SocketClient client = null;
            
            try
            {
                log.Debug("開始連結銀行並交訊");
                jsonString = JsonConvert.SerializeObject(RqtToBank);
                log.Debug(">> To Bank Json String:" + jsonString);
                byte[] toBankBytes = Encoding.UTF8.GetBytes(jsonString);
                client = new SocketClient.Domain.SocketClient(ip, port, sendTimeout, receiveTimeout);
                
                byte[] FromBankBytes = null;
                if (client.ConnectToServer())
                {
                    FromBankBytes = client.SendAndReceive(toBankBytes, out socketErr);
                }
                //-----------當作銀行傳成功----------------------------
                //RqtToBank.RC = "00";
                //jsonString = JsonConvert.SerializeObject(RqtToBank);
                //FromBankBytes = Encoding.UTF8.GetBytes(jsonString);
                //----------------------------------------------------

                //傳輸銀行端有問題時收到 null 或 SocketError狀態不為Success
                if (FromBankBytes == null || socketErr != SocketError.Success)
                {
                    //throw new Exception("Trans data to Bank Proxy Service Error!");
                    log.Debug("FromBankBytes == null?" + (FromBankBytes == null).ToString() + "  Scoket Status:" + socketErr.ToString());
                    return null;
                }
                string FBankJson = Encoding.UTF8.GetString(FromBankBytes);
                log.Debug(">> From Bank Json String:" + FBankJson);
                rspFromBank = JsonConvert.DeserializeObject<AutoloadRqt_2Bank>(FBankJson);
                return rspFromBank;
            }
            catch (Exception ex)
            {
                throw new Exception("[ConnectToBack] send data to Bank Proxy Service failed: " + ex.ToString());
            }
            finally
            {
                //紀錄銀行端交訊log
                try
                {
                    //如果連線門神(銀行)有問題則不寫DB log紀錄
                    if (socketErr == SocketError.Success)
                    {
                        //記錄此次交易結果
                        LogModual logM = new LogModual();
                        dtrsp = DateTime.Now;
                        //給銀行需要12碼,給DB紀錄只能8碼,故金額切掉多餘的碼(剩8碼)
                        RqtToBank.AMOUNT = (RqtToBank.AMOUNT.Length > 8) ? RqtToBank.AMOUNT.Substring(RqtToBank.AMOUNT.Length - 8, 8) : RqtToBank.AMOUNT;
                        //記錄此次交易
                        JsonConvert.SerializeObject(RqtToBank);
                        log.Debug("[紀錄]給銀行的JSON:" + JsonConvert.SerializeObject(RqtToBank));
                        log.Debug("[紀錄]銀行回來的的JSON:" + JsonConvert.SerializeObject(rspFromBank));
                        logM.SaveTransLog2Bank(RqtToBank, obDB, dtrqt, dtrsp);
                        if ((rspFromBank != null) && (!string.IsNullOrEmpty(rspFromBank.BANK_CODE)))
                        {
                            log.Debug("開始寫入銀行回傳的自動加值Response物件");
                            //因為銀行回傳無Field 61,所以從給銀行的POCO取
                            rspFromBank.ICC_info = RqtToBank.ICC_info;
                            //截掉銀行的代碼(DB只能3碼)-DB可以容許5碼_2015-04-28
                            //rspFromBank.BANK_CODE = rspFromBank.BANK_CODE.Substring((rspFromBank.BANK_CODE.Length - 3), 3);
                            //銀行回應的有12碼,給DB紀錄只能8碼,故金額切掉多餘的碼(剩8碼)
                            rspFromBank.AMOUNT = (rspFromBank.AMOUNT.Length > 8) ? rspFromBank.AMOUNT.Substring(rspFromBank.AMOUNT.Length - 8, 8) : rspFromBank.AMOUNT;
                            dtrsp = DateTime.Now;
                            logM.SaveTransLog2Bank(rspFromBank, obDB, dtrqt, dtrsp);
                        }
                    }

                    if (client != null)
                    {
                        client.CloseConnection();
                        client = null;
                    }
                }
                catch (Exception ex)
                {
                    log.Error(">> [State_AutoLoad][ConnectToBack][SaveTransLog2Bank]Save Autoload Trans2Bank Log Failed:\n" + ex.ToString());
                }
            }
        }

        /// <summary>
        /// AutoloadRqt資料物件 => AutoloadRqt_2Bank資料物件
        /// </summary>
        /// <param name="AutoloadRqt">內部的資料物件</param>
        /// <param name="bankCo">銀行代碼</param>
        /// <param name="hilo">取號機物件</param>
        /// <param name="message_type">服務類別(4碼)</param>
        /// <param name="processing_code">對發卡機構的Trans Type</param>
        /// <returns>給銀行的AutoloadRqt_2Bank資料物件</returns>
        private AutoloadRqt_2Bank Convert2BankObj(AutoloadRqt AutoloadRqt, string bankCo, HiLowClient.HiLowClient hilo, string message_type, string processing_code)
        {

            DateTime now = DateTime.Now;
            try
            {
                log.Debug("開始轉換給銀行的資料物件");
                string nowStr = now.ToString("yyyyMMddHHmmss");
                JulianCalendar jc = new JulianCalendar();
                string stan = hilo.GetHiLowValue("STAN").ToString("D6");//從取號機取SN
                //建立傳向銀行的Poco物件並給予值
                AutoloadRqt_2Bank RqtToBank = new AutoloadRqt_2Bank()
                {
                    AMOUNT = AutoloadRqt.TRANS_AMT.ToString("D12"),         //12碼
                    BANK_CODE = bankCo.PadLeft(4, '0'),                     // 4碼
                    ICC_NO = AutoloadRqt.ICC_NO,                            //16碼
                    MERCHANT_NO = AutoloadRqt.MERCHANT_NO.PadLeft(15, '0'), //15碼
                    MESSAGE_TYPE = message_type,        //Message Type 訊息類別 4碼
                    POS_NO = AutoloadRqt.REG_ID.PadLeft(8, '0'),            // 8碼 改補空白// 8碼
                    PROCESSING_CODE = processing_code,//與發卡機構連線之Trans type 6碼
                    STAN = stan,                                            // 6碼
                    STORE_NO = AutoloadRqt.Bank_STORE_NO,//.STORE_NO.PadLeft(8,'0'),////10碼//("st" + store no (6碼補0->8碼)) => "st00123456"
                    RRN = (nowStr.Substring(3, 1) + jc.GetDayOfYear(now).ToString("D3") + nowStr.Substring(8, 2) + stan),//12碼
                    TRANS_DATETIME = nowStr.Substring(4, 10),//10碼
                };
                RqtToBank.ICC_info = new ICC_INFO()
                {
                    AMT = AutoloadRqt.TRANS_AMT.ToString("D8"),//8碼
                    ICC_NO = AutoloadRqt.ICC_NO,//16碼
                    NECM_ID = AutoloadRqt.READER_ID.PadLeft(20, '0'),//20碼
                    REG_ID = AutoloadRqt.REG_ID.PadLeft(3, '0'),//3碼
                    RETURN_CODE = "00000000",//8碼
                    STORE_NO = AutoloadRqt.STORE_NO.PadLeft(8, '0'),//8碼
                    TX_DATETIME = nowStr//14碼
                };
                return RqtToBank;
            }
            catch (Exception ex)
            {
                throw new Exception("[Convert2BankObj] Convert Bank Object failed: " + ex.ToString());
            }
        }

        /// <summary>
        /// AutoloadRqt資料物件 => (沖正)AutoloadRqt_2Bank資料物件
        /// </summary>
        /// <param name="AutoloadRqt">內部的資料物件</param>
        /// <param name="bankCo">銀行代碼</param>
        /// <param name="hilo">取號機物件</param>
        /// <param name="message_type">服務類別(4碼)</param>
        /// <param name="processing_code">對發卡機構的Trans Type</param>
        /// <returns>給銀行的(沖正)AutoloadRqt_2Bank資料物件</returns>
        private AutoloadRqt_2Bank Convert2BankObj(AutoloadRqt_2Bank toBankObj, HiLowClient.HiLowClient hilo, string message_type, string txlogRC)
        {
            try
            {
                log.Debug("(沖正)開始轉換給銀行的資料物件");
                string stan = hilo.GetHiLowValue("STAN").ToString("D6");//STAN=(6 bytes)
                //建立傳向銀行的Poco物件並給予值
                AutoloadRqt_2Bank tempObj = ObjectCopier.JSONCopy<AutoloadRqt_2Bank>(toBankObj);
                tempObj.MESSAGE_TYPE = message_type;//4碼
                tempObj.STAN = stan;//6碼
                tempObj.RRN = toBankObj.RRN.Substring(0, 6) + stan;//12碼
                tempObj.ICC_info.RETURN_CODE = txlogRC;//加入txlog內的卡機回傳狀態(8 碼)
                tempObj.ORI_dtat = new ORI_DATA
                {
                    MESSAGE_TYPE = toBankObj.MESSAGE_TYPE,//4碼
                    TRANSACTION_DATE = toBankObj.TRANS_DATETIME,//10碼
                    STAN = toBankObj.STAN,//6碼
                    STORE_NO = toBankObj.STORE_NO.Replace("st","").PadLeft(8,'0'),//8碼
                    RRN = toBankObj.RRN//12碼
                };
                return tempObj;
            }
            catch (Exception ex)
            {
                throw new Exception(">> [State_AutoLoad][Convert2BankObj](Reversal) Convert Bank Object failed: " + ex.ToString());
            }
        }

        /// <summary>
        /// 用Socket送出轉成Json的AL2POS_Domain物件並紀錄此次交易
        /// </summary>
        /// <param name="absClientRequestHandler">Client Handler</param>
        /// <param name="socketRsp">Socket將送出的AL2POS_Domain物件</param>
        /// <param name="autoloadRqt">內部緩存資訊用的物件</param>
        /// <param name="returnCode">ReturnCode</param>
        private void SendSocketRespsone(AbsClientRequestHandler absClientRequestHandler,AL2POS_Domain socketRsp, AutoloadRqt autoloadRqt,CReturn_Code_AL returnCode)
        {
            
            try
            {
                log.Debug("(自動加值)開始送回Respsone資料物件");
                autoloadRqt.RETURN_CODE = ((int)returnCode).ToString("D6");

                //轉換要傳輸的Socket物件
                this.AutoloadRqtToSocketRsp(ref socketRsp, autoloadRqt);
                //建立Json轉換器
                ISerializer<AL2POS_Domain> parser = new JsonWorker<AL2POS_Domain>();
                string xmlStr = parser.Serialize(socketRsp);
                log.Debug(">> (Respsone)ClientSocket Send to [" + ((IPEndPoint)absClientRequestHandler.ClientSocket.RemoteEndPoint).Address.ToString() + "] =>\n" + xmlStr);
                byte[] responseBytes = parser.Serialize2Bytes(socketRsp);

                if (absClientRequestHandler.ClientSocket.Connected || absClientRequestHandler.ClientSocket.Poll(1000000,System.Net.Sockets.SelectMode.SelectWrite))
                {
                    //送出
                    int sendLength = absClientRequestHandler.ClientSocket.Send(responseBytes);
                    if (sendLength != responseBytes.Length)
                    {
                        log.Error(">> [State_AutoLoad][SendSocketRespsone]Client " + absClientRequestHandler.ClientNo + " => Send Data length:" + sendLength + @" <=\=> Real Data length:" + responseBytes.Length);
                    }
                }
                else
                {
                    log.Error(">> [State_AutoLoad][SendSocketRespsone]Client[" + absClientRequestHandler.ClientNo + "]: Not Connected!");
                }
            }
            catch (Exception ex)
            {
                log.Error(">> [State_AutoLoad][SendSocketRespsone]Send Socket Resposne Failed:\n" + ex.ToString());
                throw new Exception("[SendSocketRespsone]Send Socket Resposne Failed:\n" + ex.ToString());
            }
            finally
            {
                //AL_DBModule obDBLog=null;
                try
                {
                    ///記錄此次交易結果
                    //obDBLog = new AL_DBModule();
                    //obDBLog.OpenConnection();

                    LogModual logM = new LogModual();
                    dtrsp = DateTime.Now;

                    //記錄此次交易
                    logM.SaveTransLog<AutoloadRqt>(autoloadRqt, obDB, "", dtrqt, dtrsp);
                }
                catch(Exception ex)
                {
                    log.Error(">> [State_AutoLoad][SendSocketRespsone][SaveTransLog]Save toPOS Autoload Trans Log Failed:\n" + ex.ToString());
                }
                finally
                {
                    obDB.CloseConnection();
                    obDB = null;
                    log.Debug(">> State : AutoLoad End");
                    absClientRequestHandler.ServiceState = new State_Exit();

                }
                
            }
        }

        /// <summary>
        /// Autoload Request物件轉成要送出的AL2POS_Domain物件
        /// </summary>
        /// <param name="socketRsp">要給Socket傳回的AL2POS_Domain物件</param>
        /// <param name="autoloadRqt">內部處理用的Autoload Request物件</param>
        private void AutoloadRqtToSocketRsp(ref AL2POS_Domain socketRsp, AutoloadRqt autoloadRqt)
        {
            
            try
            {
                log.Debug("開始內部用資料物件=>POS端資料物件");
                socketRsp.AL_AMT = autoloadRqt.TRANS_AMT;
                socketRsp.AL2POS_RC = autoloadRqt.RETURN_CODE;
                socketRsp.AL2POS_SN = autoloadRqt.SN;
                socketRsp.COM_TYPE = this.SocketRqt.COM_TYPE;
                socketRsp.ICC_NO = autoloadRqt.ICC_NO;
                socketRsp.MERC_FLG = this.SocketRqt.MERC_FLG;
                socketRsp.POS_FLG = this.SocketRqt.POS_FLG;
                socketRsp.READER_ID = autoloadRqt.READER_ID;
                socketRsp.REG_ID = autoloadRqt.REG_ID;
                socketRsp.STORE_NO = autoloadRqt.STORE_NO;
            }
            catch (Exception ex)
            {
                throw new Exception("[AutoloadRqtToSocketRsp] to Socket Response failed: " + ex.ToString());
            }
        }

        /// <summary>
        /// Socket傳進來的AL2POS_Domain物件轉成Autoload Request物件
        /// </summary>
        /// <param name="socketRqt">從Socket傳進來的AL2POS_Domain物件</param>
        /// <param name="autoloadRqt">內部處理用的Autoload Request物件</param>
        /// <param name="obDB">DB模組</param>
        private void SocketRqtToAutoloadRqt(AL2POS_Domain socketRqt, ref AutoloadRqt autoloadRqt, AL_DBModule obDB)
        {
            
            if (socketRqt == null)
                throw new Exception("[SocketRqtToAutoloadRqt] Socket Request is null");
            try
            {
                log.Debug("開始POS端資料物件=>內部用資料物件");
                string merchantNo = String.Empty;
                Get_MerchantNO getMerchantNO = new Get_MerchantNO();
                string M_KIND = String.IsNullOrEmpty(autoloadRqt.M_KIND) ? System.Configuration.ConfigurationSettings.AppSettings["M_KIND"]:autoloadRqt.M_KIND;
                getMerchantNO.GetMerchantNo(M_KIND, socketRqt.MERC_FLG, obDB, out merchantNo);//用MERC_FLG欄位進資料庫查詢merchantNo

                autoloadRqt.M_KIND = M_KIND;
                autoloadRqt.ICC_NO = socketRqt.ICC_NO;
                autoloadRqt.READER_ID = socketRqt.READER_ID;
                autoloadRqt.REG_ID = socketRqt.REG_ID;
                autoloadRqt.STORE_NO = socketRqt.STORE_NO;
                autoloadRqt.TRANS_TYPE = CTransType.AUTOLOAD;
                autoloadRqt.TRANS_AMT = socketRqt.AL_AMT;
                autoloadRqt.MERCHANT_NO = merchantNo;
                log.Debug("內部用資料物件:" + JsonConvert.SerializeObject(autoloadRqt));
            }
            catch (Exception ex)
            {
                throw new Exception("[SocketRqtToAutoloadRqt] Convert to Autoload Request failed: " + ex.ToString());
            }
        }

        /// <summary>
        /// 取得端末儲存的最近一筆給銀行端的資料物件
        /// </summary>
        /// <param name="reader_id">端末Reader_ID</param>
        /// <param name="obDB">DB連線模組</param>
        /// <returns>給銀行端的資料物件</returns>
        private AutoloadRqt_2Bank GetToBankObj(string reader_id, AL_DBModule obDB)
        {
            AL_2Bank al_2bank = null;
            try
            {
                log.Debug("開始取得需沖正的原始授權資料");
                //建立2Bank模組
                al_2bank = new AL_2Bank();
                //取得此端末儲存的銀行端資料物件字串
                string toBankjsonString = al_2bank.Get_Reversal(reader_id, obDB);
                //Json String => 銀行端的資料物件(已取得銀行授權)
                AutoloadRqt_2Bank toBankObj = JsonConvert.DeserializeObject<AutoloadRqt_2Bank>(toBankjsonString);
                return toBankObj;
            }
            catch (Exception ex)
            {
                throw new Exception("[GetToBankObj]Get toBank Object Error:\n" + ex.ToString());
            }
        }

        /// <summary>
        /// 要傳給銀行的Json資料物件發佈到ActiveMQ
        /// </summary>
        /// <param name="sendToBankObj">要發佈的銀行資料物件</param>
        /// <param name="obDB">DB連線模組</param>
        private void SendBankJsonToActiveMQ(AutoloadRqt_2Bank sendToBankObj, AL_DBModule obDB, string MQdestination)
        {
            try
            {
                log.Debug("開始發佈銀行端資料物件(沖正自動加值): destination=>" + MQdestination);
                //建立toBank模組
                AL_2Bank al_2bank = new AL_2Bank();
                //將AutoloadRqt_2Bank Object轉成json String
                string sendToBankJsonStr = JsonConvert.SerializeObject(sendToBankObj);
                log.Debug(">> To Bank Json String [length:" + sendToBankJsonStr.Length + "] :\n" + sendToBankJsonStr);
                //DB紀錄傳給銀行的物件(更新)
                bool isBankup = al_2bank.InsertOrUpt_Reversal(this.SocketRqt.READER_ID, sendToBankJsonStr, obDB);
                //將給銀行的Json物件發佈到ActiveMQ存放
                bool isSendMsg = SendMessage<string>(sendToBankJsonStr, MQdestination);
                if (!(isBankup && isSendMsg))
                {
                    //發佈失敗或DB紀錄發佈訊息失敗,將給銀行的資料物件(JSON)存入ErrorLog內
                    string jsonString = JsonConvert.SerializeObject(sendToBankObj);
                    throw new Exception(">> Publish ActiveMQ Failed: JsonString=" + jsonString);
                }
            }
            catch (Exception ex)
            {
                throw new Exception("[SendBankJsonToActiveMQ] Error:\n" + ex.ToString());
            }
        }

        /// <summary>
        /// 將要給銀行的物件發佈到Active存放等待發佈
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="jsonString"></param>
        /// <param name="destination"></param>
        /// <returns></returns>
        private bool SendMessage<T>(T jsonString, string destination)
        {
            try
            {
                log.Debug("開始發佈沖正訊息到ActiveMQ:\n" + jsonString.ToString());
                //設定檔取得ActiveMQ的URL字串
                string brokerURL = System.Configuration.ConfigurationManager.AppSettings["BrokerURL"];
                //建立發佈者物件
                TopicPublisher producer = new TopicPublisher(brokerURL, destination);
                //啟動發佈程序
                producer.Start();
                //送出發佈訊息
                producer.SendMessage<T>(jsonString);
                return true;
            }
            catch (Exception ex)
            {
                log.Error(">> [State_AutoLoad][SendMessage<T>](Reversal)Send Messsage to ActiveMQ Failed: " + ex.ToString());
                return false;
            }
        }

        /// <summary>
        /// 轉換字串 ==> Enum Type
        /// </summary>
        /// <typeparam name="T">Enum型別</typeparam>
        /// <param name="value">字串</param>
        /// <param name="ignoreCase">是否忽略字串大小寫</param>
        /// <returns>Enum型別</returns>
        private T ParseEnum<T>(string value,bool ignoreCase = false)
        {
            return (T)Enum.Parse(typeof(T), value, ignoreCase);
        }
        #endregion
    }
}
