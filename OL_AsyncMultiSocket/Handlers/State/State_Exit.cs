﻿using Common.Logging;
using System.Net;
//

namespace AsyncMultiSocket.Handlers.State
{
    /// <summary>
    /// 狀態:離開處理程序並關閉此客戶端
    /// </summary>
    public class State_Exit : IState
    {
        protected static readonly ILog log = LogManager.GetLogger(typeof(State_Exit));

        public void Handle(AbsClientRequestHandler absClientRequestHandler)
        {
            log.Debug(">>[" + ((IPEndPoint)absClientRequestHandler.ClientSocket.RemoteEndPoint).ToString() + "] State : Exit");
            absClientRequestHandler.KeepService = false;
            absClientRequestHandler.SocketServer.RemoveClient(absClientRequestHandler.ClientNo);
        }
    }
}
