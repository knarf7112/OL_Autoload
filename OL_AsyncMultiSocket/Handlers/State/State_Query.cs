﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
//
using Common.Logging;
using OL_AsyncMultiSocket.Utilities;
using ALCommon;
using CardValidator;
using OL_Autoload_Lib.Controller.ALOL.CardSt_Check;
using OL_Autoload_Lib.Controller.ALOL.Bkl_Check;
using OL_Autoload_Lib;
using OL_Autoload_Lib.Controller.Common;
using AsyncMultiSocket.Handlers.State.Entities;
using OL_Autoload_Lib.Controller.Common.Record;
using System.Net;
using Newtonsoft.Json;


namespace AsyncMultiSocket.Handlers.State
{
    /// <summary>
    /// (05)自動加值查詢流程
    /// </summary>
    class State_Query : IState
    {
        private static readonly ILog log = LogManager.GetLogger(typeof(State_Query));
        #region Property
        private AL_DBModule obDB { get; set; }
        /// <summary>
        /// 從Socket接收的物件
        /// </summary>
        private AL2POS_Domain SocketRqt { get; set; }
        /// <summary>
        /// 從Socket送出的物件
        /// </summary>
        private AL2POS_Domain SocketRsp { get; set; }
        #endregion

        #region Field
        /// <summary>
        /// 內部處理用的物件
        /// </summary>
        private CheckStatusRqt queryRqt;
        DateTime dtrqt = DateTime.Now;
        DateTime dtrsp = DateTime.Now;
        #endregion

        public State_Query()
        {
        }

        /// <summary>
        /// 要處理的查詢流程
        /// </summary>
        /// <param name="absClientRequestHandler">Client Handler</param>
        public void Handle(AbsClientRequestHandler absClientRequestHandler)
        {
           
            //ALCommon.AL_DBModule obDB = null;
            try
            {
                byte[] receiveBytes = new byte[4096];
                int receiveLength = absClientRequestHandler.ClientSocket.Receive(receiveBytes);
                if (receiveLength == 0)
                {
                    absClientRequestHandler.ServiceState = new State_Exit();
                    return;
                }
                else
                {
                    log.Debug(">> State : Query");

                    Array.Resize(ref receiveBytes, receiveLength);
                    //建立轉換模組並將收到的Json陣列轉回物件
                    ISerializer<AL2POS_Domain> pocoParser = new JsonWorker<AL2POS_Domain>();
                    //還原Socket傳來的物件
                    this.SocketRqt = pocoParser.Deserialize(receiveBytes);
                    //建立Socket要傳出的物件(空)
                    this.SocketRsp = new AL2POS_Domain();
                    //建立內部緩存資訊用的POCO
                    this.queryRqt = new CheckStatusRqt();
                    //建立DB模組
                    this.obDB = new ALCommon.AL_DBModule();
                    //建立卡號檢驗模組
                    VertifyTool2 vertifyObj = new VertifyTool2();
                    //建立卡片有效性檢查模組
                    CardStatus_Check checkCardStatus = new CardStatus_Check();
                    //建立黑名單檢驗模組
                    BkL_Check checkBlackList = new BkL_Check();
                    //建立取號模組
                    HiLowClient.HiLowClient hilo = new HiLowClient.HiLowClient();
                    //開啟連線
                    this.obDB.OpenConnection();
                    //Socket物件轉成自動加值(查詢通用)物件
                    this.SocketRqtToAutoloadRqt(this.SocketRqt, ref this.queryRqt, this.obDB);
                    log.Debug("(查詢)流程一:開始驗證卡號是否為正常卡");
                    #region 1.驗證卡號是否為正常卡(boolean)
                    bool isNormalCard = vertifyObj.CardChkVertify(this.queryRqt.ICC_NO);
                    bool isCreditCard = vertifyObj.CreditCardChkVertify(this.queryRqt.ICC_NO);
                    if (!(isNormalCard && isCreditCard))
                    {
                        SendSocketRespsone(absClientRequestHandler, SocketRsp, this.queryRqt, CReturn_Code_AL.Invalid_Card);
                        return;
                    }
                    #endregion
                    log.Debug("(查詢)流程二:開始驗證卡號是否為有效卡");
                    #region 2.檢核卡片是否為有效卡(boolean)
                    CardStatus isValidCard = ParseEnum<CardStatus>(checkCardStatus.AL_CheckCardStatus(this.obDB, this.queryRqt.ICC_NO));
                    switch (isValidCard)
                    {
                        case CardStatus.Other:
                            break;
                        case CardStatus.Status3478:
                            SendSocketRespsone(absClientRequestHandler, SocketRsp, this.queryRqt, CReturn_Code_AL.Invalid_Card);
                            return;
                        case CardStatus.Status569:
                            SendSocketRespsone(absClientRequestHandler, SocketRsp, this.queryRqt, CReturn_Code_AL.Black_List);
                            return;
                        default:
                            //Null
                            SendSocketRespsone(absClientRequestHandler, SocketRsp, this.queryRqt, CReturn_Code_AL.Ap_Error);
                            return;
                    }
                    #endregion
                    log.Debug("(查詢)流程三:開始驗證卡號是否在黑名單");
                    #region 3.黑名單檢核(boolean)
                    bool isBlack = checkBlackList.AL_CheckBKL(obDB, this.queryRqt.ICC_NO);
                    if (isBlack)
                    {
                        SendSocketRespsone(absClientRequestHandler, SocketRsp, this.queryRqt, CReturn_Code_AL.Black_List);
                        return;
                    }
                    #endregion

                    #region 3.取得序號 and 傳回正常交易Socket物件
                    long SN = hilo.GetHiLowValue("Autoload");
                    this.queryRqt.SN = SN.ToString("D8");
                    SendSocketRespsone(absClientRequestHandler, SocketRsp, this.queryRqt, CReturn_Code_AL.Success);
                    #endregion
                }
            }
            catch (Exception ex)
            {
                log.Error(">> State_Query Failed:" + ex.Message);


                try
                {
                    obDB.OpenConnection();
                    LogModual LogM = new LogModual();
                    LogM.SaveErrorLog(obDB, this.GetType().Name + ex.Message, "ALQY");
                }
                catch
                {
                    log.Error(">> Save Error Failed:" + ex.Message);
                }
                finally
                {
                    SendSocketRespsone(absClientRequestHandler, SocketRsp, this.queryRqt, CReturn_Code_AL.Ap_Error);
                }
            }
            finally
            {
                try
                {
                    if (obDB != null && obDB.ALCon != null)
                    {
                        obDB.CloseConnection();
                        obDB = null;
                    }
                }
                catch (Exception ex)
                {
                    log.Error(">>[State_Query][Handle][finally] Close DB Connection Error:" + ex.ToString());
                }
            }
        }

        #region Private Method
        /// <summary>
        /// 用Socket送出轉成Json的AL2POS_Domain物件
        /// </summary>
        /// <param name="absClientRequestHandler">Client Handler</param>
        /// <param name="socketRsp">Socket將送出的AL2POS_Domain物件</param>
        /// <param name="autoloadRqt">內部緩存資訊用的物件</param>
        /// <param name="returnCode">ReturnCode</param>
        private void SendSocketRespsone(AbsClientRequestHandler absClientRequestHandler, AL2POS_Domain socketRsp, CheckStatusRqt autoloadRqt, CReturn_Code_AL returnCode)
        {
            try
            {
                log.Debug("(查詢)開始送回Respsone資料物件");
                autoloadRqt.RETURN_CODE = ((int)returnCode).ToString("D6");

                //轉換要傳輸的Socket物件
                this.AutoloadRqtToSocketRsp(ref socketRsp, autoloadRqt);
                //建立Json轉換器
                ISerializer<AL2POS_Domain> parser = new JsonWorker<AL2POS_Domain>();
                byte[] responseBytes = parser.Serialize2Bytes(socketRsp);
                string sendJSONstr = parser.Serialize(socketRsp);
                log.Debug(">> (Respsone)ClientSocket Send to [" + ((IPEndPoint)absClientRequestHandler.ClientSocket.RemoteEndPoint).Address.ToString() + "] =>\n" + sendJSONstr);
                if (absClientRequestHandler.ClientSocket.Connected || absClientRequestHandler.ClientSocket.Poll(1000000, System.Net.Sockets.SelectMode.SelectWrite))
                {
                    //送出
                    int sendLength = absClientRequestHandler.ClientSocket.Send(responseBytes);
                    if (sendLength != responseBytes.Length)
                    {
                        log.Error(">> " + absClientRequestHandler.ServiceState.ToString() + " Client " + absClientRequestHandler.ClientNo + " 送出資料大小不一致");
                    }
                }
                else
                {
                    log.Error(">> " + absClientRequestHandler.ServiceState.ToString() + " Client[" + absClientRequestHandler.ClientNo + "]: Not Connected!");
                }
            }
            catch (Exception ex)
            {
                log.Error(">> " + absClientRequestHandler.ServiceState.ToString() + " Failed: " + ex.Message);
            }
            finally
            {
                //AL_DBModule obDBLog = null;
                try
                {
                    ///記錄此次交易結果
                    //obDBLog = new AL_DBModule();
                    //obDBLog.OpenConnection();

                    dtrsp = DateTime.Now;
                    LogModual logM = new LogModual();
                    //記錄此次交易
                    logM.SaveTransLog<CheckStatusRqt>(autoloadRqt, this.obDB, "", dtrqt, dtrsp);

                }
                catch (Exception ex)
                {
                    log.Error(">>Save Query Trans Log Failed: " + ex.Message);
                }
                finally
                {
                    //obDBLog.CloseConnection();
                    //obDBLog = null;
                    log.Debug(">> State : Query End");
                    absClientRequestHandler.ServiceState = new State_Exit();
                }
            }
        }

        /// <summary>
        /// Autoload Request物件轉成要送出的AL2POS_Domain物件
        /// </summary>
        /// <param name="socketRsp">要給Socket傳回的AL2POS_Domain物件</param>
        /// <param name="autoloadRqt">內部處理用的Autoload Request物件</param>
        private void AutoloadRqtToSocketRsp(ref AL2POS_Domain socketRsp, CheckStatusRqt autoloadRqt)
        {
            try
            {
                log.Debug("開始內部用資料物件=>POS端資料物件");
                socketRsp.AL_AMT = autoloadRqt.TRANS_AMT;
                socketRsp.AL2POS_RC = autoloadRqt.RETURN_CODE;
                socketRsp.AL2POS_SN = autoloadRqt.SN == null ? "00000000" : autoloadRqt.SN;
                socketRsp.COM_TYPE = this.SocketRqt.COM_TYPE;
                socketRsp.ICC_NO = autoloadRqt.ICC_NO;
                socketRsp.MERC_FLG = this.SocketRqt.MERC_FLG;
                socketRsp.POS_FLG = this.SocketRqt.POS_FLG;
                socketRsp.READER_ID = autoloadRqt.READER_ID;
                socketRsp.REG_ID = autoloadRqt.REG_ID;
                socketRsp.STORE_NO = autoloadRqt.STORE_NO;
                log.Debug("(查詢)Response:" + JsonConvert.SerializeObject(socketRsp));
            }
            catch (Exception ex)
            {
                log.Error(">> CheckStatus Request ==> Socket Response Failed:" + ex.Message);
                throw ex;
            }
        }
        
        /// <summary>
        /// Socket傳進來的AL2POS_Domain物件轉成Autoload Request物件
        /// </summary>
        /// <param name="socketRqt">從Socket傳進來的AL2POS_Domain物件</param>
        /// <param name="autoloadRqt">內部處理用的Autoload Request物件</param>
        /// <param name="obDB">DB模組</param>
        private void SocketRqtToAutoloadRqt(AL2POS_Domain socketRqt, ref CheckStatusRqt autoloadRqt, AL_DBModule obDB)
        {
            try
            {
                log.Debug("開始POS端資料物件=>內部用資料物件");
                string merchantNo = String.Empty;
                Get_MerchantNO getMerchantNO = new Get_MerchantNO();
                string M_KIND = String.IsNullOrEmpty(autoloadRqt.M_KIND) ? System.Configuration.ConfigurationSettings.AppSettings["M_KIND"] : autoloadRqt.M_KIND;
                getMerchantNO.GetMerchantNo(M_KIND, socketRqt.MERC_FLG, obDB, out merchantNo);//用MERC_FLG欄位進資料庫查詢merchantNo

                autoloadRqt.M_KIND = M_KIND;
                autoloadRqt.ICC_NO = socketRqt.ICC_NO;
                autoloadRqt.READER_ID = socketRqt.READER_ID;
                autoloadRqt.REG_ID = socketRqt.REG_ID;
                autoloadRqt.STORE_NO = socketRqt.STORE_NO;
                autoloadRqt.TRANS_TYPE = CTransType.CHECK_CC;
                autoloadRqt.TRANS_AMT = socketRqt.AL_AMT;
                autoloadRqt.MERCHANT_NO = merchantNo;
            }
            catch (Exception ex)
            {
                log.Error(">> Socket Request Convert to CheckStatus Request Failed:" + ex.Message);
                throw ex;
            }
        }

        /// <summary>
        /// 轉換字串 ==> Enum Type
        /// </summary>
        /// <typeparam name="T">Enum型別</typeparam>
        /// <param name="value">字串</param>
        /// <param name="ignoreCase">是否忽略字串大小寫</param>
        /// <returns>Enum型別</returns>
        private T ParseEnum<T>(string value, bool ignore = false)
        {
            return (T)Enum.Parse(typeof(T), value, ignore);
        }
        #endregion

    }
}
