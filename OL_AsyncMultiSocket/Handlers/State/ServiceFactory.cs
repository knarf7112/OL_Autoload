﻿using AsyncMultiSocket.Handlers.State;
using AsyncMultiSocket.State;
using System;

namespace OL_AsyncMultiSocket.Handlers.State
{
    /// <summary>
    /// 服務選擇工廠
    /// </summary>
    public static class ServiceFactory
    {
        private static ServiceSelect service;
        public static IState GetService(string serviceName, bool ignoreCase = false)
        {
            try
            {
                service = (ServiceSelect)Enum.Parse(typeof(ServiceSelect), serviceName, ignoreCase);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            switch (service)
            {
                case ServiceSelect.HiLo:
                    return new State_HiLow("HiLo");
                case ServiceSelect.AutoLoad:
                    return new State_AutoLoad();
                case ServiceSelect.Query:
                    return new State_Query();
                case ServiceSelect.Txlog:
                    return new State_Txlog();
                case ServiceSelect.Reversal:
                    return new State_Reversal();
                case ServiceSelect.BankListener:
                    return new State_BankListener();
                case ServiceSelect.LossReportOrAddRejectAuth:
                    return new State_LossReportOrAddRejectAuth();
                default:
                    return new State_Exit();
            }
        }
    }

    public enum ServiceSelect
    {
        HiLo = 0,
        AutoLoad = 1,
        Query = 2,
        Txlog = 3,
        Reversal = 4,
        BankListener = 5,
        LossReportOrAddRejectAuth = 6
    }
}
