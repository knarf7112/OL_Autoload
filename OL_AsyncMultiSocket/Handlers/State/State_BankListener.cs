﻿using System;
using System.Text;
//
using Common.Logging;
using System.Net.Sockets;
using Newtonsoft.Json;
//using MqTest.Producer;//old
using OL_Autoload_Lib;
using ALCommon;
using OL_Autoload_Lib.Controller.Common.Record;
using ActiveMqLab.Producer;//new version

namespace AsyncMultiSocket.Handlers.State
{
    /// <summary>
    /// 負責接收銀行的SignOn/Off/Echo Test的端口
    /// </summary>
    public class State_BankListener : IState
    {
        private static readonly ILog log = LogManager.GetLogger(typeof(State_BankListener));
        public void Handle(AbsClientRequestHandler absClientRequestHandler)
        {
            //建立DB模組
            AL_DBModule obDB = null;
            try
            { 
                byte[] receieveBytes = new byte[4096];
                SocketError sckErr;
                //接收門神傳來的銀行物件
                int receiveLength = absClientRequestHandler.ClientSocket.Receive(receieveBytes, 0, receieveBytes.Length, SocketFlags.None, out sckErr);
                //Socket Transffer Error OR Receive 0 and exit
                if (receiveLength == 0)
                {
                    return;
                }
                if (sckErr != SocketError.Success) 
                {
                    throw new SocketException((int)sckErr); 
                }
                if(receiveLength > 0)
                {
                    log.Debug(">> State : Bank Listener");

                    obDB = new AL_DBModule();

                    #region 1.執行Parsing將Request電文轉換成處理物件
                    //byte[] => Sign_Domain Object
                    Sign_Domain RequestObj = ParseFromRequestBytes(receieveBytes, receiveLength);
                    //Sign_Domain Object => AutoloadRqt_NetMsg Oject
                    AutoloadRqt_NetMsg autoloadRqt_NetMsg = ParseFromRequestObj(RequestObj);
                    #endregion
                    
                    #region 2.執行Business-Publish Bank Status changed or Return Echo Test signal 
                    //建立SQL模組
                    Network_Msg network_Msg = new Network_Msg();
                    
                    string destination = null;
                    if (autoloadRqt_NetMsg != null && autoloadRqt_NetMsg.MESSAGE_TYPE == "0800")
                    {
                        string bankCo = autoloadRqt_NetMsg.BANK_CODE.PadLeft(4, '0');
                        if (autoloadRqt_NetMsg.INFO_CODE == "071" || autoloadRqt_NetMsg.INFO_CODE == "072")
                        {
                            //開啟連線
                            obDB.OpenConnection();
                            //更新銀行狀態
                            network_Msg.Upt_NetMsg(autoloadRqt_NetMsg, obDB);
                            //修改DicBanks內(發佈者端)的狀態
                            AbsClientRequestHandler.DicBanks[bankCo].INFO_CODE = autoloadRqt_NetMsg.INFO_CODE;
                            AbsClientRequestHandler.DicBanks[bankCo].STATUS = (AbsClientRequestHandler.DicBanks[bankCo].INFO_CODE == "071" ? "0" : "1");
                            AbsClientRequestHandler.DicBanks[bankCo].MOD_TIME = autoloadRqt_NetMsg.TRANS_DATETIME;
                            //設定發佈目的地名稱=> MESSAGE_TYPE(4bytes) + BANK_CODE(4bytes)
                            log.Debug("Local DicBanks[" + 
                                bankCo + "]:" +
                                "\nINFO_CODE:" + AbsClientRequestHandler.DicBanks[bankCo].INFO_CODE +
                                "\nSTATUS:" + AbsClientRequestHandler.DicBanks[bankCo].STATUS +
                                "\nMOD_TIME:" + AbsClientRequestHandler.DicBanks[bankCo].MOD_TIME
                                );
                            destination = bankCo.PadLeft(4, '0') + autoloadRqt_NetMsg.MESSAGE_TYPE.PadLeft(4, '0');//0000 + 0800設定發佈到MQ對應的Name 
                        }
                        else if (autoloadRqt_NetMsg.INFO_CODE == "301")
                        {
                            //Echo Test Process
                            if (AbsClientRequestHandler.DicBanks[bankCo].STATUS == "0")
                            {
                                autoloadRqt_NetMsg.RC = "00";//一般模式
                            }
                            if (AbsClientRequestHandler.DicBanks[bankCo].STATUS == "1")
                            {
                                autoloadRqt_NetMsg.RC = "N0";//代行授權模式
                            }
                        }
                        else
                        {
                            //Others
                            throw new Exception("autoloadRqt_NetMsg INFO_CODE not defined:" + autoloadRqt_NetMsg.INFO_CODE);
                        }
                    }
                    else
                    {
                        throw new Exception("autoloadRqt_NetMsg is null or MESSAGE_TYPE is not '0800':");
                    }
                    //檢查destination
                    if (destination != null)
                    {
                        string jsonRqt = JsonConvert.SerializeObject(autoloadRqt_NetMsg);
                        //發佈訊息
                        try
                        {
                            SendMessage<string>(jsonRqt, destination);
                        }
                        catch (Exception ex)
                        {
                            log.Error(">> [State_BankListener][Handle]SendMessage Error: " + ex.ToString() + "\nMessageData => " + jsonRqt);
                        }
                    }
                    
                    #endregion

                    #region 3.執行Parsing將物件轉回Response電文
                    Sign_Domain ResponseObj = ParseToResponseObj(autoloadRqt_NetMsg);
                    byte[] ResponseBytes = ParseToResponseBytes(ResponseObj);
                    #endregion

                    #region 4.送回電文
                    //送回Json物件
                    SocketError sckErr2;
                    int sendLength = absClientRequestHandler.ClientSocket.Send(ResponseBytes, 0, ResponseBytes.Length, SocketFlags.None, out sckErr2);
                    if (sendLength != ResponseBytes.Length || sckErr2 != SocketError.Success)
                    {
                        log.Error(">> [Socket Error] Code:" + sckErr.ToString());
                        throw new SocketException((int)sckErr2);
                    }
                    #endregion
                  
                }
            }
            catch (Exception ex)
            {
                
                log.Error(">> [State_BankListener][Handle] Error:" + ex.ToString());
                try
                {
                    //執行DB模組紀錄ErrorLog
                    obDB.OpenConnection();
                    LogModual LogM = new LogModual();

                    LogM.SaveErrorLog(obDB, this.GetType().Name + ex.Message, "BankListener");

                }
                catch
                {
                    log.Error(">> [State_BankListener][Handle]Save Log Failed:" + ex.ToString());
                }
            }
            finally
            {
                if (obDB != null && obDB.ALCon != null)
                {
                    obDB.CloseConnection();
                    obDB = null;
                }
                //關閉此次連線訊息
                absClientRequestHandler.ServiceState = new State_Exit();
            }
        }

        #region Private Method
        /// <summary>
        /// Deserialize(JSON) Socket接收進來的陣列資料(Encoding:UTF8)
        /// </summary>
        /// <param name="requestBytes">Socket接收的資料</param>
        /// <param name="requestLength">Socket接收的資料長度</param>
        /// <returns>Sign_Domain物件</returns>
        private Sign_Domain ParseFromRequestBytes(byte[] requestBytes, int requestLength)
        {
            string jsonString = Encoding.UTF8.GetString(requestBytes, 0, requestLength).TrimEnd('\n');
            try
            {
                log.Debug("開始轉換Requestbytes=>Sign資料物件");
                Sign_Domain signObj = JsonConvert.DeserializeObject(jsonString, typeof(Sign_Domain)) as Sign_Domain;
                signObj.RC = "00";
                string signStr = JsonConvert.SerializeObject(signObj);
                log.Debug("Sign Object:" + signStr);
                return signObj;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Sign_Domain資料物件 => AutoloadRqt_NetMsg資料物件
        /// </summary>
        /// <param name="requestObj">Sign_Domain資料物件</param>
        /// <returns>AutoloadRqt_NetMsg資料物件</returns>
        private AutoloadRqt_NetMsg ParseFromRequestObj(Sign_Domain requestObj)
        {
            try
            {
                log.Debug("(Request)開始銀行端資料物件(SignObj)=>內部用資料物件");
                AutoloadRqt_NetMsg autoloadRqt_NetMsg = new AutoloadRqt_NetMsg();
                autoloadRqt_NetMsg.BANK_CODE = requestObj.BankCode;
                autoloadRqt_NetMsg.INFO_CODE = requestObj.infCode;
                autoloadRqt_NetMsg.MESSAGE_TYPE = requestObj.COM_Type;
                autoloadRqt_NetMsg.RC = requestObj.RC;
                autoloadRqt_NetMsg.STAN = requestObj.traceNumber;
                autoloadRqt_NetMsg.TRANS_DATETIME = requestObj.transDateTime;
                return autoloadRqt_NetMsg;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Serialize(JSON) Socket要送出的Sign_Domain物件
        /// </summary>
        /// <param name="sign_Domain">要轉換的Sign_Domain資料物件</param>
        /// <returns>陣列資料(JSON格式)</returns>
        private byte[] ParseToResponseBytes(Sign_Domain sign_Domain)
        {
            try
            {
                log.Debug("開始銀行端資料物件(SignObj)===>(jsonObj)===>Byte Array");
                string jsonString = JsonConvert.SerializeObject(sign_Domain);
                byte[] jsonBytes = Encoding.UTF8.GetBytes(jsonString);
                return jsonBytes;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// AutoloadRqt_NetMsg資料物件 => Sign_Domain資料物件
        /// </summary>
        /// <param name="autoloadRqt_NetMsg">AutoloadRqt_NetMsg資料物件</param>
        /// <returns>Sign_Domain資料物件</returns>
        private Sign_Domain ParseToResponseObj(AutoloadRqt_NetMsg autoloadRqt_NetMsg)
        {
            try
            {
                log.Debug("(Response)開始內部用資料物件=>銀行端資料物件(SignObj)");
                Sign_Domain sign_Domain = new Sign_Domain();
                sign_Domain.BankCode = autoloadRqt_NetMsg.BANK_CODE;
                sign_Domain.infCode = autoloadRqt_NetMsg.INFO_CODE;
                sign_Domain.COM_Type = "0810";// autoloadRqt_NetMsg.MESSAGE_TYPE;//要改成0810
                sign_Domain.RC = autoloadRqt_NetMsg.RC;
                sign_Domain.traceNumber = autoloadRqt_NetMsg.STAN;
                sign_Domain.transDateTime = autoloadRqt_NetMsg.TRANS_DATETIME;
                return sign_Domain;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// 發佈訊息到ActiveMQ上
        /// URL設定於App.config[name="BrokerURL"]
        /// </summary>
        /// <typeparam name="T">Message型別</typeparam>
        /// <param name="jsonString">Message物件</param>
        /// <param name="destination">存放在ActiveMQ的Name</param>
        private void SendMessage<T>(T jsonString, string destination)
        {
            try
            {
                log.Debug("(BankPublish)開始發佈訊息到ActiveMQ:\n" + " destination:" + destination + "  jsonString:" + jsonString.ToString());
                string brokerURL = System.Configuration.ConfigurationManager.AppSettings["BrokerURL"];//"tcp://10.27.68.155:61616";
                
                TopicPublisher producer = new TopicPublisher(brokerURL, destination);
                producer.Start();
                producer.SendMessage<T>(jsonString);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion
    }
}
