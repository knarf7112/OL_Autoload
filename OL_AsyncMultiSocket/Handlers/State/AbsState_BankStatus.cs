﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Sockets;
//
using MqTest.Consumer;
using AsyncMultiSocket.Handlers.State.Entities;

namespace AsyncMultiSocket.Handlers.State
{

    public abstract class AbsState_BankStatus
    {
        //public event StatusChangeHandler OnChangeBankAlive;

        //public static IDictionary<string, string> DicTask;//存放未連線時的自動加值沖正電文
        
        //public static bool bankAlive;//銀行端連線狀態
        public static IDictionary<string, Bank> DicBanks = new Dictionary<string, Bank>();

        private static object bLock = new object();
        /// <summary>
        /// 初始化銀行狀態
        /// </summary>
        public AbsState_BankStatus()
        {
            if (DicBanks.Count == 0)
            {
                //多銀行代號 => 一家銀行
                Bank firstBank = new Bank() { BankName="第一銀", CanConnect = false };

                //add Bank number
                DicBanks.Add("0820", firstBank);
                DicBanks.Add("0821", firstBank);
                DicBanks.Add("0822", new Bank() { BankName= "中信銀", CanConnect = false });
            }

        }

        //private int reTryCount = 0;
        //private const int maxReTryCount = 3;
        //private object lockObj = new object();

        //protected TopicSubscriber subscriber;
        //protected string BrokerURLP { get; set; }
        //protected string Destination { get; set; }
        //public bool BackAlive
        //{
        //    get
        //    {
        //        return bankAlive;
        //    }
        //    set
        //    {
        //        if (OnChangeBankAlive != null)
        //        {
        //            OnChangeBankAlive.Invoke();
        //        }
        //        bankAlive = value;
        //    }
        //}

        //public AbsState_ConnectBank()
        //{
        //    this.BrokerURLP = "tcp://localhost:61616";
        //    this.Destination = "BankAlive";
        //    this.subscriber = new TopicSubscriber(this.BrokerURLP,this.Destination);
        //    this.subscriber.OnMessageReceived += (
        //        BankBool => {
        //            string tmp = (string)BankBool;
        //            if (tmp == "true")
        //            {
        //                lock (lockObj)
        //                {
        //                    bankAlive = true;
        //                }
        //            }
        //            else
        //            {
        //                lock (lockObj)
        //                {
        //                    bankAlive = false;
        //                }
        //            }
        //        });
        //}

        //protected void ConnectToBack(IPEndPoint bankIPEndPoint, byte[] sendDataToBank, ref byte[] receiveDataFromBank)
        //{
        //    try
        //    {
        //        TcpClient tcpClient = new TcpClient();
        //        tcpClient.Connect(bankIPEndPoint);
        //        if (tcpClient.Connected)
        //        {
        //            NetworkStream ns = tcpClient.GetStream();
        //            ns.Write(sendDataToBank, 0, sendDataToBank.Length);
        //            int receiveLength = ns.Read(receiveDataFromBank, 0, receiveDataFromBank.Length);
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        //log.Error(">>銀行端 Error:" + ex.Message);
        //        //重新連線銀行測試3次都有問題則改走代行授權流程
        //        if (reTryCount < maxReTryCount)
        //        {
        //            //重新連線銀行測試3次
        //            reTryCount++;
        //            this.ConnectToBack(bankIPEndPoint, sendDataToBank, ref receiveDataFromBank);
        //        }
        //        else
        //        {
        //            //BackAlive = false;
        //            //執行代行授權流程

        //        }
        //    }
        //}
    }
}
