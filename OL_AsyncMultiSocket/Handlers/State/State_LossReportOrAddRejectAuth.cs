﻿using System;
//
using Common.Logging;
using OL_AsyncMultiSocket.Utilities;
using ALCommon;
using OL_Autoload_Lib;
using OL_Autoload_Lib.Controller.Common.Record;
using OL_Autoload_Lib.Model.ReturnCode;
using AsyncMultiSocket.Handlers.State.Entities;
using System.Net;
using OL_Autoload_Lib.Controller.ALOL.CardSt_Check;
using System.Transactions;
using OL_Autoload_Lib.Controller.Common;

namespace AsyncMultiSocket.Handlers.State
{
    /// <summary>
    /// (0302)對銀行端的連線掛失卡片/取消掛失卡片/新增拒絕代行授權名單
    /// </summary>
    public class State_LossReportOrAddRejectAuth : IState
    {
        private readonly static ILog log = LogManager.GetLogger(typeof(State_LossReportOrAddRejectAuth));

        #region Property
        /// <summary>
        /// 從Socket接收的物件
        /// </summary>
        private AutoloadRqt_FBank SocketRqt { get; set; }
        /// <summary>
        /// 從Socket送出的物件
        /// </summary>
        private AutoloadRqt_FBank SocketRsp { get; set; }
        /// <summary>
        /// DB連線模組
        /// </summary>
        private AL_DBModule obDB { get; set; }
        /// <summary>
        /// 序列化轉換元件
        /// </summary>
        private ISerializer<AutoloadRqt_FBank> pocoParser { get; set; }

        /// <summary>
        /// 銀行端連線掛失檢核模組
        /// </summary>
        private AL_FBank al_fBank { get; set; }
        /// <summary>
        /// 卡主檔檢核模組
        /// </summary>
        private CardStatus_Check cardStatus_Check { get; set; }
        #endregion

        #region Field
        /// <summary>
        /// 內部用資訊物件
        /// </summary>
        private AutoloadRqt_FBank autoloadRqt_FBank;
        /// <summary>
        /// 卡主檔狀態
        /// </summary>
        private string _oriCstatus = string.Empty;
        private DateTime dtrqt = DateTime.Now;
        private DateTime dtrsp = DateTime.Now;
        #endregion

        public void Handle(AbsClientRequestHandler absClientRequestHandler)
        {
            try
            {
                //dtrsp = dtrqt = DateTime.Now;
                byte[] receiveBytes = new byte[4096];
                int receiveLength = absClientRequestHandler.ClientSocket.Receive(receiveBytes);
                if (receiveLength == 0)
                {
                    absClientRequestHandler.ServiceState = new State_Exit();
                    return;
                }
                else
                {
                    log.Debug(">> State : Loss Report");

                    Array.Resize(ref receiveBytes, receiveLength);
                    //建立轉換模組並將收到的Json陣列轉回物件
                    this.pocoParser = new JsonWorker<AutoloadRqt_FBank>();
                    //還原Socket傳來的物件
                    this.SocketRqt = pocoParser.Deserialize(receiveBytes);
                    //建立Socket要傳出的物件(初始化)
                    this.SocketRsp = new AutoloadRqt_FBank();

                    //SocketRequest轉換成內部用的資訊物件(複製一份)
                    this.autoloadRqt_FBank = ObjectCopier.JSONCopy<AutoloadRqt_FBank>(this.SocketRqt);
                    //建立DB連線模組
                    this.obDB = new AL_DBModule();
                    //建立銀行端連線掛失檢核模組
                    this.al_fBank = new AL_FBank();
                    //建立卡主檔檢核模組
                    this.cardStatus_Check = new CardStatus_Check();
                    //開啟連線
                    obDB.OpenConnection();

                    //檢查銀行代碼若為4碼則轉成3碼..[暫時先放這]
                    this.autoloadRqt_FBank.BANK_CODE = (this.autoloadRqt_FBank.BANK_CODE.Length == 4) ? this.autoloadRqt_FBank.BANK_CODE.Substring(1, 3) : this.autoloadRqt_FBank.BANK_CODE; 

                    log.Debug("(連線掛失/拒絕代行授權名單)Request data:" + pocoParser.Serialize(this.SocketRqt));

                    #region 連線掛失流程/掛失取消流程
                    if (this.SocketRqt.MESSAGE_TYPE == "0302" && this.SocketRqt.PROCESSING_CODE == "990176")
                    {
                        this.DoLossReport_Flow(absClientRequestHandler);
                    }
                    #endregion
                    #region 新增拒絕代行授權名單流程
                    else if (this.SocketRqt.MESSAGE_TYPE == "0302" && this.SocketRqt.PROCESSING_CODE == "990178")
                    {
                        this.AddRejectAuth_Flow(absClientRequestHandler);
                    }
                    #endregion
                    else
                    {
                        //Message Type 錯誤
                        this.SendSocketRespsone(absClientRequestHandler, this.SocketRsp, this.autoloadRqt_FBank, (int)BReturn_Code_LossReport.OtherError);
                    }
                }
            }
            #region 錯誤處理
            catch (Exception ex)
            {
                
                //寫入DB的Error Log表格
                try
                {
                    log.Error(">> [State_LossReport][Handle] Error:" + ex.ToString());
                    this.obDB = (this.obDB == null) ? new AL_DBModule() : this.obDB;
                    obDB.OpenConnection();
                    LogModual LogM = new LogModual();
                    LogM.SaveErrorLog(obDB, this.GetType().Name + ex.Message, "ALRL");
                }
                catch(Exception ex2)
                {
                    log.Error(">> [State_LossReport][Handle]Save Error Failed:" + ex2.ToString());
                }
                finally
                {
                    //obDB.CloseConnection();
                    //傳回物件資訊
                    try
                    {
                        if (this.SocketRsp == null) this.SocketRsp = new AutoloadRqt_FBank();
                        if (this.autoloadRqt_FBank == null) this.autoloadRqt_FBank = new AutoloadRqt_FBank();
                        SendSocketRespsone(absClientRequestHandler, this.SocketRsp, this.autoloadRqt_FBank, (int)BReturn_Code_LossReport.OtherError);
                    }
                    catch (Exception ex2)
                    {
                        log.Error(">> [State_LossReport][Handle][SendSocketRespsone]ClientSocket Connection Error:" + ex2.ToString());
                    }
                }
            }
            #endregion
            finally
            {
                try
                {
                    if (obDB != null && obDB.ALCon != null)
                    {
                        obDB.CloseConnection();
                        obDB = null;
                    }
                }
                catch (Exception ex)
                {
                    log.Error(">>[State_LossReport][Handle][finally] Close DB Connection Error:" + ex.ToString());
                }
            }
        }

        #region Private
        /// <summary>
        /// 執行(0302)連線掛失流程/(0302)掛失取消流程[ProcessingCode = 990176]
        /// </summary>
        /// <param name="absClientRequestHandler">ClientRequestHandler物件</param>
        private void DoLossReport_Flow(AbsClientRequestHandler absClientRequestHandler)
        {
            try
            {
                #region 狀態:新增卡片掛失
                if (this.SocketRqt.FILE_UPDATE_CODE == "1")
                {
                    //1.是否重複掛失
                    log.Debug("1.開始檢核是否重複掛失");
                    bool repeatReport = al_fBank.Reportloss_Check(this.SocketRqt.ICC_NO, obDB);
                    if (repeatReport)
                    {
                        this.SendSocketRespsone(absClientRequestHandler, this.SocketRsp, this.autoloadRqt_FBank, (int)BReturn_Code_LossReport.Repeat);
                        return;
                    }

                    //3.黑名單檢核----(不檢查了-2015-04-15)
                    //log.Debug("(連線掛失)3.開始檢核黑名單");
                    //TODO..................

                }
                #endregion
                #region 狀態:取消卡片掛失
                else if (this.SocketRqt.FILE_UPDATE_CODE == "3")
                {
                    log.Debug("(掛失取消)1.開始檢核是否超過時限");
                    int cancelStatus = this.al_fBank.CancelReportloss_Check(this.autoloadRqt_FBank.ICC_NO, this.obDB);
                    switch (cancelStatus)
                    {
                        case 0:
                            //沒有上次掛失資料,無法取消掛失
                            this.SendSocketRespsone(absClientRequestHandler, this.SocketRsp, this.autoloadRqt_FBank, (int)BReturn_Code_LossReport.NotFind);
                            return;
                        case 1:
                            //正常
                            break;
                        case 2:
                            //掛失超過時限
                            this.SendSocketRespsone(absClientRequestHandler, this.SocketRsp, this.autoloadRqt_FBank, (int)BReturn_Code_LossReport.Overdue);
                            return;
                    }
                }
                #endregion
                else
                {
                    //File Update Code(Field 91) 資料非規格內
                    this.SendSocketRespsone(absClientRequestHandler, this.SocketRsp, this.autoloadRqt_FBank, (int)BReturn_Code_LossReport.OtherError);
                    return;
                }

                //2.檢核卡主檔狀態
                log.Debug("(連線掛失)2.開始檢核卡主檔狀態");
                string cardStatus = this.cardStatus_Check.AL_ChkCardSts4RL(obDB, this.autoloadRqt_FBank.ICC_NO, out this._oriCstatus);
                switch (cardStatus)
                {
                    case "00":
                        //正常
                        this.autoloadRqt_FBank.RC = ((int)BReturn_Code_LossReport.OK).ToString("D2");
                        break;
                    case "05":
                        //聯名卡停卡
                        this.SendSocketRespsone(absClientRequestHandler, this.SocketRsp, this.autoloadRqt_FBank, (int)BReturn_Code_LossReport.Terminate);
                        return;
                    case "09":
                        //無此卡號
                        this.SendSocketRespsone(absClientRequestHandler, this.SocketRsp, this.autoloadRqt_FBank, (int)BReturn_Code_LossReport.NoneCardID);
                        return;
                    case "10":
                        //iCash鎖卡
                        this.SendSocketRespsone(absClientRequestHandler, this.SocketRsp, this.autoloadRqt_FBank, (int)BReturn_Code_LossReport.BlockCard);
                        return;
                    case "12":
                        //iCash非有效卡
                        this.SendSocketRespsone(absClientRequestHandler, this.SocketRsp, this.autoloadRqt_FBank, (int)BReturn_Code_LossReport.InvaildCard);
                        return;
                    case "14":
                        //已做餘額返還
                        this.SendSocketRespsone(absClientRequestHandler, this.SocketRsp, this.autoloadRqt_FBank, (int)BReturn_Code_LossReport.AlreadyBalanceReturn);
                        return;
                    case "15":
                        //若為取消掛失"3"時
                        if (this.autoloadRqt_FBank.FILE_UPDATE_CODE == "3")
                        {
                            //表示查詢卡主檔示有掛失資料
                            this.autoloadRqt_FBank.RC = ((int)BReturn_Code_LossReport.OK).ToString("D2");
                            break;
                        }
                        else
                        {
                            //重複掛失
                            this.SendSocketRespsone(absClientRequestHandler, this.SocketRsp, this.autoloadRqt_FBank, (int)BReturn_Code_LossReport.Repeat);
                            return;
                        }               
                    default:
                        //其他錯誤
                        this.SendSocketRespsone(absClientRequestHandler, this.SocketRsp, this.autoloadRqt_FBank, (int)BReturn_Code_LossReport.OtherError);
                        return;
                }

                //4.更新黑名單/卡主檔/記名卡狀態
                log.Debug("(連線掛失或取消)4.開始更新黑名單/卡主檔/記名卡狀態");
                //更新黑名單/卡主檔/記名卡狀態
                bool updateReportLoss = false;
                TransactionOptions transactionOptions = new TransactionOptions();
                transactionOptions.IsolationLevel = IsolationLevel.RepeatableRead;
                transactionOptions.Timeout = new TimeSpan(0, 0, 10);
                using (TransactionScope transactionscope = new TransactionScope(TransactionScopeOption.Required, transactionOptions)) 
                {
                    updateReportLoss = al_fBank.Upt_Reportloss(this.autoloadRqt_FBank, obDB);
                    if(updateReportLoss)
                        transactionscope.Complete();
                }
                
                if (updateReportLoss)
                {
                    //更新成功
                    this.SendSocketRespsone(absClientRequestHandler, this.SocketRsp, this.autoloadRqt_FBank, (int)BReturn_Code_LossReport.OK);
                }
                else
                {
                    //更新失敗
                    this.SendSocketRespsone(absClientRequestHandler, this.SocketRsp, this.autoloadRqt_FBank, (int)BReturn_Code_LossReport.OtherError);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// 執行(0302)新增拒絕代行授權名單流程[ProcessingCode = 990178]
        /// </summary>
        /// <param name="absClientRequestHandler">ClientRequestHandler物件</param>
        private void AddRejectAuth_Flow(AbsClientRequestHandler absClientRequestHandler)
        {
            try
            {
                //1.檢核卡主檔
                log.Debug("開始檢核卡主檔");
                string cardStatus = this.cardStatus_Check.AL_CheckCardStatus(this.obDB, this.autoloadRqt_FBank.ICC_NO);
                //查無卡主檔
                if (cardStatus.Equals("1"))
                {
                    this.SendSocketRespsone(absClientRequestHandler, this.SocketRsp, this.autoloadRqt_FBank, (int)BReturn_Code_RejectAuthorizeAgent.NoneCardID);
                    return;
                }
                //2.更新拒絕代行授權名單
                log.Debug("開始更新拒絕代行授權名單");
                bool isUpdate = this.al_fBank.Upt_RfadviceList(this.autoloadRqt_FBank, this.obDB);
                if (isUpdate)
                {
                    this.SendSocketRespsone(absClientRequestHandler, this.SocketRsp, this.autoloadRqt_FBank, (int)BReturn_Code_RejectAuthorizeAgent.OK);
                }
                else
                {
                    this.SendSocketRespsone(absClientRequestHandler, this.SocketRsp, this.autoloadRqt_FBank, (int)BReturn_Code_RejectAuthorizeAgent.OtherError);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// 內部緩存物件轉成要送出的AutoloadRqt_FBank物件
        /// </summary>
        /// <param name="socketRsp">要給Socket傳回的AutoloadRqt_FBank物件</param>
        /// <param name="autoloadRqt_fBank">內部處理用的AutoloadRqt_FBank物件</param>
        private void AutoloadRqt_FBankToSocketRsp(ref AutoloadRqt_FBank socketRsp, AutoloadRqt_FBank autoloadRqt_fBank)
        {
            try
            {
                if (autoloadRqt_fBank == null)
                {
                    throw new Exception("autoloadRqt_fBank is null!!!");
                }
                log.Debug("開始內部用資料物件=>Response銀行端資料物件");
                socketRsp = new AutoloadRqt_FBank()
                {
                    MESSAGE_TYPE = "0312",
                    BANK_CODE = autoloadRqt_fBank.BANK_CODE.PadLeft(4,'0'),
                    ICC_NO = autoloadRqt_fBank.ICC_NO,
                    PROCESSING_CODE = autoloadRqt_fBank.PROCESSING_CODE,
                    TRANS_DATETIME = autoloadRqt_fBank.TRANS_DATETIME,
                    STAN = autoloadRqt_fBank.STAN,
                    VALID_DATE = autoloadRqt_fBank.VALID_DATE,
                    RRN = autoloadRqt_fBank.RRN,
                    RC = autoloadRqt_fBank.RC,
                    ICC_info = new ICC_INFO()
                    {
                        TX_DATETIME = DateTime.Now.ToString("yyyyMMddHHmmss")
                    }
                };
            }
            catch (Exception ex)
            {
                throw new Exception("[AutoloadRqt_FBankToSocketRsp] to Socket Response failed: " + ex.ToString());
            }
        }

        /// <summary>
        /// 用Socket送出轉成Json的AutoloadRqt_FBank物件
        /// </summary>
        /// <param name="absClientRequestHandler">Client Handler</param>
        /// <param name="socketRsp">Socket將送出的AutoloadRqt_FBank物件</param>
        /// <param name="autoloadRqt_FBank">內部緩存資訊用的物件</param>
        /// <param name="returnCode">ToBankReturnCode</param>
        private void SendSocketRespsone(AbsClientRequestHandler absClientRequestHandler, AutoloadRqt_FBank socketRsp, AutoloadRqt_FBank autoloadRqt_FBank, int returnCode)
        {
            try
            {
                log.Debug("(連線掛失)開始送回Respsone資料物件");
                autoloadRqt_FBank.RC = returnCode.ToString("D2");//給銀行的ReturnCode(2碼)
                this.AutoloadRqt_FBankToSocketRsp(ref socketRsp, autoloadRqt_FBank);
                //this.SocketRsp = socketRsp;
                //建立Json轉換器
                this.pocoParser = (this.pocoParser == null) ? new JsonWorker<AutoloadRqt_FBank>() : this.pocoParser;
                string jsonStr = this.pocoParser.Serialize(socketRsp);
                log.Debug(">> (Respsone)ClientSocket Send to [" + ((IPEndPoint)absClientRequestHandler.ClientSocket.RemoteEndPoint).Address.ToString() + "] =>\n" + jsonStr);
                byte[] responseBytes = this.pocoParser.Serialize2Bytes(socketRsp);

                if (absClientRequestHandler.ClientSocket.Connected || !(absClientRequestHandler.ClientSocket.Poll(50000, System.Net.Sockets.SelectMode.SelectRead) && absClientRequestHandler.ClientSocket.Available == 0))
                {
                    //送出
                    int sendLength = absClientRequestHandler.ClientSocket.Send(responseBytes);
                    if (sendLength != responseBytes.Length)
                    {
                        log.Error(">> [State_LossReport][SendSocketRespsone]Client " + absClientRequestHandler.ClientNo + " => Send Data length:" + sendLength + @" <=\=> Real Data length:" + responseBytes.Length);
                    }
                }
                else
                {
                    log.Error(">> [State_LossReport][SendSocketRespsone]Client[" + absClientRequestHandler.ClientNo + "]: Not Connected!");
                }
            }
            catch (Exception ex)
            {
                log.Error(">> [State_LossReport][SendSocketRespsone]Send Socket Resposne Failed:\n" + ex.ToString());
                throw new Exception("[SendSocketRespsone]Send Socket Resposne Failed:\n" + ex.ToString());
            }
            finally
            {
                //AL_DBModule obDBLog=null;
                try
                {
                    if (socketRsp != null)
                    {
                        LogModual logM = new LogModual();
                        log.Debug("[State_LossReport][SendSocketRespsone]開始記錄交易Log");
                        ///記錄此次交易結果
                        //obDBLog = new AL_DBModule();
                        if (obDB.ALCon == null || obDB.ALCon.State == System.Data.ConnectionState.Closed)
                            obDB.OpenConnection();
                        //Socket Response 沒有 (Field 91)FILE_UPDATE_CODE ,所以從Socket Request物件補上
                        socketRsp.FILE_UPDATE_CODE = this.SocketRqt.FILE_UPDATE_CODE;
                        
                        //記錄此次交易
                        dtrsp = DateTime.Now;
                        bool saveLog = logM.SaveTransLog2Bank(socketRsp, obDB, dtrqt, dtrsp);
                        //只有連線掛失/取消掛失的交易才記到OL_AL_REPORTLOSS_LOG_D這個table
                        if (socketRsp.PROCESSING_CODE == "990176")
                        {
                            //記錄此次掛失交易
                            bool saveLossReportLog = this.al_fBank.Add_Reportloss_Log(socketRsp, this._oriCstatus, this.obDB);
                            if (!saveLossReportLog)
                            {
                                //記ReportLoss Log失敗後存入Common.logging
                                throw new Exception("Save ReportLoss Log Failed: CardID:" + socketRsp.ICC_NO +
                                    " TransDateTime:" + socketRsp.TRANS_DATETIME +
                                    " FileUpdateCode:" + socketRsp.FILE_UPDATE_CODE);
                            }
                        }
                        if (!saveLog)
                        {
                            //記Log失敗後存入Common.logging
                            throw new Exception("Save Log Failed: CardID:" + socketRsp.ICC_NO +
                                " TransDateTime:" + socketRsp.TRANS_DATETIME +
                                " FileUpdateCode:" + socketRsp.FILE_UPDATE_CODE);
                        }
                    }
                    else
                    {
                        throw new Exception("Socket Response POCO is null");
                    }
                }
                catch (Exception ex)
                {
                    log.Error(">> [State_LossReport][SendSocketRespsone][SaveTransLog]Save LossReport Trans Log Failed:\n" + ex.ToString());
                }
                finally
                {
                    this.obDB.CloseConnection();
                    this.obDB = null;
                    this.pocoParser = null;
                    this.al_fBank = null;
                    this.SocketRqt = null;
                    this.SocketRsp = null;
                    this.autoloadRqt_FBank = null;
                    log.Debug(">> State : Loss Report End");
                    absClientRequestHandler.ServiceState = new State_Exit();
                }

            }
        }
        #endregion
    }
}
