﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AsyncMultiSocket.Handlers.State.Entities
{
    public enum CardStatus
    {
        /// <summary>
        /// CStatus==other
        /// </summary>
        Other = 0,
        /// <summary>
        /// 查無卡主檔
        /// </summary>
        Null = 1,
        /// <summary>
        /// CStatus==5,6,9
        /// </summary>
        Status569 = 2,
        /// <summary>
        /// CStatus==3'4'7'8
        /// </summary>
        Status3478 =3,
    }
}
