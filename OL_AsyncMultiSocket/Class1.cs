﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Timers;
//
//using Spring.Context;
//using Spring.Context.Support;
using Common.Logging;
using System.Reflection;
using System.Net;
using HiLo.Business;

namespace AsyncMultiSocket
{
    public class Class1
    {
        //private static readonly ILog log = LogManager.GetLogger(typeof(Class1));

        //private static System.Timers.Timer aTimer;

        public static void Main()
        {
            
            //IApplicationContext ctx = ContextRegistry.GetContext();
            //AsyncMultiSocket sck = ctx["socketServer"] as AsyncMultiSocket;
            
            AsyncMultiSocketServer hilo = new AsyncMultiSocketServer(6100, "HiLo");
            hilo.Start();
            //自動加值
            AsyncMultiSocketServer autoload = new AsyncMultiSocketServer(6101,"AutoLoad");
            autoload.Start();
            //自動加值txlog
            AsyncMultiSocketServer txlog = new AsyncMultiSocketServer(6102, "Txlog");
            txlog.Start();
            //查詢
            AsyncMultiSocketServer query = new AsyncMultiSocketServer(6103, "Query");
            query.Start();
            //自動加值txlog沖正
            AsyncMultiSocketServer reversal = new AsyncMultiSocketServer(6104, "Reversal");
            reversal.Start();
            //銀行端監聽者
            AsyncMultiSocketServer BankListen = new AsyncMultiSocketServer(6105, "BankListener");
            BankListen.Start();

            //連線掛失or掛失取消/新增拒絕代行授權名單
            AsyncMultiSocketServer lossReport = new AsyncMultiSocketServer(6109, "LossReportOrAddRejectAuth");
            lossReport.Start();
            //Thread.Sleep(3000);
            //sck.Stop();
            // Normally, the timer is declared at the class level,
            // so that it stays in scope as long as it is needed.
            // If the timer is declared in a long-running method,  
            // KeepAlive must be used to prevent the JIT compiler 
            // from allowing aggressive garbage collection to occur 
            // before the method ends. You can experiment with this
            // by commenting out the class-level declaration and 
            // uncommenting the declaration below; then uncomment
            // the GC.KeepAlive(aTimer) at the end of the method.
            System.Timers.Timer aTimer;

            // Create a timer with a ten second interval.
            aTimer = new System.Timers.Timer(15000);

            // Hook up the Elapsed event for the timer.
            aTimer.Elapsed += new ElapsedEventHandler(OnTimedEvent);

            // Set the Interval to 2 seconds (2000 milliseconds).
            aTimer.Interval = 6000;
            //aTimer.Enabled = true;
            //aTimer.Start();
            Console.WriteLine("Press the Enter key to exit the program.");
            Console.ReadLine();
            //autoload.Stop();
            //hilo.Stop();
            // If the timer is declared in a long-running method, use
            // KeepAlive to prevent garbage collection from occurring
            // before the method ends.
            //GC.KeepAlive(aTimer);
        }

        // Specify what you want to happen when the Elapsed event is 
        // raised.
        private static void OnTimedEvent(object source, ElapsedEventArgs e)
        {
            
           // Console.WriteLine("The Elapsed event was raised at {0}", e.SignalTime);
        }
    }
}
