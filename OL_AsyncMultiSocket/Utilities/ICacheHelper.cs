﻿using System;
using System.Collections.Generic;

namespace OL_AsyncMultiSocket.Utilities
{
    public interface ICacheHelper
    {
        /// <summary>
        /// Add an object to cache with specified key
        /// </summary>
        /// <param name="key">A key value to asscociate with given object</param>
        /// <param name="obj">An object to store into cache</param>
        void Add( string key, object obj );
        
        /// <summary>
        /// Retrieve an object from cache using given key value
        /// </summary>
        /// <param name="key">A key value to retrieve object from hash</param>
        /// <returns>An object that is associated with given key value</returns>
        object Get(string key);
        
        /// <summary>
        /// Remove an object from cache using given key value
        /// </summary>
        /// <param name="key">A key value to remove item from cache</param>
        /// <returns>Result of remove operation true or false</returns>
        void Remove(string key);

        /// <summary>
        /// List all objects present in cache
        /// </summary>
        /// <returns>List of objects from cache</returns>
        IList<object> GetAll();
    }
}
