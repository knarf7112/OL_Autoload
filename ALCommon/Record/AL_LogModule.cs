﻿

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Xml;




namespace ALCommon
{

    public class AL_LogModule
    {  
        private Object thisLock = new Object(); 
       
        /// <summary>
        /// log匯入文字檔
        /// </summary>
        /// <param name="ErrorMsg">要記入之msg</param>
        /// <param name="fileURL">記錄之路徑</param>
        /// <returns>true(紀錄成功)/false(紀錄失敗)</returns>
        public bool log2file(string ErrorMsg)
        {
            lock (thisLock)
            {
                try
                {
                    string fileURL = Getlog2file_inf();
                    string timestr = DateTime.Now.Date.ToString("yyyyMMdd") + "_";
                    fileURL = fileURL.Replace("_", timestr);

                    string logMessage = string.Format("{0} {1}", DateTime.Now.ToString(), ErrorMsg);
                    Console.WriteLine(" URL=" + fileURL + " | Message= " + logMessage);

                    File.AppendAllText(fileURL, logMessage + "\r\n");
                    return true;
                }
                catch
                {
                    return false;
                }
            }
        }
      
        /// <summary>
        /// log匯入文字檔讀取儲存路徑設定檔
        /// </summary>
        private static string Getlog2file_inf()
        {
            string sResult = "";
            try
            {
                //讀取內嵌資源
                Assembly asm = Assembly.GetExecutingAssembly();
                string xmlName = asm.GetName().Name;
                Stream stream = asm.GetManifestResourceStream(xmlName + @".Record.log2File.xml");
                Console.Write(xmlName + ".Record.log2File.xml");
                //設定.xml相關內容讀取
                XmlDocument xmlDoc = null;
                xmlDoc = new XmlDocument();
                xmlDoc.Load(stream);
                XmlNode xNode;
                xNode = xmlDoc.SelectSingleNode("INFO");

                foreach (XmlNode node1 in xNode.ChildNodes)
                {
                    if (node1.Name.ToString().Trim() == "NONE")
                    {
                        sResult = node1.InnerText.ToString().Trim();
                    }
                }
                return sResult;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }

   


}
