﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Xml;

namespace ALCommon
{
    /// <summary>
    /// 資料庫連線及查詢函式庫
    /// </summary>
    public class AL_DBModule
    {
        /// <summary>
        /// SqlConnection物件
        /// </summary>
        public SqlConnection ALCon;

        /// <summary>
        /// 開啟連線    
        /// </summary>
        /// <returns>連線狀態</returns>
        /// <remarks>楊智偉 2014/12/18</remarks>
        /// <!--若要修改連線資訊請至AL_DBModuleSet.xml 節點Update-->
        public SqlConnection OpenConnection()
        {
            ALCon = new SqlConnection(GetDbInfo_Dll());
            try
            {
                if (ALCon.State.ToString() == "Closed")
                {
                    ALCon.Open();
                }

                return ALCon;
            }
            catch (Exception)
            {
                throw;
            }

        }


        /// <summary>
        /// 開啟連線
        /// </summary>
        /// <param name="sConnectionText">特約機構代碼</param>
        /// <returns>連線狀態</returns>
        /// <remarks>楊智偉 2014/12/18</remarks>
        public SqlConnection OpenConnection_Merchant(string MERCHANT_NO)
        {
            ALCon = new SqlConnection(GetDbInfo_Dll(MERCHANT_NO));
            try
            {
                if (ALCon.State.ToString() == "Closed")
                {
                    ALCon.Open();
                }
                return ALCon;
            }
            catch (Exception)
            {
                throw;
            }

        }

        /// <summary>
        /// Test
        /// </summary>
        /// <returns></returns>
        public SqlConnection OpenConnection_Dll()
        {
            ALCon = new SqlConnection(GetDbInfo_Dll("Official"));
            try
            {
                if (ALCon.State.ToString() == "Closed")
                {
                    ALCon.Open();
                }
            }
            catch (Exception ex)
            {
                //writeLog("db link error");
                Console.WriteLine(ex.Message);
            }
            return ALCon;
        }


        /// <summary>
        /// 關閉連線
        /// </summary>
        /// <remarks>楊智偉 2014/12/18</remarks>
        public void CloseConnection()
        {
            try
            {
                if (ALCon.State.ToString() == "Open")
                {
                    ALCon.Close();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// 執行SQL
        /// </summary>
        /// <param name="strCommandText">SQL字串</param>
        /// <returns>SqlDataReader</returns>
        /// <remarks>楊智偉 2014/12/18</remarks>
        public SqlDataReader ExecuteSQL(string strCommandText)
        {
            try
            {
                SqlCommand cmdGeneral = new SqlCommand(strCommandText, ALCon);
                return cmdGeneral.ExecuteReader();
            }

            catch (Exception ex)
            {
                throw ex;
            }
        }


        /// <summary>
        /// 執行SQL
        /// </summary>
        /// <param name="strCommandText">SQL字串</param>
        /// <param name="TimeOut">Time Out</param>
        /// <returns>SqlDataReader</returns>
        /// <remarks>楊智偉 2014/12/18</remarks>
        public SqlDataReader ExecuteSQL(string strCommandText, int TimeOut)
        {
            try
            {
                SqlCommand cmdGeneral = new SqlCommand(strCommandText, ALCon);
                //cmdGeneral.CommandTimeout = 90;
                cmdGeneral.CommandTimeout = TimeOut;
                return cmdGeneral.ExecuteReader();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        /// <summary>
        /// 執行SQL
        /// </summary>
        /// <param name="strCommandText">SQL字串</param>
        /// <returns>SqlDataReader</returns>
        /// <param name="aParams">參數</param>
        /// <remarks>楊智偉 2014/12/18</remarks>
        public SqlDataReader ExecuteSQL(string strCommandText, SqlParameter[] aParams)
        {
            try
            {
                SqlCommand cmdGeneral = new SqlCommand(strCommandText, ALCon);
                cmdGeneral.Parameters.AddRange(aParams);
                return cmdGeneral.ExecuteReader();
            }

            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Function Name: ExecuteNonQuery
        /// Description: 
        /// </summary>
        /// <param name="strCommandText">SQL字串</param>
        /// <returns>bool</returns>
        /// <remarks>楊智偉 2014/12/18</remarks>
        public bool ExecuteNonQuery(string strCommandText)
        {
            SqlCommand cmdGeneral = new SqlCommand(strCommandText, ALCon);
            try
            {
                int Result = cmdGeneral.ExecuteNonQuery();
                return (Result > 0) ? true : false;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        /// <summary>
        /// 查詢SQL
        /// </summary>
        /// <param name="strCommandText">SQL字串</param>
        /// <param name="TimeOut">Time Out</param>
        /// <returns>1(筆數大於0) / 2(筆數小於等於0)</returns>
        /// <remarks>楊智偉 2014/12/18</remarks>
        public string ExecuteNonQuery(string strCommandText, int TimeOut)
        {
            SqlCommand cmdGeneral = new SqlCommand(strCommandText, ALCon);
            try
            {
                //cmdGeneral.CommandTimeout = 90;
                cmdGeneral.CommandTimeout = TimeOut;
                int Result = cmdGeneral.ExecuteNonQuery();
                return (Result > 0) ? "1" : "2";
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        /// <summary>
        /// 查詢SQL
        /// </summary>
        /// <param name="strCommandText">SQL字串</param>
        /// <returns>DateSet</returns>
        /// <remarks>楊智偉 2014/12/18</remarks>
        public DataSet GetDataSet(string strCommandText)
        {
            DataSet ds = new DataSet();
            try
            {
                SqlDataAdapter adp = new SqlDataAdapter(strCommandText, ALCon);
                adp.Fill(ds);
                return ds;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        /// <summary>
        /// 查詢SQL
        /// </summary>
        /// <param name="strCommandText">SQL字串</param>
        /// <param name="DataName">Table Name</param>
        /// <returns>DateSet</returns>
        /// <remarks>楊智偉 2014/12/18</remarks>
        public DataSet GetDataSet(string strCommandText, string DataName)
        {
            try
            {
                SqlDataAdapter adp = new SqlDataAdapter(strCommandText, ALCon);
                DataSet ds = new DataSet();
                adp.Fill(ds, DataName);
                return ds;
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }


        /// <summary>
        /// 查詢SQL
        /// </summary>
        /// <param name="strCommandText">SQL字串</param>
        /// <param name="timeout">Time Out</param>
        /// <returns>DateSet</returns>
        /// <remarks>楊智偉 2014/12/18</remarks>
        public DataSet GetDataSet(string strCommandText, int timeout)
        {
            try
            {
                SqlCommand command = new SqlCommand(strCommandText, ALCon);
                command.CommandTimeout = timeout; //設定SQL timeout
                SqlDataAdapter adp = new SqlDataAdapter(command);
                DataSet ds = new DataSet();
                adp.Fill(ds);
                return ds;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        /// <summary>
        /// 查詢SQL
        /// </summary>
        /// <param name="strCommandText">SQL字串</param>
        /// <returns>DataTable</returns>
        ///  <remarks>楊智偉 2014/12/18</remarks>
        public DataTable GetDataTable(string strCommandText)
        {
            try
            {
                SqlDataAdapter adp = new SqlDataAdapter(strCommandText, ALCon);
                DataTable dt = new DataTable();
                adp.Fill(dt);
                return dt;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        /// <summary>
        /// 參數化執行SQL指令
        /// </summary>
        /// <param name="strCommandText">SQL字串</param>
        /// <param name="aParams">參數</param>
        /// <returns>1(筆數大於0) / 2(筆數小於等於0)</returns>
        ///  <remarks>楊智偉 2014/12/18</remarks>
        public string SqlExec(string strCommandText, SqlParameter[] aParams)
        {
            SqlCommand cmdGeneral = new SqlCommand(strCommandText, ALCon);
            for (int i = 0; i < aParams.Length; i++)
            {
                cmdGeneral.Parameters.AddWithValue(aParams[i].ParameterName, aParams[i].Value);
            }
            try
            {
                int Result = cmdGeneral.ExecuteNonQuery();
                return (Result > 0) ? "1" : "2";
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        /// <summary>
        /// 參數化執行SQL指令
        /// </summary>
        /// <param name="strCommandText">SQL字串</param>
        /// <param name="aParams">參數</param>
        /// <returns>1(筆數大於0) / 2(筆數小於等於0)</returns>
        ///  <remarks>楊智偉 2014/12/18</remarks>
        public String SqlExec1(String strCommandText, SqlParameter[] aParams)
        {
            SqlCommand cmdGeneral = new SqlCommand(strCommandText, ALCon);
            //for (int i = 0; i < aParams.Length; i++)
            //{
            //    cmdGeneral.Parameters.AddWithValue(aParams[i].ParameterName, aParams[i].Value);
            //}
            cmdGeneral.Parameters.AddRange(aParams);
            try
            {
                int Result = cmdGeneral.ExecuteNonQuery();
                return (Result > 0) ? "1" : "2";
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        /// <summary>
        /// 參數化執行SQL指令
        /// </summary>
        /// <param name="strCommandText">SQL字串</param>
        /// <param name="aParams">參數</param>
        /// <param name="TimeOut">TimeOut時間</param>
        /// <returns>1(筆數大於0) / 2(筆數小於等於0)</returns>
        /// <remarks>楊智偉 2014/12/18</remarks>
        public string SqlExec(string strCommandText, SqlParameter[] aParams, int TimeOut)
        {
            SqlCommand cmdGeneral = new SqlCommand(strCommandText, ALCon);
            //cmdGeneral.CommandTimeout = 90;
            cmdGeneral.CommandTimeout = TimeOut;
            for (int i = 0; i < aParams.Length; i++)
            {
                cmdGeneral.Parameters.AddWithValue(aParams[i].ParameterName, aParams[i].Value);
            }
            try
            {
                int Result = cmdGeneral.ExecuteNonQuery();
                return (Result > 0) ? "1" : "2";
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// 參數化執行SQL指令
        /// </summary>
        /// <param name="strCommandText">SQL字串</param>
        /// <param name="aParams">參數</param>
        /// <param name="TimeOut">TimeOut時間</param>
        /// <returns>1(筆數大於0) / 2(筆數小於等於0)</returns>
    
        public String SqlExec1(string strCommandText, SqlParameter[] aParams, int TimeOut)
        {
            SqlCommand cmdGeneral = new SqlCommand(strCommandText, ALCon);
            cmdGeneral.CommandTimeout = TimeOut;
         
            cmdGeneral.Parameters.AddRange(aParams);
            try
            {
                int Result = cmdGeneral.ExecuteNonQuery();
                return (Result > 0) ? "1" : "2";
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// 參數化執行SQL指令
        /// </summary>
        /// <param name="strCommandText">SQL字串</param>
        /// <returns>無資料回''/第一筆資料第1行的值</returns>
        public object SqlExecuteScalar(string strCommandText)
        {
            SqlCommand cmdGeneral = new SqlCommand(strCommandText, ALCon);          
            try
            {
                object result = cmdGeneral.ExecuteScalar();
                return (result == null) ? "" : result.ToString();

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        /// <summary>
        /// 參數化執行SQL指令
        /// </summary>
        /// <param name="strCommandText">SQL字串</param>
        /// <param name="aParams">參數</param>
        /// <returns>無資料回''/第一筆資料第1行的值</returns>
        public string SqlExecuteScalarHasParams(string strCommandText, SqlParameter[] aParams)
        {
            SqlCommand cmdGeneral = new SqlCommand(strCommandText, ALCon);
            cmdGeneral.Parameters.AddRange(aParams);
            try
            {

                object result=cmdGeneral.ExecuteScalar();
                return (result == null) ? "" : result.ToString();
               
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        /// <summary>
        /// 參數化執行SQL指令(select)
        /// </summary>
        /// <param name="strCommandText">SQL字串</param>
        /// <param name="aParams">參數array</param>
        /// <returns>DataSet</returns>
        /// <remarks>楊智偉 2014/12/18</remarks>
        public DataSet ParaGetDataSet(string strCommandText, SqlParameter[] aParams)
        {
            SqlCommand cmdGeneral = new SqlCommand("", ALCon);
            SqlDataAdapter adp = new SqlDataAdapter();
            DataSet ds = new DataSet();
            cmdGeneral.CommandText = strCommandText;
            for (int i = 0; i < aParams.Length; i++)
            {
                cmdGeneral.Parameters.AddWithValue(aParams[i].ParameterName, aParams[i].Value);
            }
            adp.SelectCommand = cmdGeneral;

            try
            {
                adp.Fill(ds);
                return ds;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// 參數化執行SQL指令(select)
        /// </summary>
        /// <param name="strCommandText">SQL字串</param>
        /// <param name="aParams">參數array</param>
        /// <returns>DataSet</returns>
        /// <remarks>楊智偉 2014/12/18</remarks>
        public DataSet ParaGetDataSet1(string strCommandText, SqlParameter[] aParams)
        {
            SqlCommand cmdGeneral = new SqlCommand(strCommandText, ALCon);
            SqlDataAdapter adp = new SqlDataAdapter();
            DataSet ds = new DataSet();
            cmdGeneral.Parameters.AddRange(aParams);
            adp.SelectCommand = cmdGeneral;

            try
            {
                adp.Fill(ds);
                return ds;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>s
        /// 參數化執行SQL指令(select)
        /// </summary>
        /// <param name="strCommandText">SQL字串</param>
        /// <param name="aParams">參數Array</param>
        /// <returns>DataTable / Exception字串</returns>
        /// <remarks>楊智偉 2014/12/18</remarks>
        public DataTable ParaGetDataTable(string strCommandText, SqlParameter[] aParams)
        {
            try
            {
                SqlCommand cmdGeneral = new SqlCommand(strCommandText, ALCon);
                SqlDataAdapter adp = new SqlDataAdapter();
                DataTable dt = new DataTable();
                //cmdGeneral.CommandText = strCommandText;
                for (int i = 0; i < aParams.Length; i++)
                {
                    cmdGeneral.Parameters.AddWithValue(aParams[i].ParameterName, aParams[i].Value);
                }
                adp.SelectCommand = cmdGeneral;

                adp.Fill(dt);
                return dt;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// 參數化執行SQL指令(select)
        /// </summary>
        /// <param name="strCommandText">SQL字串</param>
        /// <param name="aParams">參數Array</param>
        /// <returns>DataTable / Exception字串</returns>
        /// <remarks>楊智偉 2014/12/18</remarks>
        public DataTable ParaGetDataTable1(string strCommandText, SqlParameter[] aParams)
        {
            try
            {
                SqlCommand cmdGeneral = new SqlCommand(strCommandText, ALCon);
                SqlDataAdapter adp = new SqlDataAdapter();
                DataTable dt = new DataTable();              
                cmdGeneral.Parameters.AddRange(aParams);
                adp.SelectCommand = cmdGeneral;
                adp.Fill(dt);
                return dt;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        /// <summary>
        ///  取得DB連線Info
        /// </summary>
        /// <returns>連線資訊</returns>
        /// <remarks>楊智偉 2014/12/18</remarks>
        public static string GetDBInfo()
        {
            string sResult = null;
            try
            {
                XmlDocument DBModuledoc = new XmlDocument();
                //DBModuledoc.Load("LOL_DBModuleSet.xml");
                String sXmlPath = System.AppDomain.CurrentDomain.BaseDirectory + @"\AL_DBModuleSet.xml";
                DBModuledoc.Load(sXmlPath);
                XmlElement root = DBModuledoc.DocumentElement;
                XmlNode xNode;
                xNode = root.SelectSingleNode("None");
                sResult = xNode.InnerText.ToString().Trim();
                return sResult;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        /// <summary>
        /// 取得DB連線Info，需傳入參數
        /// </summary>
        /// <param name="sMerchant_No">特約機構代碼</param>
        /// <returns>連線資訊</returns>
        /// <remarks>楊智偉 2014/12/18</remarks>
        public static string GetDBInfo(string sMerchant_No)
        {
            string sResult = null;
            try
            {
                XmlDocument DBModuledoc = new XmlDocument();
                //DBModuledoc.Load("LOL_DBModuleSet.xml");
                String sXmlPath = System.AppDomain.CurrentDomain.BaseDirectory + @"AL_DBModuleSet.xml";
                DBModuledoc.Load(sXmlPath);
                XmlElement root = DBModuledoc.DocumentElement;
                XmlNode xNode;
                xNode = root.SelectSingleNode(sMerchant_No);
                sResult = xNode.InnerText.ToString().Trim();
                return sResult;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        ///  取得DB連線Info，xml為內嵌資源
        /// </summary>
        /// <returns>連線資訊</returns>
        /// <remarks>楊智偉 2014/12/18</remarks>
        public static string GetDbInfo_Dll()
        {
            string sResult = "";
            try
            {
                //讀取內嵌資源
                Assembly asm = Assembly.GetExecutingAssembly();
                string xmlName = asm.GetName().Name;
                Stream stream = asm.GetManifestResourceStream(xmlName + @".DB.AL_DBModuleSet.xml");

                //設定.xml相關內容讀取
                XmlDocument xmlDoc = null;
                xmlDoc = new XmlDocument();
                xmlDoc.Load(stream);
                XmlNode xNode;
                xNode = xmlDoc.SelectSingleNode("INFO");

                foreach (XmlNode node1 in xNode.ChildNodes)
                {
                    if (node1.Name.ToString().Trim() == "None")
                    {
                        sResult = node1.InnerText.ToString().Trim();
                    }
                }
                return sResult;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        /// <summary>
        ///  取得DB連線Info，xml為內嵌資源，需傳入參數
        /// </summary>
        /// <param name="sMerchant_No">特約機構代碼</param>
        /// <returns>連線資訊</returns>
        /// <remarks>楊智偉 2014/12/18</remarks>
        public static string GetDbInfo_Dll(string sMerchant_No)
        {
            string sResult = "";
            try
            {
                //讀取內嵌資源
                Assembly asm = Assembly.GetExecutingAssembly();
                string xmlName = asm.GetName().Name;
                Stream stream = asm.GetManifestResourceStream(xmlName + @".DB.AL_DBModuleSet.xml");
                Console.Write(xmlName + ".DB.AL_DBModuleSet.xml");
                //設定.xml相關內容讀取
                XmlDocument xmlDoc = null;
                xmlDoc = new XmlDocument();
                xmlDoc.Load(stream);
                XmlNode xNode;
                xNode = xmlDoc.SelectSingleNode("INFO");

                foreach (XmlNode node1 in xNode.ChildNodes)
                {
                    if (node1.Name.ToString().Trim() == sMerchant_No)
                    {
                        sResult = node1.InnerText.ToString().Trim();
                    }
                }
                return sResult;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }



      


    }
}
