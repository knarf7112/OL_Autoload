﻿using HiLo.Domain.Entities;
using System.Collections.Generic;
using System.Data;

namespace HiLo.Domain.Mappings
{
    
    public class SequenceDORowMapper<T> where T : SequenceDO, new()
    {
        private bool HasData = false;
        public T MapRow(IDataReader dataReader)
        {
            this.HasData = (dataReader.Read()) ? true : false;
            if (this.HasData)
            {
                T sequenceDO = new T();
                sequenceDO.KeyName = dataReader.GetString(0);
                sequenceDO.KeyVal = dataReader.GetInt64(1);
                return sequenceDO;
            }
            else
            {
                return null;
            }
        }

        public IEnumerable<T> MapRows(IDataReader dataReader)
        {
            do
            {
                yield return MapRow(dataReader);
            }
            while (this.HasData);
        }
    }
}
