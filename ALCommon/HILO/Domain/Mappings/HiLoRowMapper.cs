﻿using System.Collections.Generic;
using System.Data;
//
using HiLo.Domain.Entities;
namespace HiLo.Domain.Mappings
{
    public class HiLoDORowMapper<T> where T : HiLoDO,new()
    {
        private bool HasData = false;
        public T MapRow( IDataReader dataReader)
        {
            this.HasData = (dataReader.Read()) ? true : false;
            if(this.HasData)
            {
                T hiLo = new T();
                hiLo.KeyName = dataReader.GetString(0);
                hiLo.NextHi = dataReader.GetInt64(1);
                hiLo.MaxVal = dataReader.GetInt64(2);
                return hiLo;
            }
            else
            {
                return null;
            }
        }

        public IList<T> MapRows(IDataReader dataReader)
        {
            IList<T> list = new List<T>();
            do
            {
                T t = MapRow(dataReader);
                if (t != null)
                {
                    list.Add(t);
                }
            }
            while (this.HasData);
            return list;
        }
    }
}
