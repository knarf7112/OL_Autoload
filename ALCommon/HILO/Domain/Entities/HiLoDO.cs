﻿using System;
namespace HiLo.Domain.Entities
{
    [Serializable]
    public class HiLoDO : AbstractDO
    {
        public virtual string KeyName { get; set; }
        public virtual long NextHi { get; set; }
        public virtual long MaxVal { get; set; }
    }
}
