﻿using System;
namespace HiLo.Domain.Entities
{
    [Serializable]
    public class SequenceDO : AbstractDO
    {
        public virtual string KeyName { get; set; }
        public virtual long KeyVal { get; set; }
    }
}
