﻿using System.Data;
using System.Collections.Generic;
//
//using Spring.Data.Common;
//using Spring.Data.Generic;
//using Spring.Transaction.Interceptor;
using ALCommon;
//
using HiLo.Domain.Entities;
using HiLo.Domain.Mappings;
using System.Data.SqlClient;
using System;

namespace HiLo.Data
{
    /// <summary>
    /// Ref:
    /// http://msdn.microsoft.com/en-us/library/ms187373.aspx
    /// </summary>
    public class HiLoDAO : IDAO<HiLoDO,string>//AdoDaoSupport,IDAO<HiLo,string>
    {
        //
        private string _hiLoTable;
        public string HiLoTable
        {
            get
            {
                if (String.IsNullOrEmpty(this._hiLoTable))
                {
                    throw new Exception("HiLo table name not setting!");
                }
                return this._hiLoTable;
            }
            set
            {
                this._hiLoTable = value;
            }
        }

        private AL_DBModule al_DBModule { get; set; }

        #region Constructor
        public HiLoDAO(string tableName)
        {
            this.al_DBModule = new AL_DBModule();
            this.HiLoTable = tableName;
        }
        #endregion
        public void Insert(HiLoDO hiLo)
        {
            string cmdText =
                @"insert into " + this.HiLoTable + @" (
                     KEY_NAME, NEXT_HI, MAX_VAL
                  ) values ( 
                     @KEY_NAME, @NEXT_HI, @MAX_VAL
                  )
                "
            ;
            
            List<SqlParameter> paras = new List<SqlParameter>();
            paras.Add(new SqlParameter("@KEY_NAME",DbType.String){ 
                Direction = ParameterDirection.Input,
                Value = hiLo.KeyName
            });
            paras.Add(new SqlParameter("@NEXT_HI",DbType.Int64){ 
                Direction = ParameterDirection.Input,
                Value = hiLo.NextHi
            });
            paras.Add(new SqlParameter("@MAX_VAL",DbType.Int64){ 
                Direction = ParameterDirection.Input,
                Value = hiLo.MaxVal
            });
            try
            {
                //open connection
                this.al_DBModule.OpenConnection();
                //add parameter and execute sql
                this.al_DBModule.SqlExec(cmdText, paras.ToArray());
                //close connection
                this.al_DBModule.CloseConnection();
            }
            catch (Exception ex)
            {
                this.al_DBModule.CloseConnection();
                throw ex;
            }
        }

        public void Update(HiLoDO hiLo)
        {
            string cmdText =
                @"update " + this.HiLoTable + @" 
                  set 
                      NEXT_HI = @NEXT_HI,
                      MAX_VAL = @MAX_VAL
                  where
                      KEY_NAME = @KEY_NAME
                 "
            ;

            List<SqlParameter> paras = new List<SqlParameter>();
            paras.Add(new SqlParameter("@NEXT_HI", DbType.Int64)
            {
                Direction = ParameterDirection.Input,
                Value = hiLo.NextHi
            });
            paras.Add(new SqlParameter("@MAX_VAL", DbType.Int64)
            {
                Direction = ParameterDirection.Input,
                Value = hiLo.MaxVal
            });
            paras.Add(new SqlParameter("@KEY_NAME", DbType.String)
            {
                Direction = ParameterDirection.Input,
                Value = hiLo.KeyName
            });
            try
            {
                //open connection
                this.al_DBModule.OpenConnection();
                //add parameter and execute sql
                this.al_DBModule.SqlExec(cmdText, paras.ToArray());
                //close connection
                this.al_DBModule.CloseConnection();
            }
            catch (Exception ex)
            {
                this.al_DBModule.CloseConnection();
                throw ex;
            }
        }

        public void Delete(string keyName)
        {
            string cmdText =
                @"delete from " + this.HiLoTable + @"  
                  where
                      KEY_NAME = @KEY_NAME
                 "
            ;

            List<SqlParameter> paras = new List<SqlParameter>();
            paras.Add(new SqlParameter("@KEY_NAME", DbType.String)
            {
                Direction = ParameterDirection.Input,
                Value = keyName
            });
            try
            {
                //open connection
                this.al_DBModule.OpenConnection();
                //add parameter and execute sql
                this.al_DBModule.SqlExec(cmdText, paras.ToArray());
                //close connection
                this.al_DBModule.CloseConnection();
            }
            catch (Exception ex)
            {
                this.al_DBModule.CloseConnection();
                throw ex;
            }
        }
        
        public HiLoDO FindByPk(string keyName)
        {
            string cmdText =
                @"select 
                      KEY_NAME, NEXT_HI, MAX_VAL
                  from " + 
                      this.HiLoTable + @"                      
                  with (ROWLOCK, XLOCK)
                  where
                      KEY_NAME = @KEY_NAME
                 "
            ;
            List<SqlParameter> paras = new List<SqlParameter>();
            paras.Add(new SqlParameter("@KEY_NAME",DbType.String){ 
                Direction = ParameterDirection.Input,
                Value = keyName
            });
            HiLoDO hilo = null;
            try
            {
                //open connection
                this.al_DBModule.OpenConnection();
                //add parameter and execute sql
                SqlDataReader dr = this.al_DBModule.ExecuteSQL(cmdText, paras.ToArray());

                //mapping hilo object if non data return null
                hilo = new HiLoDORowMapper<HiLoDO>().MapRow(dr);
                //close dataReader
                if (!dr.IsClosed)
                {
                    dr.Close();
                }
                //close connection
                this.al_DBModule.CloseConnection();
            }
            catch(Exception ex)
            {
                //close connection
                this.al_DBModule.CloseConnection();
                throw ex;
            }
            return hilo;
        }

        public IList<HiLoDO> FindAll()
        {
            string cmdText = 
                @"select " + 
                      "KEY_NAME, NEXT_HI, MAX_VAL " +
                  "from " + this.HiLoTable +  
                 " order by KEY_NAME";
            IList<HiLoDO> hiloList = null;
            try
            {
                //open connection
                this.al_DBModule.OpenConnection();
                //add parameter and execute sql
                SqlDataReader dr = this.al_DBModule.ExecuteSQL(cmdText);

                //mapping hilo object if non data return null
                hiloList = new HiLoDORowMapper<HiLoDO>().MapRows(dr);
                //close dataReader
                if (!dr.IsClosed)
                {
                    dr.Close();
                }
                //close connection
                this.al_DBModule.CloseConnection();
            }
            catch(Exception ex)
            {
                //close connection
                this.al_DBModule.CloseConnection();
                throw ex;
            }
            return hiloList;
        }

        public bool Exist(string keyName)
        {
            string cmdText =
                @"select 
                      KEY_NAME
                  from " + 
                      this.HiLoTable + @"                      
                  where
                      KEY_NAME = @KEY_NAME
                 "
            ;
            List<SqlParameter> paras = new List<SqlParameter>();
            paras.Add(new SqlParameter("@KEY_NAME", DbType.String)
            {
                Direction = ParameterDirection.Input,
                Value = keyName
            });
            
            try
            {
                //open connection
                this.al_DBModule.OpenConnection();
                //add parameter and execute sql
                SqlDataReader dr = this.al_DBModule.ExecuteSQL(cmdText, paras.ToArray());
                string hiloName = null;
                if(dr.Read())
                {
                    hiloName = dr.GetString(0);
                }
                //close dataReader
                if (!dr.IsClosed)
                {
                    dr.Close();
                }
                //close connection
                this.al_DBModule.CloseConnection();
                if(hiloName != null && keyName.Equals(hiloName))
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch(Exception ex)
            {
                //close connection
                this.al_DBModule.CloseConnection();
                throw ex;
            }
        }
    }
}