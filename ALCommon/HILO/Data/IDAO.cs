﻿using System.Collections.Generic;

namespace HiLo.Data
{
    public interface IDAO<TEntity, TPk>
    {
        void Insert(TEntity entity);
        void Update(TEntity entity);
        void Delete(TPk pk);
        TEntity FindByPk(TPk pk);
        IList<TEntity> FindAll();
        bool Exist(TPk pk);
    }    
}
