﻿using System;
//
using System.Net;
using System.Net.Sockets;
using Common.Logging;
using HiLo.Utilities;
using HiLo.Domain.Entities;
//
namespace HiLowClient
{
    public class HiLowClient
    {
        #region Field
        private IPEndPoint ipEndPoint;
        private TcpClient tcpClient;
        private NetworkStream ns;
        private ISerializer<HiLoDO> hiloParser;
        #endregion

        #region Constructor
        /// <summary>
        /// 輸入要連結主機IP與主機監聽Port
        /// </summary>
        /// <param name="ip">對方主機IP</param>
        /// <param name="port">對方主機Port</param>
        public HiLowClient(string ip,int port)
        {
            this.hiloParser = new XmlWorker<HiLoDO>();
            IPAddress parseIP = null;
            if (IPAddress.TryParse(ip, out parseIP))
            {
                this.ipEndPoint = new IPEndPoint(parseIP, port);
            }
            else
            {
                throw new Exception("IP can't Parsing");
            }
        }
        /// <summary>
        /// 到app.config設定檔appSettings集合內搜尋 ex: <app key="HiLoClientConnectIpAndPort" value="10.27.88.78:6101" />
        /// 預設"10.27.88.78:6101"
        /// </summary>
        public HiLowClient()
        {
            string IpAndPort = System.Configuration.ConfigurationSettings.AppSettings["HiLoClientConnectIpAndPort"] == null ? "10.27.88.78:6100" : System.Configuration.ConfigurationSettings.AppSettings["HiLoClientConnectIpAndPort"];
            this.hiloParser = new XmlWorker<HiLoDO>();
            try
            {
                this.ipEndPoint = new IPEndPoint(IPAddress.Parse(IpAndPort.Split(':')[0]), Convert.ToInt32(IpAndPort.Split(':')[1]));
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion
        private bool ConnectionToServer()
        {
            try
            {
                this.tcpClient = new TcpClient();
                this.tcpClient.Connect(this.ipEndPoint);
                if (this.tcpClient.Connected)
                {
                    this.ns = this.tcpClient.GetStream();
                    return true;
                }
            }
            catch (SocketException)
            {
                return false;
            }
            return false;
        }
        /// <summary>
        /// 輸入KeyName取號碼牌
        /// </summary>
        /// <param name="keyName">取號機名稱</param>
        /// <returns> (long)Sequence Number</returns>
        public long GetHiLowValue(string keyName)
        {
            if (ConnectionToServer())
            {
                HiLoDO hilo = new HiLoDO()
                {
                    KeyName = keyName,
                    MaxVal = 999999,
                    NextHi = 1
                };
                byte[] hiloBytes = hiloParser.Serialize2Bytes(hilo);
                this.ns.Write(hiloBytes, 0, hiloBytes.Length);
                byte[] receiveBytes = new byte[4096];
                int receiveLength = this.ns.Read(receiveBytes, 0, receiveBytes.Length);
                
                
                Array.Resize(ref receiveBytes,receiveLength);//資料縮減至傳送的大小
                hilo = hiloParser.Deserialize(receiveBytes);

                CloseConnection();
                if (hilo.KeyName == keyName && hilo.NextHi > 0)
                {
                    return hilo.NextHi;
                }
                else
                {
                    return 0;
                }
            }
            else
            {
                return 0;
            }
        }

        private void CloseConnection()
        {
            if (this.ns != null)
            {
                this.ns.Close();
                this.ns = null;
            }
            if (this.tcpClient != null)
            {
                this.tcpClient.Close();
                this.tcpClient = null;
            }
        }
    }
}
