﻿using HiLo.Domain.Entities;

namespace HiLo.Business
{
    /// <summary>
    /// 輸入keyName,產生HiLo序號
    /// </summary>
    public interface IHiLoGenerator
    {
        long NextKey(string keyName);
        long NextKey(string keyName, long modValue);
        void CreateSequence(HiLoDO hiLo);
    }
}
