﻿using System;
using System.Collections.Generic;
//
using Common.Logging;
//
using System.Transactions;
using HiLo.Domain.Entities;
using HiLo.Data;
using System.Data;
namespace HiLo.Business
{
    public class HiLoGenerator : IHiLoGenerator
    {
        private static readonly ILog log = LogManager.GetLogger(typeof(HiLoGenerator));
        //
        private static Dictionary<string, long> currentHi = new Dictionary<string, long>();
        private static Dictionary<string, long> currentLo = new Dictionary<string, long>();
        private static Dictionary<string, long> modValDic = new Dictionary<string, long>();
        //
        private static object loLock = new object();
        private static object createLock = new object();
        public HiLoDAO HiLoDAO { get; set; }
        public bool SkipZero { get; set; }
        private const int maxLo = 1000;

        #region Constructor
        /// <summary>
        /// 輸入HiLo資料表的表格名稱
        /// 資料庫設定於DB資料夾內的AL_DBModuleSet.xml
        /// </summary>
        /// <param name="tableName">HiLo資料表的名稱</param>
        public HiLoGenerator(string tableName,bool skipZero = true)
        {
            this.SkipZero = skipZero;
            this.HiLoDAO = new HiLoDAO(tableName);
        }
        /// <summary>
        /// 自行注入HiLoDAO物件
        /// 資料庫設定於DB資料夾內的AL_DBModuleSet.xml
        /// </summary>
        public HiLoGenerator()
        {

        }
        #endregion

        public long NextKey(string keyName)
        {
            return this.NextKey(keyName, 0);
        }
        public long NextKey(string keyName, long modValue)
        {
            lock (loLock)
            {
                if (NeedsNextHi(keyName))
                {
                    try
                    {
                        TransactionOptions transactionOptions = new TransactionOptions();
                        transactionOptions.IsolationLevel = System.Transactions.IsolationLevel.RepeatableRead;//鎖定查詢中使用的所有資料
                        transactionOptions.Timeout = new TimeSpan(0, 0, 5);//交易逾時設定5秒鐘
                        using (TransactionScope ts = new TransactionScope(TransactionScopeOption.Required, transactionOptions))
                        {
                            //log.Debug("Begin Transaction " + keyName);
                            GetNextHi(keyName, modValue);
                            ts.Complete();
                            //log.Debug("End Transaction " + keyName);
                        }
                    }
                    catch (Exception ex)
                    {
                        log.Error("[HiLoGenerator][NextKey]取號失敗:" + ex.ToString());
                        throw ex;
                    }
                }
                return this.CntNextKey(keyName, modValDic[keyName]);
            }
        }
        /// <summary>
        /// 建立取號序列
        /// </summary>
        /// <param name="hiLo">要建立的取號參考物件</param>
        public void CreateSequence(HiLoDO hiLo)
        {
            lock (createLock)
            {
                try
                {
                    if (this.HiLoDAO.Exist(hiLo.KeyName))
                    {
                        throw new Exception(hiLo.KeyName + "already exists!");
                    }
                    TransactionOptions transactionOptions =new TransactionOptions();
                    transactionOptions.IsolationLevel = System.Transactions.IsolationLevel.Serializable;
                    transactionOptions.Timeout = new TimeSpan(0,5,0);
                    using (TransactionScope ts = new TransactionScope(TransactionScopeOption.Required, transactionOptions)) 
                    {
                        this.HiLoDAO.Insert(hiLo);
                        ts.Complete();
                    }
                }
                catch (Exception ex)
                {
                    log.Error("[HiLoGenerator][CreateSequence]Insert " + hiLo.KeyName + " failed: " + ex.StackTrace);
                    throw new Exception("[HiLoGenerator][CreateSequence]" + ex.ToString());
                }
            }
        }
        #region Private Method
        //取得當前hi號碼(存放在資料庫的編號)
        private long CurrentHi(string keyName)
        {
            return currentHi[keyName];
        }
        //取得當前lo(存放在程式端記憶體的號碼)的號碼再將(dic)當前lo的號碼+1
        private long NextLo(string keyName)
        {
            long lo = currentLo[keyName];
            currentLo[keyName] = lo + 1;
            return lo;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="keyName">代號名稱,</param>
        /// <param name="modValue"></param>
        /// <returns></returns>
        private long CntNextKey(string keyName, long modValue)
        {
            long nextKey = (CurrentHi(keyName) * maxLo) + NextLo(keyName);
            if (modValue != 0)
            {
                nextKey = nextKey % modValue;
                if (this.SkipZero && nextKey == 0)
                {
                    nextKey = this.NextKey(keyName, modValue);
                }
            }
            return nextKey;
        }
        //是否取得下一次的hi號碼
        private bool NeedsNextHi(string keyName)
        {
            if (!currentHi.ContainsKey(keyName))
            {
                return true;
            }
            return (currentLo[keyName] > maxLo);
        }

        private void GetNextHi(string keyName)
        {
            this.GetNextHi(keyName, 0);
        }
        /// <summary>
        /// 取得下一次的hi值
        /// </summary>
        /// <param name="keyName">代號名稱</param>
        /// <param name="modValue">號碼最大值</param>
        private void GetNextHi(string keyName, long modValue)
        {
            HiLoDO hiLo = null; ;
            
            if (!this.HiLoDAO.Exist(keyName))
            {
                //hiLo代號建立初始化到資料庫
                currentHi[keyName] = 0;
                modValDic[keyName] = modValue;
                hiLo = new HiLoDO
                {
                    KeyName = keyName,
                    NextHi = 1,
                    MaxVal = modValue - 1
                };
                this.HiLoDAO.Insert(hiLo);
            }
            else
            {
                //找到目前hilo的資料並更新hi值
                hiLo = this.HiLoDAO.FindByPk(keyName);
                currentHi[keyName] = hiLo.NextHi;
                modValDic[keyName] = hiLo.MaxVal + 1;//2001
                hiLo.NextHi += 1;
                if (hiLo.MaxVal > 1)
                {
                    //當前號碼 * 間距值 > 號碼最大值 就將hi歸零
                    if (hiLo.NextHi * maxLo > hiLo.MaxVal)
                    {
                        hiLo.NextHi = 0;
                    }
                }
                this.HiLoDAO.Update(hiLo);
            }
            currentLo[keyName] = 1;
        }
        #endregion
    }
}
